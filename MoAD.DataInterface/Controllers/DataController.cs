﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MoAD.DataObjects.EDMX;

namespace MoAD.DataInterface.Controllers
{
    [RoutePrefix("api/Data")]
    [BasicAuthentication]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DataController : ApiController
    {
        [Route("GetOrganisations")]
        [HttpGet]
        public List<GetOrganisations_Result> GetOrganisations(int? id = null, int? level = 2)
        {
            List<GetOrganisations_Result> listOfOrganisations = new List<GetOrganisations_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfOrganisations = MoADEntities.GetOrganisations(level, id).ToList<GetOrganisations_Result>();
            }

            return listOfOrganisations;
        }

        [Route("ManageOrganisation")]

        [HttpGet]
        public Task<HttpResponseMessage> ManageOrganisation(int? id, string organisationName, string organisationParent, string province, string action=null, int? section = 0, string visibility = null)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);

            if (id == null)
            {
                response = new HttpResponseMessage(HttpStatusCode.NotFound);
                return Task.FromResult(response);
            }

            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.ManageOrganisation(id, string.IsNullOrEmpty(organisationName) ? "" : organisationName, string.IsNullOrEmpty(organisationParent) ? "" : organisationParent,
                    section, string.IsNullOrEmpty(province) ? "" : province, action, string.IsNullOrEmpty(visibility) ? "" : visibility);
            }

            return Task.FromResult(response);
        }

        [Route("GetSectors")]

        [HttpGet]
        public List<GetSectors_Result> GetSectors(int? id = null)
        {
            List<GetSectors_Result> listOfSectors = new List<GetSectors_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfSectors = MoADEntities.GetSectors().ToList<GetSectors_Result>();
            }

            return listOfSectors;
        }

        [Route("GetProvinces")]

        [HttpGet]
        public List<GetProvinces_Result> GetProvinces(int? id = null)
        {
            List<GetProvinces_Result> listOfProvinces = new List<GetProvinces_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfProvinces = MoADEntities.GetProvinces().ToList<GetProvinces_Result>();
            }

            return listOfProvinces;
        }
    }
}
