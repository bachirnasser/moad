﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using MoAD.DataObjects.EDMX;
//using MoAD.DataInterface.EDMX;
using System.Web.Http;
using MoAD.DataObjects.EDMX;



namespace MoAD.DataInterface.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
        public List<GetFormElementTypes_Result> GetFormElementTypes()
        {
            List<GetFormElementTypes_Result> listOfCategories = new List<GetFormElementTypes_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfCategories = MoADEntities.GetFormElementTypes().ToList<GetFormElementTypes_Result>();
            }

            return listOfCategories;
        }

        
        public List<GetFormElements_Result> GetFormElements(string CategoryKey)
        {
            List<GetFormElements_Result> listOfCategories = new List<GetFormElements_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfCategories = MoADEntities.GetFormElements(CategoryKey).ToList<GetFormElements_Result>();
            }

            return listOfCategories;
        }

        public List<string> GetAllPossibleFormTemplateTypes()
        {
            List<string> listOfFormTemplateTypes = new List<string>();

            using (Entities MoADEntities = new Entities())
            {
                listOfFormTemplateTypes = MoADEntities.GetAllPossibleFormTemplateTypes().ToList<string>();
            }

            return listOfFormTemplateTypes;
        }

        public string CreateNewFormTemplate(string name, string description, string formTemplateType)
        {

            try
            {

                using (Entities MoADEntities = new Entities())
                {
                    
                    MoADEntities.CreateNewFormTemplate(name, description, formTemplateType);
                }
            }
            catch
            {
                return "Error";
            }

            return "Success";
        }

        [System.Web.Http.HttpGet]
        public string saveNewForm()
        {




            return "Success";
        }

    }
}