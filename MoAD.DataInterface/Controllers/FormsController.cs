﻿//using MoAD.DataObjects.EDMX;
//using MoAD.DataInterface.EDMX;
using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace MoAD.DataInterface.Controllers
{
    [RoutePrefix("api/Forms")]
    [BasicAuthentication]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class FormsController : ApiController
    {
        [HttpGet]
        public List<GetFormElementTypes_Result> GetFormElementTypes()
        {
            List<GetFormElementTypes_Result> listOfCategories = new List<GetFormElementTypes_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfCategories = MoADEntities.GetFormElementTypes().ToList<GetFormElementTypes_Result>();
            }

            return listOfCategories;
        }

        [HttpGet]
        public List<EAndTEvaluationWeighing_Result> EAndTEvaluationWeighing(string fk,string ok,string rk)
        {
            List<EAndTEvaluationWeighing_Result> result = new List<EAndTEvaluationWeighing_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.EAndTEvaluationWeighing(Int32.Parse(fk), Int32.Parse(ok), Int32.Parse(rk)).ToList<EAndTEvaluationWeighing_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<GetPBOrganisationCard_Result> GetPBOrganisationCard(string orID, string y, string yh)
        {
            List<GetPBOrganisationCard_Result> result = new List<GetPBOrganisationCard_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetPBOrganisationCard(orID, Int32.Parse(y), Int32.Parse(yh)).ToList<GetPBOrganisationCard_Result>();
            }

            return result;
        }

        [Route("GetSurveyResult")]
        [HttpGet]
        public List<GetSurveyResult_Result> GetSurveyResult(string keyFormTemplate=null)
        {
            List<GetSurveyResult_Result> result = new List<GetSurveyResult_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetSurveyResult(int.Parse(keyFormTemplate)).ToList<GetSurveyResult_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<GetUserSurveyAnswer_Result> GetUserSurveyAnswer(string uk, string ip, string cookie,string ViewKey)
        {

          
            List<GetUserSurveyAnswer_Result> result = new List<GetUserSurveyAnswer_Result>();

            using (Entities MoADEntities = new Entities())
            {

                if (uk != null)
                {
                    result = MoADEntities.GetUserSurveyAnswer(uk, null, null, ViewKey).ToList<GetUserSurveyAnswer_Result>();

                }else
                {
                    result = MoADEntities.GetUserSurveyAnswer(null, ip, cookie, ViewKey).ToList<GetUserSurveyAnswer_Result>();

                }
            }

            return result;
        }

        [HttpGet]
        public List<PBReportingTopInactiveOrganisationsWithClaims_Result> PBReportingTopInactiveOrganisationsWithClaims(string f)
        {


            List<PBReportingTopInactiveOrganisationsWithClaims_Result> result = new List<PBReportingTopInactiveOrganisationsWithClaims_Result>();

            using (Entities MoADEntities = new Entities())
            {

                    result = MoADEntities.PBReportingTopInactiveOrganisationsWithClaims().ToList<PBReportingTopInactiveOrganisationsWithClaims_Result>();

               
            }

            return result;
        }

        [HttpGet]
        public List<PBReportingCountCorruption_Result> PBReportingCountCorruption(string P)
        {


            List<PBReportingCountCorruption_Result> result = new List<PBReportingCountCorruption_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.PBReportingCountCorruption().ToList<PBReportingCountCorruption_Result>();


            }

            return result;
        }



        [HttpGet]
        public List<PBReportingCountNoncompliance_Result> PBReportingCountNoncompliance(string c)
        {


            List<PBReportingCountNoncompliance_Result> result = new List<PBReportingCountNoncompliance_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.PBReportingCountNoncompliance().ToList<PBReportingCountNoncompliance_Result>();


            }

            return result;
        }

        [HttpGet]
        public List<PBReportingSurveyAnswers_Result> PBReportingSurveyAnswers(string t)
        {


            List<PBReportingSurveyAnswers_Result> result = new List<PBReportingSurveyAnswers_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.PBReportingSurveyAnswers().ToList<PBReportingSurveyAnswers_Result>();


            }

            return result;
        }



        [HttpGet]
        public List<PBMonitoringReport_Result> PBMonitoringReport(string x)
        {


            List<PBMonitoringReport_Result> result = new List<PBMonitoringReport_Result>();

            using (MoADLogEntities MoADEntities = new MoADLogEntities())
            {

                result = MoADEntities.PBMonitoringReport().ToList<PBMonitoringReport_Result>();


            }

            return result;
        }

        [HttpGet]
        public List<PBMonitoringReportPerOrganisation_Result> PBMonitoringReportPerOrganisation(string a)
        {


            List<PBMonitoringReportPerOrganisation_Result> result = new List<PBMonitoringReportPerOrganisation_Result>();

            using (MoADLogEntities MoADEntities = new MoADLogEntities())
            {

                result = MoADEntities.PBMonitoringReportPerOrganisation().ToList<PBMonitoringReportPerOrganisation_Result>();


            }

            return result;
        }


        [HttpGet]
        public List<PBReportingCountEmployeeSatisfaction_Result> PBReportingCountEmployeeSatisfaction(string z)
        {


            List<PBReportingCountEmployeeSatisfaction_Result> result = new List<PBReportingCountEmployeeSatisfaction_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.PBReportingCountEmployeeSatisfaction().ToList<PBReportingCountEmployeeSatisfaction_Result>();


            }

            return result;
        }

        [HttpGet]
        public List<PBReportingCountClaimsPerOrganisation_Result> PBReportingCountClaimsPerOrganisation(string l)
        {


            List<PBReportingCountClaimsPerOrganisation_Result> result = new List<PBReportingCountClaimsPerOrganisation_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.PBReportingCountClaimsPerOrganisation().ToList<PBReportingCountClaimsPerOrganisation_Result>();


            }

            return result;
        }

        [HttpGet]
        public List<PBRanking_Result> PBRanking(string j)
        {


            List<PBRanking_Result> result = new List<PBRanking_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.PBRanking().ToList<PBRanking_Result>();


            }

            return result;
        }

        [HttpGet]
        public List<GetMostActiveOrganisations_Result> GetMostActiveOrganisations(string oups)
        {
            List<GetMostActiveOrganisations_Result> result = new List<GetMostActiveOrganisations_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetMostActiveOrganisations().ToList<GetMostActiveOrganisations_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<GetFormTemplateDependencies_Result> GetFormTemplateDependencies(string FTKey)
        {
            List<GetFormTemplateDependencies_Result> result = new List<GetFormTemplateDependencies_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetFormTemplateDependencies(FTKey).ToList<GetFormTemplateDependencies_Result>();
            }

            return result;
        }
        

               //[HttpGet]
        //public List<GetOrganisationCard_Result> GetOrganisationCard(string OrgId,string Y,string YHalf)
        //{
        //    List<GetOrganisationCard_Result> result = new List<GetOrganisationCard_Result>();

        //    using (Entities MoADEntities = new Entities())
        //    {
        //        result = MoADEntities.GetOrganisationCard(OrgId, Y, Int32.Parse(YHalf)).ToList<GetOrganisationCard_Result>();
        //    }

        //    return result;
        //}

        [HttpGet]
        public List<GetLogTable_Result> GetLogTable(string KeyUser, string monitor, string pagination, string Organisationns)
        {
            List<GetLogTable_Result> result = new List<GetLogTable_Result>();

            using (MoADLogEntities MoADEntities = new MoADLogEntities())
            {
                try
                {
                    if (Organisationns == "null")
                        result = MoADEntities.GetLogTable(KeyUser, monitor, pagination, null).ToList<GetLogTable_Result>();
                    else
                        result = MoADEntities.GetLogTable(KeyUser, monitor, pagination, Organisationns).ToList<GetLogTable_Result>();
                }
                catch (Exception ex)
                {

                }

            }

            return result;
        }



        [HttpGet]
        public List<GetMouwazzafinWithOrg_Result> GetMouwazzafinWithOrg(string MouwazafId, string SearchValue1, string SearchValue2, string SearchValue3, string count)
        {

            if (MouwazafId == "null")
                MouwazafId = null;
            if (SearchValue1 == "null")
                SearchValue1 = " ";
            if (SearchValue2 == "null")
                SearchValue2 = " ";
            if (SearchValue3 == "null")
                SearchValue3 = " ";

            List<GetMouwazzafinWithOrg_Result> result = new List<GetMouwazzafinWithOrg_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (count == "null")
                    result = MoADEntities.GetMouwazzafinWithOrg(MouwazafId, SearchValue1, SearchValue2, SearchValue3, null).ToList<GetMouwazzafinWithOrg_Result>();
                else
                    result = MoADEntities.GetMouwazzafinWithOrg(MouwazafId, SearchValue1, SearchValue2, SearchValue3, count).ToList<GetMouwazzafinWithOrg_Result>();
            }

            return result;
        }


        [HttpGet]
        public List<GetJobDescription_Result> GetJobDescription(string ts)
        {


            List<GetJobDescription_Result> result = new List<GetJobDescription_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetJobDescription().ToList<GetJobDescription_Result>();
            }

            return result;
        }

        [Route("AccessPivilege")]
        [HttpGet]
        public List<AccessPivilege_Result> AccessPivilege(string userRole, string ParentId, string Position)
        {


            List<AccessPivilege_Result> result = new List<AccessPivilege_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (Position == "null")
                    result = MoADEntities.AccessPivilege(userRole, ParentId, null).ToList<AccessPivilege_Result>();
                else
                    result = MoADEntities.AccessPivilege(userRole, ParentId, Position).ToList<AccessPivilege_Result>();

            }

            return result;
        }

        [HttpGet]
        public List<GetFunctionsPerPrivilige_Result> GetFunctionsPerPrivilige(string PrivilegeKey)
        {


            List<GetFunctionsPerPrivilige_Result> result = new List<GetFunctionsPerPrivilige_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.GetFunctionsPerPrivilige(Int32.Parse(PrivilegeKey)).ToList<GetFunctionsPerPrivilige_Result>();


            }

            return result;
        }

        [HttpGet]
        public List<GetRanking_Result> GetRanking(string RankingKey, string Ukey, string year, string description, string levelType)
        {

            List<GetRanking_Result> result = new List<GetRanking_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (description == "null" && RankingKey == "null" && Ukey == "null" && year == "null")
                    result = MoADEntities.GetRanking(null, null, null, null, levelType).ToList<GetRanking_Result>();
                else if (description == "null")

                    result = MoADEntities.GetRanking(null, Ukey, null, null, levelType).ToList<GetRanking_Result>();
                else
                    result = MoADEntities.GetRanking(null, null, null, description, levelType).ToList<GetRanking_Result>();

            }

            return result;
        }

        [HttpGet]
        public List<PrivelegesperRole_Result> PrivilegePerRole(string Key, string PositionType)
        {

            List<PrivelegesperRole_Result> result = new List<PrivelegesperRole_Result>();

            using (Entities MoADEntities = new Entities())
            {

                if (PositionType == "null")
                    result = MoADEntities.PrivelegesperRole(Int32.Parse(Key), null).ToList<PrivelegesperRole_Result>();

                else
                    result = MoADEntities.PrivelegesperRole(Int32.Parse(Key), PositionType).ToList<PrivelegesperRole_Result>();

            }

            return result;
        }

        [HttpGet]
        public List<FinalRankingInTheRightScenario_Result> RankingSP(string Year, string YearHalf, string levelType)
        {


            List<FinalRankingInTheRightScenario_Result> result = new List<FinalRankingInTheRightScenario_Result>();

            using (Entities MoADEntities = new Entities())
            {

                result = MoADEntities.FinalRankingInTheRightScenario(Int32.Parse(Year), Int32.Parse(YearHalf), levelType).ToList<FinalRankingInTheRightScenario_Result>();

            }

            return result;
        }
        [HttpGet]
        public List<GetLog_Result> GetLog(string uId)
        {
            List<GetLog_Result> result = new List<GetLog_Result>();

            using (MoADLogEntities MoADEntities = new MoADLogEntities())
            {
                result = MoADEntities.GetLog(uId).ToList<GetLog_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<AccessMonitoringAndChatting_Result> AccessMonitoringAndChatting(string usrId, string Action, string Orgs)
        {
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();


            using (MoADLogEntities MoADEntities = new MoADLogEntities())
            {
                try
                {
                    if (Orgs == "null")
                        result = MoADEntities.AccessMonitoringAndChatting(usrId, null, Action).ToList<AccessMonitoringAndChatting_Result>();

                    else
                        result = MoADEntities.AccessMonitoringAndChatting(usrId, Orgs, Action).ToList<AccessMonitoringAndChatting_Result>();
                }
                catch
                (Exception ex)
                {

                }

            }




            //var test = result
            //    .GroupBy(x => x.OrganisationKey)
            //    .Select(group => new
            //    {
            //        Customer = group.Key,
            //        Items = group.ToList()
            //    })
            //    .ToList();

            //var wordList = words.GroupBy(word => word[0])
            //           .OrderBy(group => group.Key)
            //           .Select(group => new {
            //               FirstLetter = group.Key,
            //               Words = group.OrderBy(x => x)
            //           })
            //           .ToList();

            //return result.OrderBy(x => x.OrganisationKey).ToList<AccessMonitoringAndChatting_Result>();

            return result;
        }

        [HttpGet]
        public List<ReportingGovernanceResponse_Result> ReportingGovernanceResponse(string OrgKey,string parentKey,string Prov)
        {
            List<ReportingGovernanceResponse_Result> result = new List<ReportingGovernanceResponse_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (OrgKey == "null" && parentKey == "null" && Prov == "null")
                    result = MoADEntities.ReportingGovernanceResponse(null,null,null).ToList<ReportingGovernanceResponse_Result>();
                else if (OrgKey == "null" && parentKey == "null")
                    result = MoADEntities.ReportingGovernanceResponse(null,null, Prov).ToList<ReportingGovernanceResponse_Result>();
                else if (parentKey == "null" && Prov == "null")
                    result = MoADEntities.ReportingGovernanceResponse(Int32.Parse(OrgKey), null, null).ToList<ReportingGovernanceResponse_Result>();
                else if (OrgKey == "null" && Prov == "null")
                    result = MoADEntities.ReportingGovernanceResponse(null, parentKey, null).ToList<ReportingGovernanceResponse_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<Honnesty_Result> Honnesty(string t, string h, string p)
        {
            List<Honnesty_Result> result = new List<Honnesty_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.Honnesty(null, null, null).ToList<Honnesty_Result>();
                if (t == "null" && h == "null" && p == "null")
                    result = MoADEntities.Honnesty(null, null, null).ToList<Honnesty_Result>();
                else if (t == "null" && h == "null")
                    result = MoADEntities.Honnesty(null, null, p).ToList<Honnesty_Result>();
                else if (h == "null" && p == "null")
                    result = MoADEntities.Honnesty(Int32.Parse(t), null, null).ToList<Honnesty_Result>();
                else if (t == "null" && p == "null")
                    result = MoADEntities.Honnesty(null, h, null).ToList<Honnesty_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<Credibility_Result> Credibility(string sation, string orgParent, string vince)
        {
            List<Credibility_Result> result = new List<Credibility_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (sation == "null" && orgParent == "null" && vince == "null")
                    result = MoADEntities.Credibility(null, null, null).ToList<Credibility_Result>();
                else if (sation == "null" && orgParent == "null")
                    result = MoADEntities.Credibility(null, null, vince).ToList<Credibility_Result>();
                else if (orgParent == "null" && vince == "null")
                    result = MoADEntities.Credibility(Int32.Parse(sation), null, null).ToList<Credibility_Result>();
                else if (sation == "null" && vince == "null")
                    result = MoADEntities.Credibility(null, orgParent, null).ToList<Credibility_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<ReportingTa7sinElKhadamat_Result> ReportingTa7sinElKhadamat(string test9, string test10,string test11)
        {
            List<ReportingTa7sinElKhadamat_Result> result = new List<ReportingTa7sinElKhadamat_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if(test9 == "null" && test10 == "null" && test11=="null")
                   result = MoADEntities.ReportingTa7sinElKhadamat(null, null,null).ToList();
                else if(test10 == "null" && test11 == "null")
                    result = MoADEntities.ReportingTa7sinElKhadamat(test9, null, null).ToList();
                else if (test9 == "null" && test10 == "null")
                    result = MoADEntities.ReportingTa7sinElKhadamat(null, null, test11).ToList();
                else if (test9 == "null" && test11 == "null")
                    result = MoADEntities.ReportingTa7sinElKhadamat(null, test10, null).ToList();

                //if (test9 == "null" && test10 == "Null")
                //    result = MoADEntities.ReportingTa7sinElKhadamat(null, null,null).ToList();
                //else if (test10 == "Null")
                //    result = MoADEntities.ReportingTa7sinElKhadamat(test9,null,null).ToList();
                //else
                //    result = MoADEntities.ReportingTa7sinElKhadamat(test9, test10,null).ToList();

            }

            return result;
        }

        [HttpGet]
        public string GetTypeForaFormTemplate(string formTemplateKey)
        {
            string result = "";

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetTypeForaFormTemplate(formTemplateKey).FirstOrDefault();
            }

            return result;
        }



        [HttpGet]
        public List<GetOrganisationType_Result> GetOrganisationType(string identityKey)
        {
            List<GetOrganisationType_Result> result = new List<GetOrganisationType_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetOrganisationType(identityKey).ToList<GetOrganisationType_Result>();
            }

            return result;
        }

        [HttpGet]
        public List<ReportingGovernanceResponseDateRanges_Result> ReportingGovernanceResponseDateRanges(string Korg, string parOrg,string vince)
        {
            List<ReportingGovernanceResponseDateRanges_Result> result = new List<ReportingGovernanceResponseDateRanges_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (Korg == "null" && parOrg == "null" && vince == "null")
                    result = MoADEntities.ReportingGovernanceResponseDateRanges(null, null,null).ToList<ReportingGovernanceResponseDateRanges_Result>();
                else if (Korg == "null" && parOrg == "null")
                    result = MoADEntities.ReportingGovernanceResponseDateRanges(null, null, vince).ToList<ReportingGovernanceResponseDateRanges_Result>();
                else if (Korg == "null" && vince == "null")
                    result = MoADEntities.ReportingGovernanceResponseDateRanges(null, parOrg, null).ToList<ReportingGovernanceResponseDateRanges_Result>();
                else if (parOrg == "null" && vince == "null")
                    result = MoADEntities.ReportingGovernanceResponseDateRanges(Int32.Parse(Korg), null, null).ToList<ReportingGovernanceResponseDateRanges_Result>();

            }

            return result;
        }

        [HttpGet]
        public List<ReportingGetSatisfactionCitizen_Result> ReportingGetSatisfactionCitizen(string Level, string OrganisationKey, string LevelType)
        {
            List<ReportingGetSatisfactionCitizen_Result> result = new List<ReportingGetSatisfactionCitizen_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if ((Level == "null" && OrganisationKey == "null") || (Level == "Null" && OrganisationKey == "null"))
                    result = MoADEntities.ReportingGetSatisfactionCitizen(null, null, null).ToList<ReportingGetSatisfactionCitizen_Result>();
                else if (Level == "null" || Level== "Null")
                    result = MoADEntities.ReportingGetSatisfactionCitizen(null, OrganisationKey, LevelType).ToList<ReportingGetSatisfactionCitizen_Result>();
                else if(OrganisationKey=="null")
                    result = MoADEntities.ReportingGetSatisfactionCitizen(Level, null, LevelType).ToList<ReportingGetSatisfactionCitizen_Result>();
                else
                    result = MoADEntities.ReportingGetSatisfactionCitizen(Level, OrganisationKey, LevelType).ToList<ReportingGetSatisfactionCitizen_Result>();

            }

            return result;
        }

        [HttpGet]
        public List<ReportingGetUnpreciseFormsCount_Result> ReportingGetUnpreciseFormsCount(string o, string PT, string v)
        {
            List<ReportingGetUnpreciseFormsCount_Result> result = new List<ReportingGetUnpreciseFormsCount_Result>();

            using (Entities MoADEntities = new Entities())
            {

                if (o == "null" && PT == "null" && v == "null")
                {
                    result = MoADEntities.ReportingGetUnpreciseFormsCount(null, null, null).ToList<ReportingGetUnpreciseFormsCount_Result>();
                }
                else if (PT == "null" && v == "null")
                {
                    result = MoADEntities.ReportingGetUnpreciseFormsCount(int.Parse(o), null, null).ToList<ReportingGetUnpreciseFormsCount_Result>();
                }
                else if (o == "null" && v == "null")
                {
                    result = MoADEntities.ReportingGetUnpreciseFormsCount(null, PT, null).ToList<ReportingGetUnpreciseFormsCount_Result>();
                }
                else if (o == "null" && PT == "null")
                {
                    result = MoADEntities.ReportingGetUnpreciseFormsCount(null, null, v).ToList<ReportingGetUnpreciseFormsCount_Result>();
                }
             
            }

            return result;
        }

        [HttpGet]
        public int? ReportingGetFalseFormsCount(string orG,string orgPT,string vince)
        {
            int? result=0;

            using (Entities MoADEntities = new Entities())
            {
                if(orG=="null" && orgPT == "null" && vince == "null")
                {
                    result = MoADEntities.ReportingGetFalseFormsCount(null, null, null).FirstOrDefault();
                }else if(orgPT == "null" && vince == "null")
                {
                    result = MoADEntities.ReportingGetFalseFormsCount(int.Parse(orG), null, null).FirstOrDefault();
                }
                else if (orG == "null" && vince == "null")
                {
                    result = MoADEntities.ReportingGetFalseFormsCount(null, orgPT, null).FirstOrDefault();
                }
                else if (orG == "null" && orgPT == "null")
                {
                    result = MoADEntities.ReportingGetFalseFormsCount(null, null, vince).FirstOrDefault();
                }

            }

            return result;
        }

        [HttpGet]
        public List<ReportingGetSatisfactionCitizenPerQuestion_Result> ReportingGetSatisfactionCitizenPerQuestion(string oKEY,string LevelType)
        {
            List<ReportingGetSatisfactionCitizenPerQuestion_Result> result = new List<ReportingGetSatisfactionCitizenPerQuestion_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if(oKEY=="NULL" || oKEY=="null")
                    result = MoADEntities.ReportingGetSatisfactionCitizenPerQuestion(null,null).ToList<ReportingGetSatisfactionCitizenPerQuestion_Result>();
                    else
                result = MoADEntities.ReportingGetSatisfactionCitizenPerQuestion(oKEY, LevelType).ToList<ReportingGetSatisfactionCitizenPerQuestion_Result>();

            }

            return result;
        }

        [Route("ReportingGetWeakQuestionsPerSlice")]
        [HttpGet]
        public List<ReportingGetWeakQuestionsPerSlice_Result> ReportingGetWeakQuestionsPerSlice(string SliceLevel, string IdOrganKey,string IdOrgParent, string Prvce, string COUNT, string QuestionWeakness)
        {
            List<ReportingGetWeakQuestionsPerSlice_Result> result = new List<ReportingGetWeakQuestionsPerSlice_Result>();

            using (Entities MoADEntities = new Entities())
            {
                if (SliceLevel == "NULL" && IdOrganKey == "null" && IdOrgParent == "null" && Prvce == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(null, null, null,null,Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel == "NULL" && IdOrgParent == "null" && Prvce == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(null, Int32.Parse(IdOrganKey), null, null, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel == "NULL" && IdOrganKey == "null" && Prvce == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(null, null, IdOrgParent, null, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel == "NULL" && IdOrgParent == "null" && IdOrganKey == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(null, null, null, Prvce, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel != "NULL" && IdOrganKey == "null" && IdOrgParent == "null" && Prvce == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(Int32.Parse(SliceLevel), null, null, null, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel != "NULL" && IdOrgParent == "null" && Prvce == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(Int32.Parse(SliceLevel), Int32.Parse(IdOrganKey), null, null, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel != "NULL" && IdOrganKey == "null" && Prvce == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(Int32.Parse(SliceLevel), null, IdOrgParent, null, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();
                else if (SliceLevel != "NULL" && IdOrgParent == "null" && IdOrganKey == "null")
                    result = MoADEntities.ReportingGetWeakQuestionsPerSlice(Int32.Parse(SliceLevel), null, null, Prvce, Int32.Parse(COUNT), QuestionWeakness).ToList<ReportingGetWeakQuestionsPerSlice_Result>();




            }

            return result;
        }

        [Route("ReportingGetSatisfactionPerSlice")]
        [HttpGet]
        public List<ReportingGetSatisfactionPerSlice_Result> ReportingGetSatisfactionPerSlice(string sl, string OGkey, string PTorg, string Pv)
        {


            List<ReportingGetSatisfactionPerSlice_Result> result = new List<ReportingGetSatisfactionPerSlice_Result>();

            using (Entities MoADEntities = new Entities())
            {
                try
                {
                    if(OGkey == "null" && sl == "NULL" && PTorg == "null" && Pv == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(null,null,null,null).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (sl == "NULL" && PTorg == "null" && Pv == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(null, Int32.Parse(OGkey), null, null).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (sl == "NULL" && OGkey == "null" && Pv == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(null,null, PTorg, null).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (sl == "NULL" && OGkey == "null" && PTorg == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(null, null, null, Pv).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (OGkey == "null" && sl != "NULL" && PTorg == "null" && Pv == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(Int32.Parse(sl), null, null, null).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (sl != "NULL" && PTorg == "null" && Pv == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(Int32.Parse(sl), Int32.Parse(OGkey), null, null).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (sl != "NULL" && OGkey == "null" && Pv == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(Int32.Parse(sl), null, PTorg, null).ToList<ReportingGetSatisfactionPerSlice_Result>();
                    else if (sl != "NULL" && OGkey == "null" && PTorg == "null")
                        result = MoADEntities.ReportingGetSatisfactionPerSlice(Int32.Parse(sl), null, null, Pv).ToList<ReportingGetSatisfactionPerSlice_Result>();

                }
                catch (Exception ex)
                {

                }


            }

            return result;
        }

        [HttpGet]
        public List<ReportingClaimStatusesPerDateRanges_Result> ClaimStatusesPerDateRanges(string startPeriod, string endPeriod, string organisationKey,string parentorgkey,string Province)
        {
            List<ReportingClaimStatusesPerDateRanges_Result> result = new List<ReportingClaimStatusesPerDateRanges_Result>();
            DateTime StartPeriod = Convert.ToDateTime(startPeriod);
            DateTime EndPeriod = Convert.ToDateTime(endPeriod);

            using (Entities MoADEntities = new Entities())
            {
                if (organisationKey == "null" && parentorgkey == "null" && Province=="null")
                    result = MoADEntities.ReportingClaimStatusesPerDateRanges(StartPeriod, EndPeriod, null, null,null).ToList<ReportingClaimStatusesPerDateRanges_Result>();
                else if (parentorgkey == "null" && Province == "null")
                    result = MoADEntities.ReportingClaimStatusesPerDateRanges(StartPeriod, EndPeriod, Int32.Parse(organisationKey), null,null).ToList<ReportingClaimStatusesPerDateRanges_Result>();
                else if (organisationKey == "null" && Province == "null")
                    result = MoADEntities.ReportingClaimStatusesPerDateRanges(StartPeriod, EndPeriod, null, parentorgkey, null).ToList<ReportingClaimStatusesPerDateRanges_Result>();
                else if (organisationKey == "null" && parentorgkey == "null")
                    result = MoADEntities.ReportingClaimStatusesPerDateRanges(StartPeriod, EndPeriod, null, null, Province).ToList<ReportingClaimStatusesPerDateRanges_Result>();

            }

            return result;
        }



        [HttpGet]
        public List<ReportingClaimsCountPerYear_Result> ReportingClaimsCountPerYear(int Year1, int Year2, string keyOrg,string OrgPar, string Pvce)
        {
            List<ReportingClaimsCountPerYear_Result> resultYear1 = new List<ReportingClaimsCountPerYear_Result>();
            List<ReportingClaimsCountPerYear_Result> resultYear2 = new List<ReportingClaimsCountPerYear_Result>();


            using (Entities MoADEntities = new Entities())
            {
                if(keyOrg == "null" && OrgPar == "null" && Pvce == "null")
                    resultYear1 = MoADEntities.ReportingClaimsCountPerYear(null, Year1, null,null).ToList<ReportingClaimsCountPerYear_Result>();
                else if(OrgPar == "null" && Pvce == "null")
                    resultYear1 = MoADEntities.ReportingClaimsCountPerYear(Int32.Parse(keyOrg), Year1, null,null).ToList<ReportingClaimsCountPerYear_Result>();
                else if (keyOrg == "null" && Pvce == "null")
                    resultYear1 = MoADEntities.ReportingClaimsCountPerYear(null, Year1, OrgPar, null).ToList<ReportingClaimsCountPerYear_Result>();
                else if(keyOrg == "null" && OrgPar == "null")
                    resultYear1 = MoADEntities.ReportingClaimsCountPerYear(null, Year1, null, Pvce).ToList<ReportingClaimsCountPerYear_Result>();

                if (resultYear1.Count == 0)
                {
                    ReportingClaimsCountPerYear_Result Report = new ReportingClaimsCountPerYear_Result();
                    Report.ClaimsCount = 0;
                    Report.Year = Year1;
                    resultYear1.Add(Report);
                }
                if (keyOrg == "null" && OrgPar == "null" && Pvce == "null")
                    resultYear2 = MoADEntities.ReportingClaimsCountPerYear(null, Year2, null,null).ToList<ReportingClaimsCountPerYear_Result>();
                else if (OrgPar == "null" && Pvce == "null")
                    resultYear2 = MoADEntities.ReportingClaimsCountPerYear(Int32.Parse(keyOrg), Year2, null, null).ToList<ReportingClaimsCountPerYear_Result>();
                else if(keyOrg == "null" && Pvce == "null")
                    resultYear2 = MoADEntities.ReportingClaimsCountPerYear(null, Year2, OrgPar, null).ToList<ReportingClaimsCountPerYear_Result>();
                else if(keyOrg == "null" && OrgPar == "null")
                    resultYear2 = MoADEntities.ReportingClaimsCountPerYear(null, Year2, null, Pvce).ToList<ReportingClaimsCountPerYear_Result>();
                if(resultYear2.Count == 0)
                {
                    ReportingClaimsCountPerYear_Result Report2 = new ReportingClaimsCountPerYear_Result();
                    Report2.ClaimsCount = 0;
                    Report2.Year = Year2;
                    resultYear2.Add(Report2);
                }


            }
            resultYear1.AddRange(resultYear2);
            return resultYear1;
        }



        [HttpGet]
        public List<GetFormElements_Result> GetFormElements(string CategoryKey)
        {
            List<GetFormElements_Result> listOfCategories = new List<GetFormElements_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfCategories = MoADEntities.GetFormElements(CategoryKey).ToList<GetFormElements_Result>();
            }

            return listOfCategories;
        }

        [HttpGet]
        public string EntidabSave(string Organizationkey, string delegatedOrgKey, string userKey)
        {

            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.ChangeOrDelegateUserToOrganisation(userKey, Organizationkey, delegatedOrgKey);
            }

            return "success";
        }

        [HttpGet]
        public string CreateUserPerOrganisation(string Organikey, string uKey)
        {

            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.CreateUserPerOrganisation(Organikey, uKey);
            }

            return "success";
        }

        [HttpGet]
        public string AssignedCodeToUser(string UserKey, string AuthenticationCode)
        {
            int result = 0;
            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.AssignCodeToUser(UserKey, AuthenticationCode);
            }

            return result.ToString();
        }
        [HttpGet]
        public List<AuthenticationCodeCheck_Result> AuthenticationCodeCheck(string SecretNumber)
        {
            List<AuthenticationCodeCheck_Result> result = new List<AuthenticationCodeCheck_Result>();
            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.AuthenticationCodeCheck(SecretNumber).ToList();
            }

            return result;
        }

        [HttpGet]
        public List<string> GetAllPossibleFormTemplateTypes(string test)
        {
            List<string> listOfFormTemplateTypes = new List<string>();

            using (Entities MoADEntities = new Entities())
            {
                listOfFormTemplateTypes = MoADEntities.GetAllPossibleFormTemplateTypes().ToList<string>();
            }

            return listOfFormTemplateTypes;
        }

        [HttpGet]
        public List<ReportingOverAllHistory_Result> ReportingGetOverAllHistory(string OrganisationKey)
        {
            List<ReportingOverAllHistory_Result> result = new List<ReportingOverAllHistory_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.ReportingOverAllHistory(OrganisationKey, null).ToList<ReportingOverAllHistory_Result>();
            }

            return result;
        }


        [HttpGet]
        public List<ReportingClaimstype_Result> GetReportingClaimsType(string test8, string test7,string test6)
        {
            List<ReportingClaimstype_Result> result = new List<ReportingClaimstype_Result>();

            using (Entities MoADEntities = new Entities())
            {

                if (test8 == "null" && test7 == "null" && test6 == "null")
                {
                    result = MoADEntities.ReportingClaimstype(null, null, null).ToList();

                }
                else if (test7=="null" && test6 == "null")
                {
                    result = MoADEntities.ReportingClaimstype(Int32.Parse(test8), null, null).ToList();

                }
                else if (test8 == "null" && test6 == "null")
                {
                    result = MoADEntities.ReportingClaimstype(null, test7, null).ToList();

                }
                else if (test8 == "null" && test7 == "null")
                {
                    result = MoADEntities.ReportingClaimstype(null, null, test6).ToList();

                }

                //if ((test8 == null && test7 == "null")|| (test8 == "null" && test7 == null))
                //{
                //    result = MoADEntities.ReportingClaimstype(null, test7,null).ToList();

                //}

                //else if (test8 == null || test8 == "NULL")
                //{
                //    result = MoADEntities.ReportingClaimstype(null, test7,null).ToList();

                //}
                //else
                //{
                //    result = MoADEntities.ReportingClaimstype(Int32.Parse(test8), null,null).ToList();

                //}
            }

            return result;
        }

        [HttpGet]
        public List<GetAllFormTemplatesForaType_Result> GetFormTemplatesForAType(string TemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> listOfFormTemplateTypes = new List<GetAllFormTemplatesForaType_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfFormTemplateTypes = MoADEntities.GetAllFormTemplatesForaType(TemplateType).ToList<GetAllFormTemplatesForaType_Result>();
            }

            return listOfFormTemplateTypes;
        }


        [Route("GetTemplateType")]
        [HttpGet]
        public List<FormTemplateKeyVisibility_Result> GetTemplateType(string type,string OrgKey, string Year,int MonthPart)
        {
            List<FormTemplateKeyVisibility_Result> listOfFormTemplateTypes = new List<FormTemplateKeyVisibility_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfFormTemplateTypes = MoADEntities.FormTemplateKeyVisibility(type, OrgKey, Year, MonthPart).ToList<FormTemplateKeyVisibility_Result>();
            }

            return listOfFormTemplateTypes;
        }

        [HttpGet]
        public string AccessFormTemplate(string uk,int ftk)
        {
            string accessFormTemplate = string.Empty;

            using (Entities MoADEntities = new Entities())
            {
                accessFormTemplate = MoADEntities.AccessFormTemplate(uk, ftk).FirstOrDefault();
            }

            return accessFormTemplate;
        }


        [Route("getUnusedAuthenticationCode")]
        [HttpGet]
        public List<GetUnusedAuthenticationCodes_Result> getUnusedAuthenticationCode(string RoleKey, string OkEY)
        {
            List<GetUnusedAuthenticationCodes_Result> UnusedCodes = new List<GetUnusedAuthenticationCodes_Result>();

            using (Entities MoADEntities = new Entities())
            {
                UnusedCodes = MoADEntities.GetUnusedAuthenticationCodes(int.Parse(RoleKey), int.Parse(OkEY)).ToList();
            }

            return UnusedCodes;
        }

        [HttpGet]
        public List<GetRolesPerOrganisation_Result> GetRolesPerOrganisation(string Ok)
        {
            List<GetRolesPerOrganisation_Result> result = new List<GetRolesPerOrganisation_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetRolesPerOrganisation(int.Parse(Ok)).ToList();
            }

            return result;
        }


        [HttpGet]
        public List<ReportingGetCountAndDatesPerFormTemplatekey_Result> ReportingGetCountAndDatesPerFormTemplatekey(string InstanceKey)
        {
            List<ReportingGetCountAndDatesPerFormTemplatekey_Result> listOfFormTemplateTypes = new List<ReportingGetCountAndDatesPerFormTemplatekey_Result>();

            using (Entities MoADEntities = new Entities())
            {
                listOfFormTemplateTypes = MoADEntities.ReportingGetCountAndDatesPerFormTemplatekey(InstanceKey).ToList<ReportingGetCountAndDatesPerFormTemplatekey_Result>();
            }

            return listOfFormTemplateTypes;
        }

        //[HttpGet]
        //public string CreateMessage(string answerTemplateKey)
        //{

        //    string ToUser = "";

        //    string message = "";
        //    string messagetitle = "";

        //    string FileName = "";
        //    string PathBase64 = "";

        //    var AnswersKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(answerTemplateKey);

        //    foreach (var item in AnswersKeyArray)
        //    {
        //        ToUser = item["ToUser"].ToString();

        //        message = item["message"].ToString(); ;
        //        messagetitle = item["messagetitle"].ToString(); ;

        //        FileName = item["FileName"].ToString(); ;
        //        PathBase64 = item["PathBase64"].ToString(); ;

        //    }

        //    string userKey = User.Identity.GetUserId();
        //    using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
        //    {
        //        MoADEntitiesLog.SetPMessages(userKey, ToUser, message, messagetitle, FileName, null, PathBase64, userKey);
        //    }

        //    return "";
        //}

        //[HttpPost]
        //public int CreateMessage([FromBody]string values)
        //{
        //    string ToUser = "";

        //    string message = "";
        //    string messagetitle = "";

        //    string FileName = "";
        //    string PathBase64 = "";


        //    var AnswersKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(values);

        //    foreach (var item in AnswersKeyArray)
        //    {
        //        ToUser = item["ToUser"].ToString();

        //        message = item["message"].ToString(); ;
        //        messagetitle = item["messagetitle"].ToString(); ;

        //        FileName = item["FileName"].ToString(); ;
        //        PathBase64 = item["PathBase64"].ToString(); ;

        //    }

        //    string userKey = User.Identity.GetUserId();
        //    using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
        //    {
        //        MoADEntitiesLog.SetPMessages(userKey, ToUser, message, messagetitle, FileName, null, PathBase64, userKey);
        //    }

        //    return 1;
        //}


        //private IAuthenticationManager AuthenticationManager
        //{
        //    get
        //    {
        //        return HttpContext.GetOwinContext().Authentication;
        //    }
        //}
        //[HttpGet]
        //public string Signout(string ti)
        //{
        //  AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //    return "";
        //}


        [HttpGet]
        public string AddRoles(string RoleName)
        {


            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.ADDRole(RoleName);
            }

            return "Success";
        }

        

      

        [HttpGet]
        public string DeleteRole(string RoleID)
        {


            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.DeleteRole(RoleID);
            }

            return "Success";
        }



        [HttpGet]
        public string CreateRanking(string UserKey, string OrganisationKey, string Ranking, string RankingDescription, string Score, string Year, string IsPublished,string Level,string formTemplateTypeMin,string formTemplateScoreMin,string formTemplateTypeMax, string formTemplateScoreMax)
        {
           
            string result;
            using (Entities MoADEntitiesLog = new Entities())
            {
                

                if (Ranking == "1")
                {
                    result= MoADEntitiesLog.CreateRanking(UserKey, null, null, RankingDescription, null, Year, IsPublished,Level,null,null,null,null).ToList()[0].ToString();

                    result= MoADEntitiesLog.CreateRanking(null,OrganisationKey, Ranking, null, Score, null, null, null,formTemplateTypeMin,formTemplateScoreMin,formTemplateTypeMax,formTemplateScoreMax).ToList()[0].ToString();
                }
                else
                {
                    result= MoADEntitiesLog.CreateRanking(null, OrganisationKey, Ranking, null, Score, null, null, null,formTemplateTypeMin, formTemplateScoreMin, formTemplateTypeMax,formTemplateScoreMax).ToList()[0].ToString();


                }

            }

            return result;
        }


        [HttpGet]
        public string CreatePrivilege(string Name, string Type, string Size, string Html, string HtmlCode, string ColumnValue, string PrivilegeName, string PrivilegeParent, string Level, string Index, string PositionType, string PrivilegeType)
        {

            string result;
            using (Entities MoADEntities= new Entities())
            {


               
                    result = MoADEntities.CreatePrivilege( Name,  Type,  Size,  Html,  HtmlCode,  ColumnValue,  PrivilegeName,  PrivilegeParent,  Int32.Parse(Level), Int32.Parse(Index),  PositionType,  PrivilegeType).ToString();



            }

            return result;
        }

        [HttpGet]
        public string CreateSurveyAnswer(string ku, string IP, string cook, string ans,string ViewKey)
        {

            string result;
            using (Entities MoADEntities = new Entities())
            {

                if (ku == null)
                {
                    result = MoADEntities.CreateSurveyAnswer(null, IP, cook, ans,Int32.Parse(ViewKey)).ToString();

                }
                else
                {
                    result = MoADEntities.CreateSurveyAnswer(ku, IP, cook, ans, Int32.Parse(ViewKey)).ToString();

                }



            }

            return result;
        }

        [Route("SaveOrganisationCard")]
        [HttpGet]
        public string SaveOrganisationCard(string RankingGroupKey, string yea, string yearHalf)
        {

      
            using (Entities MoADEntitiesLog = new Entities())
            {
                    MoADEntitiesLog.GetOrganisationCard(null, Int32.Parse(yea), Int32.Parse(yearHalf), Int32.Parse(RankingGroupKey));


            }

            return "";
        }

        [HttpGet]
        public string SetApproveUser(string Userkey, string approved)
        {


            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.SetApprovedUser(Userkey, approved);
            }

            return "Success";
        }

        [HttpGet]
        public string PublishSurvey(int ID,string actionValue)
        {

            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.PublishSurvey(ID, actionValue);//, PublishStatus
            }

            return "Success";
        }

       



        [HttpGet]
        public string PublishTakarir(string fKey,string uK, string fileExtensionName,string formTemplateKey, string PublishStatus = "Published")
        {

            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.PublishTakarir(fKey, uK, fileExtensionName, formTemplateKey);//, PublishStatus
            }

            return "Success";
        }

        [HttpGet]
        public string SetLog(string Userkey, string source, string title, string message,string IpAdress)
        {


            using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
            {
                MoADEntitiesLog.SetLog(Userkey, source, title, message, IpAdress);
            }

            return "Success";
        }
        [HttpGet]
        public string GenerateAuthenticationCodes(string OrganK, string CountCodes, string rKey)
        {


            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.GenerateAuthenticationCodes(int.Parse(OrganK), int.Parse(CountCodes), int.Parse(rKey));
            }

            return "Success";
        }

        [HttpGet]
        public string PublishRanking(string RankingKey, string Level)
        {


            using (Entities EntitiesLog = new Entities())
            {
                EntitiesLog.PublishRanking(Int32.Parse(RankingKey), Level);
            }

            return "Success";
        }

        [HttpGet]
        public string SetAccessPrivilege(string privilegeKey, string roleKey, string conditionKey, string assignedTime)
        {
            var privilegeKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(privilegeKey);
            int errorCount = 0;
            using (Entities MoADEntities = new Entities())
            {
               
                foreach (var item in privilegeKeyArray)
                {
                    string key = item["key"].ToString();

                    string RolekEY = item["roleKey"].ToString();
                    try {
                        if (RolekEY == "Empty")
                            MoADEntities.SetAccessPivilege(key, roleKey, conditionKey, assignedTime);
                        else
                            MoADEntities.SetAccessPivilege(key, roleKey, conditionKey, assignedTime);
                    } catch(Exception e)
                    {
                        errorCount++;
                    }
                }
            }
            if (errorCount != 0)
            {
                return "1 or more errors occured";
            }
            return "Success";
        }

        [HttpGet]
        public string UpdateOrganisation(string orgKey, string OrgName)
        {


            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.UpdateOrganisation(Int32.Parse(orgKey), OrgName);

            }

            return "Success";
        }


        [HttpGet]
        public string UpdateAccessPrivilege(string privKey, string rolKey, string condKey, string assignTime)
        {
            var privilegeKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(privKey);

            using (Entities MoADEntities = new Entities())
            {

                foreach (var item in privilegeKeyArray)
                {
                    string key = item["key"].ToString();

                    string RolekEY = item["roleKey"].ToString(); ;
                    if (RolekEY == "Empty")
                        MoADEntities.UpdateAccessPivilege(key, rolKey, condKey, assignTime);
                    else
                        MoADEntities.UpdateAccessPivilege(key, rolKey, condKey, assignTime);

                }
            }

            return "Success";
        }


        [HttpGet]
        public string DeleteAccessPrivilege(string pKey, string rKey, string conKey, string assiTime)
        {
            var privilegeKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(pKey);

            using (Entities MoADEntities = new Entities())
            {

                foreach (var item in privilegeKeyArray)
                {
                    string key = item["key"].ToString();

                    //string RolekEY = item["roleKey"].ToString(); ;
                    //if (RolekEY == "Empty")
                    MoADEntities.DeleteAccessPivilege(key, rKey);
                    //else
                    //    MoADEntities.DeleteAccessPivilege(RolekEY, key, rKey, conKey, assiTime);

                }
            }

            return "Success";
        }

        [HttpGet]
        public string EditMouwatin(string RowId, string FIRSTNAME, string FATHERNAME, string SURNAME, string MOTHERNAME, string BIRTHPLACE, string BIRTHDAY, string BIRTHMONTH, string BIRTHYEAR, string SIDENAME, string SIDEPARENT, string JobKey)
        {


            using (Entities MoADEntitiesLog = new Entities())
            {
                MoADEntitiesLog.SetMouwazzafinWithOrg(Int32.Parse(RowId), FIRSTNAME, SURNAME, FATHERNAME, MOTHERNAME, BIRTHPLACE, Double.Parse(BIRTHDAY), Double.Parse(BIRTHMONTH), Double.Parse(BIRTHYEAR), SIDENAME, SIDEPARENT, JobKey);
            }

            return "Success";
        }


        [HttpGet]
        public List<GetPMessages_Result> GetMessages(string user, string fromUser)
        {
            List<GetPMessages_Result> result = new List<GetPMessages_Result>();
            if (fromUser == "null")
                fromUser = null;
            using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
            {

                result = MoADEntitiesLog.GetPMessages(user, fromUser).ToList();
            }

            return result;
        }


        //[HttpGet]
        //public List<GetAccessOrganisation_Result> GetAccessOrganisation(string usrId, string Action,string Orgs)
        //{
        //    List<GetAccessOrganisation_Result> result = new List<GetAccessOrganisation_Result>();

        //    using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
        //    {

        //        if(Orgs=="null")
        //            result = MoADEntitiesLog.GetAccessOrganisation(usrId, Action, null).ToList();
        //            else
        //        result = MoADEntitiesLog.GetAccessOrganisation(usrId, Action,Orgs).ToList();
        //    }

        //    return result;
        //}

        //[HttpGet]
        //public List<GetAccessUsers_Result> GetAccessUsers(string ID, string Key, string Action)
        //{
        //    List<GetAccessUsers_Result> result = new List<GetAccessUsers_Result>();

        //    using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
        //    {
        //        result = MoADEntitiesLog.GetAccessUsers(ID, Key, Action).ToList();
        //    }

        //    return result;
        //}


        [HttpGet]
        public string ReserveCode(string status, string authenticationCode)
        {
            int result;

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.ReservedCode(status, authenticationCode);
            }

            return result.ToString();
        }

        //[HttpGet]
        //public List<ReportingGetSurveyResults_Result> GetSurveyResult(string FormTemKey)
        //{
        //    List<ReportingGetSurveyResults_Result> result = new List<ReportingGetSurveyResults_Result>();

        //    using (Entities MoADEntities = new Entities())
        //    {
        //        result = MoADEntities.ReportingGetSurveyResults(FormTemKey).ToList();
        //    }

        //    return result;
        //}


        [Route("GetUserInformation")]
        [HttpGet]
        public List<GetUserInformation_Result> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            using (Entities MoADEntities = new Entities())
            {
                result = MoADEntities.GetUserInformation(userID).ToList();
            }


            return result;
        }

        // CreateNewFormTemplate(string name, string description, string formTemplateType)
        [HttpGet]
        public string CreateNewFormTemplate(string name, string description, string formTemplateType)
        {
            string formId = string.Empty;

            try
            {
                using (Entities MoADEntities = new Entities())
                {

                    int? Form = MoADEntities.CreateNewFormTemplate(name, description, formTemplateType).FirstOrDefault();

                    formId = Form.ToString();
                }
            }
            catch (Exception e)
            {
                return "Error";
            }

            return formId;
        }

        // CreateNewFormTemplateType(string formTemplateType, string parentFormTemplateType)
        [HttpGet]
        public string CreateNewFormTemplateType(string formTemplateType, string parentFormTemplateType)
        {
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    MoADEntities.CreateNewFormTemplateType(formTemplateType, parentFormTemplateType);
                }
            }
            catch
            {
                return "Error";
            }

            return "success";
        }

        [HttpGet]
        // CreateNewQuestionTemplate(string value, string note, string displayMode, string formTemplateKey)
        public string CreateNewQuestionTemplate(string value, string note, string displayMode, string formTemplateKey)
        {
            string questionId = string.Empty;

            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    int? Form = MoADEntities.CreateNewQuestionTemplate(value, note, displayMode, formTemplateKey).FirstOrDefault();
                    questionId = Form.ToString();

                }
            }
            catch (Exception e)
            {
                return "Error";
            }

            return questionId;
        }


        // CreateAnswerTemplateWithExtendedData(string fileExtensionName, string fileExtensionType, string fileExtensionSize, string filePathOrSource, string createdBy, string hTML, string hTMLCode, string answerTemplateValue, string formElementKey, string scoreKey, string questionKey, string answerTemplateKeyIndex)
        [HttpGet]
        public string CreateAnswerTemplateWithExtendedDataonTemplate(string fileExtensionName, string fileExtensionType, string fileExtensionSize, string filePathOrSource, string createdBy, string hTML, string hTMLCode, string answerTemplateValue, string formElementKey, string scoreKey, string questionKey, string answerTemplateKeyIndex)
        {

            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    MoADEntities.CreateAnswerTemplateWithExtendedData(fileExtensionName, fileExtensionType, fileExtensionSize, filePathOrSource, createdBy, hTML, hTMLCode, answerTemplateValue, formElementKey, scoreKey, questionKey, answerTemplateKeyIndex, null, null, null);

                }
            }
            catch (Exception e)
            {
                return "Error";
            }

            return "Success";
        }




        [HttpGet]
        public string CreateQuestion(string questionvalue, string answerstext, string FormElementKeys, string formid, string AnswerIndex)
        {
            if (answerstext == null)
            {
                answerstext = "";
            }
            if (FormElementKeys == null)
            {
                FormElementKeys = "";
            }

            string[] AnswersText = answerstext.Split(',');
            string[] FormKeys = FormElementKeys.Split(',');
            string questionKey = CreateNewQuestionTemplate(questionvalue, "", "Visible", formid);

            for (var i = 0; i < AnswersText.Length; i++)
            {
                CreateAnswerTemplateWithExtendedDataonTemplate(null, null, null, null, null, null, null, AnswersText[i], FormKeys[i], "1", questionKey, i.ToString());
            }


            return "";
        }

        //GetAllFormTemplatesForaType(string formTemplateType)
        [HttpGet]
        public List<GetAllFormTemplatesForaType_Result> GetAllFormTemplatesForaType(string formTemplateType)
        {
            string FormTemplate = string.Empty;
            List<GetAllFormTemplatesForaType_Result> Form = new List<GetAllFormTemplatesForaType_Result>();

            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetAllFormTemplatesForaType(formTemplateType).ToList<GetAllFormTemplatesForaType_Result>();
                }
            }
            catch
            {
                //return "Error";
            }

            return Form;
        }

        [HttpGet]
        public List<GetAllFormsOfAUser_Result> GetAllFormTemplatesForaUser(string userKey)
        {
            string MyFormClaim = string.Empty;
            List<GetAllFormsOfAUser_Result> Form = new List<GetAllFormsOfAUser_Result>();

            try
            {
                using (Entities MoADEntities = new Entities())
                {

                    if (userKey == "null")
                    {
                        Form = MoADEntities.GetAllFormsOfAUser(null).ToList<GetAllFormsOfAUser_Result>();

                    }
                    else
                    {
                        Form = MoADEntities.GetAllFormsOfAUser(userKey).ToList<GetAllFormsOfAUser_Result>();

                    }

                }
            }
            catch
            {
                //return "Error";
            }

            return Form;
        }





        //public List<object> GetFormTemplate(string TemplateKey)
        //{
        //    List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
        //    List<object> nestedResult = new List<object>();

        //    try
        //    {
        //        using (Entities MoADEntities = new Entities())
        //        {
        //            result = MoADEntities.GetFormTemplate(TemplateKey).ToList<GetFormTemplate_Result>();
        //        }

        //        nestedResult = result
        //            .GroupBy(x => x.FormElementKey).Select(e => new
        //            {
        //                FormElementKey = e.Key,
        //                FormTemplateName = e.Select(x => x.FormTemplateName).First(),
        //                FormTemplateDescription = e.Select(x => x.FormTemplateDescription).First(),
        //                FormTemplateType = e.Select(x => x.FormTemplateType).First(),
        //                FormTemplateStatus = e.Select(x => x.FormTemplateStatus).First(),

        //                Questions = e.Select(ans => new
        //                {
        //                    QuestionKey = ans.QuestionKey,
        //                    QuestionValue = ans.QuestionValue,
        //                    QuestionNote = ans.QuestionNote,
        //                    QuestionDisplayMode = ans.QuestionDisplayMode,
        //                    QuestionKeyIndex = ans.QuestionKeyIndex,
        //                    AnswerTemplateKey = ans.AnswerTemplateKey,
        //                    AnswerValue = ans.AnswerValue,
        //                    AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
        //                    AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
        //                    AnswerFileExtensionName = ans.AnswerFileExtensionName,
        //                    AnswerFileExtensionType = ans.AnswerFileExtensionType,
        //                    AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
        //                    AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
        //                    AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
        //                    AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
        //                    FormElement = ans.FormElement,
        //                    FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
        //                    FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
        //                    FormElementFileExtensionName = ans.FormElementFileExtensionName,
        //                    FormElementFileExtensionType = ans.FormElementFileExtensionType,
        //                    FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
        //                    FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
        //                    FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
        //                    FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
        //                    FormElementTypeName = ans.FormElementTypeName,
        //                    FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
        //                    FormElementTypeKey = ans.FormElementTypeKey,
        //                    FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
        //                    FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
        //                    FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
        //                    FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
        //                    FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
        //                    FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
        //                }).ToList()
        //                    .GroupBy(x => x.QuestionKey).Select(q => new
        //                    {
        //                        QuestionKey = q.Key,
        //                        QuestionValue = q.Select(x => x.QuestionValue).First(),
        //                        QuestionNote = q.Select(x => x.QuestionNote).First(),
        //                        QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
        //                        QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

        //                        Answers = q.Select(ans => new
        //                        {
        //                            AnswerTemplateKey = ans.AnswerTemplateKey,
        //                            AnswerValue = ans.AnswerValue,
        //                            AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
        //                            AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
        //                            AnswerFileExtensionName = ans.AnswerFileExtensionName,
        //                            AnswerFileExtensionType = ans.AnswerFileExtensionType,
        //                            AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
        //                            AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
        //                            AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
        //                            AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
        //                            FormElement = ans.FormElement,
        //                            FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
        //                            FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
        //                            FormElementFileExtensionName = ans.FormElementFileExtensionName,
        //                            FormElementFileExtensionType = ans.FormElementFileExtensionType,
        //                            FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
        //                            FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
        //                            FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
        //                            FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
        //                            FormElementTypeName = ans.FormElementTypeName,
        //                            FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
        //                            FormElementTypeKey = ans.FormElementTypeKey,
        //                            FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
        //                            FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
        //                            FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
        //                            FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
        //                            FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
        //                            FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
        //                        }).ToList()
        //                    }).ToList()
        //            }
        //            ).ToList<object>();
        //    }
        //    catch
        //    {
        //        nestedResult.Add("Error");
        //        return nestedResult;
        //    }

        //    return nestedResult;
        //}

        //ObjectResult<GetFormTemplate_Result> GetFormTemplate(string formTemplateKey)
        [Route("GetFormTemplate")]
        [HttpGet]
        public List<GetFormTemplate_Result> GetFormTemplate(string TemplateKey)
        {
            List<GetFormTemplate_Result> Form = new List<GetFormTemplate_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetFormTemplate(TemplateKey).ToList<GetFormTemplate_Result>();
                }
            }
            catch (Exception e)
            {
                //return "Error";
            }

            return Form;
        }


        [Route("GetSavedForm")]
        [HttpGet]
        public List<GetSavedForm_Result> GetSavedForm(string formKey)
        {
            List<GetSavedForm_Result> Form = new List<GetSavedForm_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetSavedForm(formKey).ToList<GetSavedForm_Result>();

                }
            }
            catch
            {
                //return "Error";
            }

            return Form;
        }

        [Route("GetSavedFormToEdit")]
        [HttpGet]
        public List<GetSavedFormToEdit_Result> GetSavedFormToEdit(string formKey)
        {
            List<GetSavedFormToEdit_Result> Form = new List<GetSavedFormToEdit_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetSavedFormToEdit(formKey).ToList<GetSavedFormToEdit_Result>();

                }
            }
            catch
            {
                //return "Error";
            }

            return Form;
        }

        [Route("GetWorkFlowPerUser")]
        [HttpGet]
        public List<GetWorkFlowItemInstancePerUser_Result> GetWorkFlowPerUser(string userKey, string guid, string OrganisationKey = "null")
        {
            List<GetWorkFlowItemInstancePerUser_Result> Form = new List<GetWorkFlowItemInstancePerUser_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    userKey = (userKey == "null") ? null : userKey;
                    if (guid == "null" && OrganisationKey == "null")
                    {
                        try
                        {
                            Form = MoADEntities.GetWorkFlowItemInstancePerUser(userKey, null, null).ToList<GetWorkFlowItemInstancePerUser_Result>();

                        }
                        catch
                        {

                        }
                    }
                    else
                    {
                        if (OrganisationKey != "null")
                        {
                            if (OrganisationKey == "OPGeneralDirector")
                            {
                                Form = MoADEntities.GetWorkFlowItemInstancePerUser(null, null, null).ToList<GetWorkFlowItemInstancePerUser_Result>();
                            }
                            else
                            {
                                Form = MoADEntities.GetWorkFlowItemInstancePerUser(null, null, int.Parse(OrganisationKey)).ToList<GetWorkFlowItemInstancePerUser_Result>();
                            }
                        }
                        else
                        {
                            Form = MoADEntities.GetWorkFlowItemInstancePerUser(null, Guid.Parse(guid), null).Distinct().ToList<GetWorkFlowItemInstancePerUser_Result>();
                        }
                    }
                }
            }
            catch (InvalidCastException e)
            {

            }

            return Form;
        }


        [HttpGet]
        public string CreateNewWorkFlowInstance(string Note, string GUID, string CreatedAt, string Formkey, string InvocationType, string Updatedat, string updatedbyuserkey, string CurrentStatusKey, string NextFormTemplateKey, int ActionKey = 0, string NextOrganisationKey = "", string CurrentOrganisationKey = "")
        {


            using (Entities MoADEntities = new Entities())
            {
                GetOrganisationAndRolePerUser_Result test = getUserOrganisationAndRole(updatedbyuserkey);

                List<GetWorkFlowItemPerFormTemplate_Result> test2 = getWorkFlowRulesForForTemplate(NextFormTemplateKey, CurrentStatusKey, test.RoleId.ToString(), NextOrganisationKey);

                if (ActionKey == 0)
                {
                    test2 = getWorkFlowRulesForForTemplate(NextFormTemplateKey, CurrentStatusKey, test.RoleId.ToString(), NextOrganisationKey);
                }
                else
                {
                    test2 = getWorkFlowRulesForForTemplate(NextFormTemplateKey, CurrentStatusKey, test.RoleId.ToString(), NextOrganisationKey).Where(x => x.NextStatusKey == ActionKey).ToList();
                }



                string workflowitemkey = test2.FirstOrDefault().WorkFlowItemKey.ToString();
                string nextroleid = test2.FirstOrDefault().ExtendedNextRoleKey.ToString();
                string organizationkey = "";

                if (ActionKey==21 && GetUserInformation(updatedbyuserkey).FirstOrDefault().RoleId=="27")
                {
                     organizationkey= GetWorkFlowPerUser(null, GUID,"null").Where(x=>x.NextFormTemplateKey==109 && x.NextStatusKey==2).FirstOrDefault().NextOrganisationKey.ToString();
                }
                else
                {
                     organizationkey = test2.FirstOrDefault().NextExtendedOrganisationKey.ToString();
                }


                string ruleNextFormTemplateKey = test2.FirstOrDefault().NextExtendedFormTemplateKey.ToString();

                List<GetUserInformation_Result> UserInfoResult = new List<GetUserInformation_Result>();

                UserInfoResult = GetUserInformation(updatedbyuserkey);

                string userRole = UserInfoResult.FirstOrDefault().RoleId;

                string userId = "";
                if (userRole == "21" && ActionKey == 18)
                {
                    List<GetOPGroupsNewClaim_Result> EligibleToHandl = GetOPGroupsNewClaim(GUID);
                    userId = EligibleToHandl.FirstOrDefault().AssignedToUserKey;
                }
                else if (userRole == "27" && nextroleid == "2")
                {
                    List<GetOPGroupsNewClaim_Result> EligibleToHandl = GetOPGroupsNewClaim(GUID);
                    userId = EligibleToHandl.FirstOrDefault().UpdatedByUserKey;
                }
                else
                {
                    List<GetAllUsersPerOrganisationAndRole_Result> EligibleToHandl = getUsersEligibleToHandleWorkFlowItem(nextroleid, organizationkey);
                    userId = EligibleToHandl.FirstOrDefault().Id;
                }


                var tes11t = Guid.Parse(GUID);

                MoADEntities.CreateNewWorkFlowInstance(workflowitemkey, updatedbyuserkey, userId, Convert.ToDateTime(CreatedAt), Convert.ToDateTime(Updatedat), InvocationType, Formkey, ruleNextFormTemplateKey, Guid.Parse(GUID), Note);
            }

            return "";
        }

        [HttpGet]
        public static List<GetOPGroupsNewClaim_Result> GetOPGroupsNewClaim(string GUID)
        {
            List<GetOPGroupsNewClaim_Result> eligibleUsers = new List<GetOPGroupsNewClaim_Result>();

            using (Entities entities = new Entities())
            {
                eligibleUsers.AddRange(entities.GetOPGroupsNewClaim(GUID).ToList());
            }

            return eligibleUsers;
        }

        [HttpGet]
        public static List<GetAllUsersPerOrganisationAndRole_Result> getUsersEligibleToHandleWorkFlowItem(string userRoleIdentifier, string organisationIdentifier)
        {
            List<GetAllUsersPerOrganisationAndRole_Result> eligibleUsers = new List<GetAllUsersPerOrganisationAndRole_Result>();

            using (Entities entities = new Entities())
            {
                eligibleUsers.AddRange(entities.GetAllUsersPerOrganisationAndRole(userRoleIdentifier, organisationIdentifier).ToList());
            }

            return eligibleUsers;
        }

        [HttpGet]
        public List<GetAllUsersPerOrganisationAndRole_Result> test1(string test2, string test3)
        {
            List<GetAllUsersPerOrganisationAndRole_Result> eligibleUsers = new List<GetAllUsersPerOrganisationAndRole_Result>();

            using (Entities entities = new Entities())
            {
                eligibleUsers = entities.GetAllUsersPerOrganisationAndRole(test2, test3).ToList();
            }

            return eligibleUsers;
        }


        [HttpGet]

        public static List<GetWorkFlowItemPerFormTemplate_Result> getWorkFlowRulesForForTemplate(string formTemplateIdentifier, string currentStatusIdentifier, string userRoleIdentifier, string NextOrganisationKey)
        {
            List<GetWorkFlowItemPerFormTemplate_Result> formWorfFlowRules = new List<GetWorkFlowItemPerFormTemplate_Result>();

            using (Entities entities = new Entities())
            {
                formWorfFlowRules.AddRange(entities.GetWorkFlowItemPerFormTemplate(formTemplateIdentifier, currentStatusIdentifier, userRoleIdentifier, null, NextOrganisationKey).ToList());
            }

            return formWorfFlowRules;
        }


        [HttpGet]

        public GetOrganisationAndRolePerUser_Result testOrganisationKey(string test2)
        {
            GetOrganisationAndRolePerUser_Result result = null;
            using (Entities entities = new Entities())
            {
                result = entities.GetOrganisationAndRolePerUser(test2).ToList().FirstOrDefault(); ;
            }

            return result;
        }

        [HttpGet]

        public List<MostUsersLogMove_Result> MostUsersLogMove(string MonitorKey)
        {
            List<MostUsersLogMove_Result> result = new List<MostUsersLogMove_Result>();
            using (MoADLogEntities entities = new MoADLogEntities())
            {
                result = entities.MostUsersLogMove(MonitorKey).ToList();
            }

            return result;
        }





        [HttpGet]
        public List<GetAdManagerForms_Result> getAdManagerForms(string UserId, string FormTemplateType, string organisationKey, int UserRoleID)
        {
            List<GetAdManagerForms_Result> AdManagerForms = new List<GetAdManagerForms_Result>();

            using (Entities entities = new Entities())
            {

                if (organisationKey == "null")
                    AdManagerForms = entities.GetAdManagerForms(null, FormTemplateType, null, null).ToList();
                else
                    AdManagerForms = entities.GetAdManagerForms(null, FormTemplateType, null, null).ToList();

                // AdManagerForms = entities.GetAdManagerForms(null, FormTemplateType, Int32.Parse(organisationKey), null).ToList();

                //if (organisationKey == "null")
                //    AdManagerForms = entities.GetAdManagerForms(UserId, FormTemplateType, null, UserRoleID).ToList();
                //else
                //    AdManagerForms = entities.GetAdManagerForms(UserId, FormTemplateType, Int32.Parse(organisationKey), UserRoleID).ToList();
            }

            return AdManagerForms;
        }

        [HttpGet]
        public List<GetAdManagerForms_Result> getAdManagerFormsForMoad(string FormTemplateType, int organisationKey)
        {
            List<GetAdManagerForms_Result> AdManagerForms = new List<GetAdManagerForms_Result>();

            using (Entities entities = new Entities())
            {
                AdManagerForms = entities.GetAdManagerForms(null, FormTemplateType, organisationKey, null).ToList();
            }

            return AdManagerForms;
        }

        [HttpGet]
        public List<GETOPmanagerForms_Result> OpManagerFormsForAdManager(string Type, string key)
        {
            List<GETOPmanagerForms_Result> OPManagerForms = new List<GETOPmanagerForms_Result>();

            using (Entities entities = new Entities())
            {
                OPManagerForms = entities.GETOPmanagerForms(null, Type, int.Parse(key), null).ToList();
            }

            return OPManagerForms;
        }

        [HttpGet]
        public List<GetInitiative_Result> GetInitiatives(string o)
        {
            List<GetInitiative_Result> result = new List<GetInitiative_Result>();

            using (Entities entities = new Entities())
            {
                result = entities.GetInitiative(o).ToList();
            }

            return result;
        }

            [HttpGet]
        public string ADDInitiatives(string organisationKey,string initiativeValue,string FileExtensionKey,string FormElementKey)
        {
            string result = "";

            using (Entities entities = new Entities())
            {
                result = entities.ADDInitiative(organisationKey, initiativeValue, null, null).ToString();
            }

            return result;
        }

        [HttpGet]
        public string DeleteInitiatives(string initiativeKey)
        {
            string result = "";

            using (Entities entities = new Entities())
            {
                result = entities.DeleteInitiative(initiativeKey).ToString();
            }

            return result;
        }

        [HttpGet]
        public static GetOrganisationAndRolePerUser_Result getUserOrganisationAndRole(string userIdentifier)
        {
            GetOrganisationAndRolePerUser_Result userOrganisationAndRole = null;

            using (Entities entities = new Entities())
            {
                userOrganisationAndRole = entities.GetOrganisationAndRolePerUser(userIdentifier).ToList().FirstOrDefault();
            }

            return userOrganisationAndRole;
        }

        [HttpGet]
        public List<GetSavedFormsperGUID_Result> GetSavedFormsperGUID(string guid)
        {
            List<GetSavedFormsperGUID_Result> SvedFormPerGuid = new List<GetSavedFormsperGUID_Result>();

            using (Entities entities = new Entities())
            {
                SvedFormPerGuid = entities.GetSavedFormsperGUID(Guid.Parse(guid)).ToList();

            }

            return SvedFormPerGuid;
        }


        [HttpGet]
        public List<ReportingGeneralPieChart_Result> ReportingGeneralPieChart(int? OrgKey, string UserKey)
        {

            UserKey = (UserKey == "null") ? null : UserKey;

            List<ReportingGeneralPieChart_Result> GeneralPieChart = new List<ReportingGeneralPieChart_Result>();

            using (Entities entities = new Entities())
            {
                try
                {
                    GeneralPieChart = entities.ReportingGeneralPieChart(OrgKey, UserKey, null).ToList();
                }
                catch
                {

                }
            }
            return GeneralPieChart;
        }

        [HttpGet]
        public List<ReportingDetailedPieChart_Result> ReportingDetailedPieChart(int? nextExtendedOrganisationKey, string assignedToUserKey)
        {
            assignedToUserKey = (assignedToUserKey == "null") ? null : assignedToUserKey;

            List<ReportingDetailedPieChart_Result> DetailedPieChart = new List<ReportingDetailedPieChart_Result>();

            using (Entities entities = new Entities())
            {
                try
                {
                    DetailedPieChart = entities.ReportingDetailedPieChart(nextExtendedOrganisationKey, assignedToUserKey, null).ToList();
                }
                catch
                {

                }
            }
            return DetailedPieChart;
        }

        [Route("RepoertingRepeatedPieChart")]
        [HttpGet]
        public List<RepoertingRepeatedPieChart_Result> RepoertingRepeatedPieChart()
        {
            List<RepoertingRepeatedPieChart_Result> ReportingPieChart = new List<RepoertingRepeatedPieChart_Result>();

            using (Entities entities = new Entities())
            {
                try
                {
                    ReportingPieChart = entities.RepoertingRepeatedPieChart().ToList();
                }
                catch
                {

                }
            }
            return ReportingPieChart;
        }




        //int SaveFormTemplate(string formTemplateKey)
        [Route("SaveFormTemplate")]
        [HttpGet]
        public string SaveFormTemplate(string FormId)
        {
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    MoADEntities.SaveFormTemplate(FormId);
                }
            }
            catch
            {
                return "Error";
            }

            return "success";
        }

        [Route("SubmitFormForUser")]
        [HttpGet]
        public string SubmitFormForUser(string trackingStatusKey, string formTemplateKey, string userKey, string organisationKey, string FileExtensionName=null,string FileExtensionType = null, string createdby = null)
        {
            //int? formKey = 0;

            if (organisationKey == null)
                organisationKey = "20995";

            if (userKey == null)
                userKey = "null";
            if(FileExtensionName== "مستوى الوزارة" || FileExtensionName== "مستوى الإقليمي")
            {
                FileExtensionType = organisationKey;
                organisationKey = "20995";
            }
            string result;
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    if (createdby != "null")
                    {
                        if (userKey == "null")
                        {
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, null, organisationKey, FileExtensionName, FileExtensionType, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();

                        }
                        else if (FileExtensionName == "null")
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, null, null, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();
                        else if (FileExtensionType != null)
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, FileExtensionType, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();
                        else
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, null, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();

                    }
                    else
                    {
                        if (userKey == "null")
                        {
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, null, organisationKey, FileExtensionName, FileExtensionType, null, null, null, null, null, null, null, null).ToList()[0].ToString();

                        }
                        else if (FileExtensionName == "null")
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, null, null, null, null, null, null, null, null, null, null).ToList()[0].ToString();
                        else if (FileExtensionType != null)
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, FileExtensionType, null, null, null, null, null, null, null, null).ToList()[0].ToString();
                        else
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, null, null, null, null, null, null, null, null, null).ToList()[0].ToString();

                    }
                }
            }
            catch(Exception e)
            {
                return "Error";
            }

            return result;
        }

        [HttpGet]
        public string SignupAnswers(string FormKey, string answerTemplateKey, string answer, string type)
        {
            //int? formKey = 0;
            string result = "";
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    result = MoADEntities.AnswersByuser(FormKey, answerTemplateKey, answer, type, null, null, null, null, null, null, null, null).ToString();

                    //int.TryParse(result[0].ToString(), out formKey);
                }
            }
            catch
            {
                return "Error";
            }

            return result;
        }

        //[HttpPost]
        //public string ChangePicture([FromBody]string values)
        //{

        //    string[] substrings = Regex.Split(values, "&&&");
        //    string Ky = substrings[0];
        //    string ImageBas64 = substrings[1];

        //    using (Entities MoADEntities = new Entities())
        //    {
        //        MoADEntities.UpdateAvatar(Ky, ImageBas64);
        //    }

        //    return "Success";
        //}

        [Route("AnswersByUser")]
        [HttpPost]
        public int AnswersByUser([FromBody]string values)
        {

            string[] substrings = Regex.Split(values, "&&&");
            string formKey = substrings[0];
            string answerTemplateKey = substrings[1];

            var AnswersKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(answerTemplateKey);
            int result = 0;

            if (formKey == "0000")
            {

                if (AnswersKeyArray.Count > 1)
                    return 0;
                string ToUser = "";

                string message = "";
                string messagetitle = "";

                string FileName = "";
                string PathBase64 = "";

                string userKey = "";


                foreach (var item in AnswersKeyArray)
                {
                    ToUser = item["ToUser"].ToString();
                    userKey = item["userKey"].ToString();
                    message = item["message"].ToString();
                    messagetitle = item["messagetitle"].ToString();

                    FileName = item["FileName"].ToString();
                    PathBase64 = item["PathBase64"].ToString();

                }

                using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
                {
                    MoADEntitiesLog.SetPMessages(userKey, ToUser, message, messagetitle, FileName, null, PathBase64, userKey);
                }
            }
            else
            {
                try
                {
                    using (Entities MoADEntities = new Entities())
                    {
                        foreach (var item in AnswersKeyArray)
                        {
                            string Index = " ";
                            string Lines = " ";
                            string Image = " ";
                            string key = item["key"].ToString();
                            string Value = item["Textvalue"].ToString();
                            string type = item["type"].ToString();

                            string Parent = item["Parent"].ToString();
                            string Child = item["Child"].ToString();
                            string Count = item["count"].ToString();

                            try
                            {
                                Image = item["Image"].ToString();

                            }
                            catch { }
                            try
                            {
                                Index = item["Index"].ToString();


                            }
                            catch { }

                            try
                            {
                                Lines = item["Lines"].ToString();
                            }
                            catch { }


                            if (Value.Equals(""))
                                result = MoADEntities.AnswersByuser(formKey, key, type, null, null, Image, null, Index, Lines, Parent, Child, Count);
                            else
                                result = MoADEntities.AnswersByuser(formKey, key, Value, type, null, Image, null, Index, Lines, Parent, Child, Count);

                        }
                    }

                }
                catch (Exception e)
                {
                    return 0;
                }
            }


            return result;
        }


        [HttpGet]
        public string DeleteAnswers(string idform,string u_id)
        {
            string result = "";
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    result = MoADEntities.DeleteAnswers(int.Parse(idform), u_id).ToString();
                }

            }
            catch
            {
                return "Error";
            }

            return result;
        }

        [Route("SaveClaimForm")]
        [HttpGet]
        public string SaveClaimForm(string formKey)
        {
            string result = "";
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    result = MoADEntities.SaveForm(formKey).ToString();
                }

            }
            catch
            {
                return "Error";
            }

            return result;
        }

        [HttpGet]
        public string DeleteForm(int keyFormInstance)
        {
            string result = "";
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    result = MoADEntities.DeleteForm(keyFormInstance).ToString();
                }

            }
            catch
            {
                return "Error";
            }

            return result;
        }

        [HttpGet]
        public string OPSaveForm(string opChosenOrgKey)
        {
            string result = "";
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    result = MoADEntities.SaveOPForm(opChosenOrgKey).ToString();
                }

            }
            catch
            {
                return "Error";
            }

            return result;
        }


        [Route("GetUsers")] 
        [HttpGet]
        public List<object> GetUsers()
        {
            List<object> result = new List<object>();

            using (Entities Entity = new Entities())
            {
                result = (from u in Entity.AspNetUsers
                          select new
                          {
                              UserName = u.UserName,
                              Email = u.Email,
                              Id = u.Id
                          }).ToList<object>();
            }

            return result;
        }

        [Route("GetFormsPerAtribute")]
        [HttpGet]
        public List<GetFormsPerAtribute_Result> GetFormsPerAtribute(string Iduser,string TypeofTemplate,int KeyOrg,int KeyRole,string Attribute,string value)
        {
            List<GetFormsPerAtribute_Result> Forms = new List<GetFormsPerAtribute_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Forms = MoADEntities.GetFormsPerAtribute(Iduser, TypeofTemplate, KeyOrg, KeyRole,null, Attribute,value).ToList<GetFormsPerAtribute_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Forms;
        }

        [Route("GetServicesPerOrganisation")]
        [HttpGet]
        public List<string> GetServicesPerOrganisation(string serviceOrganisationKey)
        {
            List<string> Form = new List<string>();
            
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    if (serviceOrganisationKey == null)
                    {
                        Form = MoADEntities.GetServicesPerOrganisation(null).ToList();
                    }
                    else
                    {
                        int orgKey = int.Parse(serviceOrganisationKey);
                        Form = MoADEntities.GetServicesPerOrganisation(orgKey).ToList();
                    }

                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }

        [Route("GetAllOrganisations")] 
        [HttpGet]
        public List<GetAllOrganisations_Result> GetAllOrganisations()
        {
            List<GetAllOrganisations_Result> Form = new List<GetAllOrganisations_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetAllOrganisations(null, null).ToList<GetAllOrganisations_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }

        [Route("OPOrg")]
        [HttpGet]
        public List<GetOPOrganisations_Result> OPOrg(string KeyRoleJ, string Pipi)
        {
            List<GetOPOrganisations_Result> Form = new List<GetOPOrganisations_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetOPOrganisations(int.Parse(KeyRoleJ), int.Parse(Pipi)).ToList<GetOPOrganisations_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }

        [Route("GetUsersToCreate")]
        [HttpGet]
        public List<GetUsersToCreate_Result> GetUsersToCreate()
        {
            List<GetUsersToCreate_Result> Form = new List<GetUsersToCreate_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetUsersToCreate().ToList<GetUsersToCreate_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }

        [Route("GetHRInfo")]
        [HttpGet]
        public List<GetHRInfo_Result> GetHRInfo(string IdOrg)
        {
            List<GetHRInfo_Result> Form = new List<GetHRInfo_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetHRInfo(IdOrg).ToList<GetHRInfo_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }

        [HttpGet]
        public List<GetAllOrganisations_Result> GetAllOrganisationsReporting(string parent, string parentOrg,string r)
        {

            List<GetAllOrganisations_Result> Form = new List<GetAllOrganisations_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    

                    if (parentOrg == "null")
                        Form = MoADEntities.GetAllOrganisations(parent, null).ToList<GetAllOrganisations_Result>();

                    else if (parent == "null")
                        Form = MoADEntities.GetAllOrganisations(null, parentOrg).ToList<GetAllOrganisations_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }


        [Route("GetRoles")]
        [HttpGet]
        public List<object> GetRoles()
        {
            List<object> result = new List<object>();

            using (Entities Entity = new Entities())
            {
                result = (from u in Entity.AspNetRoles
                          select new
                          {

                              RoleName = u.ArabicName,
                              RoleEngName = u.Name,
                              RoleId = u.Id,
                          }).ToList<object>();


            }

            return result;
        }








        [HttpGet]
        public string AddRole(string role)
        {
            try
            {
                using (Entities Entity = new Entities())
                {
                    int maxid = 0;
                    var test = (from u in Entity.AspNetRoles
                                select new
                                {
                                    u.Id,
                                }).ToList();

                    foreach (var roleid in test)
                    {
                        if (Int32.Parse(roleid.Id) > maxid)
                        {
                            maxid = Int32.Parse(roleid.Id);
                        }
                    }

                    int id = maxid + 1;

                    AspNetRole newrole = new AspNetRole();
                    newrole.Name = role;
                    newrole.ArabicName = role;
                    newrole.Id = id.ToString();

                    Entity.AspNetRoles.Add(newrole);
                    Entity.SaveChanges();
                }
            }
            catch
            {
                return "Error";
            }

            return "";
        }

    }
}
