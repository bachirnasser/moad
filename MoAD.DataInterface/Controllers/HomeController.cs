﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;

namespace MoAD.DataInterface.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [System.Web.Mvc.HttpPost]
        public string ChangePicture([FromBody]string values)
        {

            string[] substrings = Regex.Split(values, "&&&");
            string Ky = substrings[0];
            string ImageBas64 = substrings[1];

            using (Entities MoADEntities = new Entities())
            {
                MoADEntities.UpdateAvatar(Ky, ImageBas64);
            }

            return "Success";
        }


    }
}
