﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MoAD.DataInterface.Controllers
{
    [Authorize]
    [RoutePrefix("api/ClaimForm")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ClaimFormController : ApiController
    {
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("GetFormTemplate")]
        [HttpGet]
        public List<GetFormTemplate_Result> GetFormTemplate(string TemplateKey)
        {
            List<GetFormTemplate_Result> Form = new List<GetFormTemplate_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetFormTemplate(TemplateKey).ToList<GetFormTemplate_Result>();
                }
            }
            catch (Exception e)
            {
                //return "Error";
            }

            return Form;
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("SubmitFormForUser")]
        [HttpGet]
        public string SubmitFormForUser(string trackingStatusKey, string formTemplateKey, string userKey, string organisationKey, string FileExtensionName = null, string FileExtensionType = null, string createdby = null)
        {
            //int? formKey = 0;

            if (organisationKey == null)
                organisationKey = "20995";

            if (userKey == null)
                userKey = "null";
            if (FileExtensionName == "مستوى الوزارة" || FileExtensionName == "مستوى الإقليمي")
            {
                FileExtensionType = organisationKey;
                organisationKey = "20995";
            }
            string result;
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    if (createdby != "null")
                    {
                        if (userKey == "null")
                        {
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, null, organisationKey, FileExtensionName, FileExtensionType, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();

                        }
                        else if (FileExtensionName == "null")
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, null, null, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();
                        else if (FileExtensionType != null)
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, FileExtensionType, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();
                        else
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, null, null, null, createdby, null, null, null, null, null).ToList()[0].ToString();

                    }
                    else
                    {
                        if (userKey == "null")
                        {
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, null, organisationKey, FileExtensionName, FileExtensionType, null, null, null, null, null, null, null, null).ToList()[0].ToString();

                        }
                        else if (FileExtensionName == "null")
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, null, null, null, null, null, null, null, null, null, null).ToList()[0].ToString();
                        else if (FileExtensionType != null)
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, FileExtensionType, null, null, null, null, null, null, null, null).ToList()[0].ToString();
                        else
                            result = MoADEntities.SubmitFormForUser(trackingStatusKey, formTemplateKey, userKey, organisationKey, FileExtensionName, null, null, null, null, null, null, null, null, null).ToList()[0].ToString();

                    }
                }
            }
            catch (Exception e)
            {
                return "Error";
            }

            return result;
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("AnswersByUser")]
        [HttpPost]
        public int AnswersByUser([FromBody] string values)
        {

            string[] substrings = Regex.Split(values, "&&&");
            string formKey = substrings[0];
            string answerTemplateKey = substrings[1];

            var AnswersKeyArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(answerTemplateKey);
            int result = 0;

            if (formKey == "0000")
            {

                if (AnswersKeyArray.Count > 1)
                    return 0;
                string ToUser = "";

                string message = "";
                string messagetitle = "";

                string FileName = "";
                string PathBase64 = "";

                string userKey = "";


                foreach (var item in AnswersKeyArray)
                {
                    ToUser = item["ToUser"].ToString();
                    userKey = item["userKey"].ToString();
                    message = item["message"].ToString();
                    messagetitle = item["messagetitle"].ToString();

                    FileName = item["FileName"].ToString();
                    PathBase64 = item["PathBase64"].ToString();

                }

                using (MoADLogEntities MoADEntitiesLog = new MoADLogEntities())
                {
                    MoADEntitiesLog.SetPMessages(userKey, ToUser, message, messagetitle, FileName, null, PathBase64, userKey);
                }
            }
            else
            {
                try
                {
                    using (Entities MoADEntities = new Entities())
                    {
                        foreach (var item in AnswersKeyArray)
                        {
                            string Index = " ";
                            string Lines = " ";
                            string Image = " ";
                            string key = item["key"].ToString();
                            string Value = item["Textvalue"].ToString();
                            string type = item["type"].ToString();

                            string Parent = item["Parent"].ToString();
                            string Child = item["Child"].ToString();
                            string Count = item["count"].ToString();

                            try
                            {
                                Image = item["Image"].ToString();

                            }
                            catch { }
                            try
                            {
                                Index = item["Index"].ToString();


                            }
                            catch { }

                            try
                            {
                                Lines = item["Lines"].ToString();
                            }
                            catch { }


                            if (Value.Equals(""))
                                result = MoADEntities.AnswersByuser(formKey, key, type, null, null, Image, null, Index, Lines, Parent, Child, Count);
                            else
                                result = MoADEntities.AnswersByuser(formKey, key, Value, type, null, Image, null, Index, Lines, Parent, Child, Count);

                        }
                    }

                }
                catch (Exception e)
                {
                    return 0;
                }
            }


            return result;
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("SaveClaimForm")]
        [HttpGet]
        public string SaveClaimForm(string formKey)
        {
            string result = "";
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    result = MoADEntities.SaveForm(formKey).ToString();
                }

            }
            catch
            {
                return "Error";
            }

            return result;
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("GetAllOrganisations")]
        [HttpGet]
        public List<GetAllOrganisations_Result> GetAllOrganisations()
        {
            List<GetAllOrganisations_Result> Form = new List<GetAllOrganisations_Result>();
            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    Form = MoADEntities.GetAllOrganisations(null, null).ToList<GetAllOrganisations_Result>();
                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("GetServicesPerOrganisation")]
        [HttpGet]
        public List<string> GetServicesPerOrganisation(string serviceOrganisationKey)
        {
            List<string> Form = new List<string>();

            try
            {
                using (Entities MoADEntities = new Entities())
                {
                    if (serviceOrganisationKey == null)
                    {
                        Form = MoADEntities.GetServicesPerOrganisation(null).ToList();
                    }
                    else
                    {
                        int orgKey = int.Parse(serviceOrganisationKey);
                        Form = MoADEntities.GetServicesPerOrganisation(orgKey).ToList();
                    }

                }
            }
            catch
            {
                //return "Error";
            }
            return Form;
        }
    }
}