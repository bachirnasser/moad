//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataInterface.EDMX
{
    using System;
    
    public partial class GetWorkFlowItemInstancePerUser_Result
    {
        public Nullable<int> WorkFlowItemKey { get; set; }
        public string UpdatedByUserKey { get; set; }
        public string UpdatedByUserName { get; set; }
        public string AssignedToUserKey { get; set; }
        public string AssighedToUserName { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> CurrentFormKey { get; set; }
        public Nullable<int> NextFormTemplateKey { get; set; }
        public string NextFormTemplateName { get; set; }
        public string NextFormTemplateDescription { get; set; }
        public Nullable<System.Guid> GUID { get; set; }
        public Nullable<int> InvocationTypeKey { get; set; }
    }
}
