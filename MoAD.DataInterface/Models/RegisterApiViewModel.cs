﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoAD.DataInterface.Models
{
    public class RegisterApiViewModel
    {
        [Required]
        //[EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        //[EmailAddress]
        [Display(Name = "Email")]
        public string Username { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public string Sex { get; set; }
        public string SSN { get; set; }
        public string MobileNumber { get; set; }
        //public Nullable<int> YearsOfDuty { get; set; }
        //public string SecretNumber { get; set; }
        public string UserImage { get; set; }
        public string PhoneNumber { get; set; }

        //public string Approved { get; set; }
    }
}