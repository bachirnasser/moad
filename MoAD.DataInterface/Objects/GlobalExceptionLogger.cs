﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace MoAD.DataInterface.Objects
{
    public class GlobalExceptionLogger: ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            LogManager.writeLog("MoAD.DataInterface", "Global Exception", context.ExceptionContext.Exception.ToString());
           
        }
    }
}