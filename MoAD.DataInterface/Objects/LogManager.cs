﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoAD.DataInterface;
using MoAD.DataObjects;
using MoAD.DataObjects.EDMX;

namespace MoAD.DataInterface.Objects
{
    public class LogManager
    {
        public static void writeLog(string soucre, string title, string message)
        {
            using (Entities MoADEntities = new Entities())
            {
                Log log = new Log();

                log.DateTime = DateTime.Now;
                log.Source = soucre;
                log.Title = title;
                log.Message = message;

                MoADEntities.Logs.Add(log);

                MoADEntities.SaveChanges();
            }
        }
    }
}