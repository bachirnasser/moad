﻿using System;
using System.Linq;
using MoAD.DataInterface.Globals;

namespace MoAD.DataInterface
{
    public class UserSecurity
    {

        public static bool Login(string username, string password)
        {
            return (
                (username == Configuration.USERNAME) 
                && 
                (password == Configuration.PASSWORD)
                );            
        }

    }
}