﻿(function () {

    $('#input_apiKey').unbind();

    function addApiKeyAuthorizationV2() {
        var key = $('#input_apiKey')[0].value;
        if (key && key.trim() != "") {
            var bearer = 'bearer ' + key + '';
            var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization(swashbuckleConfig.apiKeyName, bearer, swashbuckleConfig.apiKeyIn);
            window.swaggerUi.api.clientAuthorizations.add("api_key", apiKeyAuth);
        }
    }

    $(document).ready(function () {
        addApiKeyAuthorizationV2();

    });
})();