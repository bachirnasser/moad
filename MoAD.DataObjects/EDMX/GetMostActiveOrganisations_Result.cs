//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    
    public partial class GetMostActiveOrganisations_Result
    {
        public Nullable<int> CountEvaluatedClaims { get; set; }
        public int OrganisationKey { get; set; }
        public string OrganisationName { get; set; }
        public string OrganisationParent { get; set; }
    }
}
