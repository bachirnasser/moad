//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class PageComponentsHierarchy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PageComponentsHierarchy()
        {
            this.PageComponentsHierarchyRedirectionEntities = new HashSet<PageComponentsHierarchyRedirectionEntity>();
        }
    
        public int PageComponentHierarchyKey { get; set; }
        public Nullable<int> PageKey { get; set; }
        public Nullable<int> ComponentKey { get; set; }
        public Nullable<int> ParentComponentKey { get; set; }
        public Nullable<int> ComponentLevel { get; set; }
        public Nullable<int> ComponentOrder { get; set; }
        public Nullable<int> ComponentIndex { get; set; }
        public Nullable<int> RedirectionTypeKey { get; set; }
    
        public virtual Component Component { get; set; }
        public virtual Component Component1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PageComponentsHierarchyRedirectionEntity> PageComponentsHierarchyRedirectionEntities { get; set; }
        public virtual Page Page { get; set; }
        public virtual RedirectionsType RedirectionsType { get; set; }
    }
}
