//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScoringInstance
    {
        public int ScoringInstanceKey { get; set; }
        public Nullable<int> KPIKey { get; set; }
        public string ScoringParam { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> ItemTrackingStatusKey { get; set; }
        public string Score { get; set; }
        public string ScoreStatus { get; set; }
        public Nullable<int> ScoringRulesKey { get; set; }
    
        public virtual KPI KPI { get; set; }
    }
}
