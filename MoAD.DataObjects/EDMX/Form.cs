//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Form()
        {
            this.Answers = new HashSet<Answer>();
        }
    
        public int FormKey { get; set; }
        public string Status { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public string TrackingStatusKey { get; set; }
        public Nullable<int> FormTemplateKey { get; set; }
        public string UserKey { get; set; }
        public Nullable<int> OrganisationKey { get; set; }
        public Nullable<int> FileExtensionKey { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual Organisation Organisation { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ExtendedData ExtendedData { get; set; }
    }
}
