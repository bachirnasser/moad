//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class Log
    {
        public int Identifier { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public string Source { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
