//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    
    public partial class GetInitiative_Result
    {
        public int InitiativeKey { get; set; }
        public Nullable<int> OrganisationKey { get; set; }
        public string InitiativeValue { get; set; }
        public string CreatedAt { get; set; }
        public Nullable<int> FileExtensionKey { get; set; }
        public Nullable<int> FormElementKey { get; set; }
    }
}
