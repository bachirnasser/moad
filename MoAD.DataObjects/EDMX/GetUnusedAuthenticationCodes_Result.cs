//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    
    public partial class GetUnusedAuthenticationCodes_Result
    {
        public string AuthenticationCode { get; set; }
        public string UsedByUserKey { get; set; }
        public string Status { get; set; }
        public Nullable<int> ExtendedRoleKey { get; set; }
        public string Name { get; set; }
    }
}
