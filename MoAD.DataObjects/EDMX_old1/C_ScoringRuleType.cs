//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class C_ScoringRuleType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public C_ScoringRuleType()
        {
            this.C_ScoringRule = new HashSet<C_ScoringRule>();
        }
    
        public int ScoringRuleTypeKey { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<C_ScoringRule> C_ScoringRule { get; set; }
    }
}
