
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/10/2018 10:20:48
-- Generated from EDMX file: C:\Users\David\Documents\Source\Repos\MoAD\MoAD.DataObjects\EDMX\MoAD.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [MoAD];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_FilesExt_FilesExtensionKey_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnswersTemplate] DROP CONSTRAINT [FK_FilesExt_FilesExtensionKey_FK];
GO
IF OBJECT_ID(N'[dbo].[FK__Answers__AnswerT__5812160E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answers] DROP CONSTRAINT [FK__Answers__AnswerT__5812160E];
GO
IF OBJECT_ID(N'[dbo].[FK__Answers__FormKey__571DF1D5]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answers] DROP CONSTRAINT [FK__Answers__FormKey__571DF1D5];
GO
IF OBJECT_ID(N'[dbo].[FK__Answers__Questio__4E88ABD4]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnswersTemplate] DROP CONSTRAINT [FK__Answers__Questio__4E88ABD4];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserRoles_dbo_AspNetRoles_RoleId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo_AspNetUserRoles_dbo_AspNetRoles_RoleId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserRoles_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo_AspNetUserRoles_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_FormElement_FileExtension]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FormElement] DROP CONSTRAINT [FK_FormElement_FileExtension];
GO
IF OBJECT_ID(N'[dbo].[FK_FormElement_FormElementType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FormElement] DROP CONSTRAINT [FK_FormElement_FormElementType];
GO
IF OBJECT_ID(N'[dbo].[FK_Forms_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Forms_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_Forms_FormTemplate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Forms_FormTemplate];
GO
IF OBJECT_ID(N'[dbo].[FK_Forms_Organization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Forms_Organization];
GO
IF OBJECT_ID(N'[dbo].[FK_FormTemplate_FormTemplateType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FormTemplate] DROP CONSTRAINT [FK_FormTemplate_FormTemplateType];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemTrackingStatus_AspNetUsersAssignedTo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemTrackingStatus] DROP CONSTRAINT [FK_ItemTrackingStatus_AspNetUsersAssignedTo];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemTrackingStatus_AspNetUsersUpdatedBy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemTrackingStatus] DROP CONSTRAINT [FK_ItemTrackingStatus_AspNetUsersUpdatedBy];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemTrackingStatus_InvocationType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemTrackingStatus] DROP CONSTRAINT [FK_ItemTrackingStatus_InvocationType];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemTrackingStatus_NextFormTemplateKey]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemTrackingStatus] DROP CONSTRAINT [FK_ItemTrackingStatus_NextFormTemplateKey];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemTrackingStatus_WorkFlowItemKey]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemTrackingStatus] DROP CONSTRAINT [FK_ItemTrackingStatus_WorkFlowItemKey];
GO
IF OBJECT_ID(N'[dbo].[FK_KPI_KPIType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KPI] DROP CONSTRAINT [FK_KPI_KPIType];
GO
IF OBJECT_ID(N'[dbo].[FK_Questions_FormTemplate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Questions] DROP CONSTRAINT [FK_Questions_FormTemplate];
GO
IF OBJECT_ID(N'[dbo].[FK_UserPerOrganisation_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserPerOrganisation] DROP CONSTRAINT [FK_UserPerOrganisation_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_UserPerOrganisation_DelegatedOrganisation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserPerOrganisation] DROP CONSTRAINT [FK_UserPerOrganisation_DelegatedOrganisation];
GO
IF OBJECT_ID(N'[dbo].[FK_UserPerOrganisation_Organisations]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserPerOrganisation] DROP CONSTRAINT [FK_UserPerOrganisation_Organisations];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_CurrentOrganization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_CurrentOrganization];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_ExtendedCurrentRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_ExtendedCurrentRole];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_ExtendedFormTemplate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_ExtendedFormTemplate];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_ExtendedNextRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_ExtendedNextRole];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_NextExtendedFormTemplate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_NextExtendedFormTemplate];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_NextOrganization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_NextOrganization];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_NextStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_NextStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItem_WorkFlow]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItem_WorkFlow];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItemType_CurrentStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItemType_CurrentStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkFlowItemType_KPI]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkFlowItem] DROP CONSTRAINT [FK_WorkFlowItemType_KPI];
GO
IF OBJECT_ID(N'[dbo].[FK_FormElement_FormElementKey_PK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnswersTemplate] DROP CONSTRAINT [FK_FormElement_FormElementKey_PK];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[__MigrationHistory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[__MigrationHistory];
GO
IF OBJECT_ID(N'[dbo].[Answers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Answers];
GO
IF OBJECT_ID(N'[dbo].[AnswersTemplate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AnswersTemplate];
GO
IF OBJECT_ID(N'[dbo].[AspNetRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetRoles];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserClaims]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserClaims];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserLogins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserLogins];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserRoles];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[ExtendedData]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExtendedData];
GO
IF OBJECT_ID(N'[dbo].[FormElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FormElement];
GO
IF OBJECT_ID(N'[dbo].[FormElementType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FormElementType];
GO
IF OBJECT_ID(N'[dbo].[Forms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Forms];
GO
IF OBJECT_ID(N'[dbo].[FormTemplate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FormTemplate];
GO
IF OBJECT_ID(N'[dbo].[FormTemplateType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FormTemplateType];
GO
IF OBJECT_ID(N'[dbo].[InvocationType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InvocationType];
GO
IF OBJECT_ID(N'[dbo].[ItemTrackingStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ItemTrackingStatus];
GO
IF OBJECT_ID(N'[dbo].[KPI]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KPI];
GO
IF OBJECT_ID(N'[dbo].[KPIType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KPIType];
GO
IF OBJECT_ID(N'[dbo].[Log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Log];
GO
IF OBJECT_ID(N'[dbo].[Organisations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisations];
GO
IF OBJECT_ID(N'[dbo].[Questions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Questions];
GO
IF OBJECT_ID(N'[dbo].[Status]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Status];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[UserPerOrganisation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserPerOrganisation];
GO
IF OBJECT_ID(N'[dbo].[WorkFlow]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkFlow];
GO
IF OBJECT_ID(N'[dbo].[WorkFlowItem]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkFlowItem];
GO
IF OBJECT_ID(N'[MoADModelStoreContainer].[AnswersTest]', 'U') IS NOT NULL
    DROP TABLE [MoADModelStoreContainer].[AnswersTest];
GO
IF OBJECT_ID(N'[MoADModelStoreContainer].[FormsTest]', 'U') IS NOT NULL
    DROP TABLE [MoADModelStoreContainer].[FormsTest];
GO
IF OBJECT_ID(N'[MoADModelStoreContainer].[ItemTrackingStatusTest]', 'U') IS NOT NULL
    DROP TABLE [MoADModelStoreContainer].[ItemTrackingStatusTest];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'C__MigrationHistory'
CREATE TABLE [dbo].[C__MigrationHistory] (
    [MigrationId] nvarchar(150)  NOT NULL,
    [ContextKey] nvarchar(300)  NOT NULL,
    [Model] varbinary(max)  NOT NULL,
    [ProductVersion] nvarchar(32)  NOT NULL
);
GO

-- Creating table 'C_ActualScore'
CREATE TABLE [dbo].[C_ActualScore] (
    [ActualScoreKey] int IDENTITY(1,1) NOT NULL,
    [ScoringRuleKey] int  NOT NULL,
    [ScoringRuleType] int  NULL,
    [ScoreValue] float  NULL,
    [ScoreType] int  NULL,
    [LinkedScoringRuleKey] int  NULL,
    [LinkedScoringRuleType] int  NULL,
    [LinkedScoreValue] float  NULL,
    [LinkedScoreType] int  NULL,
    [AnswerTemplateKey] int  NOT NULL,
    [AnswerValue] nvarchar(max)  NULL,
    [QuestionKey] int  NOT NULL,
    [FileExtensionKey] int  NULL,
    [QuestionValue] nvarchar(max)  NULL,
    [FormTemplateKey] int  NULL,
    [AnswerKey] int  NOT NULL,
    [FormKey] int  NOT NULL,
    [FormCreatedAt] nvarchar(50)  NULL,
    [FormUpdatedAt] nvarchar(50)  NULL,
    [UserKey] nvarchar(128)  NULL,
    [OrganisationKey] int  NULL,
    [ActualScore] float  NULL,
    [CreatedAt] datetime  NULL
);
GO

-- Creating table 'C_Condition'
CREATE TABLE [dbo].[C_Condition] (
    [ConditionKey] int IDENTITY(1,1) NOT NULL,
    [ConditionType] nvarchar(50)  NULL,
    [Operator] nchar(10)  NULL,
    [RightOperandValue] nvarchar(50)  NULL,
    [LeftOperandValue] nvarchar(50)  NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'C_ConditionType'
CREATE TABLE [dbo].[C_ConditionType] (
    [ConditionTypeKey] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(50)  NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'C_ScoreType'
CREATE TABLE [dbo].[C_ScoreType] (
    [ScoreTypeKey] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(50)  NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'C_ScoringRule'
CREATE TABLE [dbo].[C_ScoringRule] (
    [ScoringRuleKey] int IDENTITY(1,1) NOT NULL,
    [ScoringRuleType] int  NULL,
    [ConditionKey] int  NULL,
    [BindingKey] int  NULL,
    [BindingQuery] nvarchar(500)  NULL,
    [LinkedKey] int  NULL,
    [InvocationType] int  NULL,
    [ScoreValue] float  NULL,
    [ScoreType] int  NULL
);
GO

-- Creating table 'C_ScoringRuleType'
CREATE TABLE [dbo].[C_ScoringRuleType] (
    [ScoringRuleTypeKey] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(50)  NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'Answers'
CREATE TABLE [dbo].[Answers] (
    [AnswerKey] int IDENTITY(1,1) NOT NULL,
    [FormKey] int  NULL,
    [AnswerTemplateKey] int  NULL,
    [FileExtensionKey] int  NULL
);
GO

-- Creating table 'AnswersTemplates'
CREATE TABLE [dbo].[AnswersTemplates] (
    [AnswerTemplateKey] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NULL,
    [FormElementKey] int  NULL,
    [QuestionKey] int  NULL,
    [AnswerTemplateKeyIndex] int  NULL,
    [FileExtensionKey] int  NULL,
    [ScoreKey] nvarchar(50)  NULL
);
GO

-- Creating table 'AspNetRoles'
CREATE TABLE [dbo].[AspNetRoles] (
    [Id] nvarchar(128)  NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'AspNetUserClaims'
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [ClaimType] nvarchar(max)  NULL,
    [ClaimValue] nvarchar(max)  NULL
);
GO

-- Creating table 'AspNetUserLogins'
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] nvarchar(128)  NOT NULL,
    [ProviderKey] nvarchar(128)  NOT NULL,
    [UserId] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(128)  NOT NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL,
    [FirstName] nvarchar(20)  NULL,
    [FatherName] nvarchar(20)  NULL,
    [LastName] nvarchar(20)  NULL,
    [SSN] nvarchar(100)  NULL
);
GO

-- Creating table 'ExtendedDatas'
CREATE TABLE [dbo].[ExtendedDatas] (
    [FileExtensionKey] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NULL,
    [Type] nvarchar(50)  NULL,
    [Size] nvarchar(50)  NULL,
    [CreatedAt] nvarchar(50)  NULL,
    [UpdatedAt] nvarchar(50)  NULL,
    [FilePathOrSource] nvarchar(150)  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [HTML] nvarchar(500)  NULL,
    [HTMLCode] nvarchar(max)  NULL
);
GO

-- Creating table 'ExternalFactors'
CREATE TABLE [dbo].[ExternalFactors] (
    [ExternalFactorKey] int IDENTITY(1,1) NOT NULL,
    [ExternalFactorTypeKey] int  NULL,
    [ExternalFactorValue] nvarchar(50)  NULL,
    [Weighing] nvarchar(50)  NULL,
    [ScoringInstanceKey] int  NULL
);
GO

-- Creating table 'ExternalFactorTypes'
CREATE TABLE [dbo].[ExternalFactorTypes] (
    [ExternalFactorTypeKey] int IDENTITY(1,1) NOT NULL,
    [ExternalFactorType1] nvarchar(50)  NULL,
    [ExternalFactorParentType] nvarchar(50)  NULL
);
GO

-- Creating table 'FormElements'
CREATE TABLE [dbo].[FormElements] (
    [FormElementKey] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NULL,
    [FileExtensionKey] int  NULL,
    [FormElementTypeKey] int  NULL,
    [FormElementTypeKeyDependancy] nvarchar(10)  NULL
);
GO

-- Creating table 'FormElementTypes'
CREATE TABLE [dbo].[FormElementTypes] (
    [Name] nvarchar(100)  NULL,
    [FileExtensionKey] int  NULL,
    [FormElementTypeKey] int  NOT NULL
);
GO

-- Creating table 'Forms'
CREATE TABLE [dbo].[Forms] (
    [FormKey] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(100)  NULL,
    [CreatedAt] nvarchar(50)  NULL,
    [UpdatedAt] nvarchar(50)  NULL,
    [TrackingStatusKey] nvarchar(50)  NULL,
    [FormTemplateKey] int  NULL,
    [UserKey] nvarchar(128)  NULL,
    [OrganisationKey] int  NULL
);
GO

-- Creating table 'FormTemplates'
CREATE TABLE [dbo].[FormTemplates] (
    [FormTemplateKey] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NULL,
    [Description] nvarchar(max)  NULL,
    [FormTemplateType] nvarchar(50)  NULL,
    [Status] nvarchar(50)  NULL,
    [CreatedAt] nvarchar(50)  NULL
);
GO

-- Creating table 'FormTemplateTypes'
CREATE TABLE [dbo].[FormTemplateTypes] (
    [FormTemplateType1] nvarchar(50)  NOT NULL,
    [ParentFormTemplateType] nvarchar(50)  NULL,
    [Level] nvarchar(50)  NULL
);
GO

-- Creating table 'InvocationTypes'
CREATE TABLE [dbo].[InvocationTypes] (
    [InvocationTypeKey] int IDENTITY(1,1) NOT NULL,
    [InvocationType1] nvarchar(50)  NULL
);
GO

-- Creating table 'ItemTrackingStatus'
CREATE TABLE [dbo].[ItemTrackingStatus] (
    [ItemTrackingStatusKey] int IDENTITY(1,1) NOT NULL,
    [WorkFlowItemKey] int  NULL,
    [UpdatedByUserKey] nvarchar(128)  NULL,
    [AssignedToUserKey] nvarchar(128)  NULL,
    [CreatedAt] datetime  NULL,
    [UpdatedAt] datetime  NULL,
    [CurrentFormKey] int  NULL,
    [NextFormTemplateKey] int  NULL,
    [GUID] uniqueidentifier  NULL,
    [InvocationTypeKey] int  NULL,
    [Note] nvarchar(500)  NULL
);
GO

-- Creating table 'KPIs'
CREATE TABLE [dbo].[KPIs] (
    [KPIKey] int IDENTITY(1,1) NOT NULL,
    [KPITypeKey] int  NULL,
    [KPIValue] nvarchar(50)  NULL
);
GO

-- Creating table 'KPITypes'
CREATE TABLE [dbo].[KPITypes] (
    [KPITypeKey] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(50)  NULL
);
GO

-- Creating table 'Logs'
CREATE TABLE [dbo].[Logs] (
    [Identifier] int IDENTITY(1,1) NOT NULL,
    [DateTime] datetime  NULL,
    [Source] nvarchar(250)  NULL,
    [Title] nvarchar(1000)  NULL,
    [Message] varchar(max)  NULL
);
GO

-- Creating table 'Organisations'
CREATE TABLE [dbo].[Organisations] (
    [OrganisationKey] int IDENTITY(1,1) NOT NULL,
    [OrganisationName] nvarchar(200)  NULL,
    [OrganisationParent] nvarchar(200)  NULL,
    [Level] int  NULL
);
GO

-- Creating table 'Questions'
CREATE TABLE [dbo].[Questions] (
    [QuestionKey] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DisplayMode] nvarchar(50)  NULL,
    [FormTemplateKey] int  NULL,
    [QuestionKeyIndex] int  NULL,
    [LinkedQuestion] int  NULL
);
GO

-- Creating table 'Scorings'
CREATE TABLE [dbo].[Scorings] (
    [ScoreId] int  NOT NULL,
    [ClaimGUID] uniqueidentifier  NULL,
    [Score] float  NULL
);
GO

-- Creating table 'ScoringInstances'
CREATE TABLE [dbo].[ScoringInstances] (
    [ScoringInstanceKey] int IDENTITY(1,1) NOT NULL,
    [KPIKey] int  NULL,
    [Date] datetime  NULL,
    [ItemTrackingStatusKey] int  NULL,
    [Score] nvarchar(50)  NULL,
    [ScoreStatus] nvarchar(50)  NULL,
    [ScoringRulesKey] int  NULL
);
GO

-- Creating table 'ScoringRules'
CREATE TABLE [dbo].[ScoringRules] (
    [ScoringRulesKey] int IDENTITY(1,1) NOT NULL,
    [KPIKey] int  NULL,
    [ScoringGUID] uniqueidentifier  NULL,
    [Score] nvarchar(50)  NULL
);
GO

-- Creating table 'Status'
CREATE TABLE [dbo].[Status] (
    [StatusKey] int IDENTITY(1,1) NOT NULL,
    [StatusValue] nvarchar(50)  NULL,
    [ParentStatus] nvarchar(50)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'UserPerOrganisations'
CREATE TABLE [dbo].[UserPerOrganisations] (
    [UserPerOrganisationKey] int IDENTITY(1,1) NOT NULL,
    [UserKey] nvarchar(128)  NOT NULL,
    [OrganisationKey] int  NOT NULL,
    [DelegatedOrganizationKey] int  NULL,
    [UpdateDate] nvarchar(50)  NULL
);
GO

-- Creating table 'WorkFlows'
CREATE TABLE [dbo].[WorkFlows] (
    [WorkFlowKey] int IDENTITY(1,1) NOT NULL,
    [WorkFlowName] nvarchar(50)  NULL,
    [WorkFlowType] nvarchar(50)  NULL,
    [WorkFlowDescription] nvarchar(max)  NULL
);
GO

-- Creating table 'WorkFlowItems'
CREATE TABLE [dbo].[WorkFlowItems] (
    [WorkFlowItemKey] int IDENTITY(1,1) NOT NULL,
    [ExtendedFormTemplateKey] int  NULL,
    [ExtendedCurrentRoleKey] nvarchar(128)  NULL,
    [ExtendedNextRoleKey] nvarchar(128)  NULL,
    [CurrentStatusKey] int  NULL,
    [NextStatusKey] int  NULL,
    [KPIKey] int  NULL,
    [NextExtendedFormTemplateKey] int  NULL,
    [WorkFlowItemIndex] int  NULL,
    [CurrentExtendedOrganisationKey] int  NULL,
    [NextExtendedOrganisationKey] int  NULL,
    [WorkFlowKey] int  NULL
);
GO

-- Creating table 'AnswersTests'
CREATE TABLE [dbo].[AnswersTests] (
    [AnswerTemplateKey] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NULL,
    [FormElementKey] int  NULL,
    [ScoreKey] nvarchar(50)  NULL,
    [QuestionKey] int  NULL,
    [AnswerTemplateKeyIndex] int  NULL,
    [FileExtensionKey] int  NULL
);
GO

-- Creating table 'FormsTests'
CREATE TABLE [dbo].[FormsTests] (
    [FormKey] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(100)  NULL,
    [CreatedAt] nvarchar(50)  NULL,
    [UpdatedAt] nvarchar(50)  NULL,
    [TrackingStatusKey] nvarchar(50)  NULL,
    [FormTemplateKey] int  NULL,
    [UserKey] nvarchar(128)  NULL,
    [OrganisationKey] int  NULL
);
GO

-- Creating table 'ItemTrackingStatusTests'
CREATE TABLE [dbo].[ItemTrackingStatusTests] (
    [ItemTrackingStatusKey] int  NOT NULL,
    [WorkFlowItemKey] int  NULL,
    [UpdatedByUserKey] nvarchar(128)  NULL,
    [AssignedToUserKey] nvarchar(128)  NULL,
    [CreatedAt] nvarchar(100)  NULL,
    [UpdatedAt] nvarchar(100)  NULL,
    [InvokationTypeKey] int  NULL,
    [CurrentFormKey] int  NULL,
    [NextFormKey] int  NULL,
    [GeoID] int  NULL
);
GO

-- Creating table 'AspNetUserRoles'
CREATE TABLE [dbo].[AspNetUserRoles] (
    [AspNetRoles_Id] nvarchar(128)  NOT NULL,
    [AspNetUsers_Id] nvarchar(128)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [MigrationId], [ContextKey] in table 'C__MigrationHistory'
ALTER TABLE [dbo].[C__MigrationHistory]
ADD CONSTRAINT [PK_C__MigrationHistory]
    PRIMARY KEY CLUSTERED ([MigrationId], [ContextKey] ASC);
GO

-- Creating primary key on [ActualScoreKey] in table 'C_ActualScore'
ALTER TABLE [dbo].[C_ActualScore]
ADD CONSTRAINT [PK_C_ActualScore]
    PRIMARY KEY CLUSTERED ([ActualScoreKey] ASC);
GO

-- Creating primary key on [ConditionKey] in table 'C_Condition'
ALTER TABLE [dbo].[C_Condition]
ADD CONSTRAINT [PK_C_Condition]
    PRIMARY KEY CLUSTERED ([ConditionKey] ASC);
GO

-- Creating primary key on [ConditionTypeKey] in table 'C_ConditionType'
ALTER TABLE [dbo].[C_ConditionType]
ADD CONSTRAINT [PK_C_ConditionType]
    PRIMARY KEY CLUSTERED ([ConditionTypeKey] ASC);
GO

-- Creating primary key on [ScoreTypeKey] in table 'C_ScoreType'
ALTER TABLE [dbo].[C_ScoreType]
ADD CONSTRAINT [PK_C_ScoreType]
    PRIMARY KEY CLUSTERED ([ScoreTypeKey] ASC);
GO

-- Creating primary key on [ScoringRuleKey] in table 'C_ScoringRule'
ALTER TABLE [dbo].[C_ScoringRule]
ADD CONSTRAINT [PK_C_ScoringRule]
    PRIMARY KEY CLUSTERED ([ScoringRuleKey] ASC);
GO

-- Creating primary key on [ScoringRuleTypeKey] in table 'C_ScoringRuleType'
ALTER TABLE [dbo].[C_ScoringRuleType]
ADD CONSTRAINT [PK_C_ScoringRuleType]
    PRIMARY KEY CLUSTERED ([ScoringRuleTypeKey] ASC);
GO

-- Creating primary key on [AnswerKey] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [PK_Answers]
    PRIMARY KEY CLUSTERED ([AnswerKey] ASC);
GO

-- Creating primary key on [AnswerTemplateKey] in table 'AnswersTemplates'
ALTER TABLE [dbo].[AnswersTemplates]
ADD CONSTRAINT [PK_AnswersTemplates]
    PRIMARY KEY CLUSTERED ([AnswerTemplateKey] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetRoles'
ALTER TABLE [dbo].[AspNetRoles]
ADD CONSTRAINT [PK_AspNetRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [PK_AspNetUserClaims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LoginProvider], [ProviderKey], [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [PK_AspNetUserLogins]
    PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey], [UserId] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [FileExtensionKey] in table 'ExtendedDatas'
ALTER TABLE [dbo].[ExtendedDatas]
ADD CONSTRAINT [PK_ExtendedDatas]
    PRIMARY KEY CLUSTERED ([FileExtensionKey] ASC);
GO

-- Creating primary key on [ExternalFactorKey] in table 'ExternalFactors'
ALTER TABLE [dbo].[ExternalFactors]
ADD CONSTRAINT [PK_ExternalFactors]
    PRIMARY KEY CLUSTERED ([ExternalFactorKey] ASC);
GO

-- Creating primary key on [ExternalFactorTypeKey] in table 'ExternalFactorTypes'
ALTER TABLE [dbo].[ExternalFactorTypes]
ADD CONSTRAINT [PK_ExternalFactorTypes]
    PRIMARY KEY CLUSTERED ([ExternalFactorTypeKey] ASC);
GO

-- Creating primary key on [FormElementKey] in table 'FormElements'
ALTER TABLE [dbo].[FormElements]
ADD CONSTRAINT [PK_FormElements]
    PRIMARY KEY CLUSTERED ([FormElementKey] ASC);
GO

-- Creating primary key on [FormElementTypeKey] in table 'FormElementTypes'
ALTER TABLE [dbo].[FormElementTypes]
ADD CONSTRAINT [PK_FormElementTypes]
    PRIMARY KEY CLUSTERED ([FormElementTypeKey] ASC);
GO

-- Creating primary key on [FormKey] in table 'Forms'
ALTER TABLE [dbo].[Forms]
ADD CONSTRAINT [PK_Forms]
    PRIMARY KEY CLUSTERED ([FormKey] ASC);
GO

-- Creating primary key on [FormTemplateKey] in table 'FormTemplates'
ALTER TABLE [dbo].[FormTemplates]
ADD CONSTRAINT [PK_FormTemplates]
    PRIMARY KEY CLUSTERED ([FormTemplateKey] ASC);
GO

-- Creating primary key on [FormTemplateType1] in table 'FormTemplateTypes'
ALTER TABLE [dbo].[FormTemplateTypes]
ADD CONSTRAINT [PK_FormTemplateTypes]
    PRIMARY KEY CLUSTERED ([FormTemplateType1] ASC);
GO

-- Creating primary key on [InvocationTypeKey] in table 'InvocationTypes'
ALTER TABLE [dbo].[InvocationTypes]
ADD CONSTRAINT [PK_InvocationTypes]
    PRIMARY KEY CLUSTERED ([InvocationTypeKey] ASC);
GO

-- Creating primary key on [ItemTrackingStatusKey] in table 'ItemTrackingStatus'
ALTER TABLE [dbo].[ItemTrackingStatus]
ADD CONSTRAINT [PK_ItemTrackingStatus]
    PRIMARY KEY CLUSTERED ([ItemTrackingStatusKey] ASC);
GO

-- Creating primary key on [KPIKey] in table 'KPIs'
ALTER TABLE [dbo].[KPIs]
ADD CONSTRAINT [PK_KPIs]
    PRIMARY KEY CLUSTERED ([KPIKey] ASC);
GO

-- Creating primary key on [KPITypeKey] in table 'KPITypes'
ALTER TABLE [dbo].[KPITypes]
ADD CONSTRAINT [PK_KPITypes]
    PRIMARY KEY CLUSTERED ([KPITypeKey] ASC);
GO

-- Creating primary key on [Identifier] in table 'Logs'
ALTER TABLE [dbo].[Logs]
ADD CONSTRAINT [PK_Logs]
    PRIMARY KEY CLUSTERED ([Identifier] ASC);
GO

-- Creating primary key on [OrganisationKey] in table 'Organisations'
ALTER TABLE [dbo].[Organisations]
ADD CONSTRAINT [PK_Organisations]
    PRIMARY KEY CLUSTERED ([OrganisationKey] ASC);
GO

-- Creating primary key on [QuestionKey] in table 'Questions'
ALTER TABLE [dbo].[Questions]
ADD CONSTRAINT [PK_Questions]
    PRIMARY KEY CLUSTERED ([QuestionKey] ASC);
GO

-- Creating primary key on [ScoreId] in table 'Scorings'
ALTER TABLE [dbo].[Scorings]
ADD CONSTRAINT [PK_Scorings]
    PRIMARY KEY CLUSTERED ([ScoreId] ASC);
GO

-- Creating primary key on [ScoringInstanceKey] in table 'ScoringInstances'
ALTER TABLE [dbo].[ScoringInstances]
ADD CONSTRAINT [PK_ScoringInstances]
    PRIMARY KEY CLUSTERED ([ScoringInstanceKey] ASC);
GO

-- Creating primary key on [ScoringRulesKey] in table 'ScoringRules'
ALTER TABLE [dbo].[ScoringRules]
ADD CONSTRAINT [PK_ScoringRules]
    PRIMARY KEY CLUSTERED ([ScoringRulesKey] ASC);
GO

-- Creating primary key on [StatusKey] in table 'Status'
ALTER TABLE [dbo].[Status]
ADD CONSTRAINT [PK_Status]
    PRIMARY KEY CLUSTERED ([StatusKey] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [UserPerOrganisationKey] in table 'UserPerOrganisations'
ALTER TABLE [dbo].[UserPerOrganisations]
ADD CONSTRAINT [PK_UserPerOrganisations]
    PRIMARY KEY CLUSTERED ([UserPerOrganisationKey] ASC);
GO

-- Creating primary key on [WorkFlowKey] in table 'WorkFlows'
ALTER TABLE [dbo].[WorkFlows]
ADD CONSTRAINT [PK_WorkFlows]
    PRIMARY KEY CLUSTERED ([WorkFlowKey] ASC);
GO

-- Creating primary key on [WorkFlowItemKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [PK_WorkFlowItems]
    PRIMARY KEY CLUSTERED ([WorkFlowItemKey] ASC);
GO

-- Creating primary key on [AnswerTemplateKey] in table 'AnswersTests'
ALTER TABLE [dbo].[AnswersTests]
ADD CONSTRAINT [PK_AnswersTests]
    PRIMARY KEY CLUSTERED ([AnswerTemplateKey] ASC);
GO

-- Creating primary key on [FormKey] in table 'FormsTests'
ALTER TABLE [dbo].[FormsTests]
ADD CONSTRAINT [PK_FormsTests]
    PRIMARY KEY CLUSTERED ([FormKey] ASC);
GO

-- Creating primary key on [ItemTrackingStatusKey] in table 'ItemTrackingStatusTests'
ALTER TABLE [dbo].[ItemTrackingStatusTests]
ADD CONSTRAINT [PK_ItemTrackingStatusTests]
    PRIMARY KEY CLUSTERED ([ItemTrackingStatusKey] ASC);
GO

-- Creating primary key on [AspNetRoles_Id], [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [PK_AspNetUserRoles]
    PRIMARY KEY CLUSTERED ([AspNetRoles_Id], [AspNetUsers_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ConditionKey] in table 'C_ScoringRule'
ALTER TABLE [dbo].[C_ScoringRule]
ADD CONSTRAINT [FK__ScoringRule__Condition]
    FOREIGN KEY ([ConditionKey])
    REFERENCES [dbo].[C_Condition]
        ([ConditionKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ScoringRule__Condition'
CREATE INDEX [IX_FK__ScoringRule__Condition]
ON [dbo].[C_ScoringRule]
    ([ConditionKey]);
GO

-- Creating foreign key on [ScoreType] in table 'C_ScoringRule'
ALTER TABLE [dbo].[C_ScoringRule]
ADD CONSTRAINT [FK__ScoringRule__ScoreType]
    FOREIGN KEY ([ScoreType])
    REFERENCES [dbo].[C_ScoreType]
        ([ScoreTypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ScoringRule__ScoreType'
CREATE INDEX [IX_FK__ScoringRule__ScoreType]
ON [dbo].[C_ScoringRule]
    ([ScoreType]);
GO

-- Creating foreign key on [ScoringRuleType] in table 'C_ScoringRule'
ALTER TABLE [dbo].[C_ScoringRule]
ADD CONSTRAINT [FK__ScoringRule__ScoringRuleType]
    FOREIGN KEY ([ScoringRuleType])
    REFERENCES [dbo].[C_ScoringRuleType]
        ([ScoringRuleTypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ScoringRule__ScoringRuleType'
CREATE INDEX [IX_FK__ScoringRule__ScoringRuleType]
ON [dbo].[C_ScoringRule]
    ([ScoringRuleType]);
GO

-- Creating foreign key on [InvocationType] in table 'C_ScoringRule'
ALTER TABLE [dbo].[C_ScoringRule]
ADD CONSTRAINT [FK__ScoringRule_InvocationType]
    FOREIGN KEY ([InvocationType])
    REFERENCES [dbo].[InvocationTypes]
        ([InvocationTypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ScoringRule_InvocationType'
CREATE INDEX [IX_FK__ScoringRule_InvocationType]
ON [dbo].[C_ScoringRule]
    ([InvocationType]);
GO

-- Creating foreign key on [AnswerTemplateKey] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [FK__Answers__AnswerT__5812160E]
    FOREIGN KEY ([AnswerTemplateKey])
    REFERENCES [dbo].[AnswersTemplates]
        ([AnswerTemplateKey])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Answers__AnswerT__5812160E'
CREATE INDEX [IX_FK__Answers__AnswerT__5812160E]
ON [dbo].[Answers]
    ([AnswerTemplateKey]);
GO

-- Creating foreign key on [FormKey] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [FK__Answers__FormKey__571DF1D5]
    FOREIGN KEY ([FormKey])
    REFERENCES [dbo].[Forms]
        ([FormKey])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Answers__FormKey__571DF1D5'
CREATE INDEX [IX_FK__Answers__FormKey__571DF1D5]
ON [dbo].[Answers]
    ([FormKey]);
GO

-- Creating foreign key on [FileExtensionKey] in table 'AnswersTemplates'
ALTER TABLE [dbo].[AnswersTemplates]
ADD CONSTRAINT [FK_FilesExt_FilesExtensionKey_FK]
    FOREIGN KEY ([FileExtensionKey])
    REFERENCES [dbo].[ExtendedDatas]
        ([FileExtensionKey])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FilesExt_FilesExtensionKey_FK'
CREATE INDEX [IX_FK_FilesExt_FilesExtensionKey_FK]
ON [dbo].[AnswersTemplates]
    ([FileExtensionKey]);
GO

-- Creating foreign key on [QuestionKey] in table 'AnswersTemplates'
ALTER TABLE [dbo].[AnswersTemplates]
ADD CONSTRAINT [FK__Answers__Questio__4E88ABD4]
    FOREIGN KEY ([QuestionKey])
    REFERENCES [dbo].[Questions]
        ([QuestionKey])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Answers__Questio__4E88ABD4'
CREATE INDEX [IX_FK__Answers__Questio__4E88ABD4]
ON [dbo].[AnswersTemplates]
    ([QuestionKey]);
GO

-- Creating foreign key on [FormElementKey] in table 'AnswersTemplates'
ALTER TABLE [dbo].[AnswersTemplates]
ADD CONSTRAINT [FK_FormElement_FormElementKey_PK]
    FOREIGN KEY ([FormElementKey])
    REFERENCES [dbo].[FormElements]
        ([FormElementKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FormElement_FormElementKey_PK'
CREATE INDEX [IX_FK_FormElement_FormElementKey_PK]
ON [dbo].[AnswersTemplates]
    ([FormElementKey]);
GO

-- Creating foreign key on [ExtendedCurrentRoleKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_ExtendedCurrentRole]
    FOREIGN KEY ([ExtendedCurrentRoleKey])
    REFERENCES [dbo].[AspNetRoles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_ExtendedCurrentRole'
CREATE INDEX [IX_FK_WorkFlowItem_ExtendedCurrentRole]
ON [dbo].[WorkFlowItems]
    ([ExtendedCurrentRoleKey]);
GO

-- Creating foreign key on [ExtendedNextRoleKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_ExtendedNextRole]
    FOREIGN KEY ([ExtendedNextRoleKey])
    REFERENCES [dbo].[AspNetRoles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_ExtendedNextRole'
CREATE INDEX [IX_FK_WorkFlowItem_ExtendedNextRole]
ON [dbo].[WorkFlowItems]
    ([ExtendedNextRoleKey]);
GO

-- Creating foreign key on [UserId] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserClaims]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserLogins]
    ([UserId]);
GO

-- Creating foreign key on [UserKey] in table 'Forms'
ALTER TABLE [dbo].[Forms]
ADD CONSTRAINT [FK_Forms_AspNetUsers]
    FOREIGN KEY ([UserKey])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Forms_AspNetUsers'
CREATE INDEX [IX_FK_Forms_AspNetUsers]
ON [dbo].[Forms]
    ([UserKey]);
GO

-- Creating foreign key on [AssignedToUserKey] in table 'ItemTrackingStatus'
ALTER TABLE [dbo].[ItemTrackingStatus]
ADD CONSTRAINT [FK_ItemTrackingStatus_AspNetUsersAssignedTo]
    FOREIGN KEY ([AssignedToUserKey])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemTrackingStatus_AspNetUsersAssignedTo'
CREATE INDEX [IX_FK_ItemTrackingStatus_AspNetUsersAssignedTo]
ON [dbo].[ItemTrackingStatus]
    ([AssignedToUserKey]);
GO

-- Creating foreign key on [UpdatedByUserKey] in table 'ItemTrackingStatus'
ALTER TABLE [dbo].[ItemTrackingStatus]
ADD CONSTRAINT [FK_ItemTrackingStatus_AspNetUsersUpdatedBy]
    FOREIGN KEY ([UpdatedByUserKey])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemTrackingStatus_AspNetUsersUpdatedBy'
CREATE INDEX [IX_FK_ItemTrackingStatus_AspNetUsersUpdatedBy]
ON [dbo].[ItemTrackingStatus]
    ([UpdatedByUserKey]);
GO

-- Creating foreign key on [UserKey] in table 'UserPerOrganisations'
ALTER TABLE [dbo].[UserPerOrganisations]
ADD CONSTRAINT [FK_UserPerOrganisation_AspNetUsers]
    FOREIGN KEY ([UserKey])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPerOrganisation_AspNetUsers'
CREATE INDEX [IX_FK_UserPerOrganisation_AspNetUsers]
ON [dbo].[UserPerOrganisations]
    ([UserKey]);
GO

-- Creating foreign key on [FileExtensionKey] in table 'FormElements'
ALTER TABLE [dbo].[FormElements]
ADD CONSTRAINT [FK_FormElement_FileExtension]
    FOREIGN KEY ([FileExtensionKey])
    REFERENCES [dbo].[ExtendedDatas]
        ([FileExtensionKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FormElement_FileExtension'
CREATE INDEX [IX_FK_FormElement_FileExtension]
ON [dbo].[FormElements]
    ([FileExtensionKey]);
GO

-- Creating foreign key on [ExternalFactorTypeKey] in table 'ExternalFactors'
ALTER TABLE [dbo].[ExternalFactors]
ADD CONSTRAINT [FK_ExternalFactor_ExternalFactorType]
    FOREIGN KEY ([ExternalFactorTypeKey])
    REFERENCES [dbo].[ExternalFactorTypes]
        ([ExternalFactorTypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalFactor_ExternalFactorType'
CREATE INDEX [IX_FK_ExternalFactor_ExternalFactorType]
ON [dbo].[ExternalFactors]
    ([ExternalFactorTypeKey]);
GO

-- Creating foreign key on [ScoringInstanceKey] in table 'ExternalFactors'
ALTER TABLE [dbo].[ExternalFactors]
ADD CONSTRAINT [FK_ExternalFactor_ScoringInstance]
    FOREIGN KEY ([ScoringInstanceKey])
    REFERENCES [dbo].[ScoringInstances]
        ([ScoringInstanceKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ExternalFactor_ScoringInstance'
CREATE INDEX [IX_FK_ExternalFactor_ScoringInstance]
ON [dbo].[ExternalFactors]
    ([ScoringInstanceKey]);
GO

-- Creating foreign key on [FormElementTypeKey] in table 'FormElements'
ALTER TABLE [dbo].[FormElements]
ADD CONSTRAINT [FK_FormElement_FormElementType]
    FOREIGN KEY ([FormElementTypeKey])
    REFERENCES [dbo].[FormElementTypes]
        ([FormElementTypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FormElement_FormElementType'
CREATE INDEX [IX_FK_FormElement_FormElementType]
ON [dbo].[FormElements]
    ([FormElementTypeKey]);
GO

-- Creating foreign key on [FormTemplateKey] in table 'Forms'
ALTER TABLE [dbo].[Forms]
ADD CONSTRAINT [FK_Forms_FormTemplate]
    FOREIGN KEY ([FormTemplateKey])
    REFERENCES [dbo].[FormTemplates]
        ([FormTemplateKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Forms_FormTemplate'
CREATE INDEX [IX_FK_Forms_FormTemplate]
ON [dbo].[Forms]
    ([FormTemplateKey]);
GO

-- Creating foreign key on [OrganisationKey] in table 'Forms'
ALTER TABLE [dbo].[Forms]
ADD CONSTRAINT [FK_Forms_Organization]
    FOREIGN KEY ([OrganisationKey])
    REFERENCES [dbo].[Organisations]
        ([OrganisationKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Forms_Organization'
CREATE INDEX [IX_FK_Forms_Organization]
ON [dbo].[Forms]
    ([OrganisationKey]);
GO

-- Creating foreign key on [FormTemplateType] in table 'FormTemplates'
ALTER TABLE [dbo].[FormTemplates]
ADD CONSTRAINT [FK_FormTemplate_FormTemplateType]
    FOREIGN KEY ([FormTemplateType])
    REFERENCES [dbo].[FormTemplateTypes]
        ([FormTemplateType1])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FormTemplate_FormTemplateType'
CREATE INDEX [IX_FK_FormTemplate_FormTemplateType]
ON [dbo].[FormTemplates]
    ([FormTemplateType]);
GO

-- Creating foreign key on [NextFormTemplateKey] in table 'ItemTrackingStatus'
ALTER TABLE [dbo].[ItemTrackingStatus]
ADD CONSTRAINT [FK_ItemTrackingStatus_NextFormTemplateKey]
    FOREIGN KEY ([NextFormTemplateKey])
    REFERENCES [dbo].[FormTemplates]
        ([FormTemplateKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemTrackingStatus_NextFormTemplateKey'
CREATE INDEX [IX_FK_ItemTrackingStatus_NextFormTemplateKey]
ON [dbo].[ItemTrackingStatus]
    ([NextFormTemplateKey]);
GO

-- Creating foreign key on [FormTemplateKey] in table 'Questions'
ALTER TABLE [dbo].[Questions]
ADD CONSTRAINT [FK_Questions_FormTemplate]
    FOREIGN KEY ([FormTemplateKey])
    REFERENCES [dbo].[FormTemplates]
        ([FormTemplateKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Questions_FormTemplate'
CREATE INDEX [IX_FK_Questions_FormTemplate]
ON [dbo].[Questions]
    ([FormTemplateKey]);
GO

-- Creating foreign key on [ExtendedFormTemplateKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_ExtendedFormTemplate]
    FOREIGN KEY ([ExtendedFormTemplateKey])
    REFERENCES [dbo].[FormTemplates]
        ([FormTemplateKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_ExtendedFormTemplate'
CREATE INDEX [IX_FK_WorkFlowItem_ExtendedFormTemplate]
ON [dbo].[WorkFlowItems]
    ([ExtendedFormTemplateKey]);
GO

-- Creating foreign key on [NextExtendedFormTemplateKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_NextExtendedFormTemplate]
    FOREIGN KEY ([NextExtendedFormTemplateKey])
    REFERENCES [dbo].[FormTemplates]
        ([FormTemplateKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_NextExtendedFormTemplate'
CREATE INDEX [IX_FK_WorkFlowItem_NextExtendedFormTemplate]
ON [dbo].[WorkFlowItems]
    ([NextExtendedFormTemplateKey]);
GO

-- Creating foreign key on [InvocationTypeKey] in table 'ItemTrackingStatus'
ALTER TABLE [dbo].[ItemTrackingStatus]
ADD CONSTRAINT [FK_ItemTrackingStatus_InvocationType]
    FOREIGN KEY ([InvocationTypeKey])
    REFERENCES [dbo].[InvocationTypes]
        ([InvocationTypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemTrackingStatus_InvocationType'
CREATE INDEX [IX_FK_ItemTrackingStatus_InvocationType]
ON [dbo].[ItemTrackingStatus]
    ([InvocationTypeKey]);
GO

-- Creating foreign key on [WorkFlowItemKey] in table 'ItemTrackingStatus'
ALTER TABLE [dbo].[ItemTrackingStatus]
ADD CONSTRAINT [FK_ItemTrackingStatus_WorkFlowItemKey]
    FOREIGN KEY ([WorkFlowItemKey])
    REFERENCES [dbo].[WorkFlowItems]
        ([WorkFlowItemKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemTrackingStatus_WorkFlowItemKey'
CREATE INDEX [IX_FK_ItemTrackingStatus_WorkFlowItemKey]
ON [dbo].[ItemTrackingStatus]
    ([WorkFlowItemKey]);
GO

-- Creating foreign key on [ItemTrackingStatusKey] in table 'ScoringInstances'
ALTER TABLE [dbo].[ScoringInstances]
ADD CONSTRAINT [FK_ScoringInstance_ItemTrackingstatus]
    FOREIGN KEY ([ItemTrackingStatusKey])
    REFERENCES [dbo].[ItemTrackingStatus]
        ([ItemTrackingStatusKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScoringInstance_ItemTrackingstatus'
CREATE INDEX [IX_FK_ScoringInstance_ItemTrackingstatus]
ON [dbo].[ScoringInstances]
    ([ItemTrackingStatusKey]);
GO

-- Creating foreign key on [KPITypeKey] in table 'KPIs'
ALTER TABLE [dbo].[KPIs]
ADD CONSTRAINT [FK_KPI_KPIType]
    FOREIGN KEY ([KPITypeKey])
    REFERENCES [dbo].[KPITypes]
        ([KPITypeKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_KPI_KPIType'
CREATE INDEX [IX_FK_KPI_KPIType]
ON [dbo].[KPIs]
    ([KPITypeKey]);
GO

-- Creating foreign key on [KPIKey] in table 'ScoringInstances'
ALTER TABLE [dbo].[ScoringInstances]
ADD CONSTRAINT [FK_ScoringInstance_KPI]
    FOREIGN KEY ([KPIKey])
    REFERENCES [dbo].[KPIs]
        ([KPIKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScoringInstance_KPI'
CREATE INDEX [IX_FK_ScoringInstance_KPI]
ON [dbo].[ScoringInstances]
    ([KPIKey]);
GO

-- Creating foreign key on [KPIKey] in table 'ScoringRules'
ALTER TABLE [dbo].[ScoringRules]
ADD CONSTRAINT [FK_ScoringRules_KPI]
    FOREIGN KEY ([KPIKey])
    REFERENCES [dbo].[KPIs]
        ([KPIKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScoringRules_KPI'
CREATE INDEX [IX_FK_ScoringRules_KPI]
ON [dbo].[ScoringRules]
    ([KPIKey]);
GO

-- Creating foreign key on [KPIKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItemType_KPI]
    FOREIGN KEY ([KPIKey])
    REFERENCES [dbo].[KPIs]
        ([KPIKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItemType_KPI'
CREATE INDEX [IX_FK_WorkFlowItemType_KPI]
ON [dbo].[WorkFlowItems]
    ([KPIKey]);
GO

-- Creating foreign key on [DelegatedOrganizationKey] in table 'UserPerOrganisations'
ALTER TABLE [dbo].[UserPerOrganisations]
ADD CONSTRAINT [FK_UserPerOrganisation_DelegatedOrganisation]
    FOREIGN KEY ([DelegatedOrganizationKey])
    REFERENCES [dbo].[Organisations]
        ([OrganisationKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPerOrganisation_DelegatedOrganisation'
CREATE INDEX [IX_FK_UserPerOrganisation_DelegatedOrganisation]
ON [dbo].[UserPerOrganisations]
    ([DelegatedOrganizationKey]);
GO

-- Creating foreign key on [OrganisationKey] in table 'UserPerOrganisations'
ALTER TABLE [dbo].[UserPerOrganisations]
ADD CONSTRAINT [FK_UserPerOrganisation_Organisations]
    FOREIGN KEY ([OrganisationKey])
    REFERENCES [dbo].[Organisations]
        ([OrganisationKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPerOrganisation_Organisations'
CREATE INDEX [IX_FK_UserPerOrganisation_Organisations]
ON [dbo].[UserPerOrganisations]
    ([OrganisationKey]);
GO

-- Creating foreign key on [CurrentExtendedOrganisationKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_CurrentOrganization]
    FOREIGN KEY ([CurrentExtendedOrganisationKey])
    REFERENCES [dbo].[Organisations]
        ([OrganisationKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_CurrentOrganization'
CREATE INDEX [IX_FK_WorkFlowItem_CurrentOrganization]
ON [dbo].[WorkFlowItems]
    ([CurrentExtendedOrganisationKey]);
GO

-- Creating foreign key on [NextExtendedOrganisationKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_NextOrganization]
    FOREIGN KEY ([NextExtendedOrganisationKey])
    REFERENCES [dbo].[Organisations]
        ([OrganisationKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_NextOrganization'
CREATE INDEX [IX_FK_WorkFlowItem_NextOrganization]
ON [dbo].[WorkFlowItems]
    ([NextExtendedOrganisationKey]);
GO

-- Creating foreign key on [ScoringRulesKey] in table 'ScoringInstances'
ALTER TABLE [dbo].[ScoringInstances]
ADD CONSTRAINT [FK_ScoringInstance_ScoringRules]
    FOREIGN KEY ([ScoringRulesKey])
    REFERENCES [dbo].[ScoringRules]
        ([ScoringRulesKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScoringInstance_ScoringRules'
CREATE INDEX [IX_FK_ScoringInstance_ScoringRules]
ON [dbo].[ScoringInstances]
    ([ScoringRulesKey]);
GO

-- Creating foreign key on [NextStatusKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_NextStatus]
    FOREIGN KEY ([NextStatusKey])
    REFERENCES [dbo].[Status]
        ([StatusKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_NextStatus'
CREATE INDEX [IX_FK_WorkFlowItem_NextStatus]
ON [dbo].[WorkFlowItems]
    ([NextStatusKey]);
GO

-- Creating foreign key on [CurrentStatusKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItemType_CurrentStatus]
    FOREIGN KEY ([CurrentStatusKey])
    REFERENCES [dbo].[Status]
        ([StatusKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItemType_CurrentStatus'
CREATE INDEX [IX_FK_WorkFlowItemType_CurrentStatus]
ON [dbo].[WorkFlowItems]
    ([CurrentStatusKey]);
GO

-- Creating foreign key on [WorkFlowKey] in table 'WorkFlowItems'
ALTER TABLE [dbo].[WorkFlowItems]
ADD CONSTRAINT [FK_WorkFlowItem_WorkFlow]
    FOREIGN KEY ([WorkFlowKey])
    REFERENCES [dbo].[WorkFlows]
        ([WorkFlowKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkFlowItem_WorkFlow'
CREATE INDEX [IX_FK_WorkFlowItem_WorkFlow]
ON [dbo].[WorkFlowItems]
    ([WorkFlowKey]);
GO

-- Creating foreign key on [AspNetRoles_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetRole]
    FOREIGN KEY ([AspNetRoles_Id])
    REFERENCES [dbo].[AspNetRoles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetUser]
    FOREIGN KEY ([AspNetUsers_Id])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUserRoles_AspNetUser'
CREATE INDEX [IX_FK_AspNetUserRoles_AspNetUser]
ON [dbo].[AspNetUserRoles]
    ([AspNetUsers_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------