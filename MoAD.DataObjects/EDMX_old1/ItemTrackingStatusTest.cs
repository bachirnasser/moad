//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoAD.DataObjects.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemTrackingStatusTest
    {
        public int ItemTrackingStatusKey { get; set; }
        public Nullable<int> WorkFlowItemKey { get; set; }
        public string UpdatedByUserKey { get; set; }
        public string AssignedToUserKey { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public Nullable<int> InvokationTypeKey { get; set; }
        public Nullable<int> CurrentFormKey { get; set; }
        public Nullable<int> NextFormKey { get; set; }
        public Nullable<int> GeoID { get; set; }
    }
}
