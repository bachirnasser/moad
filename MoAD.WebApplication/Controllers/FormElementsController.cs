﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class FormElementsController : Controller
    {
        private Entities db = new Entities();

        // GET: FormElements
        public async Task<ActionResult> Index(string HighlightedPart=null)
        {

            ViewBag.HighlightedPart = HighlightedPart;
            var formElements = db.FormElements.Include(f => f.ExtendedData).Include(f => f.FormElementType);
            return View(await formElements.ToListAsync());
        }

        // GET: FormElements/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormElement formElement = await db.FormElements.FindAsync(id);
            if (formElement == null)
            {
                return HttpNotFound();
            }
            return View(formElement);
        }

        // GET: FormElements/Create
        public ActionResult Create()
        {
            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name");
            ViewBag.FormElementTypeKey = new SelectList(db.FormElementTypes, "FormElementTypeKey", "Name");
            return View();
        }

        // POST: FormElements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FormElementKey,Name,FileExtensionKey,FormElementTypeKey,FormElementTypeKeyDependancy")] FormElement formElement)
        {
            if (ModelState.IsValid)
            {
                db.FormElements.Add(formElement);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name", formElement.FileExtensionKey);
            ViewBag.FormElementTypeKey = new SelectList(db.FormElementTypes, "FormElementTypeKey", "Name", formElement.FormElementTypeKey);
            return View(formElement);
        }

        // GET: FormElements/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormElement formElement = await db.FormElements.FindAsync(id);
            if (formElement == null)
            {
                return HttpNotFound();
            }
            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name", formElement.FileExtensionKey);
            ViewBag.FormElementTypeKey = new SelectList(db.FormElementTypes, "FormElementTypeKey", "Name", formElement.FormElementTypeKey);
            return View(formElement);
        }

        // POST: FormElements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FormElementKey,Name,FileExtensionKey,FormElementTypeKey,FormElementTypeKeyDependancy")] FormElement formElement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formElement).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name", formElement.FileExtensionKey);
            ViewBag.FormElementTypeKey = new SelectList(db.FormElementTypes, "FormElementTypeKey", "Name", formElement.FormElementTypeKey);
            return View(formElement);
        }

        // GET: FormElements/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormElement formElement = await db.FormElements.FindAsync(id);
            if (formElement == null)
            {
                return HttpNotFound();
            }
            return View(formElement);
        }

        // POST: FormElements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FormElement formElement = await db.FormElements.FindAsync(id);
            db.FormElements.Remove(formElement);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
