﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class OrganisationsController : Controller
    {
        // GET: Organisations
        public async Task<ActionResult> Index()
        {
            IEnumerable<GetOrganisations_Result> result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("level={0}", "2");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<IEnumerable<GetOrganisations_Result>>();
                }
            }

            return View(result);
        }

        // GET: Organisations/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            GetOrganisations_Result result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", id.ToString());
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("level={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<GetOrganisations_Result>>().Result.FirstOrDefault();
                }
            }

            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Visibility = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Selected = true, Text = "متاحة", Value = "ok"},
                new SelectListItem { Selected = false, Text = "غير مفعلة", Value = ""},}
            , "Value", "Text", result.Visibility);

            return View(result);
        }

        // GET: Organisations/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.OrganisationParent = new SelectList(await GetMinistries(), "OrganisationName", "OrganisationName");
            ViewBag.OPManagerRoleGroupKey = new SelectList(await GetSectors(), "Id", "ArabicName");
            ViewBag.Province = new SelectList(await GetProvinces(), "ProvinceKey", "Province");

            ViewBag.Visibility = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Selected = true, Text = "متاحة", Value = "ok"},
                new SelectListItem { Selected = false, Text = "غير مفعلة", Value = ""},}
            , "Value", "Text");

            return View();
        }

        // POST: Organisations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,OrganisationKey,OrganisationName,OrganisationParent,Level,OPManagerRoleGroupKey,Province,Visibility,Name,ArabicName")] GetOrganisations_Result getOrganisations_Result)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Data/ManageOrganisation/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("id={0}", getOrganisations_Result.OrganisationKey.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("organisationName={0}", getOrganisations_Result.OrganisationName.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("organisationParent={0}", getOrganisations_Result.OrganisationParent.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("province={0}", getOrganisations_Result.Province.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("action={0}", "Add");
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("visibility={0}", getOrganisations_Result.Visibility);
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("section={0}", getOrganisations_Result.OPManagerRoleGroupKey);

                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }

            }

            return View(getOrganisations_Result);
        }

        // GET: Organisations/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            GetOrganisations_Result result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", id.ToString());
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("level={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<GetOrganisations_Result>>().Result.FirstOrDefault();
                }
            }

            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.OrganisationParent = new SelectList(await GetMinistries(), "OrganisationName", "OrganisationName", result.OrganisationParent);
            ViewBag.OPManagerRoleGroupKey = new SelectList(await GetSectors(), "Id", "ArabicName");
            ViewBag.Province = new SelectList(await GetProvinces(), "ProvinceKey", "Province", result.Province);

            ViewBag.Visibility = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Selected = true, Text = "متاحة", Value = "ok"},
                new SelectListItem { Selected = false, Text = "غير مفعلة", Value = ""},}
            , "Value", "Text", result.Visibility);

            return View(result);
        }

        // POST: Organisations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,OrganisationKey,OrganisationName,OrganisationParent,Level,OPManagerRoleGroupKey,Province,Visibility,Name,ArabicName")] GetOrganisations_Result getOrganisations_Result)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Data/ManageOrganisation/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("id={0}", getOrganisations_Result.OrganisationKey.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("organisationName={0}", getOrganisations_Result.OrganisationName.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("organisationParent={0}", getOrganisations_Result.OrganisationParent.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("province={0}", getOrganisations_Result.Province.ToString());
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("action={0}", "Edit");
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("visibility={0}", getOrganisations_Result.Visibility);
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("section={0}", getOrganisations_Result.OPManagerRoleGroupKey);

                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }

            }
            return View(getOrganisations_Result);
        }

        // GET: Organisations/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            GetOrganisations_Result result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", id.ToString());
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("level={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<GetOrganisations_Result>>().Result.FirstOrDefault();
                }
            }

            if (result == null)
            {
                return HttpNotFound();
            }

            return View(result);
        }

        // POST: Organisations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/ManageOrganisation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", id.ToString());
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationName={0}", "");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationParent={0}", "");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("province={0}", "");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("action={0}", "Delete");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("visibility={0}", "");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("section={0}", "");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }

        [NonAction]
        private async Task<IEnumerable<GetSectors_Result>> GetSectors()
        {
            IEnumerable<GetSectors_Result> result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetSectors/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<IEnumerable<GetSectors_Result>>();
                }
            }

            return result;
        }

        [NonAction]
        private async Task<IEnumerable<GetOrganisations_Result>> GetMinistries()
        {
            IEnumerable<GetOrganisations_Result> result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("level={0}", "3");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<IEnumerable<GetOrganisations_Result>>();
                }
            }

            return result;
        }

        [NonAction]
        private async Task<IEnumerable<GetProvinces_Result>> GetProvinces()
        {
            IEnumerable<GetProvinces_Result> result = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Data/GetProvinces/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<IEnumerable<GetProvinces_Result>>();
                }
            }

            return result;
        }
    }
}
