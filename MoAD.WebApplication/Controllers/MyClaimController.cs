﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class MyClaimController : Controller
    {
        // GET: MyClaim
        public async Task<ActionResult> Index()
        {
            //List<MyForm> ListF = new List<MyForm>();
            MyForm F = new MyForm();

           if(User.IsInRole("Citizen"))
            {
                var userID = User.Identity.GetUserId();
                F.MyFormClaim = GetAllFormTemplatesForaUser(userID).Result;
            }else
            {
                F.MyFormClaim = GetAllFormTemplatesForaUser("null").Result;
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();

            ViewBag.MyFormNumber = F.MyFormClaim.Count();


           // return View(ListF);
            return View(F);

        }

        public async Task<ActionResult> MyClaimForm(string formKey=null)
        {
            ViewBag.formKey = formKey;
            return View();

        }

        public async Task<List<GetAllFormsOfAUser_Result>> GetAllFormTemplatesForaUser(string userKey)
        {
            List<GetAllFormsOfAUser_Result> result = new List<GetAllFormsOfAUser_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllFormTemplatesForaUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", userKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormsOfAUser_Result>>();
                }
            }
            return result;
        }


        public async Task<JsonResult> GetNotifications()
        {
            List<GetAllFormsOfAUser_Result> result = new List<GetAllFormsOfAUser_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllFormTemplatesForaUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormsOfAUser_Result>>();
                }
            }

            result.Reverse();

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public async Task<JsonResult> GetSavedForm(string formKey = null)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();
            List<object> nestedResult = new List<object>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", formKey);               

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();

                    nestedResult = result
                               .GroupBy(x => x.QuestionKey).Select(q => new
                               {
                                   QuestionKey = q.Key,
                                   QuestionValue = q.Select(x => x.QuestionValue).First(),
                                   QuestionNote = q.Select(x => x.QuestionNote).First(),
                                   QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                   QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                   Answers = q.Select(ans => new
                                   {
                                       AnswerTemplateKey = ans.AnswerTemplateKey,
                                       AnswerValue = ans.AnswerValue,
                                       AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                       AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                       AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                       AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                       AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                       AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                       AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                       AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                       FormElement = ans.FormElement,
                                       FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                       FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                       FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                       FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                       FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                       FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                       FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                       FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                       FormElementTypeName = ans.FormElementTypeName,
                                       FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                       FormElementTypeKey = ans.FormElementTypeKey,
                                       FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                       FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                       FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                       FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                       FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                       FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                   }).ToList()
                               }).ToList<object>();
                }
            }
            return Json(nestedResult, JsonRequestBehavior.AllowGet);

        }


        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }
    }
}