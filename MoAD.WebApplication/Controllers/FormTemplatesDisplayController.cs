﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using System.Web.Script.Serialization;

namespace MoAD.WebApplication.Controllers
{
    
    public class FormTemplatesDisplayController : Controller
    {
        // GET: FormTemplatesDisplay
        [Globals.YourCustomAuthorize]
        public ActionResult Index()
        {
            //List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();
            //result = GetAllFormTemplatesForaType("Claim").Result;
            Models.FormTemplate F = new Models.FormTemplate();
            F.FormsTemplates = GetAllFormTemplatesForaType("Claim").Result;

            ViewBag.TemplateNumber = F.FormsTemplates.Count();
            return View(F);
        }

        public async Task<List<GetSavedFormsperGUID_Result>> GetSavedFormsperGUID(string guid)
        {
            List<GetSavedFormsperGUID_Result> result = new List<GetSavedFormsperGUID_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedFormsperGUID");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("guid={0}", guid);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedFormsperGUID_Result>>();
                }
            }
            return result;
        }
        public async Task<List<GetWorkFlowItemInstancePerUser_Result>> GetNotes(string GUID)
        {
            List<GetWorkFlowItemInstancePerUser_Result> FormTemplateTypeResult = new List<GetWorkFlowItemInstancePerUser_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetWorkFlowPerUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("guid={0}", GUID);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<GetWorkFlowItemInstancePerUser_Result>>();
                }
            }

            return FormTemplateTypeResult;
        }

        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }
        private string SafeSqlLiteral(string inputSQL)
        {
            return inputSQL.Replace("'", "''");
        }

       
        public async Task<ActionResult> ADManagerTemplate(string formkey, string GUID, string CreatedAt, string CitizenKey, string CurrentStatusKey, string NextFormTemplateKey, string CurrentFormtemplateKey, string CurrentFormtemplateName,string NextOrganisationKey,string CurrentOrganisationKey)
        {
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            string FormInstanceKey = formkey;
            List<GetSavedForm_Result> FormSaved = GetSavedForm(FormInstanceKey).Result;
            ViewBag.FormValues = FormSaved;
            string formTemplateKey = FormSaved.FirstOrDefault().FormTemplateKey.ToString();
            List<GetUserInformation_Result> UserInfo = GetUserInformation(SafeSqlLiteral(CitizenKey)).Result;
            ViewBag.UserInforesult = UserInfo.FirstOrDefault();
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            ViewBag.SavedFormPerGuId = GetSavedFormsperGUID(GUID).Result;
            ViewBag.Instances = GetNotes(GUID).Result;
            ViewBag.CreatedAt = CreatedAt;
            ViewBag.FormKey = formkey;
            ViewBag.GUID = GUID;
            ViewBag.AssignToUserKey = CitizenKey;
            ViewBag.CurrentStatusKey = CurrentStatusKey;
            ViewBag.NextFormTemplateKey = NextFormTemplateKey;
            ViewBag.CurrentFormtemplateKey = CurrentFormtemplateKey;
            ViewBag.CurrentFormtemplateName = CurrentFormtemplateName;
            ViewBag.NextOrganisationKey = NextOrganisationKey;
            ViewBag.CurrentOrganisationKey = CurrentOrganisationKey;


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "109");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }


            foreach (var item in resultFormTemplate)
            {
                if (item.QuestionValue.Contains("توصيف الحل"))
                {
                    // ViewBag.OpNote = item.QuestionKey;
                    ViewBag.AnswerKeyTawsifAlHal = item.AnswerTemplateKey;
                }

                if (item.AnswerValue.Contains("جزئية"))
                {
                    ViewBag.AnswerKeyTawsif1 = item.AnswerTemplateKey;
                }

                if (item.AnswerValue.Contains("شاملة"))
                {
                    ViewBag.AnswerKeyTawsif2 = item.AnswerTemplateKey;
                }

                if (item.QuestionValue.Contains("القرارات / الإجراءات المتخذة"))
                {
                    ViewBag.AnswerKeyAlkararat = item.AnswerTemplateKey;
                }
     
            }

            ViewBag.AnswerKeyAlWasa2ek = "794";
            if (User.IsInRole("Citizen"))
            {
                return View("CitizenTemplate");
            }
            else
            {
                return View();
            }
           
        }



        //public async Task<ActionResult> ADManagerTemplate(string formkey, string GUID, string CreatedAt, string CitizenKey, string CurrentStatusKey, string NextFormTemplateKey,string CurrentFormtemplateKey, string CurrentFormtemplateName)
        //{
        //    List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
        //    List<object> nestedResultFormTemplate = new List<object>();

        //    ViewBag.formKey = formkey;
        //    List<GetSavedForm_Result> FormSaved = GetSavedForm(formkey).Result;
        //    ViewBag.FormValues = FormSaved;

        //    List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();
        //    List<object> nestedResult = new List<object>();

        //    ViewBag.SavedFormPerGuId = GetSavedFormsperGUID(GUID).Result;
        //    ViewBag.Instances = GetNotes(GUID).Result;
        //    ViewBag.CreatedAt = CreatedAt;
        //    ViewBag.FormKey = formkey;
        //    ViewBag.GUID = GUID;
        //    ViewBag.AssignToUserKey = CitizenKey;
        //    ViewBag.CurrentStatusKey = CurrentStatusKey;
        //    ViewBag.NextFormTemplateKey = NextFormTemplateKey;
        //    ViewBag.CurrentFormtemplateKey = CurrentFormtemplateKey;
        //    ViewBag.CurrentFormtemplateName = CurrentFormtemplateName;


        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/GetSavedForm");
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("formKey={0}", formkey);

        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
        //        }
        //    }

        //    ViewBag.mawdou3ElChakwa = result.First().FormTemplateDescription;


        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/GetFormTemplate/");
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("TemplateKey={0}", "109");
        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

        //            nestedResultFormTemplate = resultFormTemplate
        //                        .GroupBy(x => x.QuestionKey).Select(q => new
        //                        {
        //                            QuestionKey = q.Key,
        //                            QuestionValue = q.Select(x => x.QuestionValue).First(),
        //                            QuestionNote = q.Select(x => x.QuestionNote).First(),
        //                            QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
        //                            QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

        //                            Answers = q.Select(ans => new
        //                            {
        //                                AnswerTemplateKey = ans.AnswerTemplateKey,
        //                                AnswerValue = ans.AnswerValue,
        //                                AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
        //                                AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
        //                                AnswerFileExtensionName = ans.AnswerFileExtensionName,
        //                                AnswerFileExtensionType = ans.AnswerFileExtensionType,
        //                                AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
        //                                AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
        //                                AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
        //                                AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
        //                                FormElement = ans.FormElement,
        //                                FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
        //                                FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
        //                                FormElementFileExtensionName = ans.FormElementFileExtensionName,
        //                                FormElementFileExtensionType = ans.FormElementFileExtensionType,
        //                                FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
        //                                FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
        //                                FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
        //                                FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
        //                                FormElementTypeName = ans.FormElementTypeName,
        //                                FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
        //                                FormElementTypeKey = ans.FormElementTypeKey,
        //                                FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
        //                                FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
        //                                FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
        //                                FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
        //                                FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
        //                                FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
        //                            }).ToList()
        //                        }).ToList<object>();
        //        }
        //    }

        //    foreach (var item in resultFormTemplate)
        //    {
        //        if (item.QuestionValue.Contains("تحديد نوع المعالجة"))
        //        {
        //            ViewBag.Nawe3AlMou3alaja = item.QuestionKey;
        //        }

        //        if (item.AnswerValue.Contains("جزئية"))
        //        {
        //            ViewBag.AnswerKeyTawsif1 = item.AnswerTemplateKey;
        //        }

        //        if (item.AnswerValue.Contains("شاملة"))
        //        {
        //            ViewBag.AnswerKeyTawsif2 = item.AnswerTemplateKey;
        //        }

        //        if (item.QuestionValue.Contains("الوثائق المرفقة"))
        //        {
        //            ViewBag.QuestionAlWasa2ek = item.QuestionKey;

        //        }


        //        if (item.QuestionValue.Contains("توصيف الحل"))
        //        {
        //            ViewBag.QuestionTawsifAlHal = item.QuestionKey;
        //            ViewBag.AnswerKeyTawsifAlHal = item.AnswerTemplateKey;
        //        }

        //        if (item.QuestionValue.Contains("القرارات / الإجراءات المتخذة"))
        //        {
        //            ViewBag.QuestionAlkararat = item.QuestionKey;
        //            ViewBag.AnswerKeyAlkararat = item.AnswerTemplateKey;
        //        }

        //    }


        //    List<GetAllOrganisations_Result> result1 = GetAllOrganisations().Result;
        //    ViewData["Organisation"] = result1.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();
        //    ViewBag.FormTemplateKey = formkey;
        //     ViewBag.CreatedAt = CreatedAt;
        //    ViewBag.FormKey = formkey;
        //    ViewBag.GUID = GUID;
        //    ViewBag.AssignToUserKey = CitizenKey;
        //    ViewBag.CurrentStatusKey = CurrentStatusKey;
        //    ViewBag.NextFormTemplateKey = NextFormTemplateKey;


        //    return View(nestedResultFormTemplate);
        //}

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        public static async Task<List<GetAllFormTemplatesForaType_Result>> GetAllFormTemplatesForaType(string formTemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllFormTemplatesForaType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formTemplateType={0}", formTemplateType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                     result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();
                }
            }
            return result;
        }


        public async Task<JsonResult> GetQuestions(string TemplateKey)
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList()
                                }).ToList<object>();
                    }
            }
          
            return Json(nestedResult, JsonRequestBehavior.AllowGet);

        }

        public async Task<List<GetSavedForm_Result>> GetSavedForm(string FormKey)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", FormKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
                }
            }
            return result;
        }


    }
}