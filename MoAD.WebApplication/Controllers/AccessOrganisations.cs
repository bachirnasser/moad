﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Controllers
{
    public class AccessOrganisations
    {
        public string OrganisationKey { get; set; }
        public string OrganisationName { get; set; }
    }
}