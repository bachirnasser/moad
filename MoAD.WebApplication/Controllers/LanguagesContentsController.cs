﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Objects;
using MoAD.WebApplication.Models;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class LanguagesContentsController : BaseController
    {
        private Entities db = new Entities();

        // GET: LanguagesContents
        public async Task<ActionResult> Index()
        {
            var languagesContents = db.LanguagesContents.Include(l => l.Field).Include(l => l.Language);
            return View(await languagesContents.ToListAsync());
        }

        // GET: LanguagesContents/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LanguagesContent languagesContent = await db.LanguagesContents.FindAsync(id);
            if (languagesContent == null)
            {
                return HttpNotFound();
            }
            return View(languagesContent);
        }

        // GET: LanguagesContents/Create
        public ActionResult Create()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.FieldKey = new SelectList(db.Fields, "FieldKey", "Field1");
            ViewBag.LanguageKey = new SelectList(db.Languages.Where(i => i.LanguageKey == CurrentLanguageIdentifier), "LanguageKey", "Language1");
            return View();
        }

        // POST: LanguagesContents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "LanguagesContentKey,FieldKey,FieldValue,Controller,Action,LanguageKey")] LanguagesContent languagesContent)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (ModelState.IsValid)
            {
                db.LanguagesContents.Add(languagesContent);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FieldKey = new SelectList(db.Fields, "FieldKey", "Field1", languagesContent.FieldKey);
            ViewBag.LanguageKey = new SelectList(db.Languages.Where(i => i.LanguageKey == CurrentLanguageIdentifier), "LanguageKey", "Language1", languagesContent.LanguageKey);
            return View(languagesContent);
        }

        // GET: LanguagesContents/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LanguagesContent languagesContent = await db.LanguagesContents.FindAsync(id);
            if (languagesContent == null)
            {
                return HttpNotFound();
            }

            ViewBag.FieldKey = new SelectList(db.Fields, "FieldKey", "Field1", languagesContent.FieldKey);
            ViewBag.LanguageKey = new SelectList(db.Languages, "LanguageKey", "Language1", languagesContent.LanguageKey);

            EditLanguageContentsViewModel model = new EditLanguageContentsViewModel();

            model.LanguagesContentKey = languagesContent.LanguagesContentKey;
            model.Action = languagesContent.Action;
            model.Controller = languagesContent.Controller;
            model.FieldKey = Convert.ToInt32(languagesContent.FieldKey);
            model.FieldValue = languagesContent.FieldValue;
            model.LanguageKey = Convert.ToInt32(languagesContent.LanguageKey);

            return View(model);
        }

        // POST: LanguagesContents/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditLanguageContentsViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LanguagesContent languagesContent = await db.LanguagesContents.FindAsync(model.LanguagesContentKey);
            if (languagesContent == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    languagesContent.FieldValue = model.FieldValue;

                    db.Entry(languagesContent).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    //await LogManager.log(MethodBase.GetCurrentMethod().Name, e.Message);
                }
            }
            ViewBag.FieldKey = new SelectList(db.Fields, "FieldKey", "Field1", languagesContent.FieldKey);
            ViewBag.LanguageKey = new SelectList(db.Languages.Where(i => i.LanguageKey == CurrentLanguageIdentifier), "LanguageKey", "Language1", languagesContent.LanguageKey);
            return View(languagesContent);
        }

        // GET: LanguagesContents/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LanguagesContent languagesContent = await db.LanguagesContents.FindAsync(id);
            if (languagesContent == null)
            {
                return HttpNotFound();
            }
            return View(languagesContent);
        }

        // POST: LanguagesContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            LanguagesContent languagesContent = await db.LanguagesContents.FindAsync(id);
            db.LanguagesContents.Remove(languagesContent);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}