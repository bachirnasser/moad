﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Controllers
{
    public class RankingDisplay
    {
        public Nullable<double> Score { get; set; }
        public string OrganisationKey { get; set; }
        public string Organisation { get; set; }
        public string FormTemplateType { get; set; }
        public Nullable<double> FormTemplateTypeScore { get; set; }
        public string FormTemplateType1 { get; set; }
        public string OrganisationParent { get; set; }
        public string Province { get; set; }
        public string Sector { get; set; }



        public Nullable<double> FormTemplateTypeScore1 { get; set; }
    }
}