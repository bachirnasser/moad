﻿using MoAD.WebApplication.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using System.Globalization;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class ERMEmployeesController : Controller
    {
        // GET: ERMEmployees
        public async Task<ActionResult> Index()
        {
            List<ERMEmployees> ERMEmployees = new List<ERMEmployees>();
            ERMEmployees = await GetERMEmployees();
            return View(ERMEmployees);
        }

        public async Task<ActionResult> ERMReports(string OrganisationID=null)
        {
            List<GetHRInfo_Result> HRInfo = new List<GetHRInfo_Result>();
            HRInfo = await GetHRInfo(OrganisationID);
            var AllOrganisations = GetAllOrganisations().Result;
            ViewBag.AllOrganisations = AllOrganisations;
           
            if (OrganisationID==null)
            {
                OrganisationID = "General";
                ViewBag.OrganisationName = "التقرير العام";
            }else
            {
                var OrganisationName = AllOrganisations.Where(o => o.OrganisationKey == int.Parse(OrganisationID)).FirstOrDefault().OrganisationName;
                ViewBag.OrganisationName = OrganisationName;
            }

            ViewBag.OrganisationID = OrganisationID;
            

            //EmployeePerAgeReport
          List <object> EmployeePerAgeReportObject = new List<object>();
            Dictionary<string, string> EmployeePerAgeBetween51And65Dict = new Dictionary<string, string>();
            EmployeePerAgeBetween51And65Dict.Add("year", "من ٥١ حتى ٦٥ سنة");
            EmployeePerAgeBetween51And65Dict.Add("income", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 51 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 65).ToString());
            
            Dictionary<string, string> EmployeePerAgeBetween41And50Dict = new Dictionary<string, string>();
            EmployeePerAgeBetween41And50Dict.Add("year", "من ٤١ حتى ٥٠ سنة");
            EmployeePerAgeBetween41And50Dict.Add("income", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 41 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 50).ToString());
            
            Dictionary<string, string> EmployeePerAgeBetween31And40Dict = new Dictionary<string, string>();
            EmployeePerAgeBetween31And40Dict.Add("year", "من ٣١ حتى ٤٠ سنة");
            EmployeePerAgeBetween31And40Dict.Add("income", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 31 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 40).ToString());
            
            Dictionary<string, string> EmployeePerAgeBetween18And30Dict = new Dictionary<string, string>();
            EmployeePerAgeBetween18And30Dict.Add("year", "من ١٨ حتى ٣٠ سنة");
            EmployeePerAgeBetween18And30Dict.Add("income", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 18 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 30).ToString());

            EmployeePerAgeReportObject.Add(EmployeePerAgeBetween18And30Dict);
            EmployeePerAgeReportObject.Add(EmployeePerAgeBetween31And40Dict);
            EmployeePerAgeReportObject.Add(EmployeePerAgeBetween41And50Dict);
            EmployeePerAgeReportObject.Add(EmployeePerAgeBetween51And65Dict);

            ViewBag.EmployeePerAgeReportObject = EmployeePerAgeReportObject;

            //EmployeePerAgeSliceReport
            List<object> EmployeePerAgeSliceReportObject = new List<object>();
            Dictionary<string, string> EmployeePerAgeSliceBetween51And65Dict = new Dictionary<string, string>();
            EmployeePerAgeSliceBetween51And65Dict.Add("year", "من ٥١ حتى ٦٥ سنة");
            EmployeePerAgeSliceBetween51And65Dict.Add("europe", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 51 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 65 && e.Slice== "الأولى").ToString());
            EmployeePerAgeSliceBetween51And65Dict.Add("namerica", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 51 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 65 && e.Slice== "الثانية").ToString());
            EmployeePerAgeSliceBetween51And65Dict.Add("asia", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 51 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 65 && (e.Slice!= "الثانية" && e.Slice!= "الأولى")).ToString());

            Dictionary<string, string> EmployeePerAgeSliceBetween41And50Dict = new Dictionary<string, string>();
            EmployeePerAgeSliceBetween41And50Dict.Add("year", "من ٤١ حتى ٥٠ سنة");
            EmployeePerAgeSliceBetween41And50Dict.Add("europe", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 41 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 50 && e.Slice == "الأولى").ToString());
            EmployeePerAgeSliceBetween41And50Dict.Add("namerica", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 41 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 50 && e.Slice == "الثانية").ToString());
            EmployeePerAgeSliceBetween41And50Dict.Add("asia", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 41 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 50 && (e.Slice != "الثانية" && e.Slice != "الأولى")).ToString());

            Dictionary<string, string> EmployeePerAgeSliceBetween31And40Dict = new Dictionary<string, string>();
            EmployeePerAgeSliceBetween31And40Dict.Add("year", "من ٣١ حتى ٤٠ سنة");
            EmployeePerAgeSliceBetween31And40Dict.Add("europe", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 31 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 40 && e.Slice == "الأولى").ToString());
            EmployeePerAgeSliceBetween31And40Dict.Add("namerica", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 31 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 40 && e.Slice == "الثانية").ToString());
            EmployeePerAgeSliceBetween31And40Dict.Add("asia", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 31 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 40 && (e.Slice != "الثانية" && e.Slice != "الأولى")).ToString());

            Dictionary<string, string> EmployeePerAgeSliceBetween18And30Dict = new Dictionary<string, string>();
            EmployeePerAgeSliceBetween18And30Dict.Add("year", "من ١٨ حتى ٣٠ سنة");
            EmployeePerAgeSliceBetween18And30Dict.Add("europe", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 18 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 30 && e.Slice == "الأولى").ToString());
            EmployeePerAgeSliceBetween18And30Dict.Add("namerica", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 18 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 30 && e.Slice == "الثانية").ToString());
            EmployeePerAgeSliceBetween18And30Dict.Add("asia", HRInfo.Count(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 18 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 30 && (e.Slice != "الثانية" && e.Slice != "الأولى")).ToString());

            EmployeePerAgeSliceReportObject.Add(EmployeePerAgeSliceBetween18And30Dict);
            EmployeePerAgeSliceReportObject.Add(EmployeePerAgeSliceBetween31And40Dict);
            EmployeePerAgeSliceReportObject.Add(EmployeePerAgeSliceBetween41And50Dict);
            EmployeePerAgeSliceReportObject.Add(EmployeePerAgeSliceBetween51And65Dict);

            ViewBag.EmployeePerAgeSliceReportObject = EmployeePerAgeSliceReportObject;

            //EmloyeePerEmploymentDateSliceOne
            List<object> EmloyeePerEmploymentDateSliceOne = new List<object>();
            EmloyeePerEmploymentDateSliceOne = HRInfo.Where(h=>h.Slice== "الأولى").GroupBy(hr => int.Parse(hr.EmploymentDate.Split('-')[2])).Distinct().Select(group => new {
                year = group.Key,
                value = group.Count()
            })
                        .OrderBy(x => x.year).ToList<object>();

            ViewBag.EmloyeePerEmploymentDateSliceOne = EmloyeePerEmploymentDateSliceOne;

            //AdultsPerTitle
            List<object> AdultsPerTitleObject = new List<object>();           
            var AdultEmployees = HRInfo.Where(e => DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) >= 18 && DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2]) <= 40);

            Dictionary <string, string> AdultsPerTitleWithEngineersDict = new Dictionary<string, string>();
            AdultsPerTitleWithEngineersDict.Add("year", "لديهم مسمى وظيفي مع احتساب المهندسين");
            AdultsPerTitleWithEngineersDict.Add("income", AdultEmployees.Count().ToString());
            AdultsPerTitleWithEngineersDict.Add("expenses", AdultEmployees.Count(e=> e.EmploymentTitle != null && e.EmploymentTitle != "" && e.EmploymentTitle.Contains("مهندس")).ToString());

            Dictionary<string, string> AdultsPerTitleWithoutEngineersDict = new Dictionary<string, string>();
            AdultsPerTitleWithoutEngineersDict.Add("year", "لديهم مسمى وظيفي دون احتساب المهندسين");
            AdultsPerTitleWithoutEngineersDict.Add("income", AdultEmployees.Count().ToString());
            AdultsPerTitleWithoutEngineersDict.Add("expenses", AdultEmployees.Count(e => e.EmploymentTitle != null && e.EmploymentTitle != "" && !e.EmploymentTitle.Contains("مهندس")).ToString());

            AdultsPerTitleObject.Add(AdultsPerTitleWithEngineersDict);
            AdultsPerTitleObject.Add(AdultsPerTitleWithoutEngineersDict);

            ViewBag.AdultsPerTitleObject = AdultsPerTitleObject;

            //EmployeesHavingOrNotHavingTitlePerSlice
            List<object> EmployeesHavingOrNotHavingTitlePerSliceObject = new List<object>();
            var EmployeesWithTitle = HRInfo.Where(e => e.EmploymentTitle != null && e.EmploymentTitle != "");
            var EmployeesWithTitleAndSlice1 = EmployeesWithTitle.Count(e => e.Slice == "الأولى");
            var EmployeesWithTitleAndSlice2 = EmployeesWithTitle.Count(e => e.Slice == "الثانية");
            var EmployeesWithTitleAndSliceOthers = EmployeesWithTitle.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى");

            var EmployeesWithoutTitle = HRInfo.Where(e => e.EmploymentTitle == null || e.EmploymentTitle == "");
            var EmployeesWithoutTitleAndSlice1 = EmployeesWithoutTitle.Count(e => e.Slice == "الأولى");
            var EmployeesWithoutTitleTitleAndSlice2 = EmployeesWithoutTitle.Count(e => e.Slice == "الثانية");
            var EmployeesWithoutTitleTitleAndSliceOthers = EmployeesWithoutTitle.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى");

            //EmployeesWithTitleDict
            Dictionary<string, object> EmployeesWithTitleDict = new Dictionary<string, object>();
            List<object> EmployeesWithTitlePerSlicesObject = new List<object>();

            Dictionary<string, object> EmployeesWithTitlePerSlice1Dict = new Dictionary<string, object>();
            Dictionary<string, object> EmployeesWithTitlePerSlice2Dict = new Dictionary<string, object>();
            Dictionary<string, object> EmployeesWithTitlePerSliceOthersDict = new Dictionary<string, object>();

            EmployeesWithTitleDict.Add("type", "لديهم مسميات وظيفية");
            EmployeesWithTitleDict.Add("percent", EmployeesWithTitle.ToList().Count);
            EmployeesWithTitleDict.Add("color", "#ff9e01");

            EmployeesWithTitlePerSlice1Dict.Add("type", "الفئة الأولى");
            EmployeesWithTitlePerSlice1Dict.Add("percent", EmployeesWithTitleAndSlice1);

            EmployeesWithTitlePerSlice2Dict.Add("type", "الفئة الثانية");
            EmployeesWithTitlePerSlice2Dict.Add("percent", EmployeesWithTitleAndSlice2);

            EmployeesWithTitlePerSliceOthersDict.Add("type", "باقي الفئات");
            EmployeesWithTitlePerSliceOthersDict.Add("percent", EmployeesWithTitleAndSliceOthers);

            EmployeesWithTitlePerSlicesObject.Add(EmployeesWithTitlePerSlice1Dict);
            EmployeesWithTitlePerSlicesObject.Add(EmployeesWithTitlePerSlice2Dict);
            EmployeesWithTitlePerSlicesObject.Add(EmployeesWithTitlePerSliceOthersDict);
            EmployeesWithTitleDict.Add("subs", EmployeesWithTitlePerSlicesObject);

            //EmployeesWithoutTitleDict
            Dictionary<string, object> EmployeesWithoutTitleDict = new Dictionary<string, object>();
            List<object> EmployeesWithoutTitlePerSlicesObject = new List<object>();

            Dictionary<string, object> EmployeesWithoutTitlePerSlice1Dict = new Dictionary<string, object>();
            Dictionary<string, object> EmployeesWithoutTitlePerSlice2Dict = new Dictionary<string, object>();
            Dictionary<string, object> EmployeesWithoutTitlePerSliceOthersDict = new Dictionary<string, object>();

            EmployeesWithoutTitleDict.Add("type", "ليس لديهم مسميات وظيفية");
            EmployeesWithoutTitleDict.Add("percent", EmployeesWithoutTitle.ToList().Count);
            EmployeesWithoutTitleDict.Add("color", "#b0de09");

            EmployeesWithoutTitlePerSlice1Dict.Add("type", "الفئة الأولى");
            EmployeesWithoutTitlePerSlice1Dict.Add("percent", EmployeesWithoutTitleAndSlice1);

            EmployeesWithoutTitlePerSlice2Dict.Add("type", "الفئة الثانية");
            EmployeesWithoutTitlePerSlice2Dict.Add("percent", EmployeesWithoutTitleTitleAndSlice2);

            EmployeesWithoutTitlePerSliceOthersDict.Add("type", "باقي الفئات");
            EmployeesWithoutTitlePerSliceOthersDict.Add("percent", EmployeesWithoutTitleTitleAndSliceOthers);

            EmployeesWithoutTitlePerSlicesObject.Add(EmployeesWithoutTitlePerSlice1Dict);
            EmployeesWithoutTitlePerSlicesObject.Add(EmployeesWithoutTitlePerSlice2Dict);
            EmployeesWithoutTitlePerSlicesObject.Add(EmployeesWithoutTitlePerSliceOthersDict);
            EmployeesWithoutTitleDict.Add("subs", EmployeesWithoutTitlePerSlicesObject);

            //Final Result EmployeesHavingOrNotHavingTitlePerSliceObject
            if(EmployeesWithTitleAndSlice1!=0 || EmployeesWithoutTitle.ToList().Count != 0)
            {
                EmployeesHavingOrNotHavingTitlePerSliceObject.Add(EmployeesWithTitleDict);
                EmployeesHavingOrNotHavingTitlePerSliceObject.Add(EmployeesWithoutTitleDict);
            }

            ViewBag.EmployeesHavingOrNotHavingTitlePerSliceObject = EmployeesHavingOrNotHavingTitlePerSliceObject;

            //EmplyeesWithAndWithoutSubOrganisationalUnit
            List<object> EmplyeesWithAndWithoutSubOrganisationalUnitObject = new List<object>();
            var EmployeesWithTitleAndWithSubOrganisationalUnit = EmployeesWithTitle.Count(e=>e.SubOrganizationalUnit!=null || e.SubOrganizationalUnit!="");
            var EmployeesWithTitleAndWithoutSubOrganisationalUnit = EmployeesWithTitle.Count(e=>e.SubOrganizationalUnit==null || e.SubOrganizationalUnit=="");

            Dictionary<string, object> EmployeesWithTitleAndWithSubOrganisationalUnitDict = new Dictionary<string, object>();            
            EmployeesWithTitleAndWithSubOrganisationalUnitDict.Add("country", "لديهم وحدات تنظيمية فرعية");
            EmployeesWithTitleAndWithSubOrganisationalUnitDict.Add("litres", EmployeesWithTitleAndWithSubOrganisationalUnit);

            Dictionary<string, object> EmployeesWithTitleAndWithoutSubOrganisationalUnitDict = new Dictionary<string, object>();
            EmployeesWithTitleAndWithoutSubOrganisationalUnitDict.Add("country", "ليس لديهم وحدات تنظيمية فرعية");
            EmployeesWithTitleAndWithoutSubOrganisationalUnitDict.Add("litres", EmployeesWithTitleAndWithoutSubOrganisationalUnit);

            if(EmployeesWithTitleAndWithSubOrganisationalUnit!=0 || EmployeesWithTitleAndWithoutSubOrganisationalUnit != 0)
            {
                EmplyeesWithAndWithoutSubOrganisationalUnitObject.Add(EmployeesWithTitleAndWithSubOrganisationalUnitDict);
                EmplyeesWithAndWithoutSubOrganisationalUnitObject.Add(EmployeesWithTitleAndWithoutSubOrganisationalUnitDict);
            }            

            ViewBag.EmplyeesWithAndWithoutSubOrganisationalUnitObject = EmplyeesWithAndWithoutSubOrganisationalUnitObject;

            //EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnit
            List<object> EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnit = new List<object>();
            var EmplyeesWithoutTitle = HRInfo.Count(e => e.EmploymentTitle == null || e.EmploymentTitle == "");
            var EmployeesWithTitleButNotHavingSubOrganizationalUnit = EmployeesWithTitle.Count(e=> e.SubOrganizationalUnit == null || e.SubOrganizationalUnit == "");

            Dictionary<string, string> EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnitDict = new Dictionary<string, string>();
            EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnitDict.Add("year", "نسبة التوطين الوظيفي");
            EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnitDict.Add("income", EmplyeesWithoutTitle.ToString());
            EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnitDict.Add("expenses", EmployeesWithTitleButNotHavingSubOrganizationalUnit.ToString());

            EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnit.Add(EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnitDict);

            ViewBag.EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnit = EmplyeesWithoutTitleVsEmployeesWithTitleButNotHavingSubOrganizationalUnit;

            //EmployeesCertificateReport
            List<object> EmployeesCertificateReportObject = new List<object>();
            var AllMostRecentCertificate = HRInfo.Where(e => e.MostRecentCertificate != null).Select(e => e.MostRecentCertificate).Distinct();
            foreach(var item in AllMostRecentCertificate)
            {
                Dictionary<string, object> EmployyesPerSlicePerMostRecentCertificate = new Dictionary<string, object>();
                EmployyesPerSlicePerMostRecentCertificate.Add("year", item);
                EmployyesPerSlicePerMostRecentCertificate.Add("europe", HRInfo.Count(e => e.MostRecentCertificate == item && e.Slice != "الأولى" && e.Slice != "الثانية"));
                EmployyesPerSlicePerMostRecentCertificate.Add("namerica", HRInfo.Count(e => e.MostRecentCertificate == item && e.Slice == "الأولى"));
                EmployyesPerSlicePerMostRecentCertificate.Add("asia", HRInfo.Count(e => e.MostRecentCertificate == item && e.Slice == "الثانية"));
                EmployeesCertificateReportObject.Add(EmployyesPerSlicePerMostRecentCertificate);
            }

            ViewBag.EmployeesCertificateReportObject = EmployeesCertificateReportObject; 
            
            //EmployeesPerEmploymentDateReport    
            List<object> EmployeesPerEmploymentDateReportObject = new List<object>();

            Dictionary<string, string> EmployeePerEmploymentDateBetween1and5 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween1and5.Add("year", "من سنة حتى ٥ سنوات");
            EmployeePerEmploymentDateBetween1and5.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime)>=1 && int.Parse(e.EmploymentTime) <= 5 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween1and5.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime)>=1 && int.Parse(e.EmploymentTime) <= 5 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween1and5.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 1 && int.Parse(e.EmploymentTime) <= 5 && e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeePerEmploymentDateBetween6and10 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween6and10.Add("year", "من ٦ حتى ١٠ سنوات");
            EmployeePerEmploymentDateBetween6and10.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 6 && int.Parse(e.EmploymentTime) <= 10 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween6and10.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 6 && int.Parse(e.EmploymentTime) <= 10 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween6and10.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 6 && int.Parse(e.EmploymentTime) <= 10 && e.Slice == "الثانية").ToString());


            Dictionary<string, string> EmployeePerEmploymentDateBetween11and15 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween11and15.Add("year", "من ١١ حتى ١٥ سنوات");
            EmployeePerEmploymentDateBetween11and15.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 11 && int.Parse(e.EmploymentTime) <= 15 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween11and15.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 11 && int.Parse(e.EmploymentTime) <= 15 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween11and15.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 11 && int.Parse(e.EmploymentTime) <= 15 && e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeePerEmploymentDateBetween16and20 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween16and20.Add("year", "من ١٦ حتى ٢٠ سنوات");
            EmployeePerEmploymentDateBetween16and20.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 16 && int.Parse(e.EmploymentTime) <= 20 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween16and20.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 16 && int.Parse(e.EmploymentTime) <= 20 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween16and20.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 16 && int.Parse(e.EmploymentTime) <= 20 && e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeePerEmploymentDateBetween21and25 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween21and25.Add("year", "من ٢١ حتى ٢٥ سنوات");
            EmployeePerEmploymentDateBetween21and25.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 21 && int.Parse(e.EmploymentTime) <= 25 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween21and25.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 21 && int.Parse(e.EmploymentTime) <= 25 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween21and25.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 21 && int.Parse(e.EmploymentTime) <= 25 && e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeePerEmploymentDateBetween26and30 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween26and30.Add("year", "من ٢٦ حتى ٣٠ سنوات");
            EmployeePerEmploymentDateBetween26and30.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 26 && int.Parse(e.EmploymentTime) <= 30 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween26and30.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 26 && int.Parse(e.EmploymentTime) <= 30 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween26and30.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 26 && int.Parse(e.EmploymentTime) <= 30 && e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeePerEmploymentDateBetween31and35 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween31and35.Add("year", "من ٣١ حتى ٣٥ سنوات");
            EmployeePerEmploymentDateBetween31and35.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 31 && int.Parse(e.EmploymentTime) <= 35 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween31and35.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 31 && int.Parse(e.EmploymentTime) <= 35 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween31and35.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 31 && int.Parse(e.EmploymentTime) <= 35 && e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeePerEmploymentDateBetween36and40 = new Dictionary<string, string>();
            EmployeePerEmploymentDateBetween36and40.Add("year", "من ٣٦ حتى ٤٠ سنوات");
            EmployeePerEmploymentDateBetween36and40.Add("europe", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 36 && int.Parse(e.EmploymentTime) <= 40 && e.Slice != "الأولى" && e.Slice != "الثانية").ToString());
            EmployeePerEmploymentDateBetween36and40.Add("namerica", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 36 && int.Parse(e.EmploymentTime) <= 40 && e.Slice == "الأولى").ToString());
            EmployeePerEmploymentDateBetween36and40.Add("asia", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 36 && int.Parse(e.EmploymentTime) <= 40 && e.Slice == "الثانية").ToString());

            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween1and5);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween6and10);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween11and15);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween16and20);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween21and25);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween26and30);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween31and35);
            EmployeesPerEmploymentDateReportObject.Add(EmployeePerEmploymentDateBetween36and40);

            ViewBag.EmployeesPerEmploymentDateReportObject = EmployeesPerEmploymentDateReportObject;

            //EmployeesPreviousEmploymentsReport
            List<object> EmployeesPreviousEmploymentsReportObject = new List<object>();

            var EmployeesWithPreviousEmployments = HRInfo.Count(e => e.PreviousEmployments != null || e.PreviousEmployments != "");
            var EmployeesWithoutPreviousEmploymentsCount = HRInfo.Count(e => e.PreviousEmployments == null || e.PreviousEmployments == "");

            Dictionary<string, object> EmployeesWithoutPreviousEmploymentsDict = new Dictionary<string, object>();
            EmployeesWithoutPreviousEmploymentsDict.Add("country", "لم يشغلوا مواقع وظيفية سابقة");
            EmployeesWithoutPreviousEmploymentsDict.Add("litres", EmployeesWithoutPreviousEmploymentsCount);

            Dictionary<string, object> EmployeesWithPreviousEmploymentsDict = new Dictionary<string, object>();
            EmployeesWithPreviousEmploymentsDict.Add("country", "شغلوا موقع وظيفي سابق واحد على الأقل");
            EmployeesWithPreviousEmploymentsDict.Add("litres", EmployeesWithPreviousEmployments);

            if(EmployeesWithPreviousEmployments!=0 || EmployeesWithoutPreviousEmploymentsCount != -0)
            {
                EmployeesPreviousEmploymentsReportObject.Add(EmployeesWithPreviousEmploymentsDict);
                EmployeesPreviousEmploymentsReportObject.Add(EmployeesWithoutPreviousEmploymentsDict);
            }
            
            ViewBag.EmployeesPreviousEmploymentsReportObject = EmployeesPreviousEmploymentsReportObject;

            ////EmployeesPreviousEmploymentsPerSlicesReport
            List<object> EmployeesPreviousEmploymentsPerSlicesReportObject = new List<object>();
            var EmployeesPreviousEmploymentsPerSlice1Count = HRInfo.Count(e => (e.PreviousEmployments == null || e.PreviousEmployments == "") && e.Slice == "الأولى");
            var EmployeesPreviousEmploymentsPerSlice2Count = HRInfo.Count(e => (e.PreviousEmployments == null || e.PreviousEmployments == "") && e.Slice == "الثانية");
            var EmployeesPreviousEmploymentsPerSliceOthersCount = HRInfo.Count(e => (e.PreviousEmployments == null || e.PreviousEmployments == "") && (e.Slice != "الثانية" && e.Slice != "الأولى" && e.Slice != null));

            Dictionary<string, object> EmployeesPreviousEmploymentsPerSlice1Dict = new Dictionary<string, object>();
            EmployeesPreviousEmploymentsPerSlice1Dict.Add("country", "الفئة الأولى");
            EmployeesPreviousEmploymentsPerSlice1Dict.Add("litres", EmployeesPreviousEmploymentsPerSlice1Count);

            Dictionary<string, object> EmployeesPreviousEmploymentsPerSlice2Dict = new Dictionary<string, object>();
            EmployeesPreviousEmploymentsPerSlice2Dict.Add("country", "الفئة الثانية");
            EmployeesPreviousEmploymentsPerSlice2Dict.Add("litres", EmployeesPreviousEmploymentsPerSlice2Count);

            Dictionary<string, object> EmployeesPreviousEmploymentsPerSliceOthersDict = new Dictionary<string, object>();
            EmployeesPreviousEmploymentsPerSliceOthersDict.Add("country", "باقي الفئات");
            EmployeesPreviousEmploymentsPerSliceOthersDict.Add("litres", EmployeesPreviousEmploymentsPerSliceOthersCount);

            if(EmployeesPreviousEmploymentsPerSlice1Count!=0 || EmployeesPreviousEmploymentsPerSlice2Count!=0 || EmployeesPreviousEmploymentsPerSliceOthersCount != 0)
            {
                EmployeesPreviousEmploymentsPerSlicesReportObject.Add(EmployeesPreviousEmploymentsPerSlice1Dict);
                EmployeesPreviousEmploymentsPerSlicesReportObject.Add(EmployeesPreviousEmploymentsPerSlice2Dict);
                EmployeesPreviousEmploymentsPerSlicesReportObject.Add(EmployeesPreviousEmploymentsPerSliceOthersDict);
            }
           
            ViewBag.EmployeesPreviousEmploymentsPerSlicesReportObject = EmployeesPreviousEmploymentsPerSlicesReportObject;

            //EmployeesEmploymentTimeVsPreviousEmploymentsReport
            List<object> EmployeesEmploymentTimeVsPreviousEmploymentsReportObject = new List<object>();

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween1and5 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween1and5.Add("year", "من سنة حتى ٥ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween1and5.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 1 && int.Parse(e.EmploymentTime) <= 5 && e.PreviousEmployments==null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween1and5.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 1 && int.Parse(e.EmploymentTime) <= 5 && e.PreviousEmployments != null).ToString());

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween6and10 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween6and10.Add("year", "من ٦ حتى ١٠ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween6and10.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 6 && int.Parse(e.EmploymentTime) <= 10 && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween6and10.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 6 && int.Parse(e.EmploymentTime) <= 10 && e.PreviousEmployments != null).ToString());


            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween11and15 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween11and15.Add("year", "من ١١ حتى ١٥ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween11and15.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 11 && int.Parse(e.EmploymentTime) <= 15 && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween11and15.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 11 && int.Parse(e.EmploymentTime) <= 15 && e.PreviousEmployments != null).ToString());

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween16and20 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween16and20.Add("year", "من ١٦ حتى ٢٠ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween16and20.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 16 && int.Parse(e.EmploymentTime) <= 20 && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween16and20.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 16 && int.Parse(e.EmploymentTime) <= 20 && e.PreviousEmployments != null).ToString());

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween21and25 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween21and25.Add("year", "من ٢١ حتى ٢٥ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween21and25.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 21 && int.Parse(e.EmploymentTime) <= 25  && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween21and25.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 21 && int.Parse(e.EmploymentTime) <= 25 && e.PreviousEmployments != null).ToString());

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween26and30 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween26and30.Add("year", "من ٢٦ حتى ٣٠ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween26and30.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 26 && int.Parse(e.EmploymentTime) <= 30 && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween26and30.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 26 && int.Parse(e.EmploymentTime) <= 30 && e.PreviousEmployments != null).ToString());

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween31and35 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween31and35.Add("year", "من ٣١ حتى ٣٥ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween31and35.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 31 && int.Parse(e.EmploymentTime) <= 35 && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween31and35.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 31 && int.Parse(e.EmploymentTime) <= 35 && e.PreviousEmployments != null).ToString());

            Dictionary<string, string> EmployeesEmploymentTimeVsPreviousEmploymentsBetween36and40 = new Dictionary<string, string>();
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween36and40.Add("year", "من ٣٦ حتى ٤٠ سنوات");
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween36and40.Add("income", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 36 && int.Parse(e.EmploymentTime) <= 40 && e.PreviousEmployments == null).ToString());
            EmployeesEmploymentTimeVsPreviousEmploymentsBetween36and40.Add("expenses", HRInfo.Count(e => int.Parse(e.EmploymentTime) >= 36 && int.Parse(e.EmploymentTime) <= 40 && e.PreviousEmployments != null).ToString());

            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween1and5);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween6and10);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween11and15);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween16and20);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween21and25);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween26and30);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween31and35);
            EmployeesEmploymentTimeVsPreviousEmploymentsReportObject.Add(EmployeesEmploymentTimeVsPreviousEmploymentsBetween36and40);

            ViewBag.EmployeesEmploymentTimeVsPreviousEmploymentsReportObject = EmployeesEmploymentTimeVsPreviousEmploymentsReportObject;

            //EmployeesWithTrainingCoursesVsWitoutTrainingCoursesReport
            List<object> EmployeesWithTrainingCoursesVsWitoutTrainingCoursesReportObject = new List<object>();
            var EmployeesWithTrainingCoursesCount = HRInfo.Count(e => e.TrainingCourses != null || e.TrainingCourses != "");
            var EmployeesWithoutTrainingCoursesCount = HRInfo.Count(e => e.TrainingCourses == null || e.TrainingCourses == "");

            Dictionary<string, object> EmployeesWithTrainingCoursesDict = new Dictionary<string, object>();
            EmployeesWithTrainingCoursesDict.Add("country", "الذين خضعوا لدورة تدريبة واحدة على الأقل");
            EmployeesWithTrainingCoursesDict.Add("litres", EmployeesWithTrainingCoursesCount);

            Dictionary<string, object> EmployeesWithoutTrainingCoursesDict = new Dictionary<string, object>();
            EmployeesWithoutTrainingCoursesDict.Add("country", "الذين لم يخضعوا لأي دورة تدريبية");
            EmployeesWithoutTrainingCoursesDict.Add("litres", EmployeesWithoutTrainingCoursesCount);

            if(EmployeesWithTrainingCoursesCount!=0 || EmployeesWithoutTrainingCoursesCount != 0)
            {
                EmployeesWithTrainingCoursesVsWitoutTrainingCoursesReportObject.Add(EmployeesWithTrainingCoursesDict);
                EmployeesWithTrainingCoursesVsWitoutTrainingCoursesReportObject.Add(EmployeesWithoutTrainingCoursesDict);
            }
            
            ViewBag.EmployeesWithTrainingCoursesVsWitoutTrainingCoursesReportObject = EmployeesWithTrainingCoursesVsWitoutTrainingCoursesReportObject;

            //EmployeesWithoutTrainingCoursesPerEmploymentTimeReport
            List<object> EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject = new List<object>();
            var EmployeesWithoutTrainingCourses = HRInfo.Where(e => e.TrainingCourses == null || e.TrainingCourses == "");

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween1and5 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween1and5.Add("country", "من سنة حتى ٥ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween1and5.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 1 && int.Parse(e.EmploymentTime) <= 5).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween6and10 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween6and10.Add("country", "من ٦ حتى ١٠ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween6and10.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 6 && int.Parse(e.EmploymentTime) <= 10).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween11and15 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween11and15.Add("country", "من ١١ حتى ١٥ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween11and15.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 11 && int.Parse(e.EmploymentTime) <= 15).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween16and20 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween16and20.Add("country", "من ١٦ حتى ٢٠ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween16and20.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 16 && int.Parse(e.EmploymentTime) <= 20).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween21and25 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween21and25.Add("country", "من ٢١ حتى ٢٥ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween21and25.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 21 && int.Parse(e.EmploymentTime) <= 25).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween26and30 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween26and30.Add("country", "من ٢٦ حتى ٣٠ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween26and30.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 26 && int.Parse(e.EmploymentTime) <= 30).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween31and35 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween31and35.Add("country", "من ٣١ حتى ٣٥ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween31and35.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 31 && int.Parse(e.EmploymentTime) <= 35).ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween36and40 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween36and40.Add("country", "من ٣٦ حتى ٤٠ سنوات");
            EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween36and40.Add("litres", EmployeesWithoutTrainingCourses.Count(e => int.Parse(e.EmploymentTime) >= 36 && int.Parse(e.EmploymentTime) <= 40).ToString());


            
            if (EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween1and5.LastOrDefault().Value!="0" || EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween6and10.LastOrDefault().Value != "0"|| EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween11and15.LastOrDefault().Value != "0"
                || EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween16and20.LastOrDefault().Value != "0"|| EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween21and25.LastOrDefault().Value != "0"|| EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween26and30.LastOrDefault().Value != "0"
                || EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween31and35.LastOrDefault().Value != "0"|| EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween36and40.LastOrDefault().Value != "0")
            {
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween1and5);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween6and10);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween11and15);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween16and20);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween21and25);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween26and30);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween31and35);
                EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject.Add(EmployeesWithoutTrainingCoursesPerEmploymentTimeBetween36and40);
            }

            ViewBag.EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject = EmployeesWithoutTrainingCoursesPerEmploymentTimeReportObject;

            //EmployeesWithoutTrainingCoursesSlicesReport
            List<object> EmployeesWithoutTrainingCoursesSlicesReportObject = new List<object>();
            var EmployeesWithoutTrainingCoursesSlice1Count = EmployeesWithoutTrainingCourses.Count(e => e.Slice == "الأولى");
            var EmployeesWithoutTrainingCoursesSlice2Count = EmployeesWithoutTrainingCourses.Count(e => e.Slice == "الثانية");
            var EmployeesWithoutTrainingCoursesSliceOthersCount = EmployeesWithoutTrainingCourses.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى" && e.Slice != null);

            Dictionary <string, string> EmployeesWithoutTrainingCoursesSlice1 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesSlice1.Add("country", "الفئة الأولى");
            EmployeesWithoutTrainingCoursesSlice1.Add("litres", EmployeesWithoutTrainingCoursesSlice1Count.ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesSlice2 = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesSlice2.Add("country", "الفئة الثانية");
            EmployeesWithoutTrainingCoursesSlice2.Add("litres", EmployeesWithoutTrainingCoursesSlice2Count.ToString());

            Dictionary<string, string> EmployeesWithoutTrainingCoursesSliceOthers = new Dictionary<string, string>();
            EmployeesWithoutTrainingCoursesSliceOthers.Add("country", "باقي الفئات");
            EmployeesWithoutTrainingCoursesSliceOthers.Add("litres", EmployeesWithoutTrainingCoursesSliceOthersCount.ToString());

            if(EmployeesWithoutTrainingCoursesSlice1Count!=0 || EmployeesWithoutTrainingCoursesSlice2Count!=0 || EmployeesWithoutTrainingCoursesSliceOthersCount != 0)
            {
                EmployeesWithoutTrainingCoursesSlicesReportObject.Add(EmployeesWithoutTrainingCoursesSlice1);
                EmployeesWithoutTrainingCoursesSlicesReportObject.Add(EmployeesWithoutTrainingCoursesSlice2);
                EmployeesWithoutTrainingCoursesSlicesReportObject.Add(EmployeesWithoutTrainingCoursesSliceOthers);
            }
            
            ViewBag.EmployeesWithoutTrainingCoursesSlicesReportObject = EmployeesWithoutTrainingCoursesSlicesReportObject;

            //UnEmployedEmployeesReport
            List<object> UnEmployedEmployeesReportObject = new List<object>();
            var EmployeesWithoutPreviousEmployments = HRInfo.Where(e => e.PreviousEmployments == null || e.PreviousEmployments == "");
            var EmployeesWithoutPreviousEmploymentsAndWithTrainingCourses = EmployeesWithoutPreviousEmployments.Where(e => e.TrainingCourses != null && e.TrainingCourses != "");
            var EmployeesWithoutPreviousEmploymentsAndWithTrainingCoursesAndWantToContinueEducation = EmployeesWithoutPreviousEmploymentsAndWithTrainingCourses.Count(e => e.ContinueEducation == "نعم");

            Dictionary<string, string> UnEmployedEmployeesReportDict = new Dictionary<string, string>();
            UnEmployedEmployeesReportDict.Add("year", "نسبة القوى العاملة غير المستثمرة");
            UnEmployedEmployeesReportDict.Add("income", HRInfo.Count.ToString());
            UnEmployedEmployeesReportDict.Add("expenses", EmployeesWithoutPreviousEmploymentsAndWithTrainingCoursesAndWantToContinueEducation.ToString());

            UnEmployedEmployeesReportObject.Add(UnEmployedEmployeesReportDict);

            ViewBag.UnEmployedEmployeesReportObject = UnEmployedEmployeesReportObject;

            //EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducation
            List<object> EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducation = new List<object>();
            var EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCourses = EmployeesWithoutPreviousEmployments.Where(e => e.TrainingCourses == null || e.TrainingCourses == "");
            var EmployeesWithoutPreviousEmploymentsAndWithTrainingCoursesAndDoNotWantToContinueEducation = EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCourses.Count(e => e.ContinueEducation == "لا");

            Dictionary<string, string> EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducationDict = new Dictionary<string, string>();
            EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducationDict.Add("year", "نسبة القوى العاملة المترهلة");
            EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducationDict.Add("income", HRInfo.Count.ToString());
            EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducationDict.Add("expenses", EmployeesWithoutPreviousEmploymentsAndWithTrainingCoursesAndDoNotWantToContinueEducation.ToString());

            EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducation.Add(EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducationDict);

            ViewBag.EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducation = EmployeesWithoutPreviousEmploymentsAndWithoutTrainingCoursesAndDoNotWantToContinueEducation;

            //EmployeesRetiredAfter5YearsFromNowReport
            List<object> EmployeesRetiredAfter5YearsFromNowReportObject = new List<object>();
            var EmployeesRetiredAfter5YearsBecauseOfAge60Count = HRInfo.Count(e => (DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2])) + 5 >= 60);
            var EmployeesRetiredAfter5YearsBecause28EmploymentYearsCount = HRInfo.Count(e => int.Parse(e.EmploymentTime) + 5 >= 28);

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecauseOfAge60Dict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecauseOfAge60Dict.Add("year", "عند عمر الستين");
            EmployeesRetiredAfter5YearsBecauseOfAge60Dict.Add("income", HRInfo.Count.ToString());
            EmployeesRetiredAfter5YearsBecauseOfAge60Dict.Add("expenses", EmployeesRetiredAfter5YearsBecauseOfAge60Count.ToString());

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecause28EmploymentYearsDict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsDict.Add("year", "بعد ٢٨ سنة خدمة");
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsDict.Add("income", HRInfo.Count.ToString());
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsDict.Add("expenses", EmployeesRetiredAfter5YearsBecause28EmploymentYearsCount.ToString());

            EmployeesRetiredAfter5YearsFromNowReportObject.Add(EmployeesRetiredAfter5YearsBecauseOfAge60Dict);
            EmployeesRetiredAfter5YearsFromNowReportObject.Add(EmployeesRetiredAfter5YearsBecause28EmploymentYearsDict);

            ViewBag.EmployeesRetiredAfter5YearsFromNowReportObject = EmployeesRetiredAfter5YearsFromNowReportObject;

            //EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport
            List<object> EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport = new List<object>();
            var EmployeesRetiredAfter5YearsBecauseOfAge60 = HRInfo.Where(e => (DateTime.Now.Year - int.Parse(e.BirthDay.Split('-')[2])) + 5 >= 60);          

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice1Dict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice1Dict.Add("year", "الفئة الأولى");
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice1Dict.Add("income", HRInfo.Count(e => e.Slice == "الأولى").ToString());
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice1Dict.Add("expenses", EmployeesRetiredAfter5YearsBecauseOfAge60.Count(e => e.Slice == "الأولى").ToString());

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice2Dict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice2Dict.Add("year", "الفئة الثانية");
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice2Dict.Add("income", HRInfo.Count(e=>e.Slice== "الثانية").ToString());
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice2Dict.Add("expenses", EmployeesRetiredAfter5YearsBecauseOfAge60.Count(e => e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecauseOfAge60PerSliceOthersDict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSliceOthersDict.Add("year", "باقي الفئات");
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSliceOthersDict.Add("income", HRInfo.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى").ToString());
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSliceOthersDict.Add("expenses", EmployeesRetiredAfter5YearsBecauseOfAge60.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى" && e.Slice != null).ToString());

            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport.Add(EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice1Dict);
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport.Add(EmployeesRetiredAfter5YearsBecauseOfAge60PerSlice2Dict);
            EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport.Add(EmployeesRetiredAfter5YearsBecauseOfAge60PerSliceOthersDict);

            ViewBag.EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport = EmployeesRetiredAfter5YearsBecauseOfAge60PerSlicesReport;

            //EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReport
            List<object> EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReportReport = new List<object>();
            var EmployeesRetiredAfter5YearsBecause28EmploymentYears = HRInfo.Where(e => int.Parse(e.EmploymentTime) + 5 >= 28);

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice1Dict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice1Dict.Add("year", "الفئة الأولى");
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice1Dict.Add("income", HRInfo.Count(e => e.Slice == "الأولى").ToString());
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice1Dict.Add("expenses", EmployeesRetiredAfter5YearsBecause28EmploymentYears.Count(e => e.Slice == "الأولى").ToString());

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice2Dict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice2Dict.Add("year", "الفئة الثانية");
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice2Dict.Add("income", HRInfo.Count(e => e.Slice == "الثانية").ToString());
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice2Dict.Add("expenses", EmployeesRetiredAfter5YearsBecause28EmploymentYears.Count(e => e.Slice == "الثانية").ToString());

            Dictionary<string, string> EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSliceOthersDict = new Dictionary<string, string>();
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSliceOthersDict.Add("year", "باقي الفئات");
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSliceOthersDict.Add("income", HRInfo.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى").ToString());
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSliceOthersDict.Add("expenses", EmployeesRetiredAfter5YearsBecause28EmploymentYears.Count(e => e.Slice != "الثانية" && e.Slice != "الأولى" && e.Slice!=null).ToString());

            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReportReport.Add(EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice1Dict);
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReportReport.Add(EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlice2Dict);
            EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReportReport.Add(EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSliceOthersDict);

            ViewBag.EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReportReport = EmployeesRetiredAfter5YearsBecause28EmploymentYearsPerSlicesReportReport;

            //WomenPerEmployeesReport
            List<object> WomenPerEmployeesReportObject = new List<object>();
            var WomenEmployees = HRInfo.Where(e => e.Sex == "أنثى");

            Dictionary<string, string> WomenPerEmployeesReportDict = new Dictionary<string, string>();
            WomenPerEmployeesReportDict.Add("year", "عدد الإناث بالنسبة للعد الكلي للقوى العاملة");
            WomenPerEmployeesReportDict.Add("income", HRInfo.Count.ToString());
            WomenPerEmployeesReportDict.Add("expenses", WomenEmployees.Count().ToString());

            WomenPerEmployeesReportObject.Add(WomenPerEmployeesReportDict);

            ViewBag.WomenPerEmployeesReportObject = WomenPerEmployeesReportObject;

            //WomenPerEmployeesPerTitleReport
            List<object> WomenPerEmployeesPerTitleReportObject = new List<object>();
            var WomenWithSpecialTitle = WomenEmployees.Count(e => e.EmploymentTitle=="مدير" || e.EmploymentTitle== "رئيس دائرة" || e.EmploymentTitle == "مدير- رئيس دائرة");
            var MenEmployees = HRInfo.Where(e => e.Sex == "ذكر");
            var MenWithSpecialTitle = MenEmployees.Count(e => e.EmploymentTitle == "مدير" || e.EmploymentTitle == "رئيس دائرة" || e.EmploymentTitle== "مدير- رئيس دائرة");

            Dictionary<string, string> WomenWithSpecialTitleDict = new Dictionary<string, string>();
            WomenWithSpecialTitleDict.Add("country", "أنثى");
            WomenWithSpecialTitleDict.Add("litres", WomenWithSpecialTitle.ToString());

            Dictionary<string, string> MenWithSpecialTitleDict = new Dictionary<string, string>();
            MenWithSpecialTitleDict.Add("country", "ذكر");
            MenWithSpecialTitleDict.Add("litres", MenWithSpecialTitle.ToString());

            if(WomenWithSpecialTitle!=0 || MenWithSpecialTitle != 0)
            {
                WomenPerEmployeesPerTitleReportObject.Add(WomenWithSpecialTitleDict);
                WomenPerEmployeesPerTitleReportObject.Add(MenWithSpecialTitleDict);
            }

            ViewBag.WomenPerEmployeesPerTitleReportObject = WomenPerEmployeesPerTitleReportObject;
            return View();
        }

         public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string OrganisationKey = null, string OPManagerRoleGroupKey = null)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();
                }
            }
            return result;
        }

        public async Task<List<GetHRInfo_Result>> GetHRInfo(string IdOrg=null)
        {
            List<GetHRInfo_Result> HRInfo = new List<GetHRInfo_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetHRInfo");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("IdOrg={0}", IdOrg);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    HRInfo = await response.Content.ReadAsAsync<List<GetHRInfo_Result>>();
                }
            }
            return HRInfo;
        }


        public static async Task<List<ERMEmployees>> GetERMEmployees()
        {
#if DEBUG
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
#endif

            string result = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.ERMAPI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Employees/GetEmployeesRecords/");
                //requestUriBuilder.Append("?");
                //requestUriBuilder.AppendFormat("test={0}", "105");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            List<ERMEmployees> ERMEmployeesResult = JsonConvert.DeserializeObject<List<ERMEmployees>>(result);
            return ERMEmployeesResult;
        }

        [HttpPost]
        public async Task<ActionResult> SearchEmployees(int? personalNumber = null, string firstName = null, string surName = null, string fatherName = null, string motherName = null)
        {

#if DEBUG
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
#endif

            string result = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.ERMAPI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Employees/SearchEmployeesRecords/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("firstName={0}", firstName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("surName={0}", surName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("fatherName={0}", fatherName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("motherName={0}", motherName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("personalNumber={0}", personalNumber);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            List<ERMEmployees> ERMEmployeesResult = JsonConvert.DeserializeObject<List<ERMEmployees>>(result);
            ViewData["EmployeesData"]= ERMEmployeesResult;
            return PartialView("_EmployeesTable");
        }

        [HttpGet]
        public async Task<JsonResult> GetEmployeeById(int? employeeRecord = null)
        {

#if DEBUG
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
#endif

            string result = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.ERMAPI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Employees/GetEmployeesRecord/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("id={0}", employeeRecord);               

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
            }

           ERMEmployees ERMEmployeesResult = JsonConvert.DeserializeObject<ERMEmployees>(result);

            return Json(ERMEmployeesResult, JsonRequestBehavior.AllowGet);
        }
    }
}