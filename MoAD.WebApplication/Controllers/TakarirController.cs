﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MoAD.WebApplication.Controllers
{
    
    public class TakarirController : Controller
    {
        // GET: Takarir
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public async Task<ActionResult> AlTakarir(string Description=null,string FormTemplateKey=null,string HighlightedPart=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;

            if (Description == null)
            {
                ViewBag.Description = "NotPublish";
            }

            ViewBag.FormTemplateKey = FormTemplateKey;
            return View();
        }



        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Index(string OrganizationKey = null, string FormTemplateKey = null, string ManagerIndex = null, string HighlightedPart = null,string TakarirLevel=null)
        {
            ViewBag.TakarirLevel = TakarirLevel;
            if(TakarirLevel == "مستوى الوزارة")
            {
                ViewBag.LevelType = "Parent";
            }
            else if (TakarirLevel == "مستوى الإقليمي")
            {
                ViewBag.LevelType = "Province";
            }
            else
            {
                ViewBag.LevelType = "Organisation";
            }
            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;

            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();

            List<GetUserInformation_Result> UserInformation = new List<GetUserInformation_Result>();
            string OrganisationKey = "";
            UserInformation = await GetUserInformation();
            OrganisationKey = UserInformation.FirstOrDefault().OrganisationKey.ToString();

            if(OrganizationKey=="NULL")
            ViewBag.OrganisationKey = OrganizationKey;
            else
                ViewBag.OrganisationKey = OrganisationKey;

        
            string TemplateKey = SafeSqlLiteral(FormTemplateKey);
            //string TemplateKey = FormTemplateKey;

            //string TemplateKey = "106";
            string FormTemplateTitle = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //result.First().FormTemplateName
                    FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),


                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        AnswerTemplateDependency = ans.AnswerTemplateDependency.ToString(),
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementKey = ans.FormElementKey.ToString(),
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode,
                                        SqlQueries = ans.SqlQueries,
                                        SqlParameters = ans.SqlParameters,
                                        ColumnValue = ans.ColumnValue,
                                        FormTemplateName = ans.FormTemplateName

                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }
            List<GetUserInformation_Result> UserInfo = GetUserInformation().Result;


            ViewBag.Email = UserInfo.First().Email;
            ViewBag.PhoneNumber = UserInfo.First().PhoneNumber;
            ViewBag.UserName = UserInfo.First().UserName;
            ViewBag.FirstName = UserInfo.First().FirstName;
            ViewBag.FatherName = UserInfo.First().FatherName;
            ViewBag.LastName = UserInfo.First().LastName;
            ViewBag.FormTemplateKey = FormTemplateKey;
            ViewBag.FormTemplateTitle = FormTemplateTitle;
            ViewBag.ManagerIndex = ManagerIndex;


            string FormTemplateType = GetTypeForaFormTemplate(FormTemplateKey).Result;
            

            ViewBag.FormTemplateType = FormTemplateType;
            ViewBag.FormTemplateKey = FormTemplateKey;

            ViewBag.HighlightedPart = HighlightedPart;
            return View(nestedResult);
        }


        public async Task<JsonResult> ReportingClaimStatusesPerDateRanges(string startPeriod, string endPeriod, string startPeriod2, string endPeriod2, string organisationKey,string LevelType)
        {
            

            List<ReportingClaimStatusesPerDateRanges_Result> result = new List<ReportingClaimStatusesPerDateRanges_Result>();
            List<ReportingClaimStatusesPerDateRanges_Result> result2 = new List<ReportingClaimStatusesPerDateRanges_Result>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = organisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = organisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = organisationKey;
            }


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ClaimStatusesPerDateRanges/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("startPeriod={0}", startPeriod);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("endPeriod={0}", endPeriod);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentorgkey={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Province={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingClaimStatusesPerDateRanges_Result>>();

                }
            }


            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

            //    StringBuilder requestUriBuilder = new StringBuilder();
            //    requestUriBuilder.Append("api/Forms/ReportingClaimStatusesPerDateRanges/");
            //    requestUriBuilder.Append("?");
            //    requestUriBuilder.AppendFormat("startPeriod={0}", startPeriod2);
            //    requestUriBuilder.Append("&");
            //    requestUriBuilder.AppendFormat("endPeriod={0}", endPeriod2);
            //    requestUriBuilder.Append("&");
            //    requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
            //    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
            //    response.EnsureSuccessStatusCode();
            //    if (response.IsSuccessStatusCode)
            //    {
            //        result2 = await response.Content.ReadAsAsync<List<ReportingClaimStatusesPerDateRanges_Result>>();

            //    }
            //}
            List<List<string>> TitlesData = new List<List<string>>();

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.StatusValue);
                data.Add(item.ClaimsCount.ToString());
                TitlesData.Add(data);

                generalData.Add(data);
            }

            //List<List<string>> generalData2 = new List<List<string>>();
            //if (result2.Count == 0)
            //{
            //    List<string> data = new List<string>();
            //    data.Add("لا يوجد معلومات ");
            //    data.Add("100");
            //    generalData2.Add(data);
            //}

            //foreach (var item in result2)
            //{
            //    List<string> data = new List<string>();

            //    data.Add(item.StatusValue);
            //    data.Add(item.ClaimsCount.ToString());
            //    TitlesData.Add(data);
            //    generalData2.Add(data);
            //}






            // List<object> FinalResult = new List<object>();


            



            return Json(generalData, JsonRequestBehavior.AllowGet);
        }



        public async Task<JsonResult> ReportingGovernanceResponse(string OrganisationKey,string LevelType)
        {
            List<ReportingGovernanceResponse_Result> result = new List<ReportingGovernanceResponse_Result>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGovernanceResponse/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("OrgKey={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentKey={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Prov={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGovernanceResponse_Result>>();

                }
            }

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.Value);
                data.Add(item.Count.ToString());

                generalData.Add(data);
            }

            //List<object> Report1Json = new List<object>();
            //foreach (var item in generalData)
            //{
            //    Dictionary<string, string> ItemDict = new Dictionary<string, string>();
            //    ItemDict.Add("country", item[0].ToString());
            //    ItemDict.Add("litres", item[1].ToString());

            //    Report1Json.Add(ItemDict);
            //}
            return Json(generalData, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Credibility(string OrganisationKey, string LevelType)
        {
            List<Credibility_Result> result = new List<Credibility_Result>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/Credibility/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("sation={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("orgParent={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("vince={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<Credibility_Result>>();

                }
            }

            //List<List<string>> generalData = new List<List<string>>();
            //if (result.Count == 0)
            //{
            //    List<string> data = new List<string>();
            //    data.Add("لا يوجد معلومات ");
            //    data.Add("100");
            //    generalData.Add(data);
            //}
            //foreach (var item in result)
            //{
            //    List<string> data = new List<string>();

            //    data.Add(item.CountQuestions.ToString());
            //    data.Add(item.CountForms.ToString());

            //    generalData.Add(data);
            //}

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Honnesty(string OrganisationKey, string LevelType)
        {
            List<Honnesty_Result> result = new List<Honnesty_Result>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/Honnesty/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("t={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("h={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("p={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<Honnesty_Result>>();

                }
            }

            //List<List<string>> generalData = new List<List<string>>();
            //if (result.Count == 0)
            //{
            //    List<string> data = new List<string>();
            //    data.Add("لا يوجد معلومات ");
            //    data.Add("100");
            //    generalData.Add(data);
            //}
            //foreach (var item in result)
            //{
            //    List<string> data = new List<string>();

            //    data.Add(item.TrappedFormsPercentage.ToString());
            //    data.Add(item.CountTrappedForms.ToString());

            //    generalData.Add(data);
            //}

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ReportingTa7sinElKhadamatPlot(string OrganisationKey = null, string LevelType = null)
        {
            List<ReportingTa7sinElKhadamat_Result> result = new List<ReportingTa7sinElKhadamat_Result>();
            List<string> ClaimsType = new List<string>();
            List<int?> ClaimsCount = new List<int?>();

            List<object> ReportingClaimsType = new List<object>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingTa7sinElKhadamat/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test9={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test10={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test11={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingTa7sinElKhadamat_Result>>();

                    ClaimsType = result.Select(x => x.Value).ToList();
                    ClaimsCount = result.Select(x => x.Evaluations).ToList();
                }
            }
            ReportingClaimsType.Add(ClaimsType);
            ReportingClaimsType.Add(ClaimsCount);

            return Json(ReportingClaimsType, JsonRequestBehavior.AllowGet);
        }



        public async Task<JsonResult> ReportingGetWeakQuestionsPerSlice(string SliceLevel = null, string OrganisationKey = null,string LevelType=null, string COUNT = null, string QuestionWeakness = null)
        {
            List<ReportingGetWeakQuestionsPerSlice_Result> result = new List<ReportingGetWeakQuestionsPerSlice_Result>();
            List<string> ClaimsType = new List<string>();
            List<int?> ClaimsCount = new List<int?>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }
            List<object> ReportingClaimsType = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetWeakQuestionsPerSlice/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("SliceLevel={0}", SliceLevel);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IdOrganKey={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IdOrgParent={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Prvce={0}", Province);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("COUNT={0}", COUNT);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("QuestionWeakness={0}", QuestionWeakness);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetWeakQuestionsPerSlice_Result>>();

                    //ClaimsType = result.Select(x => x.Value).ToList();
                    //ClaimsCount = result.Select(x => x.Evaluations).ToList();
                }
            }
            //ReportingClaimsType.Add(ClaimsType);
            //ReportingClaimsType.Add(ClaimsCount);

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> ReportingGetSatisfactionCitizenPerQuestionPlot(string Organisations = null,string LevelType=null, string Column1 = null, string Column2 = null)
        {
            List<ReportingGetSatisfactionCitizenPerQuestion_Result> result = new List<ReportingGetSatisfactionCitizenPerQuestion_Result>();
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();

            List<object> BarCharResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetSatisfactionCitizenPerQuestion/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("oKEY={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("LevelType={0}", LevelType);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetSatisfactionCitizenPerQuestion_Result>>();
                    Name = result.Select(x => x.SliceName).ToList();
                    Count = result.Select(x => x.AVR).ToList();
                }
            }


            BarCharResult.Add(Name);
            BarCharResult.Add(Count);

            //PieChartResult.Add(Name);
            //PieChartResult.Add(Count);


            if (Column1 != null)
            {
                return Json(BarCharResult, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);

            }


        }
        public async Task<JsonResult> ReportingGetSatisfactionCitizen(string Level = null, string Organisations = null,string LevelType=null, string Column1 = null, string Column2 = null)
        {
            List<ReportingGetSatisfactionCitizen_Result> result = new List<ReportingGetSatisfactionCitizen_Result>();
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();

            List<object> PieChartResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetSatisfactionCitizen/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Level={0}", Level);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrganisationKey={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("LevelType={0}", LevelType);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetSatisfactionCitizen_Result>>();

                    //Name = result.Select(x => x.SliceName).ToList();
                    //Count = result.Select(x => x.AVRSatisfactionGeneral).ToList();
                }
            }

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.AnswerName);
                data.Add(item.AVR.ToString());

                generalData.Add(data);
            }

            //PieChartResult.Add(Name);
            //PieChartResult.Add(Count);


            if (Column1 != null)
            {
                return Json(generalData, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);

            }

        }

        public async Task<string> PublishEstetla3(string formKey = null, string fileExtensionName = null, string formTemplateKey = null,string publishAction = null)
        {
            string uk = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PublishTakarir/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("fKey={0}", formKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("uk={0}", uk);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("fileExtensionName={0}", fileExtensionName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PublishStatus={0}", publishAction);
                


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }

        public async Task<string> PublishTakarir(string formKey = null,string fileExtensionName=null,string formTemplateKey=null)
        {
            string uk = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PublishTakarir/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("fKey={0}", formKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("uk={0}", uk);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("fileExtensionName={0}", fileExtensionName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }


        public async Task<JsonResult> ReportingGetSatisfactionPerSlice(string SliceLevel = null, string OrganisationKey = null, string LevelType=null, string Column1 = null, string Column2 = null)
        {
            List<ReportingGetSatisfactionPerSlice_Result> result = new List<ReportingGetSatisfactionPerSlice_Result>();
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();

            List<object> PieChartResult = new List<object>();

            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetSatisfactionPerSlice/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("sl={0}", SliceLevel);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OGkey={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PTorg={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Pv={0}", Province);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetSatisfactionPerSlice_Result>>();

                    //Name = result.Select(x => x.SliceName).ToList();
                    //Count = result.Select(x => x.AVRSatisfactionGeneral).ToList();
                }
            }

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.SliceName);
                data.Add(item.AVRSatisfactionGeneral.ToString());

                generalData.Add(data);
            }

            //PieChartResult.Add(Name);
            //PieChartResult.Add(Count);


            if (Column1 != null)
            {
                return Json(generalData, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        public async Task<JsonResult> SatisfactionPerSlicePlot(string SliceLevel = null, string OrganisationKey = null, string LevelType = null, string Column1 = null, string Column2 = null)
        {
            List<ReportingGetSatisfactionPerSlice_Result> result = new List<ReportingGetSatisfactionPerSlice_Result>();
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisation")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }
            List<object> BarCharResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetSatisfactionPerSlice/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("sl={0}", SliceLevel);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OGkey={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PTorg={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Pv={0}", Province);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetSatisfactionPerSlice_Result>>();

                    Name = result.Select(x => x.SliceName).ToList();
                    Count = result.Select(x => x.AVRSatisfactionGeneral).ToList();
                }
            }


            BarCharResult.Add(Name);
            BarCharResult.Add(Count);


            if (Column1 != null)
            {
                return Json(BarCharResult, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        public async Task<JsonResult> ReportingGetFalseFormsCount(string SliceLevel = null, string OrganisationKey = null, string LevelType = null, string Column1 = null, string Column2 = null)
        {
            int? result = 0;
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }
            List<object> BarCharResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetFalseFormsCount/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("orG={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("orgPT={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("vince={0}", Province);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<int?>();
                }
            }
            List<Dictionary<string, object>> FinalResult = new List<Dictionary<string, object>>();
            Dictionary<string, object> FinalResultf = new Dictionary<string, object>();
            FinalResultf.Add("CountFalseForms", result);
            FinalResult.Add(FinalResultf);
            return Json(FinalResult, JsonRequestBehavior.AllowGet);

        }


        public async Task<JsonResult> ReportingGetUnpreciseFormsCount(string SliceLevel = null, string OrganisationKey = null, string LevelType = null, string Column1 = null, string Column2 = null)
        {
            List< ReportingGetUnpreciseFormsCount_Result> result =new List<ReportingGetUnpreciseFormsCount_Result>();
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }
            List<object> BarCharResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetUnpreciseFormsCount/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("o={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PT={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("v={0}", Province);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetUnpreciseFormsCount_Result>>();
                }
            }
         
                return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetServicesPerOrganisation(string serviceOrganisationKey = null)
        {

            List<string> result = new List<string>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetServicesPerOrganisation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("serviceOrganisationKey={0}", serviceOrganisationKey);
                //requestUriBuilder.Append("&");
                //requestUriBuilder.AppendFormat("OPManagerRoleGroupKey={0}", OPManagerRoleGroupKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<string>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> GetAllOrganisations(string OrganisationKey = null, string OPManagerRoleGroupKey = null)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                //requestUriBuilder.Append("?");
                //requestUriBuilder.AppendFormat("OrganisationKey={0}", OrganisationKey);
                //requestUriBuilder.Append("&");
                //requestUriBuilder.AppendFormat("OPManagerRoleGroupKey={0}", OPManagerRoleGroupKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetAllProvinces(string OrganisationKey = null, string OPManagerRoleGroupKey = null)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();
            List<GetAllOrganisations_Result> FinalResult = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();
                    var test = result

              .Select(group => new
              {

                  Province = group.Province
              })
              .ToList().Distinct();
                    foreach (var item in test)
                    {
                        GetAllOrganisations_Result resultorg = new GetAllOrganisations_Result();

                        //resultorg.OrganisationKey = item.OrganisationName;
                        resultorg.Province = item.Province;

                        FinalResult.Add(resultorg);
                    }

                }
            }
            return Json(FinalResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetAllOrganisationParent(string OrganisationKey = null, string OPManagerRoleGroupKey = null)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();
            List<GetAllOrganisations_Result> FinalResult = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();
                    
                    var test = result

              .Select(group => new
              {

                 OrganisationParent = group.OrganisationParent
              })
              .ToList().Distinct();
                    foreach (var item in test)
                    {
                        GetAllOrganisations_Result resultorg = new GetAllOrganisations_Result();

                        //resultorg.OrganisationKey = item.OrganisationName;
                        resultorg.OrganisationParent = item.OrganisationParent;

                        FinalResult.Add(resultorg);
                    }

                }
            }
            return Json(FinalResult, JsonRequestBehavior.AllowGet);
        }

        private string SafeSqlLiteral(string inputSQL)
        {
            return inputSQL.Replace("'", "''");
        }

        public async Task<string> GetTypeForaFormTemplate(string formTemplateKey)
        {
            string result = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetTypeForaFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }

            return result;

        }


        public async Task<List<GetUserInformation_Result>> GetUserInformation()
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }
        [Globals.YourCustomAuthorize]
        public ActionResult TakarirTable(string ClaimType, string HighlightedPart = null, string Level = "مستوى العام")
        {

            ViewBag.HighlightedPart = HighlightedPart;
            List<GetAllOrganisations_Result> organisations = GetAllOrganisations1().Result;
            organisations = organisations.Where(x => x.OrganisationKey != 3).ToList();
            ViewData["Organisation"] = organisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            string updatedbyuserkey = User.Identity.GetUserId();
            List<GetAdManagerForms_Result> result = getAdManagerForms(updatedbyuserkey, ClaimType, 0).Result;


            ViewBag.ManagerForms = result = result.Where(r => r.FileExtensionName == Level).ToList(); ;
            ViewBag.title = ClaimType;
            ViewBag.Level = Level;
            return View();
        }


        public async Task<ActionResult> PublishedTakarir(string FormTemplateKey, string HighlightedPart = null,string Level=null,string TakeOrg=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", FormTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }

            string FormTemplateType = resultFormTemplate.FirstOrDefault().FormTemplateType;
            string updatedbyuserkey = User.Identity.GetUserId();
            List<GetAdManagerForms_Result> result = getAdManagerForms(updatedbyuserkey, FormTemplateType, 0).Result;

            if(TakeOrg!= "placeholder")
            result = result.Where(x => x.IsPublished == "Published" && x.FileExtensionName== Level && x.OrganisationKey== Int32.Parse(TakeOrg)).ToList();
            else
                result = result.Where(x => x.IsPublished == "Published" && x.FileExtensionName == Level).ToList();

            if (result.Count == 0)
            {
                return View("AlTakarir");
            }else
            {
                // TakarirView(result.FirstOrDefault().FormKey.ToString(), resultFormTemplate.FirstOrDefault().FormTemplateName);

                return RedirectToAction("TakarirView", new
                {
                    formInstanceKey = result.FirstOrDefault().FormKey.ToString(),
                    formTemplateName = resultFormTemplate.FirstOrDefault().FormTemplateName,
                   
                });
            }
            
          
         
            return View();
        }


        
        public ActionResult TakarirView(string formInstanceKey = "", string formTemplateName = "",string FileExtensionName="")
            {
            List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result.OrderBy(x => x.QuestionKeyIndex).ToList();
            ViewBag.FormValues = FormSaved;

            
            string FormTemplateKey = FormSaved.FirstOrDefault().FormTemplateKey.ToString();
            string FormtemplateType = FormSaved.FirstOrDefault().FormTemplateType;


            List <List<string>> generalData = new List<List<string>>();
            List<List<string>> generalData1 = new List<List<string>>();
            List<List<string>> generalData2 = new List<List<string>>();
            var i = 0;
            foreach (var item in FormSaved)
            {
                if (item.AnswerInstanceFileExtensionName == "PieChart")
                {
                    var objects = JsonConvert.DeserializeObject<List<object>>(item.AnswerInstanceFileExtensionSqlQueries);
                    var Data = item.AnswerInstanceFileExtensionSqlQueries;
                    foreach (var item1 in objects)
                    {
                        var test = item1.ToString();
                        var objects1 = JsonConvert.DeserializeObject<List<object>>(test);
                        List<string> data = new List<string>();
                       
                        foreach (var item2 in objects1)
                        {                           
                            data.Add(item2.ToString());                                                       
                        }
                        generalData.Add(data);

                    }
                    
                }
               
                if (item.AnswerInstanceFileExtensionName == "PieChart1")
                {
                    var objects = JsonConvert.DeserializeObject<List<object>>(item.AnswerInstanceFileExtensionSqlQueries);
                    var Data = item.AnswerInstanceFileExtensionSqlQueries;
                    foreach (var item1 in objects)
                    {
                        var test = item1.ToString();
                        var objects1 = JsonConvert.DeserializeObject<List<object>>(test);
                        List<string> data = new List<string>();

                        foreach (var item2 in objects1)
                        {
                            data.Add(item2.ToString());
                        }

                        if (i == 0)
                            generalData1.Add(data);
                        else
                            generalData2.Add(data);

                        
                    }
                    i++;
                }
            }

            ViewBag.PieChartData = generalData;
            ViewBag.PieChart1Data = generalData1;
            ViewBag.PieChart2Data = generalData2;
            ViewBag.formTemplate = formTemplateName;
            ViewBag.formInstanceKey = formInstanceKey;
            ViewBag.FormTemplateKey = FormTemplateKey;
            ViewBag.FileExtensionName = FileExtensionName;
            ViewBag.FormtemplateType = FormtemplateType;
            return View();
        }

        public async Task<List<GetSavedForm_Result>> GetSavedForm(string FormKey)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", FormKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
                }
            }
            return result;
        }


        public async Task<List<GetAdManagerForms_Result>> getAdManagerForms(string UserId, string FormTemplateType, int organisationKey)
        {
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();

            //List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
           // string UserRoleIDString = UserInfo.FirstOrDefault().RoleId.ToString();
           // int UserRoleID = Int32.Parse(UserRoleIDString);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserId={0}", UserId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserRoleID={0}", 1);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
            }
            return result;
        }


        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations1()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }



    }
}