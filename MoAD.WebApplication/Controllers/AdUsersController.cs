﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class AdUsersController : Controller
    {
        // GET: AdUsers
        public async Task<ActionResult> Index()
        {
            List<GetAllUsersPerOrganisationAndRole_Result> result = new List<GetAllUsersPerOrganisationAndRole_Result>();
            string OrganisationKey = await GetOrganisatioKey();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/test1/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test2={0}", "20");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test3={0}", OrganisationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllUsersPerOrganisationAndRole_Result>>();

                }

            }
            ViewBag.OrganisationUser = result;

            return View();
        }

        public async Task<string> GetOrganisatioKey()
        {
            string UserKey = User.Identity.GetUserId();
            GetOrganisationAndRolePerUser_Result result=null;
            string OrganisationKey;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/testOrganisationKey/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test2={0}", UserKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<GetOrganisationAndRolePerUser_Result>();
                }

            }
            OrganisationKey = result.OrganisationKey.ToString();
            return OrganisationKey;
            }

        
        }
    }