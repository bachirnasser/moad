﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using MoAD.WebApplication.Objects;
using System.Data.Entity;

namespace MoAD.WebApplication.Controllers
{
    public class SurveyReportController : BaseController
    {
        private Entities db = new Entities();
        // GET: SurveyReport
        public async Task<ActionResult> Index(string FormTemKey)
        {
            ViewBag.PageName = "Manbar";
            ViewBag.Estetla3Ra2iPublishedForms = new List<GetAdManagerForms_Result>();
            List<GetSavedForm_Result> SavedForms = await GetSavedForm(FormTemKey);

            ViewBag.Answers = SavedForms.FirstOrDefault().AnswerInstanceFileExtensionSqlQueries;
            List<GetSurveyResult_Result> SurveyResult = new List<GetSurveyResult_Result>();
            SurveyResult = await GetSurveyResult(SavedForms.FirstOrDefault().AnswerInstanceFileExtensionqlParameters);
            if(SurveyResult.FirstOrDefault().FormTemplate== "إستطلاع رأي مركز")
            {
                List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
                List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
                List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
                foreach (var item in Estetla3Ra2iPublishedForms)
                {
                    //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                    if (item.FileType == "إستطلاع رأي مركز")
                    {
                        Estetla3Ra2iPublishedFormsForManbar.Add(item);
                    }
                }
                ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
                ViewBag.PageName = "Markaz";
            }else
            {
                /*Updated By DN*/
                List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
                List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
                List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
                foreach (var item in Estetla3Ra2iPublishedForms)
                {
                    //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                    if (item.FileType == "إستطلاع رأي منبر")
                    {
                        Estetla3Ra2iPublishedFormsForManbar.Add(item);
                    }
                }
                ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
                /*Updated By DN*/
            }
            var SurveyQuestions = SurveyResult.GroupBy(s => s.QuestionKey).Select(f => f.FirstOrDefault()).ToList();
            ViewBag.SurveyQuestions = SurveyQuestions;
            ViewBag.TitlePage = SavedForms.FirstOrDefault().AnswerInstanceFileExtensionColumnValue;
            ViewBag.FormInstanceKey = FormTemKey;
            ViewBag.FormTemplateName = SavedForms.FirstOrDefault().FormTemplateName;
            ViewBag.FormTemplateType = SavedForms.FirstOrDefault().FormTemplateType;
            ViewBag.FormTemplateKey = SavedForms.FirstOrDefault().FormTemplateKey;
            return View();
        }

        public async Task<ActionResult> AlSurvey(string type="Survey",string ClaimType = "تقارير إستطلاعات الرأي", string HighlightedPart = null)
        {
            ViewBag.Title = "نتائج إستطلاعات الرأي";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.HighlightedPart = HighlightedPart;
            List<GetAllFormTemplatesForaType_Result> AllSurveyFormTemplate = new List<GetAllFormTemplatesForaType_Result>();
            AllSurveyFormTemplate = await FormTemplatesDisplayController.GetAllFormTemplatesForaType(type);

            List<GetAllFormTemplatesForaType_Result> AllSurveyFormTemplateParent = new List<GetAllFormTemplatesForaType_Result>();
            AllSurveyFormTemplateParent = await FormTemplatesDisplayController.GetAllFormTemplatesForaType(ClaimType);

            ViewBag.ParentFormTemplateKey = AllSurveyFormTemplateParent.FirstOrDefault().FormTemplateKey;
   

            ViewBag.AllSurveyFormTemplate = AllSurveyFormTemplate;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }
        

        public static async Task<List<GetFormTemplate_Result>> GetFormTemplate(string TemplateKey = null)
        {
            List<GetFormTemplate_Result> SurveyResult = new List<GetFormTemplate_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    SurveyResult = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                }
            }
            return SurveyResult;
        }

        //public async Task<List<ReportingGetSurveyResults_Result>> GetSurvey(string key = null)
        //{
        //    List<ReportingGetSurveyResults_Result> SurveyResult = new List<ReportingGetSurveyResults_Result>();
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/GetSurveyResult/");
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("FormTemKey={0}", key);
        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            SurveyResult = await response.Content.ReadAsAsync<List<ReportingGetSurveyResults_Result>>();
        //        }
        //    }
        //    return SurveyResult;
        //}

        public async Task<ActionResult> SurveyTable(string ClaimType = null, string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            List<GetAllFormTemplatesForaType_Result> AllSurveyFormTemplate = new List<GetAllFormTemplatesForaType_Result>();
            AllSurveyFormTemplate = await FormTemplatesDisplayController.GetAllFormTemplatesForaType(ClaimType);

            List<GetAdManagerForms_Result> SavedForms = await getAdManagerForms(User.Identity.GetUserId(), ClaimType,0);
            ViewBag.AllSurveyFormTemplate = AllSurveyFormTemplate;
            ViewBag.title = ClaimType;
            ViewBag.FormTemplateKey = AllSurveyFormTemplate.FirstOrDefault().FormTemplateKey;
            ViewBag.SavedForms = SavedForms;
            return View();
        }

        public async Task<ActionResult> SurveyView(string FormTemplateKey = null,string ParentFormTemplateKey=null)
        {
            ViewBag.Title = "نتائج إستطلاعات الرأي";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", ParentFormTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }
            ViewBag.AnswerTemplateKey = resultFormTemplate.FirstOrDefault().AnswerTemplateKey.ToString();
            ViewBag.ParentFormTemplateName = resultFormTemplate.FirstOrDefault().FormTemplateName;

            List<GetSurveyResult_Result> SurveyResult = new List<GetSurveyResult_Result>();
            SurveyResult = await GetSurveyResult(FormTemplateKey);
         
            var SurveyQuestions= SurveyResult.GroupBy(s => s.QuestionKey).Select(f => f.FirstOrDefault()).ToList();

            List<object> FinalSurveyReport = new List<object>();
            int? TotalUsers = 0;

            foreach (var item in SurveyQuestions)
            {
                List<object> ReportSurevy = new List<object>();
              
                foreach (var innerItem in SurveyResult.Where(s=>s.QuestionKey==item.QuestionKey))
                {
                    Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                    ItemDict.Add("country", innerItem.AnswerTemplate);
                    ItemDict.Add("litres", innerItem.CountItems);
                    ReportSurevy.Add(ItemDict);

                    TotalUsers += innerItem.CountItems;
                }
                FinalSurveyReport.Add(ReportSurevy);
            }

            ViewBag.TotalUsers = TotalUsers / SurveyQuestions.Count;
            ViewBag.FinalSurveyReport = FinalSurveyReport;
            ViewBag.SurveyQuestions = SurveyQuestions;
            ViewBag.formTemplateName = SurveyResult.FirstOrDefault().FormTemplate;
            ViewBag.FormTemplateKey = FormTemplateKey;
            ViewBag.ParentFormTemplateKey = ParentFormTemplateKey;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }
        public async Task<List<GetSurveyResult_Result>> GetSurveyResult(string idTemplate=null)
        {
            List<GetSurveyResult_Result> result = new List<GetSurveyResult_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSurveyResult");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("keyFormTemplate={0}", idTemplate);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSurveyResult_Result>>();
                }
            }
            return result;
        }

        public async Task<List<GetSavedForm_Result>> GetSavedForm(string FormKey)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", FormKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
                }
            }
            return result;
        }


        public async Task<List<GetAdManagerForms_Result>> getAdManagerForms(string UserId, string FormTemplateType, int organisationKey)
        {
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserId={0}", UserId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserRoleID={0}", 1);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
            }
            return result;
        }

    }

}