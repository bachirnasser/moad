﻿using Microsoft.AspNet.Identity;
//using MoAD.DataInterface.EDMX;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MoAD.WebApplication.Controllers
{

    public class UserDashboardController : Controller
    {
        [Globals.YourCustomAuthorize]
        // GET: UserDashboard
        public async Task<ActionResult> Index(string OrganizationKey = "null")
        {
            var userID = string.Empty;

            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            result = result.Where(x => x.OrganisationKey != 3).ToList();
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            if (User.IsInRole("Citizen"))
            {
                userID = User.Identity.GetUserId();
            }
            else
            {
                userID = User.Identity.GetUserId();
            }

            List<GetUserInformation_Result> UserInfoResult = new List<GetUserInformation_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    UserInfoResult = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                    ViewBag.UserInfo = UserInfoResult.FirstOrDefault();
                    ViewBag.OrganisationKey = UserInfoResult.FirstOrDefault().OrganisationKey;
                    ViewBag.UserImage = UserInfoResult.FirstOrDefault().UserImage;
                }
            }
            List<GetAllFormsOfAUser_Result> formsPerUser = new List<GetAllFormsOfAUser_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllFormTemplatesForaUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    ViewData["formsPerUser"] = await response.Content.ReadAsAsync<List<GetAllFormsOfAUser_Result>>();
                }
            }


            //////


            List<ReportingOverAllHistory_Result> ReportingOverAllHistoryResult = new List<ReportingOverAllHistory_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetOverAllHistory/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("OrganisationKey={0}", "3");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    ReportingOverAllHistoryResult = await response.Content.ReadAsAsync<List<ReportingOverAllHistory_Result>>();
                    ViewBag.ReportingOverAllHistoryResult = ReportingOverAllHistoryResult;
                }
            }


            ViewBag.GeneralPieChart = ReportingGeneralPieChart(null, User.Identity.GetUserId().ToString()).Result;
            ViewBag.PieChart1 = ReportingGeneralPieChart(UserInfoResult.FirstOrDefault().OrganisationKey.ToString(), null).Result;
            ViewBag.PieChart2 = ReportingDetailedPieChart(null, User.Identity.GetUserId().ToString()).Result;
            ViewBag.PieChart3 = ReportingDetailedPieChart(UserInfoResult.FirstOrDefault().OrganisationKey.ToString(), null).Result;
            ViewBag.PieChart4 = RepoertingRepeatedPieChart().Result;





            InstancePerUser F = new InstancePerUser();
            F.listOfForms = GetWorkFlowPerUser(userID, "null", OrganizationKey).Result;
            return View(F);
        }
        public async Task<ActionResult> PBMenu()
        {

            return PartialView();
        }


        public async Task<List<GetWorkFlowItemInstancePerUser_Result>> GetWorkFlowPerUser(string userID, string guid, string OrganisationKey)
        {
            List<GetWorkFlowItemInstancePerUser_Result> result = new List<GetWorkFlowItemInstancePerUser_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetWorkFlowPerUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", userID);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("guid={0}", guid);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrganisationKey={0}", OrganisationKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetWorkFlowItemInstancePerUser_Result>>();
                }
            }

            result.Reverse();
            result = result.Where(x => x.CurrentFormtemplateKey == 106 || x.CurrentFormtemplateKey==110 || x.CurrentFormtemplateKey == 109 || x.CurrentFormtemplateKey == 111).ToList();

            return result;

        }


        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        public async Task<List<List<string>>> ReportingGeneralPieChart(string nextExtendedOrganisationKey,string assignedToUserKey)
        {
            List<ReportingGeneralPieChart_Result> result = new List<ReportingGeneralPieChart_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGeneralPieChart/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("OrgKey={0}", nextExtendedOrganisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserKey={0}", assignedToUserKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGeneralPieChart_Result>>();
                }
            }


            //Dictionary<string, decimal?> GeneralPieChartValues = new Dictionary<string, decimal?>();
            List<List<string>> generalData = new List<List<string>>();

            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data =new List<string>();

                data.Add(item.ParentStatus);
                data.Add(item.StatusCount.ToString());

                generalData.Add(data);
            }
            return generalData;
        }


        public async Task<JsonResult> ReportingGeneralPieChartJson(string nextExtendedOrganisationKey, string assignedToUserKey)
        {
            List<ReportingGeneralPieChart_Result> result = new List<ReportingGeneralPieChart_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGeneralPieChart/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("OrgKey={0}", nextExtendedOrganisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserKey={0}", assignedToUserKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGeneralPieChart_Result>>();
                }
            }


            //Dictionary<string, decimal?> GeneralPieChartValues = new Dictionary<string, decimal?>();
            List<List<string>> generalData = new List<List<string>>();

            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.ParentStatus);
                data.Add(item.StatusCount.ToString());

                generalData.Add(data);
            }
          
            return Json(generalData, JsonRequestBehavior.AllowGet);

        }



        public async Task<List<List<string>>> ReportingDetailedPieChart(string nextExtendedOrganisationKey, string assignedToUserKey)
        {
            List<ReportingDetailedPieChart_Result> result = new List<ReportingDetailedPieChart_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingDetailedPieChart/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("nextExtendedOrganisationKey={0}", nextExtendedOrganisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("assignedToUserKey={0}", assignedToUserKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingDetailedPieChart_Result>>();
                }
            }


            List<List<string>> generalData = new List<List<string>>();

            if(result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }



            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.StatusValue);
                data.Add(item.StatusCount.ToString());

                generalData.Add(data);
            }
            return generalData;
        }


        public async Task<List<List<string>>> RepoertingRepeatedPieChart()
        {
            List<RepoertingRepeatedPieChart_Result> result = new List<RepoertingRepeatedPieChart_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/RepoertingRepeatedPieChart/");
                //requestUriBuilder.Append("?");
                //requestUriBuilder.AppendFormat("nextExtendedOrganisationKey={0}", nextExtendedOrganisationKey);
                //requestUriBuilder.Append("&");
                //requestUriBuilder.AppendFormat("assignedToUserKey={0}", assignedToUserKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<RepoertingRepeatedPieChart_Result>>();
                }
            }


            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.Status);
                data.Add(item.Count.ToString());

                generalData.Add(data);
            }
            return generalData;
        }







    }
}