﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class ItemTrackingStatusController : Controller
    {
        private Entities db = new Entities();

        // GET: ItemTrackingStatus
        public async Task<ActionResult> Index()
        {
            var itemTrackingStatus = db.ItemTrackingStatus;
            //var itemTrackingStatus = db.ItemTrackingStatus.Include(i => i.AspNetUser).Include(i => i.AspNetUser1).Include(i => i.FormTemplate).Include(i => i.InvocationType).Include(i => i.WorkFlowItem);
            return View(await itemTrackingStatus.ToListAsync());
        }

        // GET: ItemTrackingStatus/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemTrackingStatu itemTrackingStatu = await db.ItemTrackingStatus.FindAsync(id);
            if (itemTrackingStatu == null)
            {
                return HttpNotFound();
            }
            return View(itemTrackingStatu);
        }

        // GET: ItemTrackingStatus/Create
        public ActionResult Create()
        {
            ViewBag.AssignedToUserKey = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.UpdatedByUserKey = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.NextFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name");
            ViewBag.InvocationTypeKey = new SelectList(db.InvocationTypes, "InvocationTypeKey", "InvocationType1");
            ViewBag.WorkFlowItemKey = new SelectList(db.WorkFlowItems, "WorkFlowItemKey", "ExtendedCurrentRoleKey");
            return View();
        }

        // POST: ItemTrackingStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ItemTrackingStatusKey,WorkFlowItemKey,UpdatedByUserKey,AssignedToUserKey,CreatedAt,UpdatedAt,CurrentFormKey,NextFormTemplateKey,GUID,InvocationTypeKey,Note")] ItemTrackingStatu itemTrackingStatu)
        {
            if (ModelState.IsValid)
            {
                db.ItemTrackingStatus.Add(itemTrackingStatu);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AssignedToUserKey = new SelectList(db.AspNetUsers, "Id", "Email", itemTrackingStatu.AssignedToUserKey);
            ViewBag.UpdatedByUserKey = new SelectList(db.AspNetUsers, "Id", "Email", itemTrackingStatu.UpdatedByUserKey);
            ViewBag.NextFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", itemTrackingStatu.NextFormTemplateKey);
            ViewBag.InvocationTypeKey = new SelectList(db.InvocationTypes, "InvocationTypeKey", "InvocationType1", itemTrackingStatu.InvocationTypeKey);
            ViewBag.WorkFlowItemKey = new SelectList(db.WorkFlowItems, "WorkFlowItemKey", "ExtendedCurrentRoleKey", itemTrackingStatu.WorkFlowItemKey);
            return View(itemTrackingStatu);
        }

        // GET: ItemTrackingStatus/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemTrackingStatu itemTrackingStatu = await db.ItemTrackingStatus.FindAsync(id);
            if (itemTrackingStatu == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssignedToUserKey = new SelectList(db.AspNetUsers, "Id", "Email", itemTrackingStatu.AssignedToUserKey);
            ViewBag.UpdatedByUserKey = new SelectList(db.AspNetUsers, "Id", "Email", itemTrackingStatu.UpdatedByUserKey);
            ViewBag.NextFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", itemTrackingStatu.NextFormTemplateKey);
            ViewBag.InvocationTypeKey = new SelectList(db.InvocationTypes, "InvocationTypeKey", "InvocationType1", itemTrackingStatu.InvocationTypeKey);
            ViewBag.WorkFlowItemKey = new SelectList(db.WorkFlowItems, "WorkFlowItemKey", "ExtendedCurrentRoleKey", itemTrackingStatu.WorkFlowItemKey);
            return View(itemTrackingStatu);
        }

        // POST: ItemTrackingStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ItemTrackingStatusKey,WorkFlowItemKey,UpdatedByUserKey,AssignedToUserKey,CreatedAt,UpdatedAt,CurrentFormKey,NextFormTemplateKey,GUID,InvocationTypeKey,Note")] ItemTrackingStatu itemTrackingStatu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(itemTrackingStatu).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AssignedToUserKey = new SelectList(db.AspNetUsers, "Id", "Email", itemTrackingStatu.AssignedToUserKey);
            ViewBag.UpdatedByUserKey = new SelectList(db.AspNetUsers, "Id", "Email", itemTrackingStatu.UpdatedByUserKey);
            ViewBag.NextFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", itemTrackingStatu.NextFormTemplateKey);
            ViewBag.InvocationTypeKey = new SelectList(db.InvocationTypes, "InvocationTypeKey", "InvocationType1", itemTrackingStatu.InvocationTypeKey);
            ViewBag.WorkFlowItemKey = new SelectList(db.WorkFlowItems, "WorkFlowItemKey", "ExtendedCurrentRoleKey", itemTrackingStatu.WorkFlowItemKey);
            return View(itemTrackingStatu);
        }

        // GET: ItemTrackingStatus/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemTrackingStatu itemTrackingStatu = await db.ItemTrackingStatus.FindAsync(id);
            if (itemTrackingStatu == null)
            {
                return HttpNotFound();
            }
            return View(itemTrackingStatu);
        }

        // POST: ItemTrackingStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ItemTrackingStatu itemTrackingStatu = await db.ItemTrackingStatus.FindAsync(id);
            db.ItemTrackingStatus.Remove(itemTrackingStatu);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
