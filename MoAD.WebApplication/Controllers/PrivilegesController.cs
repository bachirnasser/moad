﻿using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class PrivilegesController : Controller
    {
        // GET: Privileges
        public async Task<ActionResult> Index(string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;


            List<object> roles = GetRoles().Result;

            ViewBag.Roles = roles;
            return View();
        }
        public async Task<List<object>> GetRoles()
        {

            
            List<object> result = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRoles/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<object>>();


                }
            }
            return result;
        }

        public async Task<JsonResult> AccessPivilege(string RoleId = null,string ParentPrivilege = null)
        {
            List<AccessPivilege_Result> result = new List<AccessPivilege_Result>();

            string userRole = RoleId;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessPivilege/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userRole={0}", userRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ParentId={0}", ParentPrivilege);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Position={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessPivilege_Result>>();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        

     public async Task<string> DeleteAccessPrivilege(string privilegeKey = null, string roleKey = null, string conditionKey = null, string assignedTime = null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/DeleteAccessPrivilege/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("pKey={0}", privilegeKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("rKey={0}", roleKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("conKey={0}", conditionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("assiTime={0}", assignedTime);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }


        public async Task<string> SetAccessPrivilege(string privilegeKey = null, string roleKey = null, string conditionKey = null, string assignedTime = null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SetAccessPrivilege/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("privilegeKey={0}", privilegeKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("roleKey={0}", roleKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("conditionKey={0}", conditionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("assignedTime={0}", assignedTime);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }




        public async Task<string> UpdateAccessPrivilege(string privilegeKey = null, string roleKey = null, string conditionKey = null, string assignedTime = null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/UpdateAccessPrivilege/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("privKey={0}", privilegeKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("rolKey={0}", roleKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("condKey={0}", conditionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("assignTime={0}", assignedTime);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }
        


    }
}