﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Objects;
using MoAD.WebApplication.Models;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;

namespace MoAD.WebApplication.Controllers
{
    public class PageController : BaseController
    {
        // GET: Page
        private Entities db = new Entities();
        public async Task<ActionResult> Index()
        {
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            List<Page> pages = new List<Page>();
            pages = await db.Pages.Where(p => p.PageName != "Markaz" && p.PageName != "Marsad" && p.PageName != "Manbar").OrderByDescending(c => c.PageKey).ToListAsync();

            //var result = pages.Select(p => new
            //{
            //    Text = p.PageName,
            //    Value = p.PageKey
            //}).ToList();
            ViewBag.Pages = pages;
            return View();
        }
    }
}