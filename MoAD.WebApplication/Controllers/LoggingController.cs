﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
  
    public class LoggingController : Controller
    {

        private List<MostUsersLogMove_Result> MostUsersLogMoveResult;
        // GET: Logging

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Index(string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = "Logging";

            

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);
            
            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    }
                }
                else
                {
                    ViewBag.OrganisationType = null;
                }
            
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }
                
                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }



            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType != "do not get any orgs")
            {
                result = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            }
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
             var test = result

              .Select(group => new
              {
                  OrganisationKey = group.OrganisationKey,
                  OrganisationName = group.OrganisationName
              })
              .ToList().Distinct();
            foreach(var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }

             ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetLogTable_Result> TableResult = GetLogTable(null, "BETWEEN 1 AND 10", ViewBag.OrganisationType).Result;
            ViewBag.LogTable = TableResult;
           
            var Finalresult = await MostUsersLogMove(ViewBag.OrganisationType);
            ViewBag.MostUsersLogMoveResult = MostUsersLogMoveResult;


            List<object> Report1Json = new List<object>();
            foreach (var item in Finalresult.Data)
            {
                Dictionary<string, string> ItemDict = new Dictionary<string, string>();
                ItemDict.Add("year", item[0].ToString());
                ItemDict.Add("income", item[1].ToString());
               
                Report1Json.Add(ItemDict);
            }

            ViewBag.MostUsersLogMove = Report1Json;
            return View();
        }


        public async Task<ActionResult> PBIndex(string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = "Logging";



            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                ViewBag.OrganisationType = null;
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }





            List<AccessMonitoringAndChatting_Result> result = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = result

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }

            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetLogTable_Result> TableResult = GetLogTable(null, "BETWEEN 1 AND 10", ViewBag.OrganisationType).Result;
            ViewBag.LogTable = TableResult;

            var Finalresult = await MostUsersLogMove(ViewBag.OrganisationType);
            ViewBag.MostUsersLogMoveResult = MostUsersLogMoveResult;


            List<object> Report1Json = new List<object>();
            foreach (var item in Finalresult.Data)
            {
                Dictionary<string, string> ItemDict = new Dictionary<string, string>();
                ItemDict.Add("year", item[0].ToString());
                ItemDict.Add("income", item[1].ToString());

                Report1Json.Add(ItemDict);
            }

            ViewBag.MostUsersLogMove = Report1Json;
            return View();
        }


        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string parentOrg)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", parentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("r={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> MoreDetails(string userKey = null)
        {
            ViewBag.HighlightedPart = "Logging";

            List<GetLogTable_Result> TableResult = GetLogTable(userKey, "BETWEEN 1 AND 1000").Result;
            ViewBag.LogTableMoreDetails = TableResult;


            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetOrganisationType_Result>> GetOrganisationType(string parentorg)
        {


            List<GetOrganisationType_Result> result = new List<GetOrganisationType_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetOrganisationType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("identityKey={0}", parentorg);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetOrganisationType_Result>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<AccessMonitoringAndChatting_Result>> GetAccessOrganisation(string Orgs=null)
        {
            if (Orgs == null)
                Orgs = "null";
            string userKEY = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", userKEY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "Monitoring");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", Orgs);
                

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();
                    

                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> GetAccessUsers(string orgKey)
        {

            string userKEY = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", userKEY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "Monitoring");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", orgKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> GetLogTableJson(string monitor = null, string pagination = null,string Organisationns=null)
        {
            string Key = User.Identity.GetUserId();
            List<GetLogTable_Result> result = new List<GetLogTable_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetLogTable/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("KeyUser={0}", Key);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("monitor={0}", monitor);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("pagination={0}", pagination);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Organisationns={0}", Organisationns);
                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetLogTable_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> GetLog(string Id = null)
        {
            List<GetLog_Result> result = new List<GetLog_Result>();
            List<string> Message = new List<string>();
            List<int?> Count = new List<int?>();

            List<object> FinalResult = new List<object>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetLog/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("uId={0}", Id);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetLog_Result>>();
                    Message = result.Select(x => x.Message).ToList();
                    Count = result.Select(x => x.C_Count).ToList();

                }

            }
            FinalResult.Add(Message);

            FinalResult.Add(Count);
            List<object> Report7Json = new List<object>();


            foreach (var item in result)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("country", item.Message);
                ItemDict.Add("value", item.C_Count);


                Report7Json.Add(ItemDict);
            }
            return Json(Report7Json, JsonRequestBehavior.AllowGet);
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetLogTable_Result>> GetLogTable(string monitor = null, string pagination = null,string Organisationns=null)
        {
            string Key = User.Identity.GetUserId();
            List<GetLogTable_Result> result = new List<GetLogTable_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetLogTable/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("KeyUser={0}", Key);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("monitor={0}", monitor);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("pagination={0}", pagination);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Organisationns={0}", Organisationns);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetLogTable_Result>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> MostUsersLogMoveJson()
        {
            string MonitorKey = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", MonitorKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "MonitoringMLU");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();
                    
                }
            }

         
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> MostUsersLogMove(string Orgs=null)
        {

            
            string MonitorKey = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", MonitorKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "MonitoringMLU");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", Orgs);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();
                  //  MostUsersLogMoveResult = result;
                }
            }

            List<List<string>> Finalresult = new List<List<string>>();
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.UserName);
                data.Add(item.Count.ToString());
                data.Add(item.UserKey);
                Finalresult.Add(data);
            }
            return Json(Finalresult, JsonRequestBehavior.AllowGet);
        }
    }
}