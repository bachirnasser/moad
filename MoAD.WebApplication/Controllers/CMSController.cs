﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Objects;
using MoAD.WebApplication.Models;
using System.IO;
using MoAD.WebApplication.Managers;
using System.Web.Script.Serialization;

namespace MoAD.WebApplication.Controllers
{
 [Authorize]
    public class CMSController : BaseController
    {
        private Entities db = new Entities();
        // GET: CMS
        public ActionResult MarkazOld()
        {
            return View();
        }

        public ActionResult CmsSlider()
        {
            return View();
        }
        public async Task<ActionResult> Page()
        {
            ViewBag.Title = "صفحة جديدة";
            ViewBag.Languages = new SelectList(db.Languages.Where(l => l.Language1 == "Arabic" || l.Language1 == "English").ToList(), "LanguageKey", "Language1");
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Markaz").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part


            int pageKey = (await db.Pages.Where(p => p.PageName == "Markaz").FirstOrDefaultAsync()).PageKey;
            var pageComponentVideo = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey && p.Component.ComponentType.ComponentType1 == "Video").FirstOrDefaultAsync();

            string videoUrlComponent = "";
            int? videoComponentPageKey = null;
            if (pageComponentVideo != null)
            {
                videoComponentPageKey = pageComponentVideo.Component.ComponentKey;
                videoUrlComponent = pageComponentVideo.Component.ComponentSourceEntityKey;
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.pageKey = pageKey;
            ViewBag.videoUrlComponent = videoUrlComponent;
            ViewBag.videoComponentPageKey = videoComponentPageKey;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            return View();
        }

        
             public async Task<ActionResult> TestPageMenu()
        {
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;

            ViewBag.Languages = new SelectList(db.Languages.Where(l => l.Language1 == "Arabic" || l.Language1 == "English").ToList(), "LanguageKey", "Language1");
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Markaz").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part


            int pageKey = (await db.Pages.Where(p => p.PageName == "Markaz").FirstOrDefaultAsync()).PageKey;
            var pageComponentVideo = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey && p.Component.ComponentType.ComponentType1 == "Video").FirstOrDefaultAsync();

            string videoUrlComponent = "";
            int? videoComponentPageKey = null;
            if (pageComponentVideo != null)
            {
                videoComponentPageKey = pageComponentVideo.Component.ComponentKey;
                videoUrlComponent = pageComponentVideo.Component.ComponentSourceEntityKey;
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.pageKey = pageKey;
            ViewBag.videoUrlComponent = videoUrlComponent;
            ViewBag.videoComponentPageKey = videoComponentPageKey;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            return View();
        }

        public async Task<ActionResult> Slider(string type = "", int pageKey = 1)
        {
            ViewBag.Title = "إنشاء سليدر";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = new List<PageComponentsHierarchy>();
            allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageKey == pageKey).ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            string pageName = (await db.Pages.FindAsync(pageKey)).PageName;
            PageComponentsHierarchy navBarItemComponent;
            if (type == "slider1")
            {
                navBarItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            }
            else
            {
                navBarItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault();
            }
            int? navBarItemComponentKey = null;
            if (navBarItemComponent != null)
            {
                navBarItemComponentKey = navBarItemComponent.ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();
            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarDataSlider = navbarItemsGroupByOrder;
            ViewBag.SliderType = type;
            ViewBag.PageName = pageName;

            var allComponentsInPage2 = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
            //ViewBag.PageKey = allComponentsInPage2.FirstOrDefault().PageKey;
            int? navBarItemComponentKey2 = allComponentsInPage2.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems2 = allComponentsInPage2.Where(i => i.ParentComponentKey == navBarItemComponentKey2).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder2 = navbarItems2.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            //ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder2;
            return View();
        }

        public ActionResult CreateDataTranslation()
        {
            return View();
        }

        public async Task<ActionResult> Markaz()
        {
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Markaz").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentIndex).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();



            var sliderItemComponent1 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            var sliderItemComponent2 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault();

            int? sliderItemComponentKey1 = null;
            if (sliderItemComponent1 != null)
            {
                sliderItemComponentKey1 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            int? sliderItemComponentKey2 = null;
            if (sliderItemComponent2 != null)
            {
                sliderItemComponentKey2 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault().ComponentKey;
            }
            var sliderItems1 = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey1).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder1 = sliderItems1.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();
            var sliderItems2 = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey2).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder2 = sliderItems2.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();
            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey1 = sliderItemComponentKey1;
            ViewBag.SliderData1 = sliderItemsGroupByOrder1;
            ViewBag.SliderItemComponentKey2 = sliderItemComponentKey2;
            ViewBag.SliderData2 = sliderItemsGroupByOrder2;

            return View();
        }
        public async Task<ActionResult> Marsad()
        {
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentIndex).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;


            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GenerateGuidForDataTranslation(string arabicText, string englishText, string pageName)
        {
            Guid newGuid = Guid.NewGuid();
            try
            {
                int arabicLanguageKey = (await db.Languages.Where(l => l.Language1 == "Arabic").FirstOrDefaultAsync()).LanguageKey;
                int englishLanguageKey = (await db.Languages.Where(l => l.Language1 == "English").FirstOrDefaultAsync()).LanguageKey;


                DataGUID dataGUID = new DataGUID();
                dataGUID.DataGUID1 = newGuid;
                dataGUID.SourceTable = pageName;

                db.DataGUIDs.Add(dataGUID);
                await db.SaveChangesAsync();

                DataTranslation arabicDataTranslation = new DataTranslation();
                arabicDataTranslation.DataGUID = newGuid;
                arabicDataTranslation.Value = arabicText;
                arabicDataTranslation.LanguageKey = arabicLanguageKey;
                db.DataTranslations.Add(arabicDataTranslation);
                await db.SaveChangesAsync();

                DataTranslation englishDataTranslation = new DataTranslation();
                englishDataTranslation.DataGUID = newGuid;
                englishDataTranslation.Value = englishText;
                englishDataTranslation.LanguageKey = englishLanguageKey;
                db.DataTranslations.Add(englishDataTranslation);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json("Error");
            }

            return Json(newGuid);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CheckIfParentNavbarExists(int p_key, string item_name)
        {
            string pageName = (await db.Pages.FindAsync(p_key)).PageName;
            List<NavBarViewModel> navbarItemsGroupByOrder = new List<NavBarViewModel>();
           // List<PageComponentsHierarchy> navbarItems = new List<PageComponentsHierarchy>();
            if (ModelState.IsValid)
            {
                var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
                var navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;
                var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey && i.Component.ComponentType.ComponentType1=="Text" && i.ComponentLevel==1).Select(i=>i.Component.ComponentSourceEntityKey).ToArray();

                foreach(var item in navbarItems)
                {
                    Guid guidData = Guid.Parse(item);
                    var transaltion =( await db.DataTranslations.Where(d => d.DataGUID == guidData).FirstOrDefaultAsync()).Value;
                    if (transaltion.Trim() == item_name.Trim())
                    {
                        return Json(200, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(404, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CheckIfChildNavbarExists(int p_id, string child_name, int ord)
        {
            string pageName = (await db.Pages.FindAsync(p_id)).PageName;
            List<NavBarViewModel> navbarItemsGroupByOrder = new List<NavBarViewModel>();
            // List<PageComponentsHierarchy> navbarItems = new List<PageComponentsHierarchy>();
            if (ModelState.IsValid)
            {
                var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
                var navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;
                var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey && i.Component.ComponentType.ComponentType1 == "Text" && i.ComponentLevel != 1 && i.ComponentOrder == ord).Select(i => i.Component.ComponentSourceEntityKey).ToArray();

                foreach (var item in navbarItems)
                {
                    Guid guidData = Guid.Parse(item);
                    var transaltion = (await db.DataTranslations.Where(d => d.DataGUID == guidData).FirstOrDefaultAsync()).Value;
                    if (transaltion.Trim() == child_name.Trim())
                    {
                        return Json(200, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(404, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateItemNavbar(CreateItemNavbarViewModel createItemNavbarModel)
        {
            string pageName = (await db.Pages.FindAsync(createItemNavbarModel.PageKey)).PageName;
            List<NavBarViewModel> navbarItemsGroupByOrder = new List<NavBarViewModel>();
            List<PageComponentsHierarchy> navbarItems = new List<PageComponentsHierarchy>();
            if (ModelState.IsValid)
            {
                var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
                int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;
                navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

                Guid newGuid = await GetNewGuidForDataTranslation(createItemNavbarModel.ItemName, pageName);

                Component newComponent = new Component();
                newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Text");
                newComponent.ComponentSourceEntityKey = newGuid.ToString();
                db.Components.Add(newComponent);
                await db.SaveChangesAsync();

                int? componentOrder = navbarItems.Max(n => n.ComponentOrder) + 1;
                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = createItemNavbarModel.PageKey;
                pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                pageComponentsHierarchy.ParentComponentKey = createItemNavbarModel.NavBarComponentKey;
                pageComponentsHierarchy.ComponentLevel = 1;//Cause it is an item
                pageComponentsHierarchy.ComponentOrder = componentOrder == null ? 1 : componentOrder;
                pageComponentsHierarchy.ComponentIndex = 1;
                int? redirectionTypeKey = null;
                if (createItemNavbarModel.ItemType == "Pdf")
                {
                    redirectionTypeKey = (await db.RedirectionsTypes.Where(r => r.RedirectionType == "File").FirstOrDefaultAsync()).RedirectionTypeKey;
                }
                else if (createItemNavbarModel.ItemType == "Page")
                {
                    redirectionTypeKey = (await db.RedirectionsTypes.Where(r => r.RedirectionType == "Page").FirstOrDefaultAsync()).RedirectionTypeKey;
                }
                pageComponentsHierarchy.RedirectionTypeKey = redirectionTypeKey;
                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();

                bool createRediretion = false;
                if (createItemNavbarModel.ItemTypeKey != null)
                {
                    createRediretion = await Utilities.CreateRedirectionForComponent((int)pageComponentsHierarchy.ComponentKey, (int)createItemNavbarModel.ItemTypeKey, CurrentLanguageIdentifier);
                }
              
              //  pageComponentsHierarchy.RedirectionEntity = createItemNavbarModel.ItemTypeKey;
               
            }
            
            return await ReturnNavbar(pageName);

            // return PartialView("_NavbarMenu", new ViewDataDictionary { { "NavbarData", navbarItemsGroupByOrder }, { "LanguageIdentifier", CurrentLanguageIdentifier } });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateChildNavbar(CreateChildNavbarViewModel createChildNavbarViewModel)
        {
            string pageName = (await db.Pages.FindAsync(createChildNavbarViewModel.PageKey)).PageName;
            List<NavBarViewModel> navbarItemsGroupByOrder = new List<NavBarViewModel>();
            List<PageComponentsHierarchy> navbarItems = new List<PageComponentsHierarchy>();
            if (ModelState.IsValid)
            {
                var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
                int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;
                navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

                Guid newGuid = await GetNewGuidForDataTranslation(createChildNavbarViewModel.ItemName, pageName);

                Component newComponent = new Component();
                newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Text");
                newComponent.ComponentSourceEntityKey = newGuid.ToString();
                db.Components.Add(newComponent);
                await db.SaveChangesAsync();

                //int? componentOrder = navbarItems.Max(n => n.ComponentOrder) + 1;
                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = createChildNavbarViewModel.PageKey;
                pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                pageComponentsHierarchy.ParentComponentKey = createChildNavbarViewModel.NavBarComponentKey;
                pageComponentsHierarchy.ComponentLevel = 2;//Cause it is a child
                pageComponentsHierarchy.ComponentOrder = createChildNavbarViewModel.ComponentOrder;
                pageComponentsHierarchy.ComponentIndex = createChildNavbarViewModel.ComponentIndex;
                int? redirectionTypeKey = null;
                if (createChildNavbarViewModel.ItemType == "Pdf")
                {
                    redirectionTypeKey = (await db.RedirectionsTypes.Where(r => r.RedirectionType == "File").FirstOrDefaultAsync()).RedirectionTypeKey;
                }
                else if (createChildNavbarViewModel.ItemType == "Page")
                {
                    redirectionTypeKey = (await db.RedirectionsTypes.Where(r => r.RedirectionType == "Page").FirstOrDefaultAsync()).RedirectionTypeKey;
                }
                pageComponentsHierarchy.RedirectionTypeKey = redirectionTypeKey;
                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();
                bool createRediretion = false;
                if (createChildNavbarViewModel.ItemTypeKey != null)
                {
                    createRediretion = await Utilities.CreateRedirectionForComponent((int)pageComponentsHierarchy.ComponentKey, (int)createChildNavbarViewModel.ItemTypeKey, CurrentLanguageIdentifier);
                }

                //pageComponentsHierarchy.RedirectionEntity = createChildNavbarViewModel.ItemTypeKey;
            }
            return await ReturnNavbar(pageName);

        }
        public async Task<Guid> GetNewGuidForDataTranslation(string value, string source)
        {
            Guid newGuid = Guid.NewGuid();
            DataGUID dataGUID = new DataGUID();
            dataGUID.DataGUID1 = newGuid;
            dataGUID.SourceTable = source;
            db.DataGUIDs.Add(dataGUID);
            await db.SaveChangesAsync();

            DataTranslation dataTranslation = new DataTranslation();
            dataTranslation.DataGUID = newGuid;
            dataTranslation.Value = value;
            dataTranslation.LanguageKey = CurrentLanguageIdentifier;
            db.DataTranslations.Add(dataTranslation);
            await db.SaveChangesAsync();

            return newGuid;
        }
        public async Task<int> GetComponentTypeKeyByComponentTypeName(string type)
        {
            int componentTypeKey = (await db.ComponentTypes.Where(c => c.ComponentType1 == type).FirstOrDefaultAsync()).ComponentTypeKey;
            return componentTypeKey;
        }

        public async Task<int> GetRedirectionTypeKeyByRedirectionTypeName(string redirectionType)
        {
            if (redirectionType == "Pdf")
            {
                redirectionType = "File";
            }
            int componentTypeKey = (await db.RedirectionsTypes.Where(c => c.RedirectionType == redirectionType).FirstOrDefaultAsync()).RedirectionTypeKey;
            return componentTypeKey;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetItemDetails(int itemPageHierKey)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageComponentHierarchyKey == itemPageHierKey).FirstOrDefaultAsync();
                // int orderComponentsCount = await db.PageComponentsHierarchies.Where(p => p.ComponentOrder == pageComponentsHierarchy.ComponentOrder).CountAsync();
                var component = await db.PageComponentsHierarchies.Where(p => p.ComponentKey == itemPageHierKey).FirstOrDefaultAsync();

                string itemHierarchy = component.RedirectionTypeKey == null ? "Parent" : "Leaf";

                result.Add("itemHierarchy", itemHierarchy);
                result.Add("itemTranslationGuid", pageComponentsHierarchy.Component.ComponentSourceEntityKey);
                result.Add("itemName", Utilities.GetDataTransaltionByGuid(pageComponentsHierarchy.Component.ComponentSourceEntityKey, CurrentLanguageIdentifier));
                if (itemHierarchy.Equals("Parent"))
                {
                    result.Add("redirectionType", "");
                    result.Add("redirectionEntity", "");
                    result.Add("redirectionEntityKey", "");
                    result.Add("redirectionTypeKey", "");
                }
                else
                {
                    result.Add("redirectionType", pageComponentsHierarchy.RedirectionsType.RedirectionType);
                    result.Add("redirectionEntity", pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity.ToString());
                    result.Add("redirectionEntityKey", pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity);
                    result.Add("redirectionTypeKey", pageComponentsHierarchy.RedirectionTypeKey);

                }

            }
            catch (Exception e)
            {
                return Json("Error");
            }

            return Json(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateItem(int itemKeyInPageHierarchy, string redirectionType, int redirectionKey)
        {
            if(redirectionType=="null" && redirectionKey == 0)
            {
                string pageName = "";
                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageComponentHierarchyKey == itemKeyInPageHierarchy).FirstOrDefaultAsync();
                    pageName = pageComponentsHierarchy.Page.PageName;
                    return await ReturnNavbar(pageName);

            }
            else
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                string pageName = "";
                try
                {
                    PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                    pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageComponentHierarchyKey == itemKeyInPageHierarchy).FirstOrDefaultAsync();
                    string redirectionTypeText = "";
                    if (redirectionType == "" || redirectionKey == 0)
                    {

                        pageName = pageComponentsHierarchy.Page.PageName;
                        return await ReturnNavbar(pageName);
                    }
                    if (redirectionType == "Pdf")
                    {
                        redirectionTypeText = "File";
                    }
                    else
                    {
                        redirectionTypeText = "Page";
                    }
                    pageComponentsHierarchy.RedirectionTypeKey = await GetRedirectionTypeKeyByRedirectionTypeName(redirectionTypeText);

                
                    bool updateRediretion = await Utilities.UpdateRedirectionForComponent((int)pageComponentsHierarchy.ComponentKey, (int)redirectionKey, CurrentLanguageIdentifier);
                    
                    //pageComponentsHierarchy.RedirectionEntity = redirectionKey;

                    pageName = pageComponentsHierarchy.Page.PageName;
                    await db.SaveChangesAsync();
                    return await ReturnNavbar(pageName);
                }
                catch (Exception e)
                {
                    return Json(400);
                }

            }


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetChildrenPerItem(int navBarKey, int pageComponentKey)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = new List<PageComponentsHierarchy>();

            PageComponentsHierarchy parentComponentsHierarchies = await db.PageComponentsHierarchies.FindAsync(pageComponentKey);
            pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarKey && p.ComponentLevel != 1).ToListAsync();
            var result = pageComponentsHierarchies.Select(p => new
            {
                Text = Utilities.GetDataTransaltionByGuid(p.Component.ComponentSourceEntityKey),
                Value = p.PageComponentHierarchyKey,
                ComponentOrder = p.ComponentOrder
            }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetChildrenPerParent(int navBarKey, int pageComponentKey, int componentOrder)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = new List<PageComponentsHierarchy>();

            PageComponentsHierarchy parentComponentsHierarchies = await db.PageComponentsHierarchies.FindAsync(pageComponentKey);
            pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarKey && p.ComponentLevel != 1 && p.ComponentOrder == componentOrder).ToListAsync();
            var result = pageComponentsHierarchies.Select(p => new
            {
                Text = Utilities.GetDataTransaltionByGuid(p.Component.ComponentSourceEntityKey,CurrentLanguageIdentifier),
                Value = p.PageComponentHierarchyKey,
                ComponentOrder = p.ComponentOrder,
                ComponentIndex = p.ComponentIndex
            }).ToList().OrderBy(p => p.ComponentIndex);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public int GetChildrenCountPerItem(int navBarKey, int pageComponentKey)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = new List<PageComponentsHierarchy>();
            try
            {
                PageComponentsHierarchy parentComponentsHierarchies = db.PageComponentsHierarchies.Find(pageComponentKey);
                pageComponentsHierarchies = db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarKey && p.ComponentOrder == parentComponentsHierarchies.ComponentOrder && p.ComponentLevel != 1).ToList();
            }
            catch (Exception e)
            {
                return -1;
            }

            return pageComponentsHierarchies.Count();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateDataTranslation(string guid, string newTransaltion, int? language)
        {
            try
            {
                if (language == null)
                {
                    language = CurrentLanguageIdentifier;
                }
                Guid currentGuid = Guid.Parse(guid);
                DataTranslation dataTranslation = await db.DataTranslations.Where(d => d.DataGUID == currentGuid && d.LanguageKey == language).FirstOrDefaultAsync();

                if (dataTranslation != null)
                {
                    dataTranslation.Value = newTransaltion;
                    await db.SaveChangesAsync();
                }
                else
                {
                    DataTranslation newDataTranslation = new DataTranslation();
                    newDataTranslation.DataGUID = currentGuid;
                    newDataTranslation.Value = newTransaltion;
                    newDataTranslation.LanguageKey = language;

                    db.DataTranslations.Add(newDataTranslation);
                    await db.SaveChangesAsync();
                }
               
            }
            catch (Exception e)
            {
                return Json("Error");
            }

            return Json(newTransaltion);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateRedirectionPerComponent(string guid, int pageKey,string redType,int redKey)
        {
            try
            {
                int redirectionTypeKey = await GetRedirectionTypeKeyByRedirectionTypeName(redType);
                bool updateRedirection = await Utilities.SaveRedirectionPerComponent(guid, pageKey, redirectionTypeKey, redKey,CurrentLanguageIdentifier);
            }
            catch (Exception e)
            {
                return Json(400);
            }

            return Json(200);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateChild(int childKeyInPageHierarchy, int childRedirectionTypeKey, int childRedirectionKey)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                string pageName = "";
                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageComponentHierarchyKey == childKeyInPageHierarchy).FirstOrDefaultAsync();
                pageComponentsHierarchy.RedirectionTypeKey = childRedirectionTypeKey;
                bool updateRediretion = await Utilities.UpdateRedirectionForComponent((int)pageComponentsHierarchy.ComponentKey, (int)childRedirectionKey, CurrentLanguageIdentifier);

                //pageComponentsHierarchy.RedirectionEntity = childRedirectionKey;

                pageName = pageComponentsHierarchy.Page.PageName;
                await db.SaveChangesAsync();
                return await ReturnNavbar(pageName);
            }
            catch (Exception e)
            {
                return Json(400);
            }


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetAllItems(int navBarItemComponentKey)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarItemComponentKey && p.ComponentLevel == 1).ToListAsync();

            var result = pageComponentsHierarchies.Select(p => new
            {
                Text = Utilities.GetDataTransaltionByGuid(p.Component.ComponentSourceEntityKey,CurrentLanguageIdentifier),
                Value = p.PageComponentHierarchyKey,
                componentOrder = p.ComponentOrder
            }).ToList();


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetAllItemsParents(int navBarItemComponentKey)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarItemComponentKey && p.ComponentLevel == 1 && p.RedirectionTypeKey == null).ToListAsync();

            var result = pageComponentsHierarchies.Select(p => new
            {
                Text = Utilities.GetDataTransaltionByGuid(p.Component.ComponentSourceEntityKey,CurrentLanguageIdentifier),
                componentOrder = p.ComponentOrder,
                ChildsCount = GetChildrenCountPerItem(navBarItemComponentKey, p.PageComponentHierarchyKey)
            }).ToList();


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetAllItemsForIndexing(int navBarItemComponentKey)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarItemComponentKey && p.ComponentLevel == 1).ToListAsync();



            var result = pageComponentsHierarchies.Select(p => new
            {
                Text = Utilities.GetDataTransaltionByGuid(p.Component.ComponentSourceEntityKey),
                Value = p.PageComponentHierarchyKey,
                ComponentOrder = p.ComponentOrder
            }).ToList().OrderBy(p => p.ComponentOrder);


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetAllItemsParentsForIndexing(int navBarItemComponentKey)
        {
            List<PageComponentsHierarchy> pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navBarItemComponentKey && p.ComponentLevel == 1 && p.RedirectionTypeKey == null).ToListAsync();



            var result = pageComponentsHierarchies.Select(p => new
            {
                Text = Utilities.GetDataTransaltionByGuid(p.Component.ComponentSourceEntityKey),
                Value = p.PageComponentHierarchyKey,
                ComponentOrder = p.ComponentOrder
            }).ToList().OrderBy(p => p.ComponentOrder);


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteChild(int childPageHierKey)
        {
            try
            {
                PageComponentsHierarchy parentComponentsHierarchies = await db.PageComponentsHierarchies.FindAsync(childPageHierKey);
                int? componentToDeletekey = parentComponentsHierarchies.ComponentKey;
                string pageName = parentComponentsHierarchies.Page.PageName;
                int? pdfToDelete = null;
                int? pageToDelete = null;
                if (parentComponentsHierarchies.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity != null)
                {
                    if (parentComponentsHierarchies.RedirectionsType.RedirectionType == "Pdf")
                    {
                        pdfToDelete = parentComponentsHierarchies.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity;
                    }
                }

                db.PageComponentsHierarchies.Remove(parentComponentsHierarchies);
                await db.SaveChangesAsync();

                if (pdfToDelete != null)
                {
                    ExtendedData pdfItemToDelete = await db.ExtendedDatas.FindAsync(pdfToDelete);
                    System.IO.File.Delete(pdfItemToDelete.FilePathOrSource);
                    db.ExtendedDatas.Remove(pdfItemToDelete);
                    await db.SaveChangesAsync();
                }

                if (pageToDelete != null)
                {
                    Page pageItemToDelete = await db.Pages.FindAsync(pageToDelete);
                    db.Pages.Remove(pageItemToDelete);
                    await db.SaveChangesAsync();
                }

                Component componentToDelete = await db.Components.FindAsync(componentToDeletekey);
                db.Components.Remove(componentToDelete);
                await db.SaveChangesAsync();
                return await ReturnNavbar(pageName);

            }
            catch (Exception e)
            {
                return Json(400);
            }


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteItem(int pageComponentsHierarchyKey, int componentOrder, int navbarKey)
        {

            try
            {
                PageComponentsHierarchy parentItem = new PageComponentsHierarchy();
                parentItem = await db.PageComponentsHierarchies.FindAsync(pageComponentsHierarchyKey);
                string pageName = parentItem.Page.PageName;

                int? componentKey;
                componentKey = parentItem.ComponentKey;

                List<PageComponentsHierarchy> itemChildren = new List<PageComponentsHierarchy>();
                //itemChildren = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == parentItem.ComponentKey && p.ComponentOrder == componentOrder).ToListAsync();
                itemChildren = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == navbarKey && p.ComponentLevel != 1 && p.ComponentOrder == componentOrder).ToListAsync();

                if (itemChildren.Count != 0)
                {
                    foreach (var child in itemChildren)
                    {
                        if (child.RedirectionsType.RedirectionType == "File")
                        {
                            var pdftodelete = await db.ExtendedDatas.FindAsync(child.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity);
                            db.ExtendedDatas.Remove(pdftodelete);
                            await db.SaveChangesAsync();
                        }
                        //else if (child.RedirectionsType.RedirectionType == "Page")
                        //{
                        //    db.PageComponentsHierarchies.Remove(parentItem);
                        //    await db.SaveChangesAsync();
                        //    //var pagetodelete = await db.Pages.FindAsync(child.RedirectionEntity);
                        //    //db.Pages.Remove(pagetodelete);
                        //    //await db.SaveChangesAsync();
                        //}

                        var PageComponentsHierarchyRedirectionEntitiesToDeleteParent = await db.PageComponentsHierarchyRedirectionEntities.Where(p => p.PageComponentHierarchyKey == child.ComponentKey).FirstOrDefaultAsync();
                        db.PageComponentsHierarchyRedirectionEntities.Remove(PageComponentsHierarchyRedirectionEntitiesToDeleteParent);
                        await db.SaveChangesAsync();

                        db.PageComponentsHierarchies.Remove(child);
                        await db.SaveChangesAsync();
                    }
                }
                else
                {
                    if (parentItem.RedirectionsType != null)
                    {
                        if (parentItem.RedirectionsType.RedirectionType == "File")
                        {
                            var pdftodelete = await db.ExtendedDatas.FindAsync(parentItem.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity);
                            db.ExtendedDatas.Remove(pdftodelete);
                            await db.SaveChangesAsync();

                        }
                        //else if (parentItem.RedirectionsType.RedirectionType == "Page")
                        //{
                        //    db.PageComponentsHierarchies.Remove(parentItem);
                        //    await db.SaveChangesAsync();

                        //    var pagetodelete = await db.Pages.FindAsync(parentItem.RedirectionEntity);
                        //    db.Pages.Remove(pagetodelete);
                        //    await db.SaveChangesAsync();
                        //}
                    }
                }


                var PageComponentsHierarchyRedirectionEntitiesToDelete = await db.PageComponentsHierarchyRedirectionEntities.Where(p => p.PageComponentHierarchyKey == parentItem.ComponentKey).FirstOrDefaultAsync();

                if (PageComponentsHierarchyRedirectionEntitiesToDelete != null)
                {
                    db.PageComponentsHierarchyRedirectionEntities.Remove(PageComponentsHierarchyRedirectionEntitiesToDelete);
                    await db.SaveChangesAsync();
                }

                db.PageComponentsHierarchies.Remove(parentItem);
                await db.SaveChangesAsync();

                Component parentComponent = new Component();
                parentComponent = await db.Components.FindAsync(componentKey);
                db.Components.Remove(parentComponent);
                await db.SaveChangesAsync();
                return await ReturnNavbar(pageName);

            }
            catch (Exception e)
            {
                return Json("Error");
            }

            // return Json("Success");
        }
        public async Task<ActionResult> UploadFile(HttpPostedFileBase file)
        {
            string uploadsFolderPath = Globals.Configuration.UPLOADS_CMS_PDF_FOLDER;
            // string uploadsFolderPath = Server.MapPath("~/Images/CMS/pdfs/");

            if (!Directory.Exists(uploadsFolderPath))
            {
                Directory.CreateDirectory(uploadsFolderPath);
            }

            ExtendedData extendedData = new ExtendedData();
            try
            {

                string fileName = DateTime.Now.Ticks.ToString() + file.FileName;
                string uploadedFilePath = uploadsFolderPath;
                //    string uploadedFilePath = uploadsFolderPath;
                //return Json(uploadedFilePath);
                file.SaveAs(uploadedFilePath + fileName);

                extendedData.Name = fileName;
                extendedData.Type = "Pdf";
                extendedData.CreatedAt = DateTime.Now.ToString();
                //extendedData.FilePathOrSource = "/Images/CMS/pdfs/";
                extendedData.FilePathOrSource = uploadsFolderPath;

                db.ExtendedDatas.Add(extendedData);
                await db.SaveChangesAsync();

                var newExtendeddata =await db.ExtendedDatas.FindAsync(extendedData.FileExtensionKey);
                newExtendeddata.ColumnValue = newExtendeddata.FileExtensionKey.ToString();
                await db.SaveChangesAsync();

                Component newComponent = new Component();
                newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Pdf");
                newComponent.ComponentSourceEntityKey = newExtendeddata.ColumnValue;
                db.Components.Add(newComponent);
                await db.SaveChangesAsync();

                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = 1;
                pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                pageComponentsHierarchy.ComponentOrder = 1;
                pageComponentsHierarchy.ComponentIndex = 1;

                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();

                TempData["FileExtensionKeyForVideo"] = newExtendeddata.ColumnValue;
            }
            catch (Exception e)
            {
                return Json("Error");
            }

            return Json(extendedData.ColumnValue, JsonRequestBehavior.AllowGet);
        }

        // -------   Added for testing  ----------
        public async Task<ActionResult> UploadFile2(HttpPostedFileBase file)
        {

            //string uploadsFolderPath = Server.MapPath("~/Images/testForPages/");
            string uploadsFolderPath = Globals.Configuration.UPLOADS_CMS_FOLDER;
            //string uploadsFolderPath = Server.MapPath("~/Images/CMS/");
            if (!Directory.Exists(uploadsFolderPath))
            {
                Directory.CreateDirectory(uploadsFolderPath);
            }

            ExtendedData extendedData = new ExtendedData();
            try
            {

                string fileName = DateTime.Now.Ticks.ToString() + file.FileName;
                string uploadedFilePath = uploadsFolderPath + fileName;
                //    string uploadedFilePath = uploadsFolderPath;
                //return Json(uploadedFilePath);
                file.SaveAs(uploadedFilePath);

                extendedData.Name = fileName;
                extendedData.Type = "Image";
                extendedData.CreatedAt = DateTime.Now.ToString();
                extendedData.FilePathOrSource = uploadsFolderPath;

                db.ExtendedDatas.Add(extendedData);
                await db.SaveChangesAsync();

                var newExtendeddata = await db.ExtendedDatas.FindAsync(extendedData.FileExtensionKey);
                newExtendeddata.ColumnValue = newExtendeddata.FileExtensionKey.ToString();
                await db.SaveChangesAsync();

                Component newComponent = new Component();
                newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Image");
                newComponent.ComponentSourceEntityKey = newExtendeddata.ColumnValue;
                db.Components.Add(newComponent);
                await db.SaveChangesAsync();

                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = 1;
                pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                pageComponentsHierarchy.ComponentOrder = 1;
                pageComponentsHierarchy.ComponentIndex = 1;

                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();

                TempData["FileExtensionKeyForVideo"] = newExtendeddata.ColumnValue;
            }
            catch (Exception e)
            {
                return Json("Error");
            }

            return Json(extendedData.ColumnValue, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> RemoveFile(string extendedDatakey)
        {
            PageComponentsHierarchy pageComponent = await db.PageComponentsHierarchies.Where(p => p.Component.ComponentSourceEntityKey == extendedDatakey.ToString()).FirstOrDefaultAsync();

            ExtendedData data = await db.ExtendedDatas.Where(e=>e.ColumnValue== extendedDatakey).FirstOrDefaultAsync();

            Component component = await db.Components.Where(p => p.ComponentSourceEntityKey == extendedDatakey).FirstOrDefaultAsync();

            string uploadsFolderPath = data.FilePathOrSource + data.Name;

            System.IO.File.Delete(uploadsFolderPath);

            db.PageComponentsHierarchies.Remove(pageComponent);
            db.Components.Remove(component);
            db.ExtendedDatas.Remove(data);
            await db.SaveChangesAsync();
            return Content("Success");

        }
        public async Task<ActionResult> ReturnNavbar(string pageName)
        {
            var allComponentsInPageReturn = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
            int? navBarItemComponentKeyReturn = allComponentsInPageReturn.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;
            var navbarItems = allComponentsInPageReturn.Where(i => i.ParentComponentKey == navBarItemComponentKeyReturn).ToList();
            var navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                   p => p.ComponentOrder,
                   p => p,
            (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
            {
                ComponentOrder = p.ComponentOrder,
                Items = p.Items
            }).ToList();

            ViewData["NavbarData"] = navbarItemsGroupByOrder;
            ViewData["LanguageIdentifier"] = CurrentLanguageIdentifier;
            return PartialView("_NavbarMenu", new ViewDataDictionary { { "NavbarData", navbarItemsGroupByOrder }, { "LanguageIdentifier", CurrentLanguageIdentifier } });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> OrderComponents(int pageKey, int sliderComponentKey, string componentsType = "", string componentsArrString = "")
        {
            string pageName = (await db.Pages.FindAsync(pageKey)).PageName;
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                List<ComponentViewModel> components = (List<ComponentViewModel>)js.Deserialize(componentsArrString, typeof(List<ComponentViewModel>));
                List<OrderingViewModel> orderingViewModels = new List<OrderingViewModel>();
                if (componentsType == "parents")
                {
                    foreach (var component in components)
                    {
                        var parentElement = await db.PageComponentsHierarchies.FindAsync(component.componentId);
                        var childsOfThisParent = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == sliderComponentKey && p.ComponentOrder == parentElement.ComponentOrder && p.ComponentLevel != 1).ToListAsync();
                        int newComponentOrder = component.componentOrder;

                        OrderingViewModel orderingViewModel = new OrderingViewModel();
                        orderingViewModel.Key = parentElement.PageComponentHierarchyKey;
                        orderingViewModel.Order = newComponentOrder;
                        orderingViewModels.Add(orderingViewModel);

                        if (childsOfThisParent != null)
                        {
                            foreach (var child in childsOfThisParent)
                            {
                                var currentChild = await db.PageComponentsHierarchies.FindAsync(child.PageComponentHierarchyKey);
                                OrderingViewModel orderingViewModelChild = new OrderingViewModel();
                                orderingViewModelChild.Key = currentChild.PageComponentHierarchyKey;
                                orderingViewModelChild.Order = newComponentOrder;
                                orderingViewModels.Add(orderingViewModelChild);
                                //currentChild.ComponentOrder = newComponentOrder;
                                //await db.SaveChangesAsync();
                            }
                        }
                        //parentElement.ComponentOrder = newComponentOrder;
                        //await db.SaveChangesAsync();
                    }

                    foreach (var orderingObj in orderingViewModels)
                    {
                        var element = await db.PageComponentsHierarchies.FindAsync(orderingObj.Key);
                        element.ComponentOrder = orderingObj.Order;
                        await db.SaveChangesAsync();
                    }
                }
                else if (componentsType == "childrens")
                {
                    foreach (var component in components)
                    {
                        var childElement = await db.PageComponentsHierarchies.FindAsync(component.componentId);
                        childElement.ComponentIndex = component.componentOrder;
                        await db.SaveChangesAsync();
                    }
                }

                return await ReturnNavbar(pageName);
            }
            catch (Exception e)
            {
                return Json(400);
            }



        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetFileExtensionKeyForVideo()
        {
            string FileExtensionKeyForVideo = "";
            if (TempData.ContainsKey("FileExtensionKeyForVideo"))
                FileExtensionKeyForVideo = TempData["FileExtensionKeyForVideo"].ToString();
            return Json(FileExtensionKeyForVideo, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ImgUpload()
        {
            return Content("hi");
        }
    }
}