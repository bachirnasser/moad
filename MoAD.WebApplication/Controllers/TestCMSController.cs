﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Objects;
using MoAD.WebApplication.Models;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using Microsoft.AspNet.Identity;

namespace MoAD.WebApplication.Controllers
{

    [Authorize]
    public class TestCMSController : BaseController
    {
        // GET: TestCMS
        private Entities db = new Entities();

        public async Task<ActionResult> UploadFile(HttpPostedFileBase file)
        {

           // string uploadsFolderPath = Server.MapPath("~/Images/CMS/pdfs/");
            string uploadsFolderPath = Globals.Configuration.UPLOADS_CMS_PDF_FOLDER;
            //string uploadsFolderPath = Server.MapPath("~/Images/CMS/");
            if (!Directory.Exists(uploadsFolderPath))
            {
                Directory.CreateDirectory(uploadsFolderPath);
            }

            ExtendedData extendedData = new ExtendedData();
            try
            {

                string fileName = DateTime.Now.Ticks.ToString() + file.FileName;
                string uploadedFilePath = uploadsFolderPath;
                //    string uploadedFilePath = uploadsFolderPath;
                //return Json(uploadedFilePath);
                file.SaveAs(uploadedFilePath + fileName);

                extendedData.Name = fileName;
                extendedData.Type = "Pdf";
                extendedData.CreatedAt = DateTime.Now.ToString();
                extendedData.FilePathOrSource = uploadsFolderPath;

                db.ExtendedDatas.Add(extendedData);
                await db.SaveChangesAsync();

                var newExtendeddata = await db.ExtendedDatas.FindAsync(extendedData.FileExtensionKey);
                newExtendeddata.ColumnValue = newExtendeddata.FileExtensionKey.ToString();
                await db.SaveChangesAsync();

                Component newComponent = new Component();
                newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Pdf");
                newComponent.ComponentSourceEntityKey = newExtendeddata.ColumnValue;
                db.Components.Add(newComponent);
                await db.SaveChangesAsync();

                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = 1;
                pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                pageComponentsHierarchy.ComponentOrder = 1;
                pageComponentsHierarchy.ComponentIndex = 1;

                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();

                TempData["FileExtensionKeyForVideo"] = newExtendeddata.ColumnValue;
            }
            catch (Exception e)
            {
                return Json("Error");
            }

            return Json(extendedData.ColumnValue, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> Markaz()
        {
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = Managers.Utilities.CalculateMD5Hash(myIP);



            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            ViewBag.myIP = myIP;
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await NewPagesController.GetAllFormTemplatesForaType("Survey");

            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;

                if (User.Identity.IsAuthenticated && !User.IsInRole("Citizen"))
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;
                }
                else
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), null).Result;
                }

                ViewBag.formTemplateKeySurvey = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }


            string userKey = User.Identity.GetUserId();

            if (userKey != null)
            {
                MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                List<GetUserInformation_Result> userInfo = NewPagesController.GetUserInformation(userKey).Result;
                MoAD.WebApplication.Globals.Configuration.Rolekey = userInfo.FirstOrDefault().RoleId;
            }

            ViewBag.PageName = "Markaz";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();
            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }


            if (TemplateKeyParam != "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }
            else
            {
                ViewBag.Survey = new Dictionary<string, List<GetFormTemplate_Result>>();
            }

            //Navbar Part
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Markaz").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part


            int pageKey = (await db.Pages.Where(p => p.PageName == "Markaz").FirstOrDefaultAsync()).PageKey;
            var pageComponentVideo = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey && p.Component.ComponentType.ComponentType1 == "Video").FirstOrDefaultAsync();

            string videoUrlComponent = "";
            int? videoComponentPageKey = null;
            if (pageComponentVideo != null)
            {
                videoComponentPageKey = pageComponentVideo.Component.ComponentKey;
                videoUrlComponent = pageComponentVideo.Component.ComponentSourceEntityKey;
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.pageKey = pageKey;
            ViewBag.videoUrlComponent = videoUrlComponent;
            ViewBag.videoComponentPageKey = videoComponentPageKey;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
           



            var sliderItemComponent1 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            var sliderItemComponent2 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault();

            int? sliderItemComponentKey1 = null;
            if (sliderItemComponent1 != null)
            {
                sliderItemComponentKey1 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            int? sliderItemComponentKey2 = null;
            if (sliderItemComponent2 != null)
            {
                sliderItemComponentKey2 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault().ComponentKey;
            }
            var sliderItems1 = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey1).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder1 = sliderItems1.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();
            var sliderItems2 = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey2).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder2 = sliderItems2.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();
          

            ViewBag.SliderItemComponentKey1 = sliderItemComponentKey1;
            ViewBag.SliderData1 = sliderItemsGroupByOrder1;
            ViewBag.SliderItemComponentKey2 = sliderItemComponentKey2;
            ViewBag.SliderData2 = sliderItemsGroupByOrder2;
            //ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            return View();
        }

        public async Task<List<GetUserSurveyAnswer_Result>> GetUserSurveyAnswer(string ip = null, string cookie = null, string vk = null, string userkey = null)
        {
            List<GetUserSurveyAnswer_Result> result = new List<GetUserSurveyAnswer_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserSurveyAnswer/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("uk={0}", userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ip={0}", ip);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("cookie={0}", cookie);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ViewKey={0}", vk);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserSurveyAnswer_Result>>();
                }
            }

            return result;
        }

        public async Task<List<GetAllFormTemplatesForaType_Result>> GetFormTemplates(string TemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplatesForAType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateType={0}", TemplateType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();

                }
            }

            return result;
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }


        public async Task<ActionResult> Marsad()
        {
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }
        public async Task<ActionResult> Manbar()
        {
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Manbar").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }

        public async Task<ActionResult> UpdateCropTest()
        {


            return View();
        }

        public async Task<ActionResult> TestImageCustomization()
        {
            //Navbar Part
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Markaz").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentIndex).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part


            int pageKey = (await db.Pages.Where(p => p.PageName == "Markaz").FirstOrDefaultAsync()).PageKey;
            var pageComponentVideo = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey && p.Component.ComponentType.ComponentType1 == "Video").FirstOrDefaultAsync();

            string videoUrlComponent = "";
            int? videoComponentPageKey = null;
            if (pageComponentVideo != null)
            {
                videoComponentPageKey = pageComponentVideo.Component.ComponentKey;
                videoUrlComponent = pageComponentVideo.Component.ComponentSourceEntityKey;
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.pageKey = pageKey;
            ViewBag.videoUrlComponent = videoUrlComponent;
            ViewBag.videoComponentPageKey = videoComponentPageKey;
            //ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SendImage(string src,string key)
        {
            //string uploadsFolderPath = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Request.ApplicationPath.TrimEnd('/')) + "/Images/CMS/";
            //string uploadsFolderPath = Server.MapPath("~/Images/CMS/");
            string uploadsFolderPath = Globals.Configuration.UPLOADS_CMS_FOLDER;
            if (!Directory.Exists(uploadsFolderPath))
            {
                Directory.CreateDirectory(uploadsFolderPath);
            }
           
            ExtendedData extendedData = new ExtendedData();
            try
            {
                if (!await CheckIfFileExists(key)) {
                    var t = src.Substring(22);  // remove data:image/png;base64,

                    byte[] bytes = Convert.FromBase64String(t);

                    Image img;
                    var ms = new MemoryStream(bytes);

                    img = Image.FromStream(ms);



                    string fileName = DateTime.Now.Ticks.ToString() + "cms.png";
                   
                    System.IO.File.WriteAllBytes(uploadsFolderPath + fileName, bytes);
                    extendedData.Name = fileName;
                    extendedData.Type = "Image";
                    extendedData.CreatedAt = DateTime.Now.ToString();
                    extendedData.FilePathOrSource = uploadsFolderPath;

                    db.ExtendedDatas.Add(extendedData);
                    await db.SaveChangesAsync();

                    var newExtendeddata = await db.ExtendedDatas.FindAsync(extendedData.FileExtensionKey);
                    newExtendeddata.ColumnValue = newExtendeddata.FileExtensionKey.ToString();
                    await db.SaveChangesAsync();

                    Component newComponent = new Component();
                    newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Image");
                    newComponent.ComponentSourceEntityKey = newExtendeddata.ColumnValue;
                    db.Components.Add(newComponent);
                    await db.SaveChangesAsync();

                    PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                    pageComponentsHierarchy.PageKey = 1;
                    pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                    pageComponentsHierarchy.ComponentOrder = 1;
                    pageComponentsHierarchy.ComponentIndex = 1;

                    db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                    await db.SaveChangesAsync();

                    TempData["FileExtensionKeyForVideo"] = newExtendeddata.ColumnValue;
                }
                else
                {
                  
                    var t = src.Substring(22);  // remove data:image/png;base64,

                    byte[] bytes = Convert.FromBase64String(t);

                    Image img;
                    var ms = new MemoryStream(bytes);

                    img = Image.FromStream(ms);



                    string fileName = DateTime.Now.Ticks.ToString() + "cms.png";

                    System.IO.File.WriteAllBytes(uploadsFolderPath + fileName, bytes);
                    var editExtendedData =await db.ExtendedDatas.Where(e=>e.ColumnValue==key).FirstOrDefaultAsync();
                    if(editExtendedData != null)
                    {
                        editExtendedData.Name = fileName;
                        await db.SaveChangesAsync();
                    }
                    extendedData = editExtendedData;
                }
              
             
            }
            catch (Exception e)
            {
                return Json(e.ToString());
            }

            return Json(extendedData.FilePathOrSource+extendedData.Name, JsonRequestBehavior.AllowGet);
           


           
        }

        public async  Task<bool> CheckIfFileExists(string key)
        {
            if (key == "") return false;
            using (Entities db = new Entities())
            {
                var extendedData = db.ExtendedDatas.Where(e => e.ColumnValue == key).FirstOrDefault();
                if(extendedData != null)
                {
                    //string folderPath = Server.MapPath("~/"+ extendedData.FilePathOrSource + extendedData.Name);
                    //string filePath = extendedData.FilePathOrSource + extendedData.Name;
                    string folderPath = extendedData.FilePathOrSource + extendedData.Name;
                    string filePath = folderPath;

                    if (System.IO.File.Exists(folderPath))
                    {
                        System.IO.File.Delete(filePath);
                        return true;
                    }
                    return true;
                }
            }
            return false;

        }

        public async Task<int> GetComponentTypeKeyByComponentTypeName(string type)
        {
            int componentTypeKey = (await db.ComponentTypes.Where(c => c.ComponentType1 == type).FirstOrDefaultAsync()).ComponentTypeKey;
            return componentTypeKey;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveVideoPerPage(int pageKey,string link,int? componentKey=null)
        {
            try
            {
                if (componentKey != null)
                {
                    Component component = new Component();
                    component = await db.Components.FindAsync(componentKey);
                    component.ComponentSourceEntityKey = link;
                    await db.SaveChangesAsync();
                }
                else
                {
                    Component component = new Component();
                    component.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Video");
                    component.ComponentSourceEntityKey = link;
                    db.Components.Add(component);
                    await db.SaveChangesAsync();

                    PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                    pageComponentsHierarchy.PageKey = pageKey;
                    pageComponentsHierarchy.ComponentKey = component.ComponentKey;
                    pageComponentsHierarchy.ComponentOrder = 1;
                    pageComponentsHierarchy.ComponentIndex = 1;

                    db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                    await db.SaveChangesAsync();
                }
            }catch(Exception e)
            {
                return Json(404);
            }
            
            return Json(200);
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetAllPages()
        {
            List<Page> pages = new List<Page>();
            pages = await db.Pages.Where(p => p.PageName != "Markaz" && p.PageName != "Marsad" && p.PageName != "Manbar").ToListAsync();

            var result = pages.Select(p => new
            {
                Text = p.PageName,
                Value = p.PageKey,
            }).ToList().OrderByDescending(p=>p.Value);
            return Json(result,JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeLanguageInModal(string componentName="", string componentStringObject="")
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
           
            if(componentName.Length != 0 && componentName.Equals("singleElement"))
            {
                SingleElementViewModel singleElement = (SingleElementViewModel)js.Deserialize(componentStringObject, typeof(SingleElementViewModel));
                return PartialView("_SingleElementModal",singleElement);
            }    
            if (componentName.Length != 0 && componentName.Equals("card"))
                {
                    CardModalViewModel card = (CardModalViewModel)js.Deserialize(componentStringObject, typeof(CardModalViewModel));
                    return PartialView("_CardModal", card);
                }

          




            return Content("404");

        }
     
    }
}