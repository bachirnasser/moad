﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.DataObjects;
using Microsoft.AspNet.Identity;
using System.Web.Script.Serialization;
//using MoAD.DataInterface.EDMX;

namespace MoAD.WebApplication.Controllers
{
    //[Globals.YourCustomAuthorize]
    public class OrganisationChartsController : Controller
    {
        // GET: OrganisationCharts
        public async Task<ActionResult> Index(string formInstanceKey = "")
        {

            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "137");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }

            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                JsonResult json = Json(FormSaved, JsonRequestBehavior.AllowGet);
                ViewBag.FormValues = json;
            }

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            string FormTemplateType = GetTypeForaFormTemplate("137").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            ViewBag.formInstanceKey = formInstanceKey;
            return View(nestedResult);
        }

        public async Task<ActionResult> OrganisationChartVersion2(string formInstanceKey = "")

        {

            ViewBag.HighlightedPart = "menu-tanzim";
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "137");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }

            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                JsonResult json = Json(FormSaved, JsonRequestBehavior.AllowGet);
                ViewBag.FormValues = json;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            ViewBag.OrganisationName = UserInfo.FirstOrDefault().OrganisationName;
            string FormTemplateType = GetTypeForaFormTemplate("137").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            ViewBag.formInstanceKey = formInstanceKey;
            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "137").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View(nestedResult);
            return View();
        }
        public async Task<ActionResult> TableForm(string formInstanceKey = "")
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "141");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }

            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.FormValues = FormSaved;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            string FormTemplateType = GetTypeForaFormTemplate("141").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            ViewBag.formInstanceKey = formInstanceKey;

            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "141").Result;

            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View(nestedResult);
        }


        public async Task<ActionResult> TableFormChariha3(string formInstanceKey = "")
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "140");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    ViewBag.FormTemplateName = result.FirstOrDefault().FormTemplateName;
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;

            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;

            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
                ViewBag.FormValues = FormSaved;
            }
            string FormTemplateType = GetTypeForaFormTemplate("140").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            ViewBag.formInstanceKey = formInstanceKey;
            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "140").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View(nestedResult);
        }
        public async Task<ActionResult> TableFormHaikalWazifi(string formInstanceKey = "")
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "138");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }
            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                JsonResult json = Json(FormSaved, JsonRequestBehavior.AllowGet);
                ViewBag.FormValues = json;
            }
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            ViewBag.formInstanceKey = formInstanceKey;
            string FormTemplateType = GetTypeForaFormTemplate("138").Result;
            ViewBag.FormTemplateType = FormTemplateType;
            return View(nestedResult);
        }



        public async Task<ActionResult> TableFormHaikalWazifiNew(string formInstanceKey = "")
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "138");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    ViewBag.FormTemplateName = result.FirstOrDefault().FormTemplateName;
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }
            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.FormValues = FormSaved;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            ViewBag.formInstanceKey = formInstanceKey;
            string FormTemplateType = GetTypeForaFormTemplate("138").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "138").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View(nestedResult);
        }


        public async Task<ActionResult> AltadribWaltashilAlmoustamer(string formInstanceKey = "")
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "142");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //FormTemplateTitle = result.First().FormTemplateName;
                    ViewBag.FormTemplateName = result.FirstOrDefault().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }
            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.FormValues = FormSaved;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            ViewBag.formInstanceKey = formInstanceKey;
            string FormTemplateType = GetTypeForaFormTemplate("142").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "142").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View(nestedResult);
        }


        public async Task<ActionResult> CharihaOulaRepeatedQuestions(string formInstanceKey = "")
        {

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.FormValues = FormSaved;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }
            ViewBag.formInstanceKey = formInstanceKey;
            string FormTemplateType = GetTypeForaFormTemplate("118").Result;
            ViewBag.FormTemplateType = FormTemplateType;

            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "118").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View();
        }


        public async Task<ActionResult> CharihaThalithaRepeatedQuestions(string formInstanceKey = "")
        {

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.FormValues = FormSaved;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }
            ViewBag.formInstanceKey = formInstanceKey;
            string FormTemplateType = GetTypeForaFormTemplate("120").Result;
            ViewBag.FormTemplateType = FormTemplateType;
            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "120").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View();
        }



        public async Task<ActionResult> Charihakhamisa(string formInstanceKey = "")
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "162");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    //FormTemplateTitle = result.First().FormTemplateName;
                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new DataObjects.Answer
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<DataObjects.Answer>()
                                }).ToList<DataObjects.Question>();
                }
            }
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            ViewBag.OrganisationKey = UserInfo.FirstOrDefault().OrganisationKey;
            if (formInstanceKey != "")
            {
                List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result;
                ViewBag.FormValues = FormSaved;
                ViewBag.Staus = FormSaved.FirstOrDefault().FormStatus;
            }
            string FormTemplateType = GetTypeForaFormTemplate("162").Result;
            ViewBag.FormTemplateType = FormTemplateType;
            ViewBag.formInstanceKey = formInstanceKey;

            List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
            OpOrganisations = ClaimFormsController.GetOPOrganisations(UserInfo.FirstOrDefault().RoleId, "162").Result;
            ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View(nestedResult);
        }

        public async Task<List<GetSavedForm_Result>> GetSavedForm(string FormKey)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", FormKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
                }
            }
            return result;
        }



        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

        public async Task<string> GetTypeForaFormTemplate(string formTemplateKey)
        {
            string result = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetTypeForaFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }

            return result;

        }

    }
}