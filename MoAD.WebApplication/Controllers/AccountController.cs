﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MoAD.WebApplication.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Collections;
using System.Reflection;
using MoAD.DataObjects.EDMX;
using System.Net.Http.Headers;
using System.Text;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Configuration;
using System.Security.Cryptography;
using MoAD.WebApplication.Objects;

namespace MoAD.WebApplication.Controllers
{
    //[Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public static string FormTemplateKey = "";
        public static string OrganisationKey = "";
        public static string PageLoginIndex = "";
        public static string ManagerPageLoginIndex = "";
        private Entities db = new Entities();
        //public static string MouwazafIndexValue = "";
        public static int FailedLogedInNumber = 0;



        public async Task<ActionResult> Users()
        {

            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            return View();
        }
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        
        [AllowAnonymous]
        public ActionResult LoginView(string returnUrl = null, string NextPageIndex = null, string MouwazafIndex = null, string ManagerIndex = null)
        {

            //Session["FailedLogedInNumber"] = FailedLogedInNumber;





            Session["ManagerPageLoginIndex"] = ManagerIndex;



            if (NextPageIndex != null)
            {
                PageLoginIndex = NextPageIndex;

            }

            //if (ManagerIndex != null)
            //{
            //    ManagerPageLoginIndex = ManagerIndex;

            //}


            //if (MouwazafIndex != null)
            //{
            //    MouwazafIndexValue = MouwazafIndex;

            //}
            ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }


        //[AllowAnonymous]
        //public ActionResult LoginToResult(string AnswerIds)
        //{
        //    answersIds = AnswerIds;
        //    return View("Login");
        //}
        [AllowAnonymous]

        public ActionResult LoginToResult(string Organization, string FormTemplate)
        {

            FormTemplateKey = FormTemplate;
            OrganisationKey = Organization;
            return View("Login");
        }

        //
        // POST: /Account/Login
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)

        {
            //ManagerPageLoginIndex
            //if (FailedLogedInNumber > 5)
            //{
            //    var response = Request["g-recaptcha-response"];
            //    string secretKey = "6LcaGEIUAAAAADIrMnb7z5R88qt_jWscRXZFhnLm";
            //    var client = new WebClient();
            //    var result2 = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            //    var obj = Newtonsoft.Json.Linq.JObject.Parse(result2);
            //    var status = (bool)obj.SelectToken("success");

            //    if (status == false)
            //    {
            //        Session["CaptchaMessage"] = "الرجاء التأكد أنك لست برنامج روبوت";
            //        return RedirectToAction("LoginView", "Account");
            //    }
            //}






            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {

                case SignInStatus.Success:
                    FailedLogedInNumber = 0;
                    ApplicationUser LoggedInUser = UserManager.FindByName(model.Email);
                    string UserId = LoggedInUser.Id;
                    var role = UserManager.GetRoles(UserId);

                    if (role.First() == "Citizen" || role.First() == "Employee" || role.First() == "Expert" || (role.First() == "ADManager" || role.First() == "ADDirector") || (role.First() == "OPManager" || role.First() == "OPDirector") || role.First() == "OPGeneralDirector" || role.First() == "Administrator" || role.First() == "Technicien" || role.First() == "Human Development Sector" || role.First() == "Administrative sector" || role.First() == "Economic sector" || role.First() == "PB" || role.First() == "HR" || role.First() == "Editor")
                    {
                        if(role.First() == "Editor")
                        {
                            await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                            string userKey = LoggedInUser.Id;
                            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                            return RedirectToAction("Markaz", "TestCMS");
                        }

                        //if (FormTemplateKey != "")
                        //{
                        //    string formkey = FormTemplateKey;
                        //    string orgkey = OrganisationKey;
                        //    FormTemplateKey = "";
                        //    OrganisationKey = "";

                        //    await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                        //    string userKey = LoggedInUser.Id;
                        //    MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                        //    return RedirectToAction("NewClaimForm", "ClaimForms", new { @OrganizationKey = orgkey, @FormTemplateKey = formkey });
                        //    //(string OrganizationKey = null, string FormTemplateKey = null)
                        //}
                        //else
                        //{
                            if (PageLoginIndex != null && PageLoginIndex != "")
                            {


                                string page = PageLoginIndex;
                                PageLoginIndex = null;
                                if (page == "114" && role.First() == "Citizen")
                                {
                                    await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                    string userKey = LoggedInUser.Id;
                                    MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                    return RedirectToAction("Manbar", "Home");
                                }
                           

                                else
                                {
                                await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                string userKey = LoggedInUser.Id;
                                MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                return RedirectToAction("NewClaimForm", "ClaimForms", new { FormTemplateKey = page });

                                }



                            }
                            else
                            {
                                if ((role.First() == "ADManager" || role.First() == "ADDirector") || role.First() == "HR")
                                {

                                    if (model.ManagerPageLoginIndex != null && model.ManagerPageLoginIndex != "")
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                    if (role.First() == "HR")
                                    {
                                        return RedirectToAction("Index", "ERMEmployees");
                                    }
                                    else
                                    {
                                        return RedirectToAction("AdUsersLandingPage", "Home");
                                    }                                        
                                    }
                                    else
                                    {

                                        Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        
                                        return RedirectToAction("LoginView", "Account");
                                    }
                                }




                                else if (role.First() == "Administrator")
                                {

                                    if (model.ManagerPageLoginIndex != null && model.ManagerPageLoginIndex != "")
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                        return RedirectToAction("Index", "Home");
                                    }
                                    else
                                    {
                                       // await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                        Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                        return RedirectToAction("LoginView", "Account");
                                    }
                                }
                                else if ((role.First() == "OPManager" || role.First() == "OPDirector") || role.First() == "OPGeneralDirector" || role.First() == "Pb" || role.First() == "Expert" || role.First() == "Technicien" || role.First() == "Human Development Sector" || role.First() == "Administrative sector" || role.First() == "Economic sector")
                                {
                                    if (model.ManagerPageLoginIndex != null && model.ManagerPageLoginIndex != "")
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                        return RedirectToAction("Index", "UserDashboard");
                                    }
                                    else
                                    {
                                       // await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                        Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                        return RedirectToAction("LoginView", "Account");
                                    }
                                }
                                else if ((role.First() == "Editor"))
                                {
                                    if (model.ManagerPageLoginIndex != null && model.ManagerPageLoginIndex != "")
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                        return RedirectToAction("Index", "MouwazafinWithOrgs");
                                    }
                                    else
                                    {
                                       // await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                        Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                        return RedirectToAction("LoginView", "Account");
                                    }
                                }
                                else if ((role.First() == "PB"))
                                {
                                    if (model.ManagerPageLoginIndex != null && model.ManagerPageLoginIndex != "")
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                        string Rolekey = LoggedInUser.Roles.FirstOrDefault().RoleId;
                                        MoAD.WebApplication.Globals.Configuration.Rolekey = Rolekey;
                                        return RedirectToAction("Index", "PBDashboard");
                                    }
                                    else
                                    {
                                        //await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                        Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                        return RedirectToAction("LoginView", "Account");
                                    }
                                }
                                else if ((role.First() == "Employee"))
                                {
                                    //if (model.ManagerPageLoginIndex != null && model.ManagerPageLoginIndex != "")
                                    //{
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                        if (LoggedInUser.Approved == "APPROVED")
                                        {
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                        return RedirectToAction("Manbar", "Home");
                                        }
                                        else
                                        {
                                    Session["LoginStatus"] = "بانتظار الموافقة على حسابك";
                                    string userKey = User.Identity.GetUserId();
                                    await SetLog(userKey, "صفحة الخروج", "الخروج", "قام المستخدم بالخروج");
                                    // await Signout();

                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    //await LogOff();
                                    return RedirectToAction("LoginView", "Account");
                                }

                                   // }
                                    //else
                                    //{
                                    //    await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                    //    Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                    //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    //    return RedirectToAction("LoginView", "Account");
                                    //}
                                }
                                else
                                {
                                    if (model.ManagerPageLoginIndex == null || model.ManagerPageLoginIndex == "")
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");
                                        string userKey = LoggedInUser.Id;
                                        MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                                        return RedirectToAction("Manbar", "Home");

                                    }
                                    else
                                    {
                                        await SetLog(UserId, "صفحة تسجيل الدخول", "تسجيل الدخول", "قام المستخدم بتسجيل الدخول");

                                        Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                        return RedirectToAction("LoginView", "Account", new { ManagerIndex = "True" });
                                    }
                                }
                            }

                       // }

                    }


                    //string test = Roles.GetRolesForUser(User.Identity.Name);
                    //return RedirectToAction("EmployeeMain", "UserManagement");


                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    //FailedLogedInNumber++;
                    //Session["FailedLogedInNumber"] = FailedLogedInNumber;
                    ModelState.AddModelError("", "محاولة تسجيل دخول غير صالحة.");
                    //ViewBag.ErrorMessage = "محاولة تسجيل دخول غير صالحة";
                    Session["LoginStatus"] = "محاولة تسجيل دخول غير صالحة";
                   var UserAtemptFailed= db.AspNetUsers.Where(x => x.Email == model.Email).ToList();
                    if (UserAtemptFailed.Count > 0)
                    {
                        await UserManager.AccessFailedAsync(UserAtemptFailed.FirstOrDefault().Id);
                        
                    }


                    return RedirectToAction("LoginView", "Account", new { ManagerIndex = model.ManagerPageLoginIndex });
                    // return View(model);
            }
            PageLoginIndex = null;

        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult RegisterView(string ManagerRegisterIndex = null)
        {
            //if (ViewBag.ManagerRegisterIndex == null)
            //    Session["ManagerRegisterIndex"] = "False";
            //else
                Session["ManagerRegisterIndex"] = Session["ManagerPageLoginIndex"];

            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            return View("Register");
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        public async Task<List<GetUsersToCreate_Result>> GetUsersToCreate()
        {

            List<GetUsersToCreate_Result> result = new List<GetUsersToCreate_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUsersToCreate/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUsersToCreate_Result>>();


                }
            }
            return result;
        }

        public async Task<JsonResult> GetAllOrganisations(string OrganisationKey = null, string OPManagerRoleGroupKey = null)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                //requestUriBuilder.Append("?");
                //requestUriBuilder.AppendFormat("OrganisationKey={0}", OrganisationKey);
                //requestUriBuilder.Append("&");
                //requestUriBuilder.AppendFormat("OPManagerRoleGroupKey={0}", OPManagerRoleGroupKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterAdmin(RegisterViewModel model)
        {

             List<GetAllOrganisations_Result> AllOrgs  = await GetAllOrganisations();

            foreach (var item in AllOrgs)
            {
                var user = new ApplicationUser
                {
                    UserName = "مدير التنمية الإدارية" + item.OrganisationName,
                    Email = "email@email.com",
                    PhoneNumber = "123456",
                    FirstName = "مدير التنمية الإدارية" + item.OrganisationName,
                    FatherName = "",
                    LastName = "",
                    SSN = "11111",
                    Sex = "Male",
                    MobileNumber = "123456",
                    YearsOfDuty = 1,
                    SecretNumber = "qwewq",
                    UserImage = ""
                };

                var result = UserManager.CreateAsync(user, "123456").Result;

                //var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, FirstName = model.FirstName, FatherName = model.FatherName, LastName=model.LastName, SSN=model.SSN, Sex=model.Sex, MobileNumber=model.MobileNumber, YearsOfDuty=model.YearsOfDuty, SecretNumber=model.SecretNumber, UserImage=model.UserImage };
                //var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {

                    ApplicationDbContext context = new ApplicationDbContext();
                    UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                    UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
                    String userId = user.Id;
                    //String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                    ApplicationUser cUser = await store.FindByIdAsync(userId);

                    //string oldrole = UserManager.GetRoles(cUser.Id)[0].ToString();
                    ////await store.SetPasswordHashAsync(cUser, hashedNewPassword);
                    //await store.RemoveFromRoleAsync(cUser, oldrole);

                    await store.AddToRoleAsync(cUser, "ADManager");
                    await store.UpdateAsync(cUser);
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    await EntidabSave(item.OrganisationKey.ToString(), user.Id);


                }
            }


            //if (!model.UserType.Equals("مواطن") && (model.SecretNumber == null || model.SecretNumber == string.Empty))
            //{
            //    Session["RegisterError"] = "الرجاء إدخل رقم سري";

            //    return RedirectToAction("Users", "Account");

            //}




            ////if (!ValidatePassword(model.Password, model.ConfirmPassword))
            ////    return RedirectToAction("Register", "Account");
            //if (!ModelState.IsValid)
            //{

            //    Session["RegisterError"] = "كلمات السر غير مطابقة";

            //    return RedirectToAction("Users", "Account");

            //}
            //if (ModelState.IsValid)
            //{

            //    HttpPostedFileBase file = Request.Files["UserImage"];
            //    if (file != null)
            //    {
            //        string theFileName = Path.GetFileName(file.FileName);
            //        byte[] thePictureAsBytes = new byte[file.ContentLength];
            //        using (BinaryReader theReader = new BinaryReader(file.InputStream))
            //        {
            //            thePictureAsBytes = theReader.ReadBytes(file.ContentLength);
            //        }
            //        model.UserImage = Convert.ToBase64String(thePictureAsBytes);
            //        //var UserImage = new byte[file.ContentLength];
            //        //string thePictureDataAsString = Convert.ToBase64String(UserImage);
            //        ////file.InputStream.Read(model.UserImage, 0, file.ContentLength);
            //    }
            //    var user = new ApplicationUser
            //    {
            //        UserName = model.Email,
            //        Email = model.Email,
            //        PhoneNumber = model.PhoneNumber,
            //        FirstName = model.FirstName,
            //        FatherName = model.FatherName,
            //        LastName = model.LastName,
            //        SSN = model.SSN,
            //        Sex = model.Sex,
            //        MobileNumber = model.MobileNumber,
            //        YearsOfDuty = model.YearsOfDuty,
            //        SecretNumber = model.SecretNumber,
            //        UserImage = model.UserImage
            //    };

            //    var result = UserManager.CreateAsync(user, model.Password).Result;

            //    //var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, FirstName = model.FirstName, FatherName = model.FatherName, LastName=model.LastName, SSN=model.SSN, Sex=model.Sex, MobileNumber=model.MobileNumber, YearsOfDuty=model.YearsOfDuty, SecretNumber=model.SecretNumber, UserImage=model.UserImage };
            //    //var result = await UserManager.CreateAsync(user, model.Password);
            //    if (result.Succeeded)
            //    {

            //        ApplicationDbContext context = new ApplicationDbContext();
            //        UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            //        UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            //        String userId = user.Id;
            //        //String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            //        ApplicationUser cUser = await store.FindByIdAsync(userId);

            //        //string oldrole = UserManager.GetRoles(cUser.Id)[0].ToString();
            //        ////await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            //        //await store.RemoveFromRoleAsync(cUser, oldrole);
            //        if (model.UserType.Equals("مواطن"))
            //        {
            //            await store.AddToRoleAsync(cUser, "citizen");
            //            await store.UpdateAsync(cUser);
            //            //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            //            await EntidabSave("3", user.Id);
            //        }
            //        else
            //        {
            //            await store.AddToRoleAsync(cUser, "Employee");
            //            await store.UpdateAsync(cUser);
            //            //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            //            await EntidabSave(model.Organisation, user.Id);

            //        }


            //        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            //        // Send an email with this link
            //        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            //        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            //        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
            //        string userKey = User.Identity.GetUserId();

            //        return RedirectToAction("Index", "AspNetUsers");

            //    }

            //    //Session["RegisterError"] = "Passwords must be at least 6 characters.";
            //    AddErrors(result);
            //}

            //Session["RegisterError"] = "اسم المستخدم موجود";

            // If we got this far, something failed, redisplay form
            //return View("Login");
            return RedirectToAction("Users", "Account");
            //return View(model);
        }

        public async Task<string> SubmitFormForUser(string trackingStatusKey = null, string formTemplateKey = null, string userKey = null, string organisationKey = null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SubmitFormForUser");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("trackingStatusKey={0}", trackingStatusKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userKey={0}", userKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<string> SignupAnswers(string formkey = null,string answerTemplateKey = null, string answer = null,string type=null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SignupAnswers");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("FormKey={0}", formkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("answerTemplateKey={0}", answerTemplateKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("answer={0}", answer);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("type={0}", type);
                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }
        

        public async Task<string> SaveClaimForm(string formKey = null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SaveClaimForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", formKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

       

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            List<AuthenticationCodeCheck_Result> CodeCheck = new List<AuthenticationCodeCheck_Result>();

            if (!model.UserType.Equals("مواطن") && (model.SecretNumber == null || model.SecretNumber == string.Empty))
            {
                Session["RegisterError"] = "الرجاء إدخل رقم سري.";
                Session["UserType"] = "موظف";

                return RedirectToAction("LoginView", "Account");

            }
            if (!model.UserType.Equals("مواطن"))
            {
                CodeCheck = await AuthenticationCodeCheck(model.SecretNumber);
                if (CodeCheck.FirstOrDefault().AuthenticationCodeCheck == "Failed")
                {
                    Session["RegisterError"] = "الرقم السري غير صالح.";
                    Session["UserType"] = "موظف";

                    return RedirectToAction("LoginView", "Account");
                }
            }



            //if (!ValidatePassword(model.Password, model.ConfirmPassword))
            //    return RedirectToAction("Register", "Account");
            if (!ModelState.IsValid)
            {

                Session["RegisterError"] = "كلمات السر غير مطابقة.";
                Session["UserType"] = model.UserType;

                return RedirectToAction("LoginView", "Account");

            }
            if (ModelState.IsValid)
            {
                var allorgs = await GetUsersToCreate();
                foreach (var item in allorgs)
                {
                    HttpPostedFileBase file = Request.Files["UserImage"];
                    if (file != null)
                    {
                        string theFileName = Path.GetFileName(file.FileName);
                        byte[] thePictureAsBytes = new byte[file.ContentLength];
                        using (BinaryReader theReader = new BinaryReader(file.InputStream))
                        {
                            thePictureAsBytes = theReader.ReadBytes(file.ContentLength);
                        }
                        model.UserImage = Convert.ToBase64String(thePictureAsBytes);
                        //var UserImage = new byte[file.ContentLength];
                        //string thePictureDataAsString = Convert.ToBase64String(UserImage);
                        ////file.InputStream.Read(model.UserImage, 0, file.ContentLength);
                    }

                    model.Approved = "APPROVED";
                    var user = new ApplicationUser
                    {
                        UserName = item.UserName,
                        Email = item.UserName,
                        PhoneNumber = model.PhoneNumber,
                        FirstName = model.FirstName,
                        FatherName = model.FatherName,
                        LastName = model.LastName,
                        SSN = model.SSN,
                        Sex = model.Sex,
                        MobileNumber = model.MobileNumber,
                        YearsOfDuty = model.YearsOfDuty,
                        SecretNumber = model.SecretNumber,
                        UserImage = model.UserImage,
                        Approved = model.Approved
                    };
                    var result = UserManager.CreateAsync(user, model.Password).Result;

                    //var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, FirstName = model.FirstName, FatherName = model.FatherName, LastName=model.LastName, SSN=model.SSN, Sex=model.Sex, MobileNumber=model.MobileNumber, YearsOfDuty=model.YearsOfDuty, SecretNumber=model.SecretNumber, UserImage=model.UserImage };
                    //var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {

                        ApplicationDbContext context = new ApplicationDbContext();
                        UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                        UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
                        String userId = user.Id;
                        //String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
                        ApplicationUser cUser = await store.FindByIdAsync(userId);

                        //string oldrole = UserManager.GetRoles(cUser.Id)[0].ToString();
                        ////await store.SetPasswordHashAsync(cUser, hashedNewPassword);
                        //await store.RemoveFromRoleAsync(cUser, oldrole);
                        if (model.UserType.Equals("مواطن"))
                        {
                            await store.AddToRoleAsync(cUser, item.Role);
                            await store.UpdateAsync(cUser);
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            await EntidabSave(item.OrganisationKey.ToString(), user.Id);
                        }
                        else
                        {

                            string RoleName = CodeCheck.FirstOrDefault().Role;
                            await store.AddToRoleAsync(cUser, RoleName);
                            await store.UpdateAsync(cUser);
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            await EntidabSave(model.Organisation, user.Id);
                            string userRole = user.Roles.FirstOrDefault().RoleId;


                            if (userRole == "24")
                            {
                                string formKey = await SubmitFormForUser("25", "183", user.Id, model.Organisation);


                                await SignupAnswers(formKey, "7765", model.Email, "UserInput");
                                await SignupAnswers(formKey, "7766", model.PhoneNumber, "UserInput");
                                await SignupAnswers(formKey, "7767", model.LastName, "UserInput");

                                if (model.Sex == "ذكر")
                                    await SignupAnswers(formKey, "7768", model.Sex, "Radio");
                                else if (model.Sex == "أنثى")
                                    await SignupAnswers(formKey, "7769", model.Sex, "Radio");

                                await SignupAnswers(formKey, "7770", model.SSN, "UserInput");
                                await SignupAnswers(formKey, "7777", model.PhoneNumber, "UserInput");
                                await SignupAnswers(formKey, "7771", model.Email, "UserInput");
                                await SignupAnswers(formKey, "7773", model.Organisation, "UserInput");
                                await SignupAnswers(formKey, "7774", model.YearsOfDuty.ToString(), "UserInput");
                                await SignupAnswers(formKey, "7776", model.UserImage, "FileUpload");

                                await SaveClaimForm(formKey);
                                Session["RegisterError"] = "بانتظار الموافقة على حسابك";
                                //  return RedirectToAction("LoginView", "Account");

                            }


                        }


                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        if (FormTemplateKey != "")
                        {
                            string formkey = FormTemplateKey;
                            string orgkey = OrganisationKey;
                            FormTemplateKey = "";
                            OrganisationKey = "";

                            // return RedirectToAction("NewClaimForm", "ClaimForms", new { @OrganizationKey = orgkey, @FormTemplateKey = formkey });
                        }
                        else
                        {
                            await AssignedCodeToUser(user.Id, model.SecretNumber);
                            //if (!model.UserType.Equals("مواطن"))
                            //                       {
                            //Wait For Approval
                            //  await LogOff();
                            //}



                            //else
                            //{                       
                            // return RedirectToAction("Index", "Home");

                            // }


                        }
                    }

                    //Session["RegisterError"] = "Passwords must be at least 6 characters.";
                    AddErrors(result);
                }

            //    HttpPostedFileBase file = Request.Files["UserImage"];
            //if (file != null)
            //{
            //    string theFileName = Path.GetFileName(file.FileName);
            //    byte[] thePictureAsBytes = new byte[file.ContentLength];
            //    using (BinaryReader theReader = new BinaryReader(file.InputStream))
            //    {
            //        thePictureAsBytes = theReader.ReadBytes(file.ContentLength);
            //    }
            //    model.UserImage = Convert.ToBase64String(thePictureAsBytes);
            //    //var UserImage = new byte[file.ContentLength];
            //    //string thePictureDataAsString = Convert.ToBase64String(UserImage);
            //    ////file.InputStream.Read(model.UserImage, 0, file.ContentLength);
            //}

            //model.Approved = "NOTAPPROVED";
            //var user = new ApplicationUser
            //{
            //    UserName = model.Email,
            //    Email = model.Email,
            //    PhoneNumber = model.PhoneNumber,
            //    FirstName = model.FirstName,
            //    FatherName = model.FatherName,
            //    LastName = model.LastName,
            //    SSN = model.SSN,
            //    Sex = model.Sex,
            //    MobileNumber = model.MobileNumber,
            //    YearsOfDuty = model.YearsOfDuty,
            //    SecretNumber = model.SecretNumber,
            //    UserImage = model.UserImage,
            //    Approved = model.Approved
            //};
            //var result = UserManager.CreateAsync(user, model.Password).Result;

            ////var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, FirstName = model.FirstName, FatherName = model.FatherName, LastName=model.LastName, SSN=model.SSN, Sex=model.Sex, MobileNumber=model.MobileNumber, YearsOfDuty=model.YearsOfDuty, SecretNumber=model.SecretNumber, UserImage=model.UserImage };
            ////var result = await UserManager.CreateAsync(user, model.Password);
            //if (result.Succeeded)
            //{

            //    ApplicationDbContext context = new ApplicationDbContext();
            //    UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            //    UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            //    String userId = user.Id;
            //    //String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            //    ApplicationUser cUser = await store.FindByIdAsync(userId);

            //    //string oldrole = UserManager.GetRoles(cUser.Id)[0].ToString();
            //    ////await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            //    //await store.RemoveFromRoleAsync(cUser, oldrole);
            //    if (model.UserType.Equals("مواطن"))
            //    {
            //        await store.AddToRoleAsync(cUser, "citizen");
            //        await store.UpdateAsync(cUser);
            //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            //        await EntidabSave("3", user.Id);
            //    }
            //    else
            //    {

            //        string RoleName = CodeCheck.FirstOrDefault().Role;
            //        await store.AddToRoleAsync(cUser, RoleName);
            //        await store.UpdateAsync(cUser);
            //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            //        await EntidabSave(model.Organisation, user.Id);
            //        string userRole = user.Roles.FirstOrDefault().RoleId;


            //        if (userRole == "24")
            //        {
            //            string formKey = await SubmitFormForUser("25", "183", user.Id, model.Organisation);


            //            await SignupAnswers(formKey, "7765", model.Email, "UserInput");
            //            await SignupAnswers(formKey, "7766", model.PhoneNumber, "UserInput");
            //            await SignupAnswers(formKey, "7767", model.LastName, "UserInput");

            //            if (model.Sex == "ذكر")
            //                await SignupAnswers(formKey, "7768", model.Sex, "Radio");
            //            else if (model.Sex == "أنثى")
            //                await SignupAnswers(formKey, "7769", model.Sex, "Radio");

            //            await SignupAnswers(formKey, "7770", model.SSN, "UserInput");
            //            await SignupAnswers(formKey, "7777", model.PhoneNumber, "UserInput");
            //            await SignupAnswers(formKey, "7771", model.Email, "UserInput");
            //            await SignupAnswers(formKey, "7773", model.Organisation, "UserInput");
            //            await SignupAnswers(formKey, "7774", model.YearsOfDuty.ToString(), "UserInput");
            //            await SignupAnswers(formKey, "7776", model.UserImage, "FileUpload");

            //            await SaveClaimForm(formKey);
            //            Session["RegisterError"] = "بانتظار الموافقة على حسابك";
            //            string userKey = User.Identity.GetUserId();
            //            await SetLog(userKey, "صفحة الخروج", "الخروج", "قام المستخدم بالخروج");
            //            // await Signout();

            //            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //            return RedirectToAction("LoginView", "Account");

            //        }


            //    }


            //    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            //    // Send an email with this link
            //    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            //    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            //    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
            //    if (FormTemplateKey != "")
            //    {
            //        string formkey = FormTemplateKey;
            //        string orgkey = OrganisationKey;
            //        FormTemplateKey = "";
            //        OrganisationKey = "";

            //        return RedirectToAction("NewClaimForm", "ClaimForms", new { @OrganizationKey = orgkey, @FormTemplateKey = formkey });
            //    }
            //    else
            //    {
            //        await AssignedCodeToUser(user.Id, model.SecretNumber);
            //        //if (!model.UserType.Equals("مواطن"))
            //        //                       {
            //        //Wait For Approval
            //        //  await LogOff();
            //        //}



            //        //else
            //        //{                       
            //        return RedirectToAction("Index", "Home");

            //        // }


            //    }
            //}

            ////Session["RegisterError"] = "Passwords must be at least 6 characters.";
            //AddErrors(result);
        }

            Session["RegisterError"] = "اسم المستخدم موجود";
            Session["UserType"] = model.UserType;

            // If we got this far, something failed, redisplay form
            //return View("Login");
            return RedirectToAction("LoginView", "Account");
            //return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BulkRegister(RegisterViewModel model)
        {
          
            return RedirectToAction("LoginView", "Account");
        }

        public async Task<List<AuthenticationCodeCheck_Result>> AuthenticationCodeCheck(string SecretNumber)
        {
            List<AuthenticationCodeCheck_Result> result = new List<AuthenticationCodeCheck_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AuthenticationCodeCheck/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("SecretNumber={0}", SecretNumber);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AuthenticationCodeCheck_Result>>();
                }
            }
            return result;
        }

        public async Task<string> AssignedCodeToUser(string UserKey, string AuthenticationCode)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AssignedCodeToUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserKey={0}", UserKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("AuthenticationCode={0}", AuthenticationCode);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<string> EntidabSave(string Organizationkey, string userKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateUserPerOrganisation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Organikey={0}", Organizationkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("uKey={0}", userKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public static bool ValidatePassword(string Password, string ConfirmPassword)
        {
            if (Password == ConfirmPassword)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> LogOff()
        {
            string userKey = User.Identity.GetUserId();
            await SetLog(userKey, "صفحة الخروج", "الخروج", "قام المستخدم بالخروج");
           // await Signout();

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Markaz", "NewPages");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }
        [Globals.YourCustomAuthorize]
        [HttpGet]
        public async Task<String> ChangeRole(string id, string role)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            String userId = id;
            //String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            ApplicationUser cUser = await store.FindByIdAsync(userId);

            string oldrole = UserManager.GetRoles(cUser.Id)[0].ToString();
            //await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            await store.RemoveFromRoleAsync(cUser, oldrole);
            await store.AddToRoleAsync(cUser, role);
            await store.UpdateAsync(cUser);
            return "done";
        }


        [Globals.YourCustomAuthorize]
        public async Task<String> ChangePassword(string id, string password)
        {
            if (id == "")
            {
                id = User.Identity.GetUserId();
            }
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            //String userId = User.Identity.GetUserId();//"<YourLogicAssignsRequestedUserId>";
            String userId = id;
            String newPassword = password; //" <PasswordAsTypedByUser>";
            String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            ApplicationUser cUser = await store.FindByIdAsync(userId);

            //string oldrole = UserManager.GetRoles(cUser.Id)[0].ToString();
            await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            //await store.RemoveFromRoleAsync(cUser, oldrole);
            //await store.AddToRoleAsync(cUser, role);
            await store.UpdateAsync(cUser);
            return "done";
        }
        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static async Task<string> ErrorSetLog(string ErrorUserkey, string Errorsource, string Errortitle, string Errormessage)
        {
         
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SetLog/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Userkey={0}", ErrorUserkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("source={0}", Errorsource);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("title={0}", Errortitle);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("message={0}", Errormessage);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IpAdress={0}", "");


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }
        public async Task<string> SetLog(string Userkey, string source, string title, string message)
        {
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SetLog/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Userkey={0}", Userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("source={0}", source);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("title={0}", title);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("message={0}", message);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IpAdress={0}", myIP);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }
        public async Task<string> Signout()
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/Signout");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("ti={0}", "oiuh");
              


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();
                     RedirectToAction("Index", "Home");


                }
            }
            return "";
        }

        [Globals.YourCustomAuthorize]
        public async Task<String> DeleteUser(string id)
        {


            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            String userId = id;
            ApplicationUser cUser = await store.FindByIdAsync(userId);

            await store.DeleteAsync(cUser);
            return "done";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }

    internal class TripleDESCryptoServiceProvider
    {
        public byte[] Key { get; internal set; }
        public object Mode { get; internal set; }
        public PaddingMode Padding { get; internal set; }

        internal void Clear()
        {
            throw new NotImplementedException();
        }

        internal ICryptoTransform CreateDecryptor()
        {
            throw new NotImplementedException();
        }
    }

    internal class MD5CryptoServiceProvider
    {
        internal void Clear()
        {
            throw new NotImplementedException();
        }

        internal byte[] ComputeHash(byte[] v)
        {
            throw new NotImplementedException();
        }
    }
}