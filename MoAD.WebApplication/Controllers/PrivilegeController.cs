﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
    public class PrivilegeController : Controller
    {
        public async Task<string> CreatePrivilege(string Name = null, string Type = null, string Size = null, string Html = null, string HtmlCode = null, string ColumnValue = null, string PrivilegeName = null, string PrivilegeParent = null, string Level = null, string Index = null, string PositionType = null, string PrivilegeType = null)
        {

            string value = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreatePrivilege/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Name={0}", Name);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Type={0}", Type);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Size={0}", Size);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Html={0}", Html);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("HtmlCode={0}", HtmlCode);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ColumnValue={0}", ColumnValue);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PrivilegeName={0}", PrivilegeName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PrivilegeParent={0}", PrivilegeParent);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Level={0}", Level);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Index={0}", Index);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PositionType={0}", PositionType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PrivilegeType={0}", PrivilegeType);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    value = await response.Content.ReadAsStringAsync();
                }
            }


            return "";
        }


        // GET: Privilege
        public ActionResult Index()
        {
            return View();
        }
    }


}