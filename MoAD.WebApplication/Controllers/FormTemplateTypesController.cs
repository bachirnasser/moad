﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class FormTemplateTypesController : Controller
    {
        private Entities db = new Entities();

        // GET: FormTemplateTypes
        public async Task<ActionResult> Index(string HighlightedPart=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            return View(await db.FormTemplateTypes.ToListAsync());
        }

        // GET: FormTemplateTypes/Details/5
        
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormTemplateType formTemplateType = await db.FormTemplateTypes.FindAsync(id);
            if (formTemplateType == null)
            {
                return HttpNotFound();
            }
            return View(formTemplateType);
        }

        // GET: FormTemplateTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FormTemplateTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FormTemplateType1,ParentFormTemplateType,Level")] FormTemplateType formTemplateType)
        {
            if (ModelState.IsValid)
            {
                db.FormTemplateTypes.Add(formTemplateType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(formTemplateType);
        }

        // GET: FormTemplateTypes/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormTemplateType formTemplateType = await db.FormTemplateTypes.FindAsync(id);
            if (formTemplateType == null)
            {
                return HttpNotFound();
            }
            return View(formTemplateType);
        }

        // POST: FormTemplateTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FormTemplateType1,ParentFormTemplateType,Level")] FormTemplateType formTemplateType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formTemplateType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(formTemplateType);
        }

        // GET: FormTemplateTypes/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormTemplateType formTemplateType = await db.FormTemplateTypes.FindAsync(id);
            if (formTemplateType == null)
            {
                return HttpNotFound();
            }
            return View(formTemplateType);
        }

        // POST: FormTemplateTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            FormTemplateType formTemplateType = await db.FormTemplateTypes.FindAsync(id);
            db.FormTemplateTypes.Remove(formTemplateType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
