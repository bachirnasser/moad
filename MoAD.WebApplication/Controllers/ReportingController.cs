﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using System.Text;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Threading.Tasks;

namespace MoAD.WebApplication.Controllers
{
   // [Globals.YourCustomAuthorize]
    public class ReportingController : Controller
    {

        public static List<object> FinalResult = new List<object>();

        // GET: Reporting
        public async Task<ActionResult> Index(string section = "", string HighlightedPart=null)
        {

            ViewBag.HighlightedPart = HighlightedPart;
            //List<ReportingClaimstype_Result> ReportingClaimsType = new List<ReportingClaimstype_Result>();
            //ReportingClaimsType = await GetReportingClaimsType(null,"0");

            List<GetAllOrganisations_Result> result = GetAllOrganisations(null).Result;
            ViewBag.Organizations = result;
            result = result.Where(x => x.OrganisationKey != 3).ToList();
            
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllOrganisations_Result> resultOrganisationsWithParent = GetAllOrganisations("1").Result;
            ViewBag.OrganizationsOrganisationsWithParent = resultOrganisationsWithParent;
            ViewData["OrganisationsWithParent"] = resultOrganisationsWithParent.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            ViewBag.section = section;
            return View();
        }

        public async Task<ActionResult> PrintGraphs(string Title = null, string Image = null,string formTemplateName=null, string formInstanceKey=null)
        {
            ViewBag.Title = Title;
            ViewBag.Image = Image;
            ViewBag.formTemplate = formTemplateName;
            ViewBag.formInstanceKey = formInstanceKey;
            ViewBag.FinalResult = FinalResult;
            return View();
        }
        

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string parent)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", parent);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("r={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        public async Task<JsonResult> GetReportingClaimsType(string OrganisationKey = null,string LevelType = null)
        {
            List<ReportingClaimstype_Result> result = new List<ReportingClaimstype_Result>();
            List<string> ClaimsType = new List<string>();
            List<int?> ClaimsCount = new List<int?>();

            List<object> ReportingClaimsType = new List<object>();
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetReportingClaimsType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test8={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test7={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test6={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingClaimstype_Result>>();
                  
               ClaimsType= result.Select(x => x.Value).ToList();
               ClaimsCount= result.Select(x => x.Count).ToList();
                }
            }
            ReportingClaimsType.Add(ClaimsType);
            ReportingClaimsType.Add(ClaimsCount);

            return Json(ReportingClaimsType, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ReportingTa7sinElKhadamat(string OrganisationKey = null, string LevelType = null)
        {
            List<ReportingTa7sinElKhadamat_Result> result = new List<ReportingTa7sinElKhadamat_Result>();
            List<string> ClaimsType = new List<string>();
            List<int?> ClaimsCount = new List<int?>();

            List<object> ReportingClaimsType = new List<object>();

            string Organisations = "null";
            string ParentOrganisationKey = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrganisationKey = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingTa7sinElKhadamat/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test9={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test10={0}", ParentOrganisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("test11={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingTa7sinElKhadamat_Result>>();

                    ClaimsType = result.Select(x => x.Value).ToList();
                    ClaimsCount = result.Select(x => x.Evaluations).ToList();
                }
            }
            ReportingClaimsType.Add(ClaimsType);
            ReportingClaimsType.Add(ClaimsCount);

            return Json(ReportingClaimsType, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ReportingGovernanceResponse(string OrganisationKey)
        {
            List<ReportingGovernanceResponse_Result> result = new List<ReportingGovernanceResponse_Result>();
           


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGovernanceResponse/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("OrgKey={0}", OrganisationKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGovernanceResponse_Result>>();

                }
            }

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.Value);
                data.Add(item.Count.ToString());

                generalData.Add(data);
            }
    

            return Json(generalData, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> ReportingClaimStatusesPerDateRanges(string startPeriod, string endPeriod, string startPeriod2, string endPeriod2, string organisationKey)
        {
            FinalResult.Clear();

            List<ReportingClaimStatusesPerDateRanges_Result> result = new List<ReportingClaimStatusesPerDateRanges_Result>();
            List<ReportingClaimStatusesPerDateRanges_Result> result2 = new List<ReportingClaimStatusesPerDateRanges_Result>();



            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingClaimStatusesPerDateRanges/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("startPeriod={0}", startPeriod);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("endPeriod={0}", endPeriod);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingClaimStatusesPerDateRanges_Result>>();

                }
            }


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingClaimStatusesPerDateRanges/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("startPeriod={0}", startPeriod2);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("endPeriod={0}", endPeriod2);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result2 = await response.Content.ReadAsAsync<List<ReportingClaimStatusesPerDateRanges_Result>>();

                }
            }
            List<List<string>> TitlesData = new List<List<string>>();

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.StatusValue);
                data.Add(item.ClaimsCount.ToString());
                TitlesData.Add(data);

                generalData.Add(data);
            }

            List<List<string>> generalData2 = new List<List<string>>();
            if (result2.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData2.Add(data);
            }

            foreach (var item in result2)
            {
                List<string> data = new List<string>();
                
                data.Add(item.StatusValue);
                data.Add(item.ClaimsCount.ToString());
                TitlesData.Add(data);
                generalData2.Add(data);
            }






           // List<object> FinalResult = new List<object>();
           

            FinalResult.Add(generalData);
            FinalResult.Add(generalData2);
            


            return Json(FinalResult, JsonRequestBehavior.AllowGet);
        }






        public async Task<JsonResult> ReportingClaimsCountPerYear(string Year1, string Year2 ,string OrganisationKey,string LevelType)
        {
            List<ReportingClaimsCountPerYear_Result> result = new List<ReportingClaimsCountPerYear_Result>();
            string Organisations = "null";
            string ParentOrganisationKey = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrganisationKey = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingClaimsCountPerYear/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Year1={0}", Year1);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Year2={0}", Year2);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("keyOrg={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrgPar={0}", ParentOrganisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Pvce={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingClaimsCountPerYear_Result>>();
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }





        
        public async Task<JsonResult> ReportingGovernanceResponseDateRanges(string OrganisationKey, string LevelType, string Type)
        {
            string Organisations = "null";
            string ParentOrg = "null";
            string Province = "null";

            if (LevelType == "Organisations")
            {
                Organisations = OrganisationKey;
            }
            else if (LevelType == "Parent")
            {
                ParentOrg = OrganisationKey;
            }
            else if (LevelType == "Province")
            {
                Province = OrganisationKey;
            }


            List<ReportingGovernanceResponseDateRanges_Result> result = new List<ReportingGovernanceResponseDateRanges_Result>();
            List<ReportingGovernanceResponseDateRanges_Result> resultChemle = new List<ReportingGovernanceResponseDateRanges_Result>();
            List<ReportingGovernanceResponseDateRanges_Result> resultJez2ye = new List<ReportingGovernanceResponseDateRanges_Result>();

            List<string> DateRangesJez2ye = new List<string>();
            List<string> DateRangesChemle = new List<string>();
            List<int?> ClaimsCountJez2ye = new List<int?>();
            List<int?> ClaimsCountChemle = new List<int?>();
            

            List<object> ReportingJez2ye = new List<object>();
            List<object> ReportingChemle = new List<object>();
            List<object> ReportingResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGovernanceResponseDateRanges/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Korg={0}", Organisations);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parOrg={0}", ParentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("vince={0}", Province);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGovernanceResponseDateRanges_Result>>();

                    if(Type== "Jez2ia")
                    resultJez2ye = result.Where(o => o.Value.Equals("جزئية\r\n  ")).ToList();
                    else
                    resultChemle = result.Where(o => o.Value.Equals("شاملة\r\n  ")).ToList();

                    ClaimsCountJez2ye = resultJez2ye.Select(x => x.ClaimCount).ToList();
                    ClaimsCountChemle = resultChemle.Select(x => x.ClaimCount).ToList();

                    DateRangesJez2ye = resultJez2ye.Select(x => x.DateRanges).ToList();
                    DateRangesChemle = resultChemle.Select(x => x.DateRanges).ToList();

                }
            }

            if (Type == "Jez2ia")
            {
                ReportingJez2ye.Add(DateRangesJez2ye);
                ReportingJez2ye.Add(ClaimsCountJez2ye);
            }else
            {
                ReportingChemle.Add(DateRangesChemle);
                ReportingChemle.Add(ClaimsCountChemle);
            }



            if (Type == "Jez2ia")
                ReportingResult.Add(ReportingJez2ye);
            else
            ReportingResult.Add(ReportingChemle);

            return Json(ReportingResult, JsonRequestBehavior.AllowGet);
        }
        public class async<T>
        {
        }
    }
}