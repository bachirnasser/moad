﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using MoAD.WebApplication.Objects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
   
    public class RankingController : BaseController
    {
        private Entities db = new Entities();
        public async Task<ActionResult> SliderTest(string description = null,string levelType = null,string OrgK=null)
        {
            List<GetRanking_Result> result = new List<GetRanking_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RankingKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Ukey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("year={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("description={0}", description);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetRanking_Result>>();


                    List<GetRanking_Result> RankingResult = result.OrderBy(x => x.Ranking).ToList();
                    ViewBag.RankingCard = RankingResult.Where(x => x.Organisation == OrgK).ToList();
                    ViewBag.RankingCardCount = RankingResult.Where(x => x.Organisation == OrgK).ToList().Count;


                }
            }

            List<GetInitiative_Result> Initiatives = new List<GetInitiative_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetInitiatives/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("o={0}", OrgK);
              
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    Initiatives = await response.Content.ReadAsAsync<List<GetInitiative_Result>>();



                    ViewBag.Initiatives = Initiatives.ToList(); 


                }
            }



            return View();
        }


        public async Task<ActionResult> SliderTestPB(string Year = null, string YearHalf = null, string levelType = null,string OrgK = null,string orkid=null)
        {

            ViewBag.HighlightedPart = "Ranking";

            List<GetPBOrganisationCard_Result> result = new List<GetPBOrganisationCard_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetPBOrganisationCard/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("y={0}", Year);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("yh={0}", YearHalf);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("orID={0}", orkid);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetPBOrganisationCard_Result>>();

                   

                    ViewBag.RankingCard = result;



                }
            }

            List<GetInitiative_Result> Initiatives = new List<GetInitiative_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetInitiatives/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("o={0}", orkid);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    Initiatives = await response.Content.ReadAsAsync<List<GetInitiative_Result>>();



                    ViewBag.Initiatives = Initiatives.ToList();


                }
            }



            return View();
        }

        // GET: Ranking
        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Index(string Year=null,string YearHalf=null,string levelType=null)
        {
            if (YearHalf == null)
            {
                YearHalf = "1";
            }
            ViewBag.HighlightedPart = "Ranking";

            List<FinalRankingInTheRightScenario_Result> result = new List<FinalRankingInTheRightScenario_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/RankingSP/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Year={0}", DateTime.Now.Year.ToString());
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("YearHalf={0}", YearHalf);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<FinalRankingInTheRightScenario_Result>>();
                    

                    List<RankingDisplay> Ranking = new List<RankingDisplay>();
                    List<RankingDisplay> Ranking1 = new List<RankingDisplay>();

                    var test = result

                     .Select(group => new
                     {
                         Score = group.Score,
                         OrganisationKey = group.OrganisationKey,
                         Organisation = group.Organisation,
                         Sector = group.Sector,

                     })
                     .ToList().Distinct();


                    foreach (var item in test)
                    {
                        RankingDisplay resultorg = new RankingDisplay();

                        resultorg.Score = item.Score;
                        resultorg.OrganisationKey = item.OrganisationKey.ToString();
                        resultorg.Organisation = item.Organisation;
                        resultorg.Sector = item.Sector;

                        Ranking.Add(resultorg);
                    }

                    foreach (var item1 in test)
                    {
                        var Found = result.Where(x => x.OrganisationKey == item1.OrganisationKey).ToList();
                        var j = 0;
                        RankingDisplay testwlo = new RankingDisplay();
                        foreach (var item2 in Found)
                        {
                            if (j == 0 && Found.Count==2)
                            {


                                testwlo.Score = item2.Score;
                                testwlo.Sector = item2.Sector;
                                testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                testwlo.Organisation = item2.Organisation;
                                testwlo.FormTemplateType = item2.FormTemplateType;
                                testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                            }
                            else if (j == 1 && Found.Count == 2)
                            {
                                testwlo.FormTemplateType1 = item2.FormTemplateType;
                                testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                Ranking1.Add(testwlo);
                            }else
                            {
                                testwlo.Score = item2.Score;
                                testwlo.Sector = item2.Sector;
                                testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                testwlo.Organisation = item2.Organisation;
                                testwlo.FormTemplateType1 = item2.FormTemplateType;
                                testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                testwlo.FormTemplateType = "";
                                testwlo.FormTemplateTypeScore = 0;
                                Ranking1.Add(testwlo);
                            }
                            j++;
                        }
                        
                    }

                    ViewBag.RankingTable = Ranking1;
                    ViewBag.Max = result.Max(x => x.Score);
                    ViewBag.RankingTableIndex = 0;
                    ViewBag.levelType = levelType;
                }
            }

            return View();
        }

        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

        public async Task<List<GetOrganisationType_Result>> GetOrganisationType(string parentorg)
        {


            List<GetOrganisationType_Result> result = new List<GetOrganisationType_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetOrganisationType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("identityKey={0}", parentorg);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetOrganisationType_Result>>();


                }
            }
            return result;
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string parentOrg)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", parentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("r={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        public async Task<List<AccessMonitoringAndChatting_Result>> GetAccessOrganisation(string Orgs = null)
        {
            if (Orgs == null)
                Orgs = "null";
            string userKEY = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", userKEY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "Monitoring");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", Orgs);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();


                }
            }
            return result;
        }
        public async Task<ActionResult> ViewMoubadaratOrgs(string HighlightedPart=null)
        {

            ViewBag.HighlightedPart = HighlightedPart;

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();

            string userID = User.Identity.GetUserId();

            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {

                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().OPManagerRoleGroupKey.ToString());
                    string allOrg = "";

                    int i = 0;
                    foreach (var item in ChildOrg)
                    {
                        if (i == ChildOrg.Count - 1)
                            allOrg += item.OrganisationKey;
                        else
                            allOrg += item.OrganisationKey + ",";
                        i++;
                    }

                    ViewBag.OrganisationType = allOrg;
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = organisations

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View();
        }


        public async Task<ActionResult> MoubadaratOrgs(string HighlightedPart = null)
        {

            ViewBag.HighlightedPart = HighlightedPart;

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();

            string userID = User.Identity.GetUserId();

            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {

                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().OPManagerRoleGroupKey.ToString());
                    string allOrg = "";

                    int i = 0;
                    foreach (var item in ChildOrg)
                    {
                        if (i == ChildOrg.Count - 1)
                            allOrg += item.OrganisationKey;
                        else
                            allOrg += item.OrganisationKey + ",";
                        i++;
                    }

                    ViewBag.OrganisationType = allOrg;
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = organisations

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View();
        }



        public async Task<ActionResult> TasnifAlAda2AlSanawi(string description=null)
        {
            ViewBag.PageName = "Marsad";
            ViewBag.HighlightedPart = "Ranking";

            if(description!=null)
            ViewBag.Description = description;
            else
                ViewBag.Description = "NotPublish";

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> RankingView(string description,string levelType)

        {
            ViewBag.PageName = "Marsad";
            ViewBag.Title = "مرصد الأداء الإداري";
            ViewBag.HighlightedPart = "Ranking";
            string userKey = User.Identity.GetUserId();
            List<GetRanking_Result> result = new List<GetRanking_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RankingKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Ukey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("year={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("description={0}", description);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetRanking_Result>>();
                    
                    List<GetRanking_Result> RankingResult = result.OrderBy(x => x.Ranking).ToList();
                    List<GetRanking_Result> Ranking = new List<GetRanking_Result>();
                    var test = RankingResult

                     .Select(group => new
                     {
                         RankingGroupKey = group.RankingGroupKey,
                         RankingGroupName = group.RankingGroupName,
                         UserKey = group.UserKey,
                         CreatedAt = group.CreatedAt,
                         YEAR = group.YEAR,
                         IsPublished = group.IsPublished,
                         Level = group.Level,
                         RankingKey = group.RankingKey,
                         Ranking = group.Ranking,
                         Score = group.Score,
                         FormTemplateTypeMin = group.FormTemplateTypeMin,
                         FormTemplateScoreMin = group.FormTemplateScoreMin,
                         FormTemplateTypeMax = group.FormTemplateTypeMax,
                         FormTemplateScoreMax = group.FormTemplateScoreMax,
                         //FormTemplateType = group.FormTemplateType,
                         //FormTemplateTypeScore = group.FormTemplateTypeScore,
                         //FormTemplateTypeScoreRatio = group.FormTemplateTypeScoreRatio,
                         //FormTemplateTypeClass = group.FormTemplateTypeClass,
                         Organisation = group.Organisation,
                         OrganisationName = group.OrganisationName,
                         UserName = group.UserName
           
                     })
                     .ToList().Distinct();
                    foreach (var item in test)
                    {
                        GetRanking_Result resultrank = new GetRanking_Result();

                        resultrank.RankingGroupKey = item.RankingGroupKey;
                        resultrank.RankingGroupName = item.RankingGroupName;
                        resultrank.UserKey = item.UserKey;
                        resultrank.CreatedAt = item.CreatedAt;
                        resultrank.YEAR = item.YEAR;
                        resultrank.IsPublished = item.IsPublished;
                        resultrank.Level = item.Level;
                        resultrank.RankingKey = item.RankingKey;
                        resultrank.Ranking = item.Ranking;
                        resultrank.Score = item.Score;
                        resultrank.FormTemplateTypeMin = item.FormTemplateTypeMin;
                        resultrank.FormTemplateScoreMin = item.FormTemplateScoreMin;
                        resultrank.FormTemplateTypeMax = item.FormTemplateTypeMax;
                        resultrank.FormTemplateScoreMax = item.FormTemplateScoreMax;
                        //resultrank.FormTemplateType = item.FormTemplateType;
                        //resultrank.FormTemplateTypeScore = item.FormTemplateTypeScore;
                        //resultrank.FormTemplateTypeScoreRatio = item.FormTemplateTypeScoreRatio;
                        //resultrank.FormTemplateTypeClass = item.FormTemplateTypeClass;
                        resultrank.Organisation = item.Organisation;
                        resultrank.OrganisationName = item.OrganisationName;
                        resultrank.UserName = item.UserName;


                        Ranking.Add(resultrank);
                    }
                    ViewBag.RankingView = Ranking;
                    ViewBag.Max = Convert.ToDouble(result.Max(x => x.Score));
                    ViewBag.RankingDescription = result.FirstOrDefault().RankingGroupName;

                    ViewBag.RankingGroupKey = result.FirstOrDefault().RankingGroupKey;
                    ViewBag.Description = levelType;


                }
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }


        public async Task<ActionResult> GetPublishedRanking(string levelType=null)
        {

            ViewBag.HighlightedPart = "Ranking";
            string userKey = User.Identity.GetUserId();
            List<GetRanking_Result> result = new List<GetRanking_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RankingKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Ukey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("year={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("description={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetRanking_Result>>();
                   
                }
                await RankingView(result.Where(x=>x.IsPublished=="Published").ToList().FirstOrDefault().RankingGroupName, levelType);
            }
            ViewBag.Published = "Yes";
            return View("RankingView");
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> GetRanking(string levelType=null)
        {

            ViewBag.HighlightedPart = "Ranking";
            string userKey = User.Identity.GetUserId();
            List<GetRanking_Result> result = new List<GetRanking_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RankingKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Ukey={0}", userKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("year={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("description={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetRanking_Result>>();

                    ViewBag.Description = levelType;


                    List<GetRanking_Result> resultDistinct = result
                  .GroupBy(a => a.RankingGroupName)
                  .Select(g => g.First())
                  .ToList();

                    ViewBag.GetRankingTable = resultDistinct;
                }
            }

            return View();
        }

        public async Task<string> PublishRanking(string RankingKey = null, string Level=null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PublishRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RankingKey={0}", RankingKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Level={0}", Level);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();
                }
            }


            return "";
        }

        

    public async Task<JsonResult> DeleteInitiatives(string InitiativesKey = null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/DeleteInitiatives/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("initiativeKey={0}", InitiativesKey);
                

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();
                }
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> ADDInitiatives(string organisationKey=null, string initiativeValue = null, string FileExtensionKey = null, string FormElementKey = null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ADDInitiatives/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("initiativeValue={0}", initiativeValue);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FileExtensionKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormElementKey={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();
                }
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        
        public async Task<string> CreateRanking(string OrganisationKey = null, string Ranking = null, string RankingDescription = null, string Score = null, string Year = null, string Level=null,string formTemplateTypeMin=null,string formTemplateScoreMin=null,string formTemplateTypeMax=null,string formTemplateScoreMax=null)
        {
            string UserKey = User.Identity.GetUserId();
            string value = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserKey={0}", UserKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrganisationKey={0}", OrganisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Ranking={0}", Ranking);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("RankingDescription={0}", RankingDescription);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Score={0}", Score);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Year={0}", Year);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IsPublished={0}", "NotPublished");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Level={0}", Level);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateTypeMin={0}", formTemplateTypeMin);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateScoreMin={0}", formTemplateScoreMin);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateTypeMax={0}", formTemplateTypeMax);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateScoreMax={0}", formTemplateScoreMax);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                     value = await response.Content.ReadAsStringAsync();   
                }
            }


            return value;
        }

        public async Task<JsonResult> SaveOrganisationCard(string RankingGroupKey = null, string yea = null, string yearHalf = null)
        {
           
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SaveOrganisationCard/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RankingGroupKey={0}", RankingGroupKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("yea={0}", yea);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("yearHalf={0}", yearHalf);
                

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string value = await response.Content.ReadAsStringAsync();
                }
            }


            return Json("", JsonRequestBehavior.AllowGet);
        }



        //public async Task<List<GetOrganisationCard_Result>> GetMou2achirat(string OrgId = null, string Year = null, string YearHalf = null)
        //{
        //    List<GetOrganisationCard_Result> Mou2achirat = new List<GetOrganisationCard_Result>();


        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/GetOrganisationCard");
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("OrgId={0}", OrgId);
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("Y={0}", Year);
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("YHalf={0}", YearHalf);


        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            Mou2achirat = await response.Content.ReadAsAsync<List<GetOrganisationCard_Result>>();
                   
        //        }
        //    }
        //    return Mou2achirat;
        //}

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> RankingTableJson(string Year = null,string YearHalf=null,string levelType=null)
        {


            List<FinalRankingInTheRightScenario_Result> result = new List<FinalRankingInTheRightScenario_Result>();
            List<RankingDisplay> Ranking1 = new List<RankingDisplay>();
            List<RankingDisplay> Ranking2 = new List<RankingDisplay>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/RankingSP/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Year={0}", Year);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("YearHalf={0}", YearHalf);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<FinalRankingInTheRightScenario_Result>>();
                    List<RankingDisplay> Ranking = new List<RankingDisplay>();
                  


                    if (levelType == "Parent")
                    {


                        var test = result

                         .Select(group => new
                         {
                             Score = group.Score,

                             OrganisationParent = group.OrganisationParent


                         })
                         .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;

                            resultorg.OrganisationParent = item.OrganisationParent;


                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.OrganisationParent == item1.OrganisationParent).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;

                                    testwlo.OrganisationKey = item2.OrganisationParent;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.OrganisationParent;
                                    testwlo.Organisation = item2.Organisation;
                                    testwlo.OrganisationParent = item2.OrganisationParent;

                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType = "";
                                    testwlo.FormTemplateTypeScore = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }
                        ViewBag.RankingTable = Ranking1;
                    }
                    else if (levelType == "Province")
                    {
                        var test = result

                       .Select(group => new
                       {
                           Score = group.Score,

                           Province = group.Province


                       })
                       .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;

                            resultorg.Province = item.Province;


                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.Province == item1.Province).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.Province;
                                    testwlo.Province = item2.Province;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.Province;
                                    
                                    testwlo.Province = item2.Province;

                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType = "";
                                    testwlo.FormTemplateTypeScore = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                   

                        }
                        var test1 = Ranking1

              .Select(group => new
              {
                  Score = group.Score,

                  Province = group.Province,
                  FormTemplateType = group.FormTemplateType,
                  FormTemplateType1 = group.FormTemplateType1,
                  FormTemplateTypeScore1 = group.FormTemplateTypeScore,
                  FormTemplateTypeScore = group.FormTemplateTypeScore1,
                  OrganisationKey=group.Province
              })
              .ToList().Distinct();

                        foreach (var item2 in test1)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item2.Score;

                            resultorg.Province = item2.Province;
                            resultorg.OrganisationKey = item2.Province;
                            resultorg.FormTemplateType = item2.FormTemplateType;
                            resultorg.FormTemplateType1 = item2.FormTemplateType1;
                            resultorg.FormTemplateTypeScore1 = item2.FormTemplateTypeScore1;
                            resultorg.FormTemplateTypeScore = item2.FormTemplateTypeScore;

                            Ranking2.Add(resultorg);
                        }
                    }
                    else if (levelType == "Sector")
                    {
                        var test = result

                       .Select(group => new
                       {
                           Score = group.Score,

                           Sector = group.Sector


                       })
                       .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;

                            resultorg.Sector = item.Sector;


                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.Sector == item1.Sector).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;

                                    testwlo.Sector = item2.Sector;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    // testwlo.OrganisationKey = item2.OrganisationKey.ToString();

                                    testwlo.Sector = item2.Sector;

                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType1 = "";
                                    testwlo.FormTemplateTypeScore1 = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }



                        var test1 = Ranking1

                       .Select(group => new
                       {
                           Score = group.Score,

                           Sector = group.Sector,
                           FormTemplateType = group.FormTemplateType,
                           FormTemplateType1 = group.FormTemplateType1,
                           FormTemplateTypeScore1 = group.FormTemplateTypeScore,
                           FormTemplateTypeScore = group.FormTemplateTypeScore1

                       })
                       .ToList().Distinct();

                        foreach (var item1 in test1)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item1.Score;

                            resultorg.OrganisationKey= item1.Sector;

                            resultorg.FormTemplateType = item1.FormTemplateType;
                            resultorg.FormTemplateType1 = item1.FormTemplateType1;
                            resultorg.FormTemplateTypeScore1 = item1.FormTemplateTypeScore1;
                            resultorg.FormTemplateTypeScore = item1.FormTemplateTypeScore;

                            Ranking2.Add(resultorg);
                        }
                        ViewBag.RankingTable = Ranking2;
                    }
                    else
                    {


                        var test = result

                         .Select(group => new
                         {
                             Score = group.Score,
                             OrganisationKey = group.OrganisationKey,
                             OrganisationParent = group.OrganisationParent,
                             Organisation = group.Organisation,
                             Sector = group.Sector


                         })
                         .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;
                            resultorg.OrganisationKey = item.OrganisationKey.ToString();
                            resultorg.OrganisationParent = item.OrganisationParent;
                            resultorg.Organisation = item.Organisation;
                            resultorg.Sector = item.Sector;

                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.OrganisationKey == item1.OrganisationKey).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                    testwlo.Organisation = item2.Organisation;
                                    testwlo.OrganisationParent = item2.OrganisationParent;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                    testwlo.Organisation = item2.Organisation;
                                    testwlo.OrganisationParent = item2.OrganisationParent;

                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType = "";
                                    testwlo.FormTemplateTypeScore = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }
                       
                    }


                 
                }
            }

            if (levelType == "Province" || levelType == "Sector")
                return Json(Ranking2, JsonRequestBehavior.AllowGet);

            else
                return Json(Ranking1, JsonRequestBehavior.AllowGet);
        }
        
  public async Task<JsonResult> GetInitiatives(string organisationKey=null)
        {


            List<GetInitiative_Result> result = new List<GetInitiative_Result>();
           
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetInitiatives/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("o={0}", organisationKey);
                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetInitiative_Result>>();
                   
                }
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> RankingTable(string Year = null, string YearHalf = null, string levelType = null)
        {

            ViewBag.HighlightedPart = "Ranking";

            if(Year==null && YearHalf==null && levelType == null)
            {
                Year = DateTime.Now.Year.ToString();

                if (DateTime.Now.Month < 6)
                {
                    YearHalf = "1";
                }else
                {
                    YearHalf = "2";
                }
                
                levelType = "Organisation";


            }

            List<FinalRankingInTheRightScenario_Result> result = new List<FinalRankingInTheRightScenario_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/RankingSP/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Year={0}", Year);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("YearHalf={0}", YearHalf);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("levelType={0}", levelType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<FinalRankingInTheRightScenario_Result>>();

                    List<RankingDisplay> Ranking = new List<RankingDisplay>();
                    List<RankingDisplay> Ranking1 = new List<RankingDisplay>();
                    List<RankingDisplay> Ranking2 = new List<RankingDisplay>();


                    if (levelType == "Parent")
                    {
                       

                        var test = result

                         .Select(group => new
                         {
                             Score = group.Score,
                             
                             OrganisationParent = group.OrganisationParent
                             

                         })
                         .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;
                          
                            resultorg.OrganisationParent = item.OrganisationParent;
                           

                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.OrganisationParent == item1.OrganisationParent).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;
                                  
                                    testwlo.OrganisationParent = item2.OrganisationParent;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                    testwlo.Organisation = item2.Organisation;
                                    testwlo.OrganisationParent = item2.OrganisationParent;

                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType = "";
                                    testwlo.FormTemplateTypeScore = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }
                        ViewBag.RankingTable = Ranking1;
                    }else if (levelType == "Province")
                    {
                        var test = result

                       .Select(group => new
                       {
                           Score = group.Score,

                           Province = group.Province


                       })
                       .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;

                            resultorg.Province = item.Province;


                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.Province == item1.Province).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;

                                    testwlo.Province = item2.Province;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                   // testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                    
                                    testwlo.Province = item2.Province;

                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType1 = "";
                                    testwlo.FormTemplateTypeScore1 = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }
                        


                        var test1 = Ranking1

                       .Select(group => new
                       {
                           Score = group.Score,

                           Province = group.Province,
                           FormTemplateType = group.FormTemplateType,
                           FormTemplateType1=group.FormTemplateType1,
                           FormTemplateTypeScore1=group.FormTemplateTypeScore,
                           FormTemplateTypeScore=group.FormTemplateTypeScore1

                       })
                       .ToList().Distinct();

                        foreach (var item1 in test1)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item1.Score;

                            resultorg.Province = item1.Province;

                            resultorg.FormTemplateType = item1.FormTemplateType;
                            resultorg.FormTemplateType1 = item1.FormTemplateType1;
                            resultorg.FormTemplateTypeScore1 = item1.FormTemplateTypeScore1;
                            resultorg.FormTemplateTypeScore = item1.FormTemplateTypeScore;

                            Ranking2.Add(resultorg);
                        }
                        ViewBag.RankingTable = Ranking2;
                    }
                    else if (levelType == "Sector")
                    {
                        var test = result

                       .Select(group => new
                       {
                           Score = group.Score,

                           Sector = group.Sector


                       })
                       .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;

                            resultorg.Sector = item.Sector;


                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.Sector == item1.Sector).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;

                                    testwlo.Sector = item2.Sector;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    // testwlo.OrganisationKey = item2.OrganisationKey.ToString();

                                    testwlo.Sector = item2.Sector;

                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType1 = "";
                                    testwlo.FormTemplateTypeScore1 = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }



                        var test1 = Ranking1

                       .Select(group => new
                       {
                           Score = group.Score,

                           Sector = group.Sector,
                           FormTemplateType = group.FormTemplateType,
                           FormTemplateType1 = group.FormTemplateType1,
                           FormTemplateTypeScore1 = group.FormTemplateTypeScore,
                           FormTemplateTypeScore = group.FormTemplateTypeScore1

                       })
                       .ToList().Distinct();

                        foreach (var item1 in test1)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item1.Score;

                            resultorg.Sector = item1.Sector;

                            resultorg.FormTemplateType = item1.FormTemplateType;
                            resultorg.FormTemplateType1 = item1.FormTemplateType1;
                            resultorg.FormTemplateTypeScore1 = item1.FormTemplateTypeScore1;
                            resultorg.FormTemplateTypeScore = item1.FormTemplateTypeScore;

                            Ranking2.Add(resultorg);
                        }
                        ViewBag.RankingTable = Ranking2;
                    }
                    else
                    {
                      

                        var test = result

                         .Select(group => new
                         {
                             Score = group.Score,
                             OrganisationKey = group.OrganisationKey,
                             OrganisationParent = group.OrganisationParent,
                             Organisation = group.Organisation


                         })
                         .ToList().Distinct();


                        foreach (var item in test)
                        {
                            RankingDisplay resultorg = new RankingDisplay();

                            resultorg.Score = item.Score;
                            resultorg.OrganisationKey = item.OrganisationKey.ToString();
                            resultorg.OrganisationParent = item.OrganisationParent;
                            resultorg.Organisation = item.Organisation;

                            Ranking.Add(resultorg);
                        }

                        foreach (var item1 in test)
                        {
                            var Found = result.Where(x => x.OrganisationKey == item1.OrganisationKey).ToList();
                            var j = 0;
                            RankingDisplay testwlo = new RankingDisplay();
                            foreach (var item2 in Found)
                            {
                                if (j == 0 && Found.Count == 2)
                                {


                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                    testwlo.Organisation = item2.Organisation;
                                    testwlo.OrganisationParent = item2.OrganisationParent;
                                    testwlo.FormTemplateType = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore = item2.FormTemplateTypeScore;
                                }
                                else if (j == 1 && Found.Count == 2)
                                {
                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    Ranking1.Add(testwlo);
                                }
                                else
                                {
                                    testwlo.Score = item2.Score;
                                    testwlo.OrganisationKey = item2.OrganisationKey.ToString();
                                    testwlo.Organisation = item2.Organisation;
                                    testwlo.OrganisationParent = item2.OrganisationParent;

                                    testwlo.FormTemplateType1 = item2.FormTemplateType;
                                    testwlo.FormTemplateTypeScore1 = item2.FormTemplateTypeScore;
                                    testwlo.FormTemplateType = "";
                                    testwlo.FormTemplateTypeScore = 0;
                                    Ranking1.Add(testwlo);
                                }
                                j++;
                            }

                        }
                        ViewBag.RankingTable = Ranking1;
                    }
                    

                    ViewBag.Max = result.Max(x => x.Score);
                    ViewBag.RankingTableIndex = 0;
                    ViewBag.Level = levelType;
                }
            }


            return PartialView();
        }

    }
}