﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using MoAD.WebApplication.Objects;
using System.Data.Entity;

namespace MoAD.WebApplication.Controllers
{

    //[Authorize]
    public class HomeController : BaseController
    {
        private Entities db = new Entities();

        public async Task<ActionResult> Index3()
        {
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();



            return View();
        }

        public async Task<ActionResult> AboutMarsad()
        {



            return View();
        }

        public async Task<ActionResult> AboutWizara()
        {
            return View();
        }

        public async Task<ActionResult> AboutMachari3()
        {
            return View();
        }





        public async Task<ActionResult> AboutManbar()
        {
            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي منبر")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/

            ViewBag.PageName = "Manbar";

            return View();
        }


        public async Task<ActionResult> Machari3Wizara()
        {

            ViewBag.PageName = "Markaz";

            return View();
        }


        public async Task<ActionResult> Khousousya()
        {

            ViewBag.PageName = "Manbar";

            return View();
        }

        public async Task<ActionResult> Himayatalserya()
        {

            ViewBag.PageName = "Manbar";

            return View();
        }


        public async Task<ActionResult> EchReport()
        {
            ViewBag.PageName = "Marsad";
            ViewBag.Title = "مرصد الأداء الإداري";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            //Report 8/////////////

            List<PBMonitoringReportPerOrganisation_Result> Report8 = new List<PBMonitoringReportPerOrganisation_Result>();
            Report8 = PBMonitoringReportPerOrganisation().Result;



            List<object> Report8Json = new List<object>();


            foreach (var item8 in Report8.Where(x => x.SystemUsing == "EachMonth"))
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item8.OrganisationName);
                ItemDict.Add("income", item8.CountActivities);



                Report8Json.Add(ItemDict);
            }

            //End Report 8/////////////

            //Report 9/////////////

            List<PBMonitoringReportPerOrganisation_Result> Report9 = new List<PBMonitoringReportPerOrganisation_Result>();
            Report9 = PBMonitoringReportPerOrganisation().Result;



            List<object> Report9Json = new List<object>();


            foreach (var item9 in Report9.Where(x => x.SystemUsing == "Each2Month"))
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item9.OrganisationName);
                ItemDict.Add("income", item9.CountActivities);



                Report9Json.Add(ItemDict);
            }

            //End Report 9/////////////



            //Report 10/////////////

            List<PBMonitoringReportPerOrganisation_Result> Report10 = new List<PBMonitoringReportPerOrganisation_Result>();
            Report10 = PBMonitoringReportPerOrganisation().Result;



            List<object> Report10Json = new List<object>();


            foreach (var item10 in Report10.Where(x => x.SystemUsing == "EachWeek"))
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item10.OrganisationName);
                ItemDict.Add("income", item10.CountActivities);



                Report10Json.Add(ItemDict);
            }

            //End Report 10/////////////

            ViewBag.Report8 = Report8Json;
            ViewBag.Report9 = Report9Json;
            ViewBag.Report10 = Report10Json;


            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }

        public async Task<ActionResult> Tarahoulan()
        {
            ViewBag.PageName = "Marsad";
            ViewBag.Title = "مرصد الأداء الإداري";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            //Report 3/////////////

            List<PBReportingCountNoncompliance_Result> Report3 = new List<PBReportingCountNoncompliance_Result>();
            Report3 = PBReportingCountNoncompliance().Result;



            List<object> Report3Json = new List<object>();

            foreach (var item3 in Report3)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item3.OrganisationName);
                ItemDict.Add("income", item3.CountCorruption);


                Report3Json.Add(ItemDict);
            }

            //End Report 3/////////////
            ViewBag.Report3 = Report3Json;

            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }

        [HttpGet]
        public async Task<List<PBMonitoringReportPerOrganisation_Result>> PBMonitoringReportPerOrganisation()
        {
            List<PBMonitoringReportPerOrganisation_Result> result = new List<PBMonitoringReportPerOrganisation_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBMonitoringReportPerOrganisation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("a={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBMonitoringReportPerOrganisation_Result>>();
                }
            }

            return result;
        }

        [HttpGet]
        public async Task<List<PBReportingCountNoncompliance_Result>> PBReportingCountNoncompliance()
        {
            List<PBReportingCountNoncompliance_Result> result = new List<PBReportingCountNoncompliance_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingCountNoncompliance/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("c={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingCountNoncompliance_Result>>();
                }
            }

            return result;
        }


        public async Task<ActionResult> Estetla3Ra2i()
        {
            ViewBag.PageName = "Marsad";
            ViewBag.Title = "مرصد الأداء الإداري";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            //Report 7/////////////

            List<PBReportingSurveyAnswers_Result> Report = new List<PBReportingSurveyAnswers_Result>();
            Report = PBReportingSurveyAnswers().Result;



            List<object> Report7Json = new List<object>();


            foreach (var item in Report)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("country", item.Answer);
                ItemDict.Add("litres", item.CountVoters);


                Report7Json.Add(ItemDict);
            }

            //End Report 7/////////////

            ViewBag.Report7 = Report7Json;

            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;

            return View();
        }

        [HttpGet]
        public async Task<List<PBReportingSurveyAnswers_Result>> PBReportingSurveyAnswers()
        {
            List<PBReportingSurveyAnswers_Result> result = new List<PBReportingSurveyAnswers_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingSurveyAnswers/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("t={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingSurveyAnswers_Result>>();
                }
            }

            return result;
        }
        public async Task<ActionResult> AkhbarWizara()
        {

            ViewBag.PageName = "Markaz";

            return View();
        }



        public async Task<ActionResult> AboutMarkaz()
        {
            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي مركز")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/

            ViewBag.PageName = "Markaz";

            return View();
        }


        public async Task<ActionResult> TakrirAda2Sanawi()
        {
            ViewBag.PageName = "Marsad";
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.Title = "مرصد الأداء الإداري";
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;


            return View();
        }

        public async Task<ActionResult> TechExpert(string ClaimType, string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;


            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    }
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType!= "do not get any orgs")
            {
                 organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            }
          
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = organisations

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            string updatedbyuserkey = User.Identity.GetUserId();

            if (ClaimType == "تسجيل الدخول")
            {


                List<GetAdManagerForms_Result> result = getAdManagerForms(updatedbyuserkey, ClaimType, ViewBag.OrganisationType).Result;
                List<GetAdManagerForms_Result> NotApprovedResult = new List<GetAdManagerForms_Result>();
                foreach (var item in result)
                {
                    List<GetUserInformation_Result> userInfo = await GetUserInformation(item.UserKey);
                    string approved = userInfo.FirstOrDefault().Approved;

                    if (approved == "NOTAPPROVED")
                    {
                        NotApprovedResult.Add(item);
                    }
                }

                ViewBag.ManagerForms = NotApprovedResult;
            }
            else
            {
                List<GetAdManagerForms_Result> result = getAdManagerForms(updatedbyuserkey, ClaimType, ViewBag.OrganisationType).Result;
                ViewBag.ManagerForms = result;
            }


            ViewBag.title = ClaimType;


            return View();
        }


        public async Task<ActionResult> RedirectionPage()
        {
            string userId = User.Identity.GetUserId();
            List<GetUserInformation_Result> userInfo = GetUserInformation(userId).Result;
            string roleId = userInfo.FirstOrDefault().RoleId;
            Entities db = new Entities();

            ViewBag.Role = db.AspNetRoles.Where(x => x.Id == roleId).ToList().FirstOrDefault().ArabicName;


            return View();
        }

        public async Task<ActionResult> Manbar1()
        {
            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
            ViewBag.PageName = "Manbar";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();

            return View();
        }

        
              public async Task<ActionResult> ManbarNewDesign()
        {

            //String address = "";
            //WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
            //using (WebResponse response = request.GetResponse())
            //using (StreamReader stream = new StreamReader(response.GetResponseStream()))
            //{
            //    address = stream.ReadToEnd();
            //}
            //int first = address.IndexOf("Address: ") + 9;
            //int last = address.LastIndexOf("</body>");
            //address = address.Substring(first, last - first);
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);
            ViewBag.myIP = myIP;


            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await GetAllFormTemplatesForaType("Survey");
            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;
                userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for manbar").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;

                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch (Exception ex)
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }



            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
            ViewBag.PageName = "Manbar";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();

            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for manbar").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }

            if (TemplateKeyParam != "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }
            else
            {
                ViewBag.Survey = new Dictionary<string, List<GetFormTemplate_Result>>();
            }

            /* 29-08-2019 */


            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي منبر")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/

            return View();
        }
        // [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Manbar()
        {
            return RedirectToAction("Manbar", "NewPages");
            //String address = "";
            //WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
            //using (WebResponse response = request.GetResponse())
            //using (StreamReader stream = new StreamReader(response.GetResponseStream()))
            //{
            //    address = stream.ReadToEnd();
            //}
            //int first = address.IndexOf("Address: ") + 9;
            //int last = address.LastIndexOf("</body>");
            //address = address.Substring(first, last - first);
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);
            ViewBag.myIP = myIP;


            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await GetAllFormTemplatesForaType("Survey");
            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;
               userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for manbar").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;

                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch(Exception ex)
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }



            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
            ViewBag.PageName = "Manbar";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();

            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for manbar").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }

            if(TemplateKeyParam != "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }else
            {
                ViewBag.Survey= new Dictionary<string, List<GetFormTemplate_Result>>();
            }

            /* 29-08-2019 */


            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms =  getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>(); 
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي منبر")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/


            //We have to check if employee and can view 
            return View();
        }

        public async Task<List<GetAdManagerForms_Result>> getAdManagerForms(string UserId, string FormTemplateType, int organisationKey)
        {
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserId={0}", UserId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserRoleID={0}", 1);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
            }
            return result;
        }

        public async Task<ActionResult> PrintAuthenticationCodes(string authenticationCodes, string Organisation)
        {



            var authenticationCodesArray = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(authenticationCodes);


            List<string> result = new List<string>();
            foreach (var item in authenticationCodesArray)
            {

                string key = item["Code"].ToString();
                result.Add(key);

            }
            ViewBag.authenticationCodes = result;
            ViewBag.Organisation = Organisation;
            return View();
        }

        public async Task<ActionResult> Index6()
        {


            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;

            ViewBag.PageName = "Markaz";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();

            return View();
        }

        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        
             public async Task<ActionResult> IndexNewDesign()
        {
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);



            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            ViewBag.myIP = myIP;
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await GetAllFormTemplatesForaType("Survey");

            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;

                if (User.Identity.IsAuthenticated && !User.IsInRole("Citizen"))
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;
                }
                else
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), null).Result;
                }

                ViewBag.formTemplateKeySurvey = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }


            string userKey = User.Identity.GetUserId();

            if (userKey != null)
            {
                MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                List<GetUserInformation_Result> userInfo = GetUserInformation(userKey).Result;
                MoAD.WebApplication.Globals.Configuration.Rolekey = userInfo.FirstOrDefault().RoleId;
            }

            ViewBag.PageName = "Markaz";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();
            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }


            if (TemplateKeyParam != "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }
            else
            {
                ViewBag.Survey = new Dictionary<string, List<GetFormTemplate_Result>>();
            }

            /* 29-08-2019 */

            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي مركز")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/
            return View();
        }

        public static async Task<List<GetAllFormTemplatesForaType_Result>> GetAllFormTemplatesForaType(string formTemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllFormTemplatesForaType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formTemplateType={0}", formTemplateType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();
                }
            }
            return result;
        }

        public async Task<ActionResult> Index()
        {
            return RedirectToAction("Markaz","NewPages");
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);



            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            ViewBag.myIP = myIP;
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await GetAllFormTemplatesForaType("Survey");

            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;

                if (User.Identity.IsAuthenticated && !User.IsInRole("Citizen"))
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f=>f.Description== "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;
                }
                else
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), null).Result;
                }

                ViewBag.formTemplateKeySurvey = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }


            string userKey = User.Identity.GetUserId();

            if (userKey != null)
            {
                MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                List<GetUserInformation_Result> userInfo = GetUserInformation(userKey).Result;
                MoAD.WebApplication.Globals.Configuration.Rolekey = userInfo.FirstOrDefault().RoleId;
            }

            ViewBag.PageName = "Markaz";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();
            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }


            if(TemplateKeyParam!= "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }else
            {
                ViewBag.Survey = new Dictionary<string, List<GetFormTemplate_Result>>();
            }

            /* 29-08-2019 */

            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي مركز")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/
            return View();
        }


        public async Task<string> CreateSurveyAnswer(string ans = null, string vk = null)
        {
            List<GetUserSurveyAnswer_Result> result = new List<GetUserSurveyAnswer_Result>();
            string userkey = User.Identity.GetUserId();
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);


            string cookie = Request.Cookies["Karchoune"].Value;


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateSurveyAnswer/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Ku={0}", userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IP={0}", myIP);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("cook={0}", cookie);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ans={0}", ans);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ViewKey={0}", vk);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();
                }
            }

            return "";
        }


        public async Task<List<GetUserSurveyAnswer_Result>> GetUserSurveyAnswer(string ip = null, string cookie = null, string vk = null, string userkey = null)
        {
            List<GetUserSurveyAnswer_Result> result = new List<GetUserSurveyAnswer_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserSurveyAnswer/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("uk={0}", userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ip={0}", ip);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("cookie={0}", cookie);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ViewKey={0}", vk);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserSurveyAnswer_Result>>();
                }
            }

            return result;
        }
        public async Task<ActionResult> Marsad()
        {
            return RedirectToAction("Marsad", "NewPages");
            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
            ViewBag.PageName = "Marsad";

            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();

            /*Updated By DN*/
         List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
       
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/
            return View();
        }

        public async Task<ActionResult> MarsadNew()
        {
            string userKey = User.Identity.GetUserId();
            MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
            ViewBag.PageName = "Marsad";

            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();

            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();

            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/
            return View();
        }


        [HttpGet]
        public async Task<JsonResult> AccessPivilege(string ParentPrivilege = null,string uKey=null)
        {
            List<AccessPivilege_Result> result = new List<AccessPivilege_Result>();
            string userkey = User.Identity.GetUserId();
            //Globals.Configuration.UserKey = userkey;
            List<GetUserInformation_Result> UserInfo = new List<GetUserInformation_Result>();
            if (userkey == null)
            {
                UserInfo = GetUserInformation(uKey).Result;
            }
            else
            {
               UserInfo = GetUserInformation(userkey).Result;
            }
           
            string userRole = UserInfo.FirstOrDefault().RoleId;
            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessPivilege/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userRole={0}", userRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ParentId={0}", ParentPrivilege);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Position={0}", "Right");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessPivilege_Result>>();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> EAndTEvaluationWeighing(string fk = null, string ok = null)
        {
            List<EAndTEvaluationWeighing_Result> result = new List<EAndTEvaluationWeighing_Result>();
            string userkey = User.Identity.GetUserId();
            //Globals.Configuration.UserKey = userkey;
            List<GetUserInformation_Result> UserInfo = GetUserInformation(userkey).Result;
            string userRole = UserInfo.FirstOrDefault().RoleId;
            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/EAndTEvaluationWeighing/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("fk={0}", fk);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ok={0}", ok);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("rk={0}", userRole);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<EAndTEvaluationWeighing_Result>>();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult IndexNew()
        {
            return View();
        }
        public async Task<ActionResult> Profile()
        {

            string UserId = User.Identity.GetUserId();
            List<GetUserInformation_Result> UserInformation = new List<GetUserInformation_Result>();
            UserInformation = await GetUserInformation(UserId);

            ViewBag.UserName = UserInformation.FirstOrDefault().UserName;
            ViewBag.SSN = UserInformation.FirstOrDefault().SSN;
            ViewBag.Role = UserInformation.FirstOrDefault().ArabicRoleName;
            ViewBag.FirstName = UserInformation.FirstOrDefault().FirstName;
            ViewBag.FatherName = UserInformation.FirstOrDefault().FatherName;
            ViewBag.LastName = UserInformation.FirstOrDefault().LastName;
            ViewBag.Email = UserInformation.FirstOrDefault().Email;
            ViewBag.PhoneNumber = UserInformation.FirstOrDefault().PhoneNumber;
            ViewBag.AccessFailed = UserInformation.FirstOrDefault().AccessFailedCount;
            ViewBag.UserImage = UserInformation.FirstOrDefault().UserImage;

            int orgKey = UserInformation.FirstOrDefault().OrganisationKey;
            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            result = GetAllOrganisations().Result;

            if (!(User.IsInRole("OPManager") || User.IsInRole("OPDirector") || User.IsInRole("OPGeneralDirector")))
            {
                result = result.Where(x => x.OrganisationKey == orgKey).ToList();
                try
                {
                    string OrganisationName = result.FirstOrDefault().OrganisationName.ToString();
                    ViewBag.OrganisationName = OrganisationName;
                }
                catch (Exception ex)
                {
                    ViewBag.OrganisationName = "";
                }

                //ViewBag.OrganisationName = result.Where(x => x.OrganisationKey == UserInformation.FirstOrDefault().OrganisationKey);
            }



            return View();
        }


        public ActionResult IndexMarsadNew()
        {
            return View();
        }

        public ActionResult MarkazAlKada()
        {
            return View();
        }

        public ActionResult KawaderBachariya()
        {
            return View();
        }

        public ActionResult DaemIdari()
        {
            return View();
        }

        public ActionResult IbdaaHoukoumi()
        {
            return View();
        }

        public ActionResult MinbarSilatWasel()
        {
            return View();
        }


        public async Task<List<GetAllFormTemplatesForaType_Result>> GetFormTemplates(string TemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplatesForAType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateType={0}", TemplateType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();

                }
            }

            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> SignupView(string formkey, string GUID, string CreatedAt, string CitizenKey, string CurrentStatusKey, string NextFormTemplateKey, string CurrentFormtemplateKey, string CurrentFormtemplateName, string NextStatusName, string CurrentStatusName, string NextOrganisationKey, string CurrentOrganisationKey, string UserKey)
        {
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            string FormInstanceKey = formkey;
            List<GetSavedForm_Result> FormSaved = GetSavedForm(FormInstanceKey).Result;
            ViewBag.FormValues = FormSaved;
            string formTemplateKey = FormSaved.FirstOrDefault().FormTemplateKey.ToString();
            List<GetUserInformation_Result> UserInfo = GetUserInformation(CitizenKey).Result;
            ViewBag.UserInforesult = UserInfo.FirstOrDefault();
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            ViewBag.SavedFormPerGuId = GetSavedFormsperGUID(GUID).Result;
            ViewBag.Instances = GetNotes(GUID).Result;
            ViewBag.CreatedAt = CreatedAt;
            ViewBag.FormKey = formkey;
            ViewBag.GUID = GUID;
            ViewBag.AssignToUserKey = CitizenKey;
            ViewBag.CurrentStatusKey = CurrentStatusKey;
            ViewBag.NextFormTemplateKey = NextFormTemplateKey;
            ViewBag.CurrentFormtemplateKey = CurrentFormtemplateKey;
            ViewBag.CurrentFormtemplateName = CurrentFormtemplateName;
            ViewBag.NextStatusName = NextStatusName;
            ViewBag.CurrentStatusName = CurrentStatusName;
            ViewBag.NextOrganisationKey = NextOrganisationKey;
            ViewBag.CurrentOrganisationKey = CurrentOrganisationKey;
            ViewBag.formTemplateName = "تصديق الدخول";
            ViewBag.formTemplate = FormSaved;
            ViewBag.UserKey = UserKey;
            List<GetAllOrganisations_Result> allOrgs = GetAllOrganisations().Result;
            ViewBag.allOrgs = allOrgs;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "111");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }


            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> FormInstance(string formkey, string GUID, string CreatedAt, string CitizenKey, string CurrentStatusKey, string NextFormTemplateKey, string CurrentFormtemplateKey, string CurrentFormtemplateName, string NextStatusName, string CurrentStatusName, string NextOrganisationKey, string CurrentOrganisationKey)
        {
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            string FormInstanceKey = formkey;
            List<GetSavedForm_Result> FormSaved = GetSavedForm(FormInstanceKey).Result;
            ViewBag.FormValues = FormSaved;
            string formTemplateKey = FormSaved.FirstOrDefault().FormTemplateKey.ToString();
            List<GetUserInformation_Result> UserInfo = GetUserInformation(CitizenKey).Result;
            ViewBag.UserInforesult = UserInfo.FirstOrDefault();
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            ViewBag.SavedFormPerGuId = GetSavedFormsperGUID(GUID).Result;
            ViewBag.Instances = GetNotes(GUID).Result;
            ViewBag.CreatedAt = CreatedAt;
            ViewBag.FormKey = formkey;
            ViewBag.GUID = GUID;
            ViewBag.AssignToUserKey = CitizenKey;
            ViewBag.CurrentStatusKey = CurrentStatusKey;
            ViewBag.NextFormTemplateKey = NextFormTemplateKey;
            ViewBag.CurrentFormtemplateKey = CurrentFormtemplateKey;
            ViewBag.CurrentFormtemplateName = CurrentFormtemplateName;
            ViewBag.NextStatusName = NextStatusName;
            ViewBag.CurrentStatusName = CurrentStatusName;
            ViewBag.NextOrganisationKey = NextOrganisationKey;
            ViewBag.CurrentOrganisationKey = CurrentOrganisationKey;




            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "111");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }


            foreach (var item in resultFormTemplate)
            {
                if (item.QuestionValue.Contains("وصف الشكوى"))
                {
                    ViewBag.OpNote = item.AnswerTemplateKey;
                }

                if (item.AnswerValue.Contains("جزئية"))
                {
                    ViewBag.AnswerKeyTawsif1 = item.AnswerTemplateKey;
                }

                if (item.AnswerValue.Contains("شاملة"))
                {
                    ViewBag.AnswerKeyTawsif2 = item.AnswerTemplateKey;
                }


            }
            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<string> GenerateAuthenticationCodes(string OrganK = null, string CountCodes = null, string rKey = null)
        {
            string Userkey = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                // requestUriBuilder.Append("api/Forms/SetLog/");
                requestUriBuilder.Append("api/Forms/GenerateAuthenticationCodes/");



                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("OrganK={0}", OrganK);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("CountCodes={0}", CountCodes);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("rKey={0}", rKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }
        [Globals.YourCustomAuthorize]
        public async Task<string> SetApproveUser(string Userkey, string approved)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SetApproveUser/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Userkey={0}", Userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("approved={0}", approved);



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }


        public async Task<string> SetLog(string source, string title, string message)
        {
            string Userkey = User.Identity.GetUserId();

            List<GetUserSurveyAnswer_Result> result = new List<GetUserSurveyAnswer_Result>();
            string userkey = User.Identity.GetUserId();
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SetLog/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Userkey={0}", Userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("source={0}", source);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("title={0}", title);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("message={0}", message);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IpAdress={0}", myIP);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }


        [Globals.YourCustomAuthorize]
        public async Task<List<GetSavedFormsperGUID_Result>> GetSavedFormsperGUID(string guid)
        {
            List<GetSavedFormsperGUID_Result> result = new List<GetSavedFormsperGUID_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedFormsperGUID");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("guid={0}", guid);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedFormsperGUID_Result>>();
                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetWorkFlowItemInstancePerUser_Result>> GetNotes(string GUID)
        {
            List<GetWorkFlowItemInstancePerUser_Result> FormTemplateTypeResult = new List<GetWorkFlowItemInstancePerUser_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetWorkFlowPerUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("guid={0}", GUID);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<GetWorkFlowItemInstancePerUser_Result>>();
                }
            }

            return FormTemplateTypeResult;
        }

        [Globals.YourCustomAuthorize]
        public async Task<string> CreateNewWorkFlowInstance(string Note, string GUID, string CreatedAt, string Formkey, string CurrentStatusKey, string NextFormTemplateKey, int ActionKey = 0, string NextOrganisationKey = "", string currenOrganisationKey = "")
        {
            //string WorkFlowItemKey, string UpdatedByUserKey, string AssignedToUserKey, string CreatedAt, string UpdatedAt, string InvocationTypeKey, string CurrentFormKey, string NextFormTemplateKey, string GUID, string Note
            string InvocationType = "1";
            string Updatedat = DateTime.Now.ToString("HH:mm:ss");
            string updatedbyuserkey = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateNewWorkFlowInstance");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Note={0}", Note);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("GUID={0}", GUID);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("CreatedAt={0}", CreatedAt);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Formkey={0}", Formkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("InvocationType={0}", InvocationType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Updatedat={0}", Updatedat);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("updatedbyuserkey={0}", updatedbyuserkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("CurrentStatusKey={0}", SafeSqlLiteral(CurrentStatusKey));
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("NextFormTemplateKey={0}", NextFormTemplateKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ActionKey={0}", ActionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("NextOrganisationKey={0}", SafeSqlLiteral(NextOrganisationKey));
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("CurrentOrganisationKey={0}", currenOrganisationKey);





                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                }
            }
            return "";
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetSavedForm_Result>> GetSavedForm(string FormKey)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", FormKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public static async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<ReportingGetCountAndDatesPerFormTemplatekey_Result>> ReportingGetCountAndDatesPerFormTemplatekey(string InstanceKey)
        {
            List<ReportingGetCountAndDatesPerFormTemplatekey_Result> result = new List<ReportingGetCountAndDatesPerFormTemplatekey_Result>();

            var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetCountAndDatesPerFormTemplatekey");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("InstanceKey={0}", InstanceKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetCountAndDatesPerFormTemplatekey_Result>>();
                }
            }
            return result;
        }



        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> ClaimViewManager(string formKey)
        {
            GetSavedForm_Result test = new GetSavedForm_Result();
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", formKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();

                }
            }

            foreach (var item in result)
            {
                ViewBag.FormTemplateDescription = item.FormTemplateDescription;
                ViewBag.FormTemplateName = item.FormTemplateName;

            }

            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            List<object> nestedResultFormTemplate = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "109");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    nestedResultFormTemplate = resultFormTemplate
                                .GroupBy(x => x.QuestionKey).Select(q => new
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList()
                                }).ToList<object>();
                }
            }
            foreach (var item in resultFormTemplate)
            {
                if (item.QuestionValue.Contains("الوثائق المرفقة"))
                    ViewBag.QuestionAlWasa2ek = item.QuestionKey;

                if (item.QuestionValue.Contains("توصيف الحل"))
                    ViewBag.QuestionTawsifAlHal = item.QuestionKey;

                if (item.QuestionValue.Contains("تحديد نوع المعالجة"))
                    ViewBag.QuestionNaw3AlMouaalaja = item.QuestionKey;

                if (item.QuestionValue.Contains("القرارات / الإجراءات المتخذة"))
                    ViewBag.QuestionAlkararat = item.QuestionKey;

            }
            List<GetAllOrganisations_Result> result1 = GetAllOrganisations().Result;
            ViewData["Organisation"] = result1.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();
            ViewBag.FormTemplateKey = formKey;
            return View(nestedResultFormTemplate);
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> ClaimViewManager2(string formKey)
        {
            GetSavedForm_Result test = new GetSavedForm_Result();
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", formKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();

                }
            }

            foreach (var item in result)
            {
                ViewBag.FormTemplateDescription = item.FormTemplateDescription;
                ViewBag.FormTemplateName = item.FormTemplateName;

            }

            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            List<object> nestedResultFormTemplate = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", "110");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    nestedResultFormTemplate = resultFormTemplate
                                .GroupBy(x => x.QuestionKey).Select(q => new
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList()
                                }).ToList<object>();
                }
            }
            //foreach (var item in resultFormTemplate)
            //{
            //    if (item.QuestionValue.Contains("الوثائق المرفقة"))
            //        ViewBag.QuestionAlWasa2ek = item.QuestionKey;

            //    if (item.QuestionValue.Contains("توصيف الحل"))
            //        ViewBag.QuestionTawsifAlHal = item.QuestionKey;

            //    if (item.QuestionValue.Contains("تحديد نوع المعالجة"))
            //        ViewBag.QuestionNaw3AlMouaalaja = item.QuestionKey;

            //    if (item.QuestionValue.Contains("القرارات / الإجراءات المتخذة"))
            //        ViewBag.QuestionAlkararat = item.QuestionKey;

            //}
            List<GetAllOrganisations_Result> result1 = GetAllOrganisations().Result;
            ViewData["Organisation"] = result1.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();
            ViewBag.FormTemplateKey = formKey;
            return View(nestedResultFormTemplate);
        }

        [Globals.YourCustomAuthorize]
        public ActionResult ClaimViewManager3()
        {
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();
            return View();
        }

        public ActionResult universitylp()
        {
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();


            return View();
        }
        

            public async Task<JsonResult> SaveContact(string name, string email,string subject,string message,string phoneNumber)
        {
            try
            {
                ContactForm contactForm = new ContactForm();
                contactForm.PersonName = name;
                contactForm.EMail = email;
                contactForm.Subject = subject;
                contactForm.Text = message;
                contactForm.PhoneNumber = phoneNumber;
                contactForm.CreatedAt = DateTime.Now;

                db.ContactForms.Add(contactForm);
                await db.SaveChangesAsync();
            }catch(Exception e)
            {
                return Json(400, JsonRequestBehavior.AllowGet);
            }
            return Json(200,JsonRequestBehavior.AllowGet);
        }
        public ActionResult InitialPage()
        {
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public async Task<ActionResult> ContactUs()
        {
            int pageKey = (await db.Pages.Where(p => p.PageName == "Manbar").FirstOrDefaultAsync()).PageKey;
            List<PageComponentsHierarchy> pageComponentsHierarchy = new List<PageComponentsHierarchy>();
            pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey).ToListAsync();

            var results = pageComponentsHierarchy.OrderBy(p => p.ComponentOrder).GroupBy(
    p => p.ComponentOrder,

    (key, g) => new { ComponentOrder = key, Components = g.ToList() }).Select(p => new NavBarViewModel
    {
        ComponentOrder = p.ComponentOrder,
        Items = p.Components
    }).ToList();
            ViewBag.PageItems = results;
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Manbar").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part
            ViewBag.Message = "Your contact page.";
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.PageName = "Manbar";
            return View();
        }

        [Globals.YourCustomAuthorize]
        public ActionResult Dashboard()
        {
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationName }).ToList();

            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> UserManagement()
        {
            ViewBag.Message = "Your contact page.";
            List<Dictionary<string, string>> users = GetUsers().Result;
            List<Dictionary<string, string>> roles = GetRoles().Result;
            ViewData["allUsers"] = users.Select(user => new SelectListItem { Text = user.ToList()[0].Value, Value = user.ToList()[2].Value }).ToList();
            ViewData["allRoles"] = roles.Select(role => new SelectListItem { Text = role.ToList()[0].Value, Value = role.ToList()[0].Value }).ToList();

            return View();
        }


        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> UsersRoles(string RolesOrOrganisations, string UserName = null, string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            ViewBag.Message = "Your contact page.";
            if (UserName == null)
            {
                ViewBag.UserName = string.Empty;
            }
            else
            {
                ViewBag.UserName = UserName;
            }

            if (RolesOrOrganisations != null && RolesOrOrganisations != "")
                ViewBag.RolesOrOrganisations = RolesOrOrganisations;

            List<Dictionary<string, string>> users = GetUsers().Result;
            List<Dictionary<string, string>> roles = GetRoles().Result;
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;

            List<AspNetUser> Allusers = new List<AspNetUser>();
            Entities db = new Entities();
            Allusers =  db.AspNetUsers.Take(10).ToList();
            ViewBag.Useres = Allusers;


            ViewData["allUsers"] = users.Select(user => new SelectListItem { Text = user.ToList()[0].Value, Value = user.ToList()[2].Value }).ToList();

            ViewData["allRoles"] = roles.Select(role => new SelectListItem { Text = role.ToList()[0].Value, Value = role.ToList()[1].Value }).ToList();
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LookupUsers(string userName)
   {
                Entities db = new Entities();
                var users = db.UserNameSearch(userName).Select(u => new
                {
                    id = u.UserKey.ToString(),
                    title = u.UserName,
                    text = u.UserName
                });

                return Json(users);
        }


        [Globals.YourCustomAuthorize]
        public async Task<List<Dictionary<string, string>>> GetUsers()
        {

            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUsers/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<Dictionary<string, string>>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<Dictionary<string, string>>> GetRoles()
        {

            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRoles/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<Dictionary<string, string>>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }


        public async Task<JsonResult> GetFormTemplatesForAType(string TemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplatesForAType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateType={0}", SafeSqlLiteral(TemplateType));
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();

                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AccessFormTemplate(int ftk)
        {
            string result =string.Empty;
            string userKey = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("uk={0}", userKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ftk={0}", ftk);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            if (result == null)
            {
                result = "Success";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> AdUsersLandingPage(int? employeeRecord = null)
        {
            List<GetAllFormTemplatesForaType_Result> GetAllFormTemplatesForaType = new List<GetAllFormTemplatesForaType_Result>();
            if (User.IsInRole("HR") && employeeRecord != null)
            {
                List<GetFormsPerAtribute_Result> GetFormsPerAtribute = new List<GetFormsPerAtribute_Result>();
                
                GetFormsPerAtribute = await ClaimFormsController.GetFormsPerAtribute(User.Identity.GetUserId().ToString(), "خارطة الموارد البشرية", 20995, 38, "CreatedBy", employeeRecord.ToString());
                GetAllFormTemplatesForaType = await GetFormTemplates("خارطة الموارد البشرية");
                string[] arrayOfAllFormTemplates = new string[GetAllFormTemplatesForaType.Count];
                arrayOfAllFormTemplates = GetAllFormTemplatesForaType.Select(f => f.FormTemplateKey.ToString()).ToArray();

                string[] arrayOfCurrentFormTemplates = new string[GetFormsPerAtribute.Count];
                arrayOfCurrentFormTemplates = GetFormsPerAtribute.Select(f => f.FormTemplateKey.ToString()).ToArray();
                string[] arrayDiffAllFormTemplates = new string[GetAllFormTemplatesForaType.Count];
                arrayDiffAllFormTemplates = arrayOfAllFormTemplates.Except(arrayOfCurrentFormTemplates).ToArray();

                GetAllFormTemplatesForaType = GetAllFormTemplatesForaType.Where(f => arrayDiffAllFormTemplates.Contains(f.FormTemplateKey.ToString())).ToList();
                ViewBag.GetAllFormTemplatesForaType = GetAllFormTemplatesForaType;
                if (GetAllFormTemplatesForaType.Count == 0)
                {
                    return RedirectToAction("Index", "ERMEmployees");
                }
            }
            ViewBag.GetAllFormTemplatesForaType = GetAllFormTemplatesForaType;
           
            ViewBag.employeeRecord = employeeRecord;
            return View();
        }

        private string SafeSqlLiteral(string inputSQL)
        {
            return inputSQL.Replace("'", "''");
        }

        [Globals.YourCustomAuthorize]
        public ActionResult AdUsersForms(string type, string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates(SafeSqlLiteral(type)).Result;
            //ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();
            ViewBag.Forms = result2;
            ViewBag.Type = type;

            return View();
        }




        public async Task<ActionResult> AdUsersForms2(string type)
        {

            List<FormTemplateKeyVisibility_Result> result2 = new List<FormTemplateKeyVisibility_Result>();

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();

            string sMonth = DateTime.Now.ToString("MM");
            int MonthPart;
            if (Convert.ToInt32(sMonth) <= 6)
            {
                MonthPart = 1;
            }
            else
            {
                MonthPart = 2;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetTemplateType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("type={0}", type);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrgKey={0}", UserOrgKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Year={0}", DateTime.Now.ToString("yyyy"));
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("MonthPart={0}", MonthPart);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result2 = await response.Content.ReadAsAsync<List<FormTemplateKeyVisibility_Result>>();

                }
            }




            ViewBag.Forms = result2;
            ViewBag.Type = type;

            return View("AdUsersForms");
        }

        public ActionResult _Organisations()
        {
            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            //ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationParent }).ToList();
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            return View();
        }

        public async Task<List<GetOrganisationType_Result>> GetOrganisationType(string parentorg)
        {


            List<GetOrganisationType_Result> result = new List<GetOrganisationType_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetOrganisationType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("identityKey={0}", parentorg);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetOrganisationType_Result>>();


                }
            }
            return result;
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string parentOrg)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", parentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("r={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }


        public async Task<List<AccessMonitoringAndChatting_Result>> GetAccessOrganisation(string Orgs = null)
        {
            if (Orgs == null)
                Orgs = "null";
            string userKEY = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", userKEY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "Monitoring");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", Orgs);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();


                }
            }
            return result;
        }
        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> SignupToApprove()
        {
            ViewBag.HighlightedPart = "SignupToApprove";
            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();

            string userID = User.Identity.GetUserId();

            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {

                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    }
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType != "do not get any orgs")
            {
                organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            }
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = organisations

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();




            InstancePerUser F = new InstancePerUser();
            F.listOfForms = GetWorkFlowPerUser(userID, "null", UserOrgKey).Result;


            return View(F);
        }

        public async Task<List<GetWorkFlowItemInstancePerUser_Result>> GetWorkFlowPerUser(string userID, string guid, string OrganisationKey)
        {
            List<GetWorkFlowItemInstancePerUser_Result> result = new List<GetWorkFlowItemInstancePerUser_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetWorkFlowPerUser/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userKey={0}", userID);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("guid={0}", guid);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrganisationKey={0}", OrganisationKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetWorkFlowItemInstancePerUser_Result>>();
                }
            }

            result.Reverse();
            Entities db = new Entities();
            List<GetWorkFlowItemInstancePerUser_Result> Finalresult = new List<GetWorkFlowItemInstancePerUser_Result>();

            result = result.Where(x => x.CurrentFormtemplateKey == 183).ToList();
            foreach (var item in result)
            {
                List<AspNetUser> user = new List<AspNetUser>();
                user = db.AspNetUsers.Where(x => x.Id == item.UpdatedByUserKey).ToList();

                if (user.FirstOrDefault().Approved != "APPROVED")
                {
                    Finalresult.Add(item);
                }

            }


            return Finalresult;

        }


        [Globals.YourCustomAuthorize]

        public async Task<ActionResult> AdManagersClaims(string ClaimType, string HighlightedPart = null,string empID=null, string fullName=null)
        {
            ViewBag.fullName = fullName;
            ViewBag.HighlightedPart = HighlightedPart;

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();
            string userRoleId = UserInfo.FirstOrDefault().RoleId.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    }
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType != "do not get any orgs")
            {
                    organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result; 
            }
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = organisations

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            string updatedbyuserkey = User.Identity.GetUserId();

            if (ClaimType == "تسجيل الدخول")
            {
                List<GetAdManagerForms_Result> result = getAdManagerForms(updatedbyuserkey, ClaimType, ViewBag.OrganisationType).Result;
                List<GetAdManagerForms_Result> NotApprovedResult = new List<GetAdManagerForms_Result>();
                foreach (var item in result)
                {
                    List<GetUserInformation_Result> userInfo = await GetUserInformation(item.UserKey);
                    string approved = userInfo.FirstOrDefault().Approved;

                    if (approved == "NOTAPPROVED")
                    {
                        NotApprovedResult.Add(item);
                    }
                }

                ViewBag.ManagerForms = NotApprovedResult;
            }
            else
            {
                if (!User.IsInRole("HR") && empID==null)
                {
                    List<GetAdManagerForms_Result> result = getAdManagerForms(updatedbyuserkey, ClaimType, ViewBag.OrganisationType).Result;
                    List<GETOPmanagerForms_Result> opManagersForms = new List<GETOPmanagerForms_Result>();
                    if (userRoleId == "20")
                    {
                        opManagersForms = ClaimFormsController.GetopManagerFormsForAdManager(ClaimType, UserOrgKey).Result;
                    }
                    ViewBag.opManagersForms = opManagersForms;
                    ViewBag.ManagerForms = result;
                }else
                {
                    List<GETOPmanagerForms_Result> opManagersForms = new List<GETOPmanagerForms_Result>();
                    List<GetFormsPerAtribute_Result> GetFormsPerAtribute = new List<GetFormsPerAtribute_Result>();
                    GetFormsPerAtribute = ClaimFormsController.GetFormsPerAtribute(User.Identity.GetUserId().ToString(), "خارطة الموارد البشرية", 20995, 38, "CreatedBy", empID.ToString()).Result;
                    ViewBag.opManagersForms = opManagersForms;
                    ViewBag.ManagerForms = GetFormsPerAtribute;
                }              
            }

            TempData["empID"] = empID;
            ViewBag.empID = empID;
            ViewBag.title = ClaimType;
            return View();
        }
        [Globals.YourCustomAuthorize]
        public ActionResult EstibanatView(string formInstanceKey = "", string formTemplateName = "", string UserKey = "")
        {

            ViewBag.HighlightedPart = "Estibanat";

            var userInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            List<GetSavedForm_Result> FormSaved = GetSavedForm(formInstanceKey).Result.OrderBy(x => x.QuestionKeyIndex).ToList();
            ViewBag.FormValues = FormSaved;
            List<GetAllOrganisations_Result> allOrgs = GetAllOrganisations().Result;
            ViewBag.allOrgs = allOrgs;
            string roleId = userInfo.FirstOrDefault().RoleId.ToString();
            string FormStatus = FormSaved[1].FormStatus;
            ViewBag.AdEditOrApproveIndex = "False";
            if (FormStatus == "OPSaved" && roleId == "20")
            {
                ViewBag.AdEditOrApproveIndex = "True";
            }

            ViewBag.FormTemplateType = FormSaved.FirstOrDefault().FormTemplateType;
            ViewBag.FormTemplateKey = FormSaved.FirstOrDefault().FormTemplateKey;
            ViewBag.formInstanceKey = formInstanceKey;
            ViewBag.formTemplate = formTemplateName;
            ViewBag.UserKey = UserKey;
            return View();
        }
        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> getAdManagerFormsForMoad(string ClaimType, int organisationKey, string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();

            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    }
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType != "do not get any orgs")
            {
                organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            }
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = organisations

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            //List<GetAllOrganisations_Result> organisations = GetAllOrganisations().Result;
            //ViewData["Organisation"] = organisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", ClaimType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
                ViewBag.ManagerForms = result;
                ViewBag.title = ClaimType;
            }
            return View("AdManagersClaims");

        }


        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> opManagerFormsTable(string ClaimType, int organisationKey)
        {

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();
            string UserRoleId = UserInfo.FirstOrDefault().RoleId.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    }
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

           List<AccessMonitoringAndChatting_Result> organisations = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType!= "do not get any orgs")
            {
                 organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            }
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            List<object> Finalresult = new List<object>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", ClaimType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                    List<GETOPmanagerForms_Result> opmangers = new List<GETOPmanagerForms_Result>();
                    if (UserRoleId == "30"|| UserRoleId == "31"|| UserRoleId == "32"|| UserRoleId == "33"|| UserRoleId == "34"|| UserRoleId == "35" || UserRoleId == "36")
                    {
                        opmangers = ClaimFormsController.GetopManagerFormsForAdManager(ClaimType, organisationKey.ToString()).Result;
                        if (opmangers.Count > 0)
                        {
                            foreach (var Item3 in opmangers)
                            {
                                Finalresult.Add(Item3);
                            }

                        }
                    }else if(UserRoleId == "20")
                    {
                        opmangers = ClaimFormsController.GetopManagerFormsForAdManager(ClaimType, UserOrgKey).Result;
                        if (opmangers.Count > 0)
                        {
                            foreach (var Item3 in opmangers)
                            {
                                Finalresult.Add(Item3);
                            }

                        }
                    }
                    
                    foreach (var item2 in organisations)
                    {
                        List<GetAdManagerForms_Result> item = new List<GetAdManagerForms_Result>();
                        item = result.Where(x => x.UserKey == item2.UserKey).ToList();

                        if (item.Count > 0)
                        {
                            foreach (var Item in item)
                            {

                                Finalresult.Add(Item);
                            }

                        }


                    }

                }
            }

                return Json(Finalresult, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> opManagerFormsTableTechExpert(string ClaimType, int organisationKey, string FormTemplateKey = null)
        {

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                if (UserInfo.FirstOrDefault().RoleId == "30" || UserInfo.FirstOrDefault().RoleId == "31" || UserInfo.FirstOrDefault().RoleId == "32" || UserInfo.FirstOrDefault().RoleId == "33")
                {
                    List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(UserInfo.FirstOrDefault().RoleId.ToString());
                    if (ChildOrg.Count == 0)
                    {
                        ViewBag.OrganisationType = "do not get any orgs";
                    }
                    else
                    {
                        string allOrg = "";

                        int i = 0;
                        foreach (var item in ChildOrg)
                        {
                            if (i == ChildOrg.Count - 1)
                                allOrg += item.OrganisationKey;
                            else
                                allOrg += item.OrganisationKey + ",";
                            i++;
                        }

                        ViewBag.OrganisationType = allOrg;
                    } 
                }
                else
                {
                    ViewBag.OrganisationType = "null";
                }
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> organisations = new List<AccessMonitoringAndChatting_Result>();
            if (ViewBag.OrganisationType != "do not get any orgs")
            {
                organisations = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            }
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            List<GetAdManagerForms_Result> Finalresult = new List<GetAdManagerForms_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", ClaimType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();

                    foreach (var item2 in organisations)
                    {
                        GetAdManagerForms_Result item = new GetAdManagerForms_Result();
                        item = result.Where(x => x.UserKey == item2.UserKey).FirstOrDefault();

                        if (item != null)
                            Finalresult.Add(item);

                    }
                    Finalresult = Finalresult.Where(x => x.FormTemplateKey.ToString() == FormTemplateKey).ToList();
                }
            }
            return Json(Finalresult, JsonRequestBehavior.AllowGet);
        }


        [Globals.YourCustomAuthorize]
        public async Task<List<GetAdManagerForms_Result>> getAdManagerForms(string UserId, string FormTemplateType, string organisationKey)
        {
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();

            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            string UserRoleIDString = UserInfo.FirstOrDefault().RoleId.ToString();
            int UserRoleID = Int32.Parse(UserRoleIDString);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserId={0}", UserId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserRoleID={0}", UserRoleID);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
            }
            return result;
        }

        public ActionResult EmailView()
        {

            return View();
        }

        public async Task<JsonResult> GetAuthenticationCodes(string RoleKey = null, string OkEY = null)
        {
            List<GetUnusedAuthenticationCodes_Result> result = new List<GetUnusedAuthenticationCodes_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getUnusedAuthenticationCode/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RoleKey={0}", RoleKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OkEY={0}", OkEY);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUnusedAuthenticationCodes_Result>>();
                    result = result.Take(500).ToList();
                }
                ViewBag.AuthenticationCodes = result;

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> GetRolesPerOrganisation(string OkEY = null)
        {
            List<GetRolesPerOrganisation_Result> result = new List<GetRolesPerOrganisation_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRolesPerOrganisation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Ok={0}", OkEY);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetRolesPerOrganisation_Result>>();
                    result = result.Take(500).ToList();
                }


            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> AuthenticationCodes(string HighlightedPart = null)
        {

            ViewBag.HighlightedPart = HighlightedPart;
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            string orgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();


            List<GetAllOrganisations_Result> resultOrg = GetAllOrganisations().Result;

            Entities db = new Entities();
            List<AspNetRole> roles = db.AspNetRoles.ToList();
            if (User.IsInRole("ADManager"))
            {
                resultOrg = resultOrg.Where(x => x.OrganisationKey == int.Parse(orgKey)).ToList();
                roles = roles.Where(x => x.Id == "24").ToList();
            }
            else
            {
                resultOrg = resultOrg.Where(x => x.OrganisationKey != 3).ToList();
            }
            ViewData["Organisations"] = resultOrg.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            ViewData["Roles"] = roles.Select(org => new SelectListItem { Text = org.Name, Value = org.Id }).ToList();
            return View();

        }

        [Globals.YourCustomAuthorize]

        public async Task<string> ReserveCode(string status, string authenticationCode)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReserveCode/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("status={0}", status);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("authenticationCode={0}", authenticationCode);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;

        }
        /// <summary>  
        ///  Change Language function.  
        /// </summary>
        [HttpGet]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public JsonResult ChangeLanguage(string id)
        {
            bool secure;
            if (Request.IsSecureConnection)
            {
                secure = true;
            }
            else
            {
                secure = false;
            }
            HttpCookie languageCookie = new HttpCookie("Language")
            {
                Secure = secure,
                HttpOnly = true
            };
            languageCookie.Value = id;
            languageCookie.Expires = DateTime.MaxValue;

            Response.Cookies.Add(languageCookie);
            return Json("", JsonRequestBehavior.AllowGet);
        }

    }
}