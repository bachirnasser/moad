﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
   
    //[Globals.YourCustomAuthorize(Roles = "Administrator")]
    public class ChatPageController : Controller
    {
        // GET: ChatPage
        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Index(string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;

            List<GetUserInformation_Result> UserInfo = await GetUserInformation(User.Identity.GetUserId());
            string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();


            List<GetOrganisationType_Result> OrganisationType = await GetOrganisationType(UserOrgKey);

            if (OrganisationType.FirstOrDefault().OrganisationType == "Full")
            {
                ViewBag.OrganisationType = null;
            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "ParentLevel")
            {
                List<GetAllOrganisations_Result> ChildOrg = await GetAllOrganisations(OrganisationType.FirstOrDefault().OrganisationName);
                string allOrg = "";

                int i = 0;
                foreach (var item in ChildOrg)
                {
                    if (i == ChildOrg.Count - 1)
                        allOrg += item.OrganisationKey;
                    else
                        allOrg += item.OrganisationKey + ",";
                    i++;
                }

                ViewBag.OrganisationType = allOrg;

            }
            else if (OrganisationType.FirstOrDefault().OrganisationType == "LeafLevel")
            {
                ViewBag.OrganisationType = UserOrgKey;
            }

            List<AccessMonitoringAndChatting_Result> result = GetAccessOrganisation(ViewBag.OrganisationType).Result;
            List<AccessOrganisations> Organisation = new List<AccessOrganisations>();
            var test = result

             .Select(group => new
             {
                 OrganisationKey = group.OrganisationKey,
                 OrganisationName = group.OrganisationName
             })
             .ToList().Distinct();
            foreach (var item in test)
            {
                AccessOrganisations resultorg = new AccessOrganisations();

                resultorg.OrganisationKey = item.OrganisationKey;
                resultorg.OrganisationName = item.OrganisationName;

                Organisation.Add(resultorg);
            }
            ViewData["Organisation"] = Organisation.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            return View();
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string parentOrg)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", parentOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("r={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }


        public async Task<List<GetOrganisationType_Result>> GetOrganisationType(string parentorg)
        {


            List<GetOrganisationType_Result> result = new List<GetOrganisationType_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetOrganisationType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("identityKey={0}", parentorg);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetOrganisationType_Result>>();


                }
            }
            return result;
        }


        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<AccessMonitoringAndChatting_Result>> GetAccessOrganisation(string Orgs = null)
        {
            if (Orgs == null)
                Orgs = "null";
            string userKEY = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", userKEY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "Chatting");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", Orgs);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public async Task<JsonResult> GetAccessUsers(string orgKey)
        {

            string userKEY = User.Identity.GetUserId();
            List<AccessMonitoringAndChatting_Result> result = new List<AccessMonitoringAndChatting_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessMonitoringAndChatting/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("usrId={0}", userKEY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Action={0}", "Chatting");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Orgs={0}", orgKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessMonitoringAndChatting_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        //[Globals.YourCustomAuthorize]
        //[HttpPost]
        //public async Task<string> CreateMessage(string messagetitle, string message, string ToUser,string FileName,string PathBase64)
        //{

        //    string userKey = User.Identity.GetUserId();
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/CreateMessage/");

        //        //requestUriBuilder.Append(1);
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("messagetitle={0}", messagetitle);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("message={0}", message);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("userKey={0}", userKey);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("ToUser={0}", ToUser);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("FileName={0}", FileName);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("PathBase64={0}", PathBase64);


        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            String value = await response.Content.ReadAsStringAsync();


        //        }
        //    }
        //    return "";
        //}

        //[HttpPost]
        //public async Task<string> CreateMessage(string answerTemplateKey = null)
        //{
        //    string result = string.Empty;
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


        //        StringContent content = new StringContent(JsonConvert.SerializeObject(answerTemplateKey), Encoding.UTF8, "application/json");
        //        // HTTP POST
        //        HttpResponseMessage response = await client.PostAsync("api/Forms/CreateMessage", content);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            string data = await response.Content.ReadAsStringAsync();
        //        }

        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            result = await response.Content.ReadAsAsync<string>();
        //        }
        //    }
        //    return result;
        //}

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Inbox()
        {
            List<GetPMessages_Result> result = new List<GetPMessages_Result>();
            string userKEY = User.Identity.GetUserId();
            result = GetMessages(userKEY).Result;
            var messagesorderbykey = result.OrderByDescending(x => x.MessageKey);

            ViewBag.messages = messagesorderbykey;

            return PartialView("Inbox");
        }



        public async Task<ActionResult> Inbox1(string fromUser = null)
        {

            List<GetPMessages_Result> result = new List<GetPMessages_Result>();
            result = GetMessages(fromUser).Result;
            var messagesorderbykey = result.OrderByDescending(x => x.MessageKey);

            ViewBag.messages = messagesorderbykey;

            return PartialView("Inbox");
        }


        [Globals.YourCustomAuthorize]
        public async Task<List<GetPMessages_Result>> GetMessages(string fromUser  = null)
        {

            string user = User.Identity.GetUserId();
            List<GetPMessages_Result> result = new List<GetPMessages_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMessages/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("user={0}", user);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("fromUser={0}", fromUser);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetPMessages_Result>>();


                }
            }
            return result;
        }


        [Globals.YourCustomAuthorize]
        [HttpGet]
        public async Task<JsonResult> GetMessagesFilter(string fromUser = null)
        {

            string user = User.Identity.GetUserId();
            List<GetPMessages_Result> result = new List<GetPMessages_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMessages/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("user={0}", user);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("fromUser={0}", fromUser);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                 result = await response.Content.ReadAsAsync<List<GetPMessages_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}