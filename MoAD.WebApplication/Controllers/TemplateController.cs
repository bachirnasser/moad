﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Objects;
using MoAD.WebApplication.Models;
using System.IO;
using System.Text;
using MoAD.WebApplication.Managers;
using System.Web.Script.Serialization;

namespace MoAD.WebApplication.Controllers
{
    public class TemplateController : BaseController
    {
        private Entities db = new Entities();
        // GET: Template
        public async Task<ActionResult> Index(int id = 1,string pageName=null)
        {
            if (pageName == null || pageName=="")
            {
                pageName = "Markaz";
            }
            ViewBag.PageName = pageName;
            List<PageComponentsHierarchy> pageComponentsHierarchy = new List<PageComponentsHierarchy>();
            pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageKey == id).ToListAsync();

            var results = pageComponentsHierarchy.OrderBy(p => p.ComponentOrder).GroupBy(
    p => p.ComponentOrder,

    (key, g) => new { ComponentOrder = key, Components = g.ToList() }).Select(p => new NavBarViewModel
    {
        ComponentOrder = p.ComponentOrder,
        Items = p.Components
    }).ToList();
            ViewBag.PageItems = results;
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).OrderBy(n => n.ComponentLevel).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part


            int pageKey = (await db.Pages.Where(p => p.PageName == pageName).FirstOrDefaultAsync()).PageKey;
            var pageComponentVideo = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey && p.Component.ComponentType.ComponentType1 == "Video").FirstOrDefaultAsync();

            string videoUrlComponent = "";
            int? videoComponentPageKey = null;
            if (pageComponentVideo != null)
            {
                videoComponentPageKey = pageComponentVideo.Component.ComponentKey;
                videoUrlComponent = pageComponentVideo.Component.ComponentSourceEntityKey;
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.pageKey = pageKey;
            ViewBag.videoUrlComponent = videoUrlComponent;
            ViewBag.videoComponentPageKey = videoComponentPageKey;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            return View();
        }

        public ActionResult GenerateNewPage()
        {
            return View();
        }

        public async Task<Page> CreateNewPage(string pageName)
        {
            Guid newGuid = Guid.NewGuid();
            //Create new page
            Page newPage = new Page();
            newPage.PageName = pageName;
            newPage.Controller = "Template";
            newPage.Action = "Index";
            newPage.InstanceIdentifier = newGuid;

            db.Pages.Add(newPage);
            await db.SaveChangesAsync();
            return newPage;
        }

        public async Task<int> GetComponentTypeKeyByComponentTypeName(string type)
        {
            int componentTypeKey = (await db.ComponentTypes.Where(c => c.ComponentType1 == type).FirstOrDefaultAsync()).ComponentTypeKey;
            return componentTypeKey;
        }

        public async Task<int> CreateNewComponentPerPage(int componentTypeKey, string componentSourceEntityKey, int pageKey, int componentOrder, int componentIndex)
        {
            try
            {
                Component component = new Component();
                component.ComponentTypeKey = componentTypeKey;

                component.ComponentSourceEntityKey = componentSourceEntityKey;

                db.Components.Add(component);
                await db.SaveChangesAsync();

                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = pageKey;
                pageComponentsHierarchy.ComponentKey = component.ComponentKey;
                pageComponentsHierarchy.ComponentOrder = componentOrder;
                pageComponentsHierarchy.ComponentIndex = componentIndex;

                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return 400;
            }

            return 200;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<int> AdjustComponent(string components, string pageName)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ComponentViewModel> componentsList = (List<ComponentViewModel>)serializer.Deserialize(components, typeof(List<ComponentViewModel>));

                //Create a Page
                Page page = await CreateNewPage(pageName);

                foreach (var component in componentsList)
                {
                    var componentSourceKey = "";

                    int componentTypeKey = await GetComponentTypeKeyByComponentTypeName(component.componentType);

                    //means if text
                    if (component.componentType == "Text")
                    {
                        //componentSourceEntityKey in this case is summernote val

                        componentSourceKey = await GenerateGuidForDataTranslation(component.componentSourceEntityKey, page.PageName);
                    }
                    else
                    {
                        componentSourceKey = component.componentSourceEntityKey;
                    }

                    await CreateNewComponentPerPage(componentTypeKey, componentSourceKey, page.PageKey, component.componentOrder, component.componentIndex);
                }

                return page.PageKey;
            }
            catch (Exception e)
            {
                return 400;
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> EditComponent(int componentKey, string translation, int language, string newComponentVal,string componentType)
        {
            try
            {
                Component component = await db.Components.FindAsync(componentKey);
                if (component.ComponentType.ComponentType1 == "Text")
                {
                    component.ComponentSourceEntityKey = await UpdateDataTranslation(translation, newComponentVal, language);
                    await db.SaveChangesAsync();
                    if (componentType != null)
                    {
                        if(componentType == "SummerNote")
                        {
                            var translationValue = Utilities.GetDataTransaltionByGuid(component.ComponentSourceEntityKey, language);
                            var stringconverted2 = System.Convert.FromBase64String(translationValue);
                            var result = Encoding.UTF8.GetString(stringconverted2);
                            return result;
                        }
                        else
                        {
                            return newComponentVal;
                        }
                    }
              
                }

                if(component.ComponentType.ComponentType1 == "Image")
                {
                    component.ComponentSourceEntityKey = newComponentVal;
                    await db.SaveChangesAsync();
                    ExtendedData extendedData = await db.ExtendedDatas.FindAsync(int.Parse(newComponentVal));
                 
                    return extendedData.FilePathOrSource + extendedData.Name;
                }

                return "400";

            }
            catch (Exception e)
            {
                return "400";
            }

        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> DeletePage(int keyPage)
        {
            try
            {
                List<PageComponentsHierarchy> pageComponentsHierarchy = new List<PageComponentsHierarchy>();
                pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.PageKey == keyPage).ToListAsync();

                foreach (PageComponentsHierarchy currentItem in pageComponentsHierarchy)
                {
                    string componentType = currentItem.Component.ComponentType.ComponentType1;
                    if (componentType == "Text")
                    {
                        Guid guid = Guid.Parse(currentItem.Component.ComponentSourceEntityKey);
                        var dataTranslation = await db.DataTranslations.Where(d => d.DataGUID == guid).FirstOrDefaultAsync();
                        db.DataTranslations.Remove(dataTranslation);
                        await db.SaveChangesAsync();

                        var dataGuid = await db.DataGUIDs.Where(d => d.DataGUID1 == guid).FirstOrDefaultAsync();
                        db.DataGUIDs.Remove(dataGuid);
                        await db.SaveChangesAsync();
                    }
                    else if (componentType != "Slider" && componentType != "NavigationBar")
                    {
                        int fileExtensionKey = int.Parse(currentItem.Component.ComponentSourceEntityKey);
                        var extendedData = await db.ExtendedDatas.Where(e => e.FileExtensionKey == fileExtensionKey).FirstOrDefaultAsync();
                        db.ExtendedDatas.Remove(extendedData);
                        await db.SaveChangesAsync();
                    }

                    var component = await db.Components.Where(c => c.ComponentKey == currentItem.ComponentKey).FirstOrDefaultAsync();
                    db.Components.Remove(component);
                    await db.SaveChangesAsync();

                    db.PageComponentsHierarchies.Remove(currentItem);
                    await db.SaveChangesAsync();

                   
                }
                var pageToDelete = await db.Pages.Where(p => p.PageKey == keyPage).FirstOrDefaultAsync();
                db.Pages.Remove(pageToDelete);
                await db.SaveChangesAsync();
                List<Page> pages = new List<Page>();
                pages = await db.Pages.Where(p => p.PageName != "Markaz" && p.PageName != "Marsad" && p.PageName != "Manbar").OrderByDescending(c => c.PageKey).ToListAsync();
                return PartialView("~/Views/Page/_PagesTable.cshtml", pages);
            }
            catch (Exception e)
            {
                return Json("400");
            }

        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> GenerateGuidForDataTranslation(string text, string pageName)
        {
            Guid newGuid = Guid.NewGuid();
            try
            {
                int arabicLanguageKey = (await db.Languages.Where(l => l.Language1 == "Arabic").FirstOrDefaultAsync()).LanguageKey;
                int englishLanguageKey = (await db.Languages.Where(l => l.Language1 == "English").FirstOrDefaultAsync()).LanguageKey;


                DataGUID dataGUID = new DataGUID();
                dataGUID.DataGUID1 = newGuid;
                dataGUID.SourceTable = pageName;

                db.DataGUIDs.Add(dataGUID);
                await db.SaveChangesAsync();

                DataTranslation arabicDataTranslation = new DataTranslation();
                arabicDataTranslation.DataGUID = newGuid;
                arabicDataTranslation.Value = text;
                arabicDataTranslation.LanguageKey = arabicLanguageKey;
                db.DataTranslations.Add(arabicDataTranslation);
                await db.SaveChangesAsync();

                DataTranslation englishDataTranslation = new DataTranslation();
                englishDataTranslation.DataGUID = newGuid;
                englishDataTranslation.Value = text;
                englishDataTranslation.LanguageKey = englishLanguageKey;
                db.DataTranslations.Add(englishDataTranslation);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return "";
            }

            return newGuid.ToString();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> UpdateDataTranslation(string guid, string newTransaltion, int language)
        {
            try
            {

                Guid currentGuid = Guid.Parse(guid);
                DataTranslation dataTranslation = await db.DataTranslations.Where(d => d.DataGUID == currentGuid && d.LanguageKey == language).FirstOrDefaultAsync();
                dataTranslation.Value = newTransaltion;

                await db.SaveChangesAsync();
                return dataTranslation.DataGUID.ToString();
            }
            catch (Exception e)
            {
                return "";
            }


        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<JsonResult> ChangePageImage(HttpPostedFileBase file, int imageKey)
        //{
        //    try
        //    {


        //        var extendedData = await CheckIfFileExistsAndRemove(imageKey);

        //        //string uploadsFolderPath = Server.MapPath("~/Images/testForPages/");
        //        string uploadsFolderPath = Globals.Configuration.UPLOADS_CMS_FOLDER;
        //        string fileName = DateTime.Now.Ticks.ToString() + file.FileName;
        //        string uploadedFilePath = uploadsFolderPath + fileName;


        //        if (!Directory.Exists(uploadsFolderPath))
        //        {
        //            Directory.CreateDirectory(uploadsFolderPath);
        //        }

        //        file.SaveAs(uploadedFilePath);


             
        //        extendedData = new ExtendedData();
        //        extendedData.Name = fileName;
        //        extendedData.Type = "Image";
        //        extendedData.CreatedAt = DateTime.Now.ToString();
        //        extendedData.FilePathOrSource = uploadsFolderPath;

        //        db.ExtendedDatas.Add(extendedData);
        //        await db.SaveChangesAsync();

        //        Component newComponent = new Component();
        //        newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Image");
        //        newComponent.ComponentSourceEntityKey = extendedData.FileExtensionKey.ToString();
        //        db.Components.Add(newComponent);
        //        await db.SaveChangesAsync();

        //        PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
        //        pageComponentsHierarchy.PageKey = 1;
        //        pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
        //        pageComponentsHierarchy.ComponentOrder = 1;
        //        pageComponentsHierarchy.ComponentIndex = 1;

        //        db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
        //        await db.SaveChangesAsync();

        //        TempData["FileExtensionKeyForVideo"] = extendedData.FileExtensionKey;



        //        return Json(extendedData.FileExtensionKey, JsonRequestBehavior.AllowGet);
        

        //    }
        //    catch (Exception e)
        //    {
        //        return Json("400");
        //    }
        //}

        //public async Task<ExtendedData> CheckIfFileExistsAndRemove(int key = 0)
        //{
        //    if (key == 0) return null;
        //    using (Entities db = new Entities())
        //    {
        //        var extendedData = await db.ExtendedDatas.Where(e => e.FileExtensionKey == key).FirstOrDefaultAsync();
        //        if (extendedData != null)
        //        {
        //            string folderPath = extendedData.FilePathOrSource + extendedData.Name;

        //            if (System.IO.File.Exists(folderPath))
        //            {
        //                System.IO.File.Delete(folderPath);
        //                return extendedData;
        //            }
        //        }
        //    }
        //    return null;

        //}

    }
}
     
    