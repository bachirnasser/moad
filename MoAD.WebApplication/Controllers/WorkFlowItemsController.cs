﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class WorkFlowItemsController : Controller
    {
        private Entities db = new Entities();

        // GET: WorkFlowItems
        public async Task<ActionResult> Index()
        {
            var workFlowItems = db.WorkFlowItems.Include(w => w.KPI).Include(w => w.Organisation).Include(w => w.Organisation1).Include(w => w.Status).Include(w => w.Status1);
            //var workFlowItems = db.WorkFlowItems.Include(w => w.AspNetRole).Include(w => w.AspNetRole1).Include(w => w.FormTemplate).Include(w => w.FormTemplate1).Include(w => w.KPI).Include(w => w.Organisation).Include(w => w.Organisation1).Include(w => w.Status).Include(w => w.Status1);
            return View(await workFlowItems.ToListAsync());
        }

        // GET: WorkFlowItems/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkFlowItem workFlowItem = await db.WorkFlowItems.FindAsync(id);
            if (workFlowItem == null)
            {
                return HttpNotFound();
            }
            return View(workFlowItem);
        }

        // GET: WorkFlowItems/Create
        public ActionResult Create()
        {
            ViewBag.ExtendedCurrentRoleKey = new SelectList(db.AspNetRoles, "Id", "Name");
            ViewBag.ExtendedNextRoleKey = new SelectList(db.AspNetRoles, "Id", "Name");
            ViewBag.ExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name");
            ViewBag.NextExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name");
            ViewBag.KPIKey = new SelectList(db.KPIs, "KPIKey", "KPIValue");
            ViewBag.CurrentExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName");
            ViewBag.NextExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName");
            ViewBag.NextStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue");
            ViewBag.CurrentStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue");
            return View();
        }

        // POST: WorkFlowItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "WorkFlowItemKey,ExtendedFormTemplateKey,ExtendedCurrentRoleKey,ExtendedNextRoleKey,CurrentStatusKey,NextStatusKey,KPIKey,NextExtendedFormTemplateKey,WorkFlowItemIndex,CurrentExtendedOrganisationKey,NextExtendedOrganisationKey")] WorkFlowItem workFlowItem)
        {
            if (ModelState.IsValid)
            {
                db.WorkFlowItems.Add(workFlowItem);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ExtendedCurrentRoleKey = new SelectList(db.AspNetRoles, "Id", "Name", workFlowItem.ExtendedCurrentRoleKey);
            ViewBag.ExtendedNextRoleKey = new SelectList(db.AspNetRoles, "Id", "Name", workFlowItem.ExtendedNextRoleKey);
            ViewBag.ExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", workFlowItem.ExtendedFormTemplateKey);
            ViewBag.NextExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", workFlowItem.NextExtendedFormTemplateKey);
            ViewBag.KPIKey = new SelectList(db.KPIs, "KPIKey", "KPIValue", workFlowItem.KPIKey);
            ViewBag.CurrentExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName", workFlowItem.CurrentExtendedOrganisationKey);
            ViewBag.NextExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName", workFlowItem.NextExtendedOrganisationKey);
            ViewBag.NextStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue", workFlowItem.NextStatusKey);
            ViewBag.CurrentStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue", workFlowItem.CurrentStatusKey);
            return View(workFlowItem);
        }

        // GET: WorkFlowItems/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkFlowItem workFlowItem = await db.WorkFlowItems.FindAsync(id);
            if (workFlowItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExtendedCurrentRoleKey = new SelectList(db.AspNetRoles, "Id", "Name", workFlowItem.ExtendedCurrentRoleKey);
            ViewBag.ExtendedNextRoleKey = new SelectList(db.AspNetRoles, "Id", "Name", workFlowItem.ExtendedNextRoleKey);
            ViewBag.ExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", workFlowItem.ExtendedFormTemplateKey);
            ViewBag.NextExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", workFlowItem.NextExtendedFormTemplateKey);
            ViewBag.KPIKey = new SelectList(db.KPIs, "KPIKey", "KPIValue", workFlowItem.KPIKey);
            ViewBag.CurrentExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName", workFlowItem.CurrentExtendedOrganisationKey);
            ViewBag.NextExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName", workFlowItem.NextExtendedOrganisationKey);
            ViewBag.NextStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue", workFlowItem.NextStatusKey);
            ViewBag.CurrentStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue", workFlowItem.CurrentStatusKey);
            return View(workFlowItem);
        }

        // POST: WorkFlowItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "WorkFlowItemKey,ExtendedFormTemplateKey,ExtendedCurrentRoleKey,ExtendedNextRoleKey,CurrentStatusKey,NextStatusKey,KPIKey,NextExtendedFormTemplateKey,WorkFlowItemIndex,CurrentExtendedOrganisationKey,NextExtendedOrganisationKey")] WorkFlowItem workFlowItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workFlowItem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ExtendedCurrentRoleKey = new SelectList(db.AspNetRoles, "Id", "Name", workFlowItem.ExtendedCurrentRoleKey);
            ViewBag.ExtendedNextRoleKey = new SelectList(db.AspNetRoles, "Id", "Name", workFlowItem.ExtendedNextRoleKey);
            ViewBag.ExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", workFlowItem.ExtendedFormTemplateKey);
            ViewBag.NextExtendedFormTemplateKey = new SelectList(db.FormTemplates, "FormTemplateKey", "Name", workFlowItem.NextExtendedFormTemplateKey);
            ViewBag.KPIKey = new SelectList(db.KPIs, "KPIKey", "KPIValue", workFlowItem.KPIKey);
            ViewBag.CurrentExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName", workFlowItem.CurrentExtendedOrganisationKey);
            ViewBag.NextExtendedOrganisationKey = new SelectList(db.Organisations, "OrganisationKey", "OrganisationName", workFlowItem.NextExtendedOrganisationKey);
            ViewBag.NextStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue", workFlowItem.NextStatusKey);
            ViewBag.CurrentStatusKey = new SelectList(db.Status, "StatusKey", "StatusValue", workFlowItem.CurrentStatusKey);
            return View(workFlowItem);
        }

        // GET: WorkFlowItems/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkFlowItem workFlowItem = await db.WorkFlowItems.FindAsync(id);
            if (workFlowItem == null)
            {
                return HttpNotFound();
            }
            return View(workFlowItem);
        }

        // POST: WorkFlowItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            WorkFlowItem workFlowItem = await db.WorkFlowItems.FindAsync(id);
            db.WorkFlowItems.Remove(workFlowItem);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
