﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class FormTemplatesController : Controller
    {
        private Entities db = new Entities();

        // GET: FormTemplates
        public async Task<ActionResult> Index(string HighlightedPart=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            //var formTemplates = db.FormTemplates.Include(f => f.FormTemplateType1);
            var formTemplates = db.FormTemplates;
            return View(await formTemplates.ToListAsync());
        }

        // GET: FormTemplates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormTemplate formTemplate = await db.FormTemplates.FindAsync(id);
            if (formTemplate == null)
            {
                return HttpNotFound();
            }
            return View(formTemplate);
        }

        // GET: FormTemplates/Create
        public ActionResult Create()
        {
            ViewBag.FormTemplateType = new SelectList(db.FormTemplateTypes, "FormTemplateType1", "ParentFormTemplateType");
            return View();
        }

        // POST: FormTemplates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FormTemplateKey,Name,Description,FormTemplateType,Status,CreatedAt")] FormTemplate formTemplate)
        {
            if (ModelState.IsValid)
            {
                db.FormTemplates.Add(formTemplate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FormTemplateType = new SelectList(db.FormTemplateTypes, "FormTemplateType1", "ParentFormTemplateType", formTemplate.FormTemplateType);
            return View(formTemplate);
        }

        // GET: FormTemplates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormTemplate formTemplate = await db.FormTemplates.FindAsync(id);
            if (formTemplate == null)
            {
                return HttpNotFound();
            }
            ViewBag.FormTemplateType = new SelectList(db.FormTemplateTypes, "FormTemplateType1", "ParentFormTemplateType", formTemplate.FormTemplateType);
            return View(formTemplate);
        }

        // POST: FormTemplates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FormTemplateKey,Name,Description,FormTemplateType,Status,CreatedAt")] FormTemplate formTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formTemplate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.FormTemplateType = new SelectList(db.FormTemplateTypes, "FormTemplateType1", "ParentFormTemplateType", formTemplate.FormTemplateType);
            return View(formTemplate);
        }

        // GET: FormTemplates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormTemplate formTemplate = await db.FormTemplates.FindAsync(id);
            if (formTemplate == null)
            {
                return HttpNotFound();
            }
            return View(formTemplate);
        }

        // POST: FormTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FormTemplate formTemplate = await db.FormTemplates.FindAsync(id);
            db.FormTemplates.Remove(formTemplate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
