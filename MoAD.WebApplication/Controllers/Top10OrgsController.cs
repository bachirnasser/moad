﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace MoAD.WebApplication.Controllers
{
    public class Top10OrgsController : Controller
    {
        // GET: Top10Orgs
        public async Task<ActionResult> Index()
        {

            List<GetMostActiveOrganisations_Result> result = new List<GetMostActiveOrganisations_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMostActiveOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("oups={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetMostActiveOrganisations_Result>>();
                    ViewBag.Top10Result= result;
                    ViewBag.Top10ResultCount = result.Count;
                }
            }
            return View();
        }
    }
}