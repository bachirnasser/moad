﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
   [Authorize]
    public class SurveyGeneratorController : Controller
    {
        private Entities db = new Entities();
        // GET: SurveyGenerator

        //[Globals.YourCustomAuthorize]
        public async System.Threading.Tasks.Task<ActionResult> Index(string HighlightedPart = "SurveyGenerator")
        {
            ViewBag.HighlightedPart = HighlightedPart;
            string ClaimType = "Survey";
            List<GetAllFormTemplatesForaType_Result> AllSurveyFormTemplate = new List<GetAllFormTemplatesForaType_Result>();
            AllSurveyFormTemplate = await FormTemplatesDisplayController.GetAllFormTemplatesForaType(ClaimType);

            List<GetAdManagerForms_Result> SavedForms = await getAdManagerForms(User.Identity.GetUserId(), ClaimType, 0);
            ViewBag.AllSurveyFormTemplate = AllSurveyFormTemplate;
            ViewBag.title = "إدارة  استطلاعات الرأي";
            ViewBag.FormTemplateKey = AllSurveyFormTemplate.FirstOrDefault().FormTemplateKey;
            ViewBag.SavedForms = SavedForms;
            return View();
        }


        public async System.Threading.Tasks.Task<ActionResult> Forms(string HighlightedPart = "LanguageContent")
        {
            ViewBag.HighlightedPart = HighlightedPart;
            string ClaimType = "Claim";
            string RidaMouwatenType = "رضا المواطن";
            string RidaMouwazafType = "رضا الموظف";
            string TabsitType = "معا لتبسيط الإجراءات";
            string ThouboutyatType = "معا لثبوتيات أقل";
            string JoudatType = "تنظيم مؤسساتي";
            List<GetAllFormTemplatesForaType_Result> AllSurveyFormTemplate = new List<GetAllFormTemplatesForaType_Result>();
            List<GetAllFormTemplatesForaType_Result> AllSurveyFormTemplateJoudat = new List<GetAllFormTemplatesForaType_Result>();
          
           GetAllFormTemplatesForaType_Result AllSurveyFormTemplateTabsit = new GetAllFormTemplatesForaType_Result();
            GetAllFormTemplatesForaType_Result AllSurveyFormTemplateThouboutyat = new GetAllFormTemplatesForaType_Result();
            GetAllFormTemplatesForaType_Result AllSurveyFormTemplateClaim = new GetAllFormTemplatesForaType_Result();
            GetAllFormTemplatesForaType_Result AllSurveyFormTemplateRidaMouwaten= new GetAllFormTemplatesForaType_Result();
            GetAllFormTemplatesForaType_Result AllSurveyFormTemplateRidaMouwazaf= new GetAllFormTemplatesForaType_Result();
            AllSurveyFormTemplateRidaMouwaten = (await FormTemplatesDisplayController.GetAllFormTemplatesForaType(RidaMouwatenType)).FirstOrDefault();
            AllSurveyFormTemplateClaim = (await FormTemplatesDisplayController.GetAllFormTemplatesForaType(ClaimType)).FirstOrDefault();
            AllSurveyFormTemplateRidaMouwazaf = (await FormTemplatesDisplayController.GetAllFormTemplatesForaType(RidaMouwazafType)).FirstOrDefault();
            AllSurveyFormTemplateTabsit = (await FormTemplatesDisplayController.GetAllFormTemplatesForaType(TabsitType)).FirstOrDefault();
            AllSurveyFormTemplateThouboutyat = (await FormTemplatesDisplayController.GetAllFormTemplatesForaType(ThouboutyatType)).FirstOrDefault();
            AllSurveyFormTemplateJoudat = await FormTemplatesDisplayController.GetAllFormTemplatesForaType(JoudatType);
            AllSurveyFormTemplate.Add(AllSurveyFormTemplateRidaMouwaten);
            AllSurveyFormTemplate.Add(AllSurveyFormTemplateClaim);
            AllSurveyFormTemplate.Add(AllSurveyFormTemplateRidaMouwazaf);
            AllSurveyFormTemplate.Add(AllSurveyFormTemplateTabsit);
            AllSurveyFormTemplate.Add(AllSurveyFormTemplateThouboutyat);
            AllSurveyFormTemplate.AddRange(AllSurveyFormTemplateJoudat);

            // List<GetAdManagerForms_Result> SavedForms = await getAdManagerForms(User.Identity.GetUserId(), ClaimType, 0);
            ViewBag.AllSurveyFormTemplate = AllSurveyFormTemplate;
            ViewBag.title = "إدارة الاستبيانات";
           // ViewBag.FormTemplateKey = AllSurveyFormTemplateClaim.FirstOrDefault().FormTemplateKey;
           // ViewBag.SavedForms = SavedForms;
            return View();
        }
        //[Globals.YourCustomAuthorize]
        public async System.Threading.Tasks.Task<ActionResult> AddSurveyTemplate(string HighlightedPart = "SurveyGenerator")
        {
            ViewBag.HighlightedPart = HighlightedPart;
            return View();
        }

        //[Globals.YourCustomAuthorize]
        public async System.Threading.Tasks.Task<ActionResult> SurveyTemplateView(string HighlightedPart = "SurveyGenerator", string formTemplateKey = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            ViewBag.formTemplateKey = formTemplateKey;
            //var formTemplates = await HomeController.GetAllFormTemplatesForaType("Survey");
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", formTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }

            ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                 s => s.Items);
            ViewBag.SurveyTitle = resultFormTemplate.FirstOrDefault().FormTemplateName;
            return View();
        }

        //[Globals.YourCustomAuthorize]
        public async System.Threading.Tasks.Task<ActionResult> EditSurveyTemplate(string HighlightedPart = "SurveyGenerator", string f_if = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            ViewBag.formTemplateKey = f_if;
            //var formTemplates = await HomeController.GetAllFormTemplatesForaType("Survey");
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", f_if);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                }
            }

            ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                 s => s.Items);
            ViewBag.SurveyTitle = resultFormTemplate.FirstOrDefault().FormTemplateName;
            ViewBag.SurveyDescription = resultFormTemplate.FirstOrDefault().FormTemplateDescription;
            ViewBag.OnlyQuestionsEdit = "False";
            if (f_if=="106" || f_if=="113" || f_if == "114")
            {
                ViewBag.OnlyQuestionsEdit = "True";
            }
            return View();
        }

        public async Task<List<GetAdManagerForms_Result>> getAdManagerForms(string UserId, string FormTemplateType, int organisationKey)
        {
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserId={0}", UserId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserRoleID={0}", 1);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
            }
            return result;
        }

        public async Task<string> PublishSurvey(string id = null, string publishAction = null)
        {
            string uk = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PublishSurvey/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("ID={0}", int.Parse(id));
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("actionValue={0}", publishAction);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "Success";
        }

        public async Task<string> DeleteSurvey(string surveyTemplateKey = null)
        {
            int key = int.Parse(surveyTemplateKey);
            var formTemplate = db.FormTemplates.Where(f => f.FormTemplateKey == key).FirstOrDefault();
            db.FormTemplates.Remove(formTemplate);
            await db.SaveChangesAsync();
            return "Success";
        }

        public async Task<string> UpdateQuestion(int? q_key = null, string q_value = null)
        {
            var question = db.Questions.Where(q => q.QuestionKey == q_key).FirstOrDefault();
            question.Value = q_value;
            await db.SaveChangesAsync();
            return "Success";
        }

        public async Task<string> DeleteQuestion(int? id_quest = null)
        {
            var question = db.Questions.Where(q => q.QuestionKey == id_quest).FirstOrDefault();
            db.Questions.Remove(question);
            await db.SaveChangesAsync();
            return "Success";
        }

        public async Task<string> UpdateAnswer(int? a_key = null, string a_value = null)
        {
            var answer = db.AnswersTemplates.Where(q => q.AnswerTemplateKey == a_key).FirstOrDefault();
            answer.Value = a_value;
            await db.SaveChangesAsync();
            return "Success";
        }
        public async Task<string> DeleteAnswer(int? id_ans = null)
        {
            var answer = db.AnswersTemplates.Where(q => q.AnswerTemplateKey == id_ans).FirstOrDefault();
            db.AnswersTemplates.Remove(answer);
            await db.SaveChangesAsync();
            return "Success";
        }

        [HttpPost]
        public async Task<string> UpdateSurveyTemplate(int? surveyId = null, string title = null, string answers = null, string questions = null)
        {
            List<Dictionary<string, object>> QuestionsList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(questions);
            List<Dictionary<string, object>> AnswersList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(answers);

            var currentTemplate = db.FormTemplates.Where(f => f.FormTemplateKey == surveyId).FirstOrDefault();
            currentTemplate.Name = title;
            await db.SaveChangesAsync();

            var questionIndex = db.Questions.Where(q => q.FormTemplateKey == surveyId).Count();
            questionIndex++;
            foreach (var ques in QuestionsList)
            {
                int questionKey = int.Parse(ques["key"].ToString());
                if (questionKey != 0)
                {
                    var currentQuestion = db.Questions.Where(q => q.QuestionKey == questionKey).FirstOrDefault();
                    currentQuestion.Value = ques["value"].ToString();
                    await db.SaveChangesAsync();
                }
                else
                {
                    Question newQuestion = new Question();
                    newQuestion.Value = ques["value"].ToString();
                    newQuestion.DisplayMode = "Visible";
                    newQuestion.FormTemplateKey = surveyId;
                    newQuestion.QuestionKeyIndex = questionIndex;
                    newQuestion.LinkedQuestion = 0;

                    db.Questions.Add(newQuestion);
                    await db.SaveChangesAsync();


                    int answerIndex = 1;
                    foreach (var ans in AnswersList)
                    {
                        
                        string qValue = ans["questionValue"].ToString();
                        if (qValue == newQuestion.Value)
                        {
                            AnswersTemplate newAsnwer = new AnswersTemplate();
                            newAsnwer.Value = ans["value"].ToString();
                            newAsnwer.FormElementKey = 3;
                            newAsnwer.QuestionKey = newQuestion.QuestionKey;
                            newAsnwer.AnswerTemplateKeyIndex = answerIndex;
                            newAsnwer.ScoreKey = "1";

                            db.AnswersTemplates.Add(newAsnwer);
                            await db.SaveChangesAsync();
                            answerIndex++;
                        }
                    }
                    questionIndex++;
                }
            }

            foreach (var ans in AnswersList)
            {
                int answerKey = int.Parse(ans["key"].ToString());
                if (answerKey != 0)
                {
                    var currentAnswer = db.AnswersTemplates.Where(q => q.AnswerTemplateKey == answerKey).FirstOrDefault();
                    currentAnswer.Value= ans["value"].ToString();
                    await db.SaveChangesAsync();
                }
                else
                {
                    AnswersTemplate answersTemplate = new AnswersTemplate();
                    answersTemplate.Value= ans["value"].ToString();
                    answersTemplate.FormElementKey = 3;
                    answersTemplate.QuestionKey =int.Parse(ans["questionKey"].ToString());
                    answersTemplate.AnswerTemplateKeyIndex =(int.Parse(ans["lastAnswerIndex"].ToString()) + 1 );
                    db.AnswersTemplates.Add(answersTemplate);
                    await db.SaveChangesAsync();
                }
            }
            return "Success";
        }
    }
}