﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MoAD.WebApplication.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Data.Entity;

namespace MoAD.WebApplication.Controllers
{
    public class NewPagesController : BaseController
    {
        private Entities db = new Entities();
        public async Task<ActionResult> Markaz()
        {
            ViewBag.PageName = "Markaz";
            if (User.Identity.IsAuthenticated && User.IsInRole("Editor"))
            {
                return RedirectToAction("Markaz", "TestCMS");
            }

            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);



            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            ViewBag.myIP = myIP;
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await GetAllFormTemplatesForaType("Survey");

            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;

                if (User.Identity.IsAuthenticated && !User.IsInRole("Citizen"))
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;
                }
                else
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), null).Result;
                }

                ViewBag.formTemplateKeySurvey = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }


            string userKey = User.Identity.GetUserId();

            if (userKey != null)
            {
                MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                List<GetUserInformation_Result> userInfo = GetUserInformation(userKey).Result;
                MoAD.WebApplication.Globals.Configuration.Rolekey = userInfo.FirstOrDefault().RoleId;
            }

            ViewBag.PageName = "Markaz";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();
            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }


            if (TemplateKeyParam != "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }
            else
            {
                ViewBag.Survey = new Dictionary<string, List<GetFormTemplate_Result>>();
            }
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;

            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Markaz").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).OrderBy(n => n.ComponentLevel).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;
            //End of Navbar Part


            int pageKey = (await db.Pages.Where(p => p.PageName == "Markaz").FirstOrDefaultAsync()).PageKey;
            var pageComponentVideo = await db.PageComponentsHierarchies.Where(p => p.PageKey == pageKey && p.Component.ComponentType.ComponentType1 == "Video").FirstOrDefaultAsync();

            string videoUrlComponent = "";
            int? videoComponentPageKey = null;
            if (pageComponentVideo != null)
            {
                videoComponentPageKey = pageComponentVideo.Component.ComponentKey;
                videoUrlComponent = pageComponentVideo.Component.ComponentSourceEntityKey;
            }

            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.pageKey = pageKey;
            ViewBag.videoUrlComponent = videoUrlComponent;
            ViewBag.videoComponentPageKey = videoComponentPageKey;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;




            var sliderItemComponent1 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            var sliderItemComponent2 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault();

            int? sliderItemComponentKey1 = null;
            if (sliderItemComponent1 != null)
            {
                sliderItemComponentKey1 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            int? sliderItemComponentKey2 = null;
            if (sliderItemComponent2 != null)
            {
                sliderItemComponentKey2 = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault().ComponentKey;
            }
            var sliderItems1 = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey1).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder1 = sliderItems1.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();
            var sliderItems2 = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey2).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder2 = sliderItems2.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();


            ViewBag.SliderItemComponentKey1 = sliderItemComponentKey1;
            ViewBag.SliderData1 = sliderItemsGroupByOrder1;
            ViewBag.SliderItemComponentKey2 = sliderItemComponentKey2;
            ViewBag.SliderData2 = sliderItemsGroupByOrder2;
            return View();
        }

        public async Task<ActionResult> Marsad()
        {
            ViewBag.PageName = "Marsad";
            if (User.Identity.IsAuthenticated && User.IsInRole("Editor"))
            {
                return RedirectToAction("Marsad", "TestCMS");
            }
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Marsad").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }


        public async Task<ActionResult> Manbar()
        {
            ViewBag.PageName = "Manbar";
            if (User.Identity.IsAuthenticated && User.IsInRole("Editor"))
            {
                return RedirectToAction("Manbar", "TestCMS");
            }
            ViewBag.Locale = await MoAD.WebApplication.Managers.Utilities.GetLocalisationPerControllerAndAction("Home", "Index", CurrentLanguageIdentifier);
            ViewBag.Languageidentifier = CurrentLanguageIdentifier;
            ViewBag.CurrentLanguageIdentifier = CurrentLanguageIdentifier;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Manbar").ToListAsync();
            ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
            int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

            var sliderItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            int? sliderItemComponentKey = null;
            if (sliderItemComponent != null)
            {
                sliderItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault().ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                    p => p.ComponentOrder,
                    p => p,
             (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
             {
                 ComponentOrder = p.ComponentOrder,
                 Items = p.Items
             }).ToList();

            var sliderItems = allComponentsInPage.Where(i => i.ParentComponentKey == sliderItemComponentKey).ToList();

            List<NavBarViewModel> sliderItemsGroupByOrder = sliderItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
            ViewBag.NavbarData = navbarItemsGroupByOrder;

            ViewBag.SliderItemComponentKey = sliderItemComponentKey;
            ViewBag.SliderData = sliderItemsGroupByOrder;
            return View();
        }

        public async Task<ActionResult> Repeater()
        {
            return View();
        }
        public async Task<ActionResult> Index()
        {
            string myIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(myIP))
            {
                myIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            var HashedAdress = CalculateMD5Hash(myIP);



            String strUserName, strDomainName, strMachineName;
            strMachineName = Environment.MachineName.ToString();
            strDomainName = Environment.UserDomainName.ToString();
            strUserName = Environment.UserName.ToString();
            ViewBag.myIP = myIP;
            List<GetUserSurveyAnswer_Result> userInfoSurvey = new List<GetUserSurveyAnswer_Result>();
            var formTemplates = await GetAllFormTemplatesForaType("Survey");

            try
            {
                string Karchoune = Request.Cookies["Karchoune"].Value;
                ViewBag.myCookie = Karchoune;

                if (User.Identity.IsAuthenticated && !User.IsInRole("Citizen"))
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), User.Identity.GetUserId()).Result;
                }
                else
                {
                    userInfoSurvey = GetUserSurveyAnswer(myIP, Karchoune, formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString(), null).Result;
                }

                ViewBag.formTemplateKeySurvey = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
                ViewBag.GetAnswers = userInfoSurvey;
            }
            catch
            {
                HttpCookie myCookie = new HttpCookie("Karchoune");
                string guid = Guid.NewGuid().ToString();
                myCookie["k"] = guid + "+" + HashedAdress + "+" + strMachineName + "+" + strDomainName + "+" + strUserName;
                //myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                ViewBag.myCookie = myCookie["k"];
                ViewBag.GetAnswers = userInfoSurvey;
            }


            string userKey = User.Identity.GetUserId();

            if (userKey != null)
            {
                MoAD.WebApplication.Globals.Configuration.UserKey = userKey;
                List<GetUserInformation_Result> userInfo = GetUserInformation(userKey).Result;
                MoAD.WebApplication.Globals.Configuration.Rolekey = userInfo.FirstOrDefault().RoleId;
            }

            ViewBag.PageName = "Markaz";
            List<string> FormTemplateTypeResult = new List<string>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllPossibleFormTemplateTypes/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    FormTemplateTypeResult = await response.Content.ReadAsAsync<List<string>>();
                    ViewBag.FormTemplateTypeResult = FormTemplateTypeResult;
                }
            }


            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;
            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllFormTemplatesForaType_Result> result2 = GetFormTemplates("Claim").Result;
            ViewData["Forms"] = result2.Select(form => new SelectListItem { Text = form.Name, Value = form.FormTemplateKey.ToString() }).ToList();
            /* 29-08-2019 */
            List<GetFormTemplate_Result> resultFormTemplate = new List<GetFormTemplate_Result>();
            var TemplateKeyParam = "Empty";
            try
            {
                TemplateKeyParam = formTemplates.Where(f => f.Description == "Survey for markaz").FirstOrDefault().FormTemplateKey.ToString();
            }
            catch (Exception ex)
            {

            }


            if (TemplateKeyParam != "Empty")
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKeyParam);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        resultFormTemplate = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    }
                }

                ViewBag.Survey = resultFormTemplate.GroupBy(s => s.QuestionKey).Select(group => new { questionKey = group.Key, Items = group.ToList() }).ToDictionary(s => s.questionKey.ToString(),
                                     s => s.Items);
            }
            else
            {
                ViewBag.Survey = new Dictionary<string, List<GetFormTemplate_Result>>();
            }

            /* 29-08-2019 */

            /*Updated By DN*/
            List<GetAdManagerForms_Result> Estetla3Ra2iSavedForms = getAdManagerForms(null, "تقارير إستطلاعات الرأي", 0).Result;
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedForms = Estetla3Ra2iSavedForms.Where(f => f.IsPublished == "Published").ToList();
            List<GetAdManagerForms_Result> Estetla3Ra2iPublishedFormsForManbar = new List<GetAdManagerForms_Result>();
            foreach (var item in Estetla3Ra2iPublishedForms)
            {
                //var FormTemplateInfo = SurveyReportController.GetFormTemplate(item.FileExtensionName).Result;
                if (item.FileType == "إستطلاع رأي مركز")
                {
                    Estetla3Ra2iPublishedFormsForManbar.Add(item);
                }
            }
            ViewBag.Estetla3Ra2iPublishedForms = Estetla3Ra2iPublishedFormsForManbar;
            /*Updated By DN*/
            return View();
        }

        public async Task<List<GetAllFormTemplatesForaType_Result>> GetFormTemplates(string TemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplatesForAType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateType={0}", TemplateType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();

                }
            }

            return result;
        }

        public async Task<List<GetAdManagerForms_Result>> getAdManagerForms(string UserId, string FormTemplateType, int organisationKey)
        {
            List<GetAdManagerForms_Result> result = new List<GetAdManagerForms_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/getAdManagerForms/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("UserId={0}", UserId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormTemplateType={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("UserRoleID={0}", 1);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAdManagerForms_Result>>();
                }
            }
            return result;
        }


        public async Task<List<GetUserSurveyAnswer_Result>> GetUserSurveyAnswer(string ip = null, string cookie = null, string vk = null, string userkey = null)
        {
            List<GetUserSurveyAnswer_Result> result = new List<GetUserSurveyAnswer_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserSurveyAnswer/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("uk={0}", userkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ip={0}", ip);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("cookie={0}", cookie);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ViewKey={0}", vk);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserSurveyAnswer_Result>>();
                }
            }

            return result;
        }
        [Globals.YourCustomAuthorize]
        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }



        public static async Task<List<GetAllFormTemplatesForaType_Result>> GetAllFormTemplatesForaType(string formTemplateType)
        {
            List<GetAllFormTemplatesForaType_Result> result = new List<GetAllFormTemplatesForaType_Result>();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllFormTemplatesForaType/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formTemplateType={0}", formTemplateType);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllFormTemplatesForaType_Result>>();
                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public static async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }


        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }


        public ActionResult Carousel()
        {
            return View();
        }


    }
}