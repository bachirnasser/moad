﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class AnswersTemplatesController : Controller
    {
        private Entities db = new Entities();

        // GET: AnswersTemplates
        public async Task<ActionResult> Index(string HighlightedPart=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            var answersTemplates = db.AnswersTemplates.Include(a => a.ExtendedData).Include(a => a.Question).Include(a => a.FormElement);
            return View(await answersTemplates.ToListAsync());
        }

        // GET: AnswersTemplates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswersTemplate answersTemplate = await db.AnswersTemplates.FindAsync(id);
            if (answersTemplate == null)
            {
                return HttpNotFound();
            }
            return View(answersTemplate);
        }

        // GET: AnswersTemplates/Create
        public ActionResult Create()
        {
            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name");
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Value");
            ViewBag.FormElementKey = new SelectList(db.FormElements, "FormElementKey", "Name");
            return View();
        }

        // POST: AnswersTemplates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AnswerTemplateKey,Value,FormElementKey,ScoreKey,QuestionKey,AnswerTemplateKeyIndex,FileExtensionKey")] AnswersTemplate answersTemplate)
        {
            if (ModelState.IsValid)
            {
                db.AnswersTemplates.Add(answersTemplate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name", answersTemplate.FileExtensionKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Value", answersTemplate.QuestionKey);
            ViewBag.FormElementKey = new SelectList(db.FormElements, "FormElementKey", "Name", answersTemplate.FormElementKey);
            return View(answersTemplate);
        }

        // GET: AnswersTemplates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswersTemplate answersTemplate = await db.AnswersTemplates.FindAsync(id);
            if (answersTemplate == null)
            {
                return HttpNotFound();
            }
            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name", answersTemplate.FileExtensionKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Value", answersTemplate.QuestionKey);
            ViewBag.FormElementKey = new SelectList(db.FormElements, "FormElementKey", "Name", answersTemplate.FormElementKey);
            return View(answersTemplate);
        }

        // POST: AnswersTemplates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AnswerTemplateKey,Value,FormElementKey,ScoreKey,QuestionKey,AnswerTemplateKeyIndex,FileExtensionKey")] AnswersTemplate answersTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answersTemplate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.FileExtensionKey = new SelectList(db.ExtendedDatas, "FileExtensionKey", "Name", answersTemplate.FileExtensionKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Value", answersTemplate.QuestionKey);
            ViewBag.FormElementKey = new SelectList(db.FormElements, "FormElementKey", "Name", answersTemplate.FormElementKey);
            return View(answersTemplate);
        }

        // GET: AnswersTemplates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswersTemplate answersTemplate = await db.AnswersTemplates.FindAsync(id);
            if (answersTemplate == null)
            {
                return HttpNotFound();
            }
            return View(answersTemplate);
        }

        // POST: AnswersTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AnswersTemplate answersTemplate = await db.AnswersTemplates.FindAsync(id);
            db.AnswersTemplates.Remove(answersTemplate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
