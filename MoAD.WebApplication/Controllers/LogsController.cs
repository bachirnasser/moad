﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class LogsController : Controller
    {
        private Entities db = new Entities();

        // GET: Logs
        public async Task<ActionResult> Index()
        {
            return View(await db.Logs.ToListAsync());
        }

        // GET: Logs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log log = await db.Logs.FindAsync(id);
            if (log == null)
            {
                return HttpNotFound();
            }
            return View(log);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
