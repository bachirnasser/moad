﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using Microsoft.AspNet.Identity;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class MouwazafinWithOrgsController : Controller
    {
        // GET: MouwazafinWithOrgs
        public async Task<ActionResult> Index()
        {
            ViewBag.HighlightedPart = "Ta3rifMouwazafin";

            List<GetMouwazzafinWithOrg_Result> result = new List<GetMouwazzafinWithOrg_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMouwazzafinWithOrg/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("MouwazafId={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue1={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue2={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue3={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("count={0}", "BETWEEN 0 AND 1000");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetMouwazzafinWithOrg_Result>>();
                    ViewBag.MouwazafinWithOrgsTable = result;
                }
            }


            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Mouwazafin(string SearchValue1 = null, string SearchValue2 = null, string SearchValue3 = null,string count=null)
        {

            List<GetMouwazzafinWithOrg_Result> result = new List<GetMouwazzafinWithOrg_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMouwazzafinWithOrg/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("MouwazafId={0}", "");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue1={0}", SearchValue1);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue2={0}", SearchValue2);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue3={0}", SearchValue3);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("count={0}", count);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetMouwazzafinWithOrg_Result>>();
                    ViewBag.MouwazafinWithOrgsTable = result;
                }
            }


            return PartialView();
        }


        public async Task<ActionResult> Edit(string RowId = null)
        {

            List<GetMouwazzafinWithOrg_Result> result = new List<GetMouwazzafinWithOrg_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMouwazzafinWithOrg/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("MouwazafId={0}", RowId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue1={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue2={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue3={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("count={0}", "BETWEEN 0 AND 1000");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetMouwazzafinWithOrg_Result>>();
                    ViewBag.MouwazafinToEdit = result.FirstOrDefault();
                    ViewBag.SIDEPARENT = result.FirstOrDefault().SIDEPARENT;
                    ViewBag.SIDENAME = result.FirstOrDefault().SIDENAME;
                    ViewBag.JobName = result.FirstOrDefault().JobKey;
                }
            }


            //List<GetAllOrganisations_Result> resultOrg = GetAllOrganisations(null).Result;
           
           

            //ViewData["Organisation"] = resultOrg.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetAllOrganisations_Result> resultOrganisationsWithParent = GetAllOrganisations("1").Result;
            ViewBag.OrganizationsOrganisationsWithParent = resultOrganisationsWithParent;
            ViewData["OrganisationsWithParent"] = resultOrganisationsWithParent.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<GetJobDescription_Result> JobDescription = GetJobDescription().Result;
            ViewData["JobDescription"] = JobDescription.Select(org => new SelectListItem { Text = org.JobName, Value = org.JobKey.ToString() }).ToList();

            return View();
        }


        public async Task<ActionResult> UpdateOrgs(string organKey = null, string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            ViewBag.Message = "Your contact page.";
            if (organKey == null)
            {
                ViewBag.organKey = string.Empty;
            }
            else
            {
                ViewBag.organKey = organKey;
            }

            

            List<GetAllOrganisations_Result> result = GetAllOrganisations().Result;

            //List<AspNetUser> Allusers = new List<AspNetUser>();
            //Entities db = new Entities();
            //Allusers = db.AspNetUsers.ToList();
            //ViewBag.Useres = Allusers;


            ViewData["Organisation"] = result.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
            return View();
        }

        public async Task<string> UpdateOrganisation(string orgKey=null, string OrgName=null)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SetAccessPrivilege/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("orgKey={0}", orgKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OrgName={0}", OrgName);



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }

        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }


        public async Task<JsonResult> SearchMouwazzaf(string SearchValue = null)
        {

            List<GetMouwazzafinWithOrg_Result> result = new List<GetMouwazzafinWithOrg_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMouwazzafinWithOrg/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("MouwazafId={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SearchValue={0}", SearchValue);

                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetMouwazzafinWithOrg_Result>>();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public async Task<List<GetJobDescription_Result>> GetJobDescription()
        {

            List<GetJobDescription_Result> result = new List<GetJobDescription_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetJobDescription/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("ts={0}", "d");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetJobDescription_Result>>();


                }
            }
            return result;
        }


        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations(string parent)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", parent);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("r={0}", "null");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        public async Task<JsonResult> GetAllOrganisationsJson(string parentOrg)
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisationsReporting/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("parent={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("parentOrg={0}", parentOrg);
                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public async Task<string> EditMouwatin(string RowId = null, string FIRSTNAME = null, string FATHERNAME = null, string SURNAME = null, string MOTHERNAME = null, string BIRTHPLACE = null, string BIRTHDAY = null, string BIRTHMONTH = null, string BIRTHYEAR = null, string SIDENAME = null, string SIDEPARENT = null,string JobKey = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/EditMouwatin/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RowId={0}", RowId);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FIRSTNAME={0}", FIRSTNAME);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FATHERNAME={0}", FATHERNAME);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SURNAME={0}", SURNAME);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("MOTHERNAME={0}", MOTHERNAME);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("BIRTHPLACE={0}", BIRTHPLACE);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("BIRTHDAY={0}", BIRTHDAY);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("BIRTHMONTH={0}", BIRTHMONTH);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("BIRTHYEAR={0}", BIRTHYEAR);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SIDENAME={0}", SIDENAME);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("SIDEPARENT={0}", SIDEPARENT);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("JobKey={0}", JobKey);
                
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();
                }
            }


            return "success";
        }
        
    }
}