﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.DataObjects;
using Microsoft.AspNet.Identity;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Newtonsoft.Json;
using MoAD.WebApplication.Models;
using System.Data.Entity;
//using MoAD.DataInterface.EDMX;

namespace MoAD.WebApplication.Controllers
{
  
    public class ClaimFormsController : Controller
    {
        private Entities db = new Entities();
        // GET: ClaimForms
        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> Index(string AnswerIds = "", string OrganizationKey = null, string FormTemplateKey = null)
        {
            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<object> nestedResult = new List<object>();
            string TemplateKey = "106";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    nestedResult = result
                                .GroupBy(x => x.QuestionKey).Select(q => new
                                {
                                    QuestionKey = q.Key,
                                    QuestionValue = q.Select(x => x.QuestionValue).First(),
                                    QuestionNote = q.Select(x => x.QuestionNote).First(),
                                    QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                    QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                    Answers = q.Select(ans => new
                                    {
                                        AnswerTemplateKey = ans.AnswerTemplateKey,
                                        AnswerValue = ans.AnswerValue,
                                        AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                        AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                        AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                        AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                        AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                        AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                        AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                        AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                        FormElement = ans.FormElement,
                                        FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                        FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                        FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                        FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                        FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                        FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                        FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                        FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                        FormElementTypeName = ans.FormElementTypeName,
                                        FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                        FormElementTypeKey = ans.FormElementTypeKey,
                                        FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                        FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                        FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                        FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                        FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                        FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                                    }).ToList<object>()
                                }).ToList<object>();
                }
            }
            ViewBag.Answers = AnswerIds;


            return View(nestedResult);
        }

       
        public async Task<string> SubmitFormForUser(string trackingStatusKey = null, string formTemplateKey = null, string userKey = null, string organisationKey = null,string FileExtensionName=null, string FileExtensionType = null,string createdby=null)
        {
            string result = string.Empty;
            if(organisationKey==null)
            {
                List<GetUserInformation_Result> userInfo = GetUserInformation().Result;
                organisationKey = userInfo.FirstOrDefault().OrganisationKey.ToString();
            }

            if (FileExtensionName == null)
            {
                FileExtensionName = "null";
            }

            if (createdby == null)
            {
                createdby = "null";
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SubmitFormForUser");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("trackingStatusKey={0}", trackingStatusKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userKey={0}", userKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("organisationKey={0}", organisationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FileExtensionName={0}", FileExtensionName);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FileExtensionType={0}", FileExtensionType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("createdby={0}", createdby);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }
        [HttpPost]
        public async Task<string> AnswersByUser(string formKey = null, string answerTemplateKey = null)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringContent content = new StringContent(JsonConvert.SerializeObject(formKey + "&&&" + answerTemplateKey), Encoding.UTF8, "application/json");
                // HTTP POST
                //HttpResponseMessage response = await client.PostAsync("api/Forms/AnswersByUserSignup", content);
                HttpResponseMessage response = await client.PostAsync("api/Forms/AnswersByUser", content);
                if (response.IsSuccessStatusCode)
                {
                    string data = await response.Content.ReadAsStringAsync();
                }

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<string> DeleteAnswers(string idForm = null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/DeleteAnswers");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("idform={0}", idForm);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("u_id={0}", User.Identity.GetUserId());

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }


        public async Task<string> SaveClaimForm(string formKey = null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SaveClaimForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", formKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<string> DeleteForm(string Instancekey = null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/DeleteForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("keyFormInstance={0}", int.Parse(Instancekey));


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }
        public async Task<string> SaveOPForm(string OpOrgKey = null)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/OPSaveForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("opChosenOrgKey={0}", OpOrgKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public static async Task<List<GetFormsPerAtribute_Result>> GetFormsPerAtribute(string Iduser = null, string TypeofTemplate = null,int? KeyOrg=null,int? KeyRole=null,string Attribute=null,string value=null)
        {
            List<GetFormsPerAtribute_Result> result = new List<GetFormsPerAtribute_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormsPerAtribute");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Iduser={0}", Iduser);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("TypeofTemplate={0}", TypeofTemplate);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("KeyOrg={0}", KeyOrg);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("KeyRole={0}", KeyRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Attribute={0}", Attribute);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("value={0}", value);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormsPerAtribute_Result>>();
                }
            }
            return result;
        }

        public static async Task<List<GETOPmanagerForms_Result>> GetopManagerFormsForAdManager(string FormTemplateType = null,string organisationKey=null)
        {
            List<GETOPmanagerForms_Result> result = new List<GETOPmanagerForms_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/OpManagerFormsForAdManager");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Type={0}", FormTemplateType);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("key={0}", organisationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GETOPmanagerForms_Result>>();
                }
            }
            return result;
        }

        public async Task<List<GetUserInformation_Result>> GetUserInformation()
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }
        


        public async Task<JsonResult> CheckDependecies(string FTKey=null,string Fk=null,string keyo = null)
        { 
            List<GetFormTemplateDependencies_Result> result = new List<GetFormTemplateDependencies_Result>();

            var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplateDependencies");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("FTKey={0}", FTKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplateDependencies_Result>>();

                    foreach(var item in result)
                    {
                        List<GetFormTemplate_Result> resultDependencies = GetFormTemplate(item.FormTemplateKey.ToString()).Result;
                        List<GetFormTemplate_Result> resultBase= GetFormTemplate(FTKey).Result;
                        List<GetSavedForm_Result> SavedForm = GetSavedForm(Fk).Result;
                        List<Dictionary<string, object>> AnswersArray = new List<Dictionary<string, object>>();
                        

                        foreach (var resultDependenciesItem in resultDependencies)
                        {
                            foreach (var SavedFormItem in SavedForm)
                            {
                                if (resultDependenciesItem.AnswerTemplateDependency == SavedFormItem.AnswerTemplateKey)
                                {

                                    Dictionary<string, object> Answers = new Dictionary<string, object>();
                                    Answers.Add("key", resultDependenciesItem.AnswerTemplateKey.ToString());
                                    Answers.Add("Textvalue", "");
                                    Answers.Add("type", "RadioCheckBox");
                                    Answers.Add("Parent", "");
                                    Answers.Add("Child", "");
                                    Answers.Add("count","");
                                    AnswersArray.Add(Answers);

                                }
                            }
                        }
                       
                        string formKey = await SubmitFormForUser("1", item.FormTemplateKey.ToString(), User.Identity.GetUserId(), keyo, null);
                        
                        await AnswersByUser(formKey, JsonConvert.SerializeObject(AnswersArray));
                        await SaveClaimForm(formKey);

                        AnswersArray = new List<Dictionary<string, object>>();


                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<List<GetSavedForm_Result>> GetSavedForm(string FormKey)
        {
            List<GetSavedForm_Result> result = new List<GetSavedForm_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetSavedForm");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formKey={0}", FormKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetSavedForm_Result>>();
                }
            }
            return result;
        }

        private async Task<List<GetFormTemplate_Result>> GetFormTemplate(string key)
        {

            List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
            List<GetFormTemplate_Result> nestedResult = new List<GetFormTemplate_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("TemplateKey={0}", key);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();

                    //nestedResult = result
                    //            .GroupBy(x => x.QuestionKey).Select(q => new
                    //            {
                    //                QuestionKey = q.Key,
                    //                QuestionValue = q.Select(x => x.QuestionValue).First(),
                    //                QuestionNote = q.Select(x => x.QuestionNote).First(),
                    //                QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                    //                QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                    //                Answers = q.Select(ans => new
                    //                {
                    //                    AnswerTemplateKey = ans.AnswerTemplateKey,
                    //                    AnswerValue = ans.AnswerValue,
                    //                    AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                    //                    AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                    //                    AnswerFileExtensionName = ans.AnswerFileExtensionName,
                    //                    AnswerFileExtensionType = ans.AnswerFileExtensionType,
                    //                    AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                    //                    AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                    //                    AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                    //                    AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                    //                    FormElement = ans.FormElement,
                    //                    FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                    //                    FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                    //                    FormElementFileExtensionName = ans.FormElementFileExtensionName,
                    //                    FormElementFileExtensionType = ans.FormElementFileExtensionType,
                    //                    FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                    //                    FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                    //                    FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                    //                    FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                    //                    FormElementTypeName = ans.FormElementTypeName,
                    //                    FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                    //                    FormElementTypeKey = ans.FormElementTypeKey,
                    //                    FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                    //                    FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                    //                    FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                    //                    FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                    //                    FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                    //                    FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode
                    //                }).ToList<object>()
                    //            }).ToList<object>();
                }
            }

            return result;
        }
        private string SafeSqlLiteral(string inputSQL)
        {
            return inputSQL.Replace("'", "''");
        }


        [Globals.YourCustomAuthorize]
        public async Task<ActionResult> NewClaimForm(string OrganizationKey = null, string FormTemplateKey = null, string ManagerIndex = null,string HighlightedPart = null,string formInstanceKey=null,string employeeRecord=null)
        {
            ViewBag.employeeRecord = employeeRecord;
            List<GetUserInformation_Result> UserInfo = await HomeController.GetUserInformation(User.Identity.GetUserId());
            if (UserInfo.FirstOrDefault().RoleId == "20" && FormTemplateKey=="130" && formInstanceKey==null)
            {
                List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", FormTemplateKey);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                    }
                }

                List<FormTemplateKeyVisibility_Result> result2 = new List<FormTemplateKeyVisibility_Result>();
                string UserOrgKey = UserInfo.FirstOrDefault().OrganisationKey.ToString();

                string sMonth = DateTime.Now.ToString("MM");
                int MonthPart;
                if (Convert.ToInt32(sMonth) <= 6)
                {
                    MonthPart = 1;
                }
                else
                {
                    MonthPart = 2;
                }

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetTemplateType/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("type={0}", result.FirstOrDefault().FormTemplateType);
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("OrgKey={0}", UserOrgKey);
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("Year={0}", DateTime.Now.ToString("yyyy"));
                    requestUriBuilder.Append("&");
                    requestUriBuilder.AppendFormat("MonthPart={0}", MonthPart);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        result2 = await response.Content.ReadAsAsync<List<FormTemplateKeyVisibility_Result>>();

                    }

                    if(result2.FirstOrDefault().FormCount>0 && (result2.FirstOrDefault().Status == "Saved" || result2.FirstOrDefault().Status == "OPSaved"))
                    {
                        ViewBag.Languageidentifier = 3;
                        return RedirectToAction("AdManagersClaims", "Home",
                            new { ClaimType = result.FirstOrDefault().FormTemplateType, HighlightedPart = "jaredel5adamat" });
                    }
                }

              
            }
           


            if (formInstanceKey == null)
            {
                ViewBag.formInstanceKey = "null";
            }else
            {
                ViewBag.formInstanceKey = formInstanceKey;
            }
          
            if (formInstanceKey != null)
            {
                ViewBag.HighlightedPart = HighlightedPart;

                List<GetSavedFormToEdit_Result> result = new List<GetSavedFormToEdit_Result>();
                List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();

                List<GetUserInformation_Result> UserInformation = new List<GetUserInformation_Result>();
                string OrganisationKey = "";
                UserInformation = await GetUserInformation();
                OrganisationKey = UserInformation.FirstOrDefault().OrganisationKey.ToString();
                ViewBag.OrganisationKey = OrganisationKey;
                string TemplateKey = SafeSqlLiteral(FormTemplateKey);
                //string TemplateKey = FormTemplateKey;

                //string TemplateKey = "106";
                string FormTemplateTitle = "";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetSavedFormToEdit/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("formKey={0}", formInstanceKey);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<List<GetSavedFormToEdit_Result>>();
                        //result.First().FormTemplateName
                        FormTemplateTitle = result.First().FormTemplateName;

                        nestedResult = result
                                    .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                    {
                                        QuestionKey=q.Key,
                                        QuestionValue = q.Select(x => x.QuestionValue).First(),
                                        QuestionNote = q.Select(x => x.QuestionNote).First(),
                                        QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                        QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                        Answers = q.Select(ans => new DataObjects.Answer
                                        {
                                            AnswerInstanceFileExtensionFilePathOrSource=ans.AnswerInstanceFileExtensionFilePathOrSource,
                                            AnswerTemplateKey = ans.AnswerTemplateKey,
                                            AnswerValue = ans.AnswerValue,
                                            AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                            AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                            AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                            AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                            AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                            AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                            AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                            AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                            FormElement = ans.FormElement,
                                            FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                            FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                            FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                            FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                            FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                            FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                            FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                            FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                            FormElementTypeName = ans.FormElementTypeName,
                                            FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                            FormElementTypeKey = ans.FormElementTypeKey,
                                            FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                            FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                            FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                            FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                            FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                            FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode,
                                            AnswerInstanceFileExtensionName = ans.AnswerInstanceFileExtensionName,
                                            AnswerKey=ans.AnswerKey
                                        }).ToList<DataObjects.Answer>()
                                    }).ToList<DataObjects.Question>();
                    }
                }


                ViewBag.Email = UserInfo.First().Email;
                ViewBag.PhoneNumber = UserInfo.First().PhoneNumber;
                ViewBag.UserName = UserInfo.First().UserName;
                ViewBag.FirstName = UserInfo.First().FirstName;
                ViewBag.FatherName = UserInfo.First().FatherName;
                ViewBag.LastName = UserInfo.First().LastName;
                ViewBag.FormTemplateKey = FormTemplateKey;
                ViewBag.FormTemplateTitle = FormTemplateTitle;
                ViewBag.ManagerIndex = ManagerIndex;

                string FormTemplateType = GetTypeForaFormTemplate(FormTemplateKey).Result;
                ViewBag.FormTemplateType = FormTemplateType;

                List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
                OpOrganisations = GetOPOrganisations(UserInformation.FirstOrDefault().RoleId,TemplateKey).Result;
                ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
                ViewBag.Languageidentifier = 3;

               
                return View(nestedResult);
            }
            else
            {
                ViewBag.HighlightedPart = HighlightedPart;

                List<GetFormTemplate_Result> result = new List<GetFormTemplate_Result>();
                List<DataObjects.Question> nestedResult = new List<DataObjects.Question>();

                List<GetUserInformation_Result> UserInformation = new List<GetUserInformation_Result>();
                string OrganisationKey = "";
                UserInformation = await GetUserInformation();
                OrganisationKey = UserInformation.FirstOrDefault().OrganisationKey.ToString();
                ViewBag.OrganisationKey = OrganisationKey;
                string TemplateKey = SafeSqlLiteral(FormTemplateKey);
                //string TemplateKey = FormTemplateKey;

                //string TemplateKey = "106";
                string FormTemplateTitle = "";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                    StringBuilder requestUriBuilder = new StringBuilder();
                    requestUriBuilder.Append("api/Forms/GetFormTemplate/");
                    requestUriBuilder.Append("?");
                    requestUriBuilder.AppendFormat("TemplateKey={0}", TemplateKey);
                    HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<List<GetFormTemplate_Result>>();
                        //result.First().FormTemplateName
                        FormTemplateTitle = result.First().FormTemplateName;

                        nestedResult = result
                                    .GroupBy(x => x.QuestionKey).Select(q => new DataObjects.Question
                                    {
                                        QuestionKey = q.Key,
                                        QuestionValue = q.Select(x => x.QuestionValue).First(),
                                        QuestionNote = q.Select(x => x.QuestionNote).First(),
                                        QuestionDisplayMode = q.Select(x => x.QuestionDisplayMode).First(),
                                        QuestionKeyIndex = q.Select(x => x.QuestionKeyIndex).First(),

                                        Answers = q.Select(ans => new DataObjects.Answer
                                        {
                                            AnswerTemplateKey = ans.AnswerTemplateKey,
                                            AnswerValue = ans.AnswerValue,
                                            AnswerTemplateKeyIndex = ans.AnswerTemplateKeyIndex,
                                            AnswerFileExtensionKey = ans.AnswerFileExtensionKey,
                                            AnswerFileExtensionName = ans.AnswerFileExtensionName,
                                            AnswerFileExtensionType = ans.AnswerFileExtensionType,
                                            AnswerFileExtensionSize = ans.AnswerFileExtensionSize,
                                            AnswerFileExtensionPath = ans.AnswerFileExtensionPath,
                                            AnswerFileExtensionHTML = ans.AnswerFileExtensionHTML,
                                            AnswerFileExtensionHTMLCode = ans.AnswerFileExtensionHTMLCode,
                                            AnswerTemplateDependency = ans.AnswerTemplateDependency.ToString(),
                                            FormElement = ans.FormElement,
                                            FormElementFileExtensionKey = ans.FormElementFileExtensionKey,
                                            FormElementTypeKeyDependancy = ans.FormElementTypeKeyDependancy,
                                            FormElementFileExtensionName = ans.FormElementFileExtensionName,
                                            FormElementFileExtensionType = ans.FormElementFileExtensionType,
                                            FormElementFileExtensionSize = ans.FormElementFileExtensionSize,
                                            FormElementFileExtensionPathOrSource = ans.FormElementFileExtensionPathOrSource,
                                            FormElementFileExtensionHTML = ans.FormElementFileExtensionHTML,
                                            FormElementFileExtensionHTMLCode = ans.FormElementFileExtensionHTMLCode,
                                            FormElementTypeName = ans.FormElementTypeName,
                                            FormElementTypeFileExtensionKey = ans.FormElementTypeFileExtensionKey,
                                            FormElementTypeKey = ans.FormElementTypeKey,
                                            FormElementTypeFileExtensionName = ans.FormElementTypeFileExtensionName,
                                            FormElementTypeFileExtensionType = ans.FormElementTypeFileExtensionType,
                                            FormElementTypeFileExtensionSize = ans.FormElementTypeFileExtensionSize,
                                            FormElementTypeFileExtensionFilePathOrSource = ans.FormElementTypeFileExtensionFilePathOrSource,
                                            FormElementTypeFileExtensionHTML = ans.FormElementTypeFileExtensionHTML,
                                            FormElementTypeFileExtensionHTMLCode = ans.FormElementTypeFileExtensionHTMLCode,
                                            SqlQueries = ans.SqlQueries,
                                            SqlParameters = ans.SqlParameters,
                                            ColumnValue = ans.ColumnValue,

                                        }).ToList<DataObjects.Answer>()
                                    }).ToList<DataObjects.Question>();
                    }
                }

                ViewBag.Email = UserInfo.First().Email;
                ViewBag.PhoneNumber = UserInfo.First().PhoneNumber;
                ViewBag.UserName = UserInfo.First().UserName;
                ViewBag.FirstName = UserInfo.First().FirstName;
                ViewBag.FatherName = UserInfo.First().FatherName;
                ViewBag.LastName = UserInfo.First().LastName;
                ViewBag.FormTemplateKey = FormTemplateKey;
                ViewBag.FormTemplateTitle = FormTemplateTitle;
                ViewBag.ManagerIndex = ManagerIndex;

                string FormTemplateType = GetTypeForaFormTemplate(FormTemplateKey).Result;
                ViewBag.FormTemplateType = FormTemplateType;

                List<GetOPOrganisations_Result> OpOrganisations = new List<GetOPOrganisations_Result>();
                OpOrganisations = GetOPOrganisations(UserInformation.FirstOrDefault().RoleId, TemplateKey).Result;
                ViewBag.OpOrganisations = OpOrganisations.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();
                ViewBag.Languageidentifier = 3;

                var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == "Manbar").ToListAsync();
                ViewBag.PageKey = allComponentsInPage.FirstOrDefault().PageKey;
                int? navBarItemComponentKey = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "NavigationBar").FirstOrDefault().ComponentKey;

                var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();

                List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                        p => p.ComponentOrder,
                        p => p,
                 (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
                 {
                     ComponentOrder = p.ComponentOrder,
                     Items = p.Items
                 }).ToList();

                ViewBag.NavBarItemComponentKey = navBarItemComponentKey;
                ViewBag.NavbarData = navbarItemsGroupByOrder;
                ViewBag.PageName = "Manbar";
                //End of Navbar Part
                return View(nestedResult);
            }
          
        }

        [Globals.YourCustomAuthorize]
        public static async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }

        [Globals.YourCustomAuthorize]
        public static async Task<List<GetOPOrganisations_Result>> GetOPOrganisations(string KeyRole, string KeyTemplate)
        {

            List<GetOPOrganisations_Result> result = new List<GetOPOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/OPOrg/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("KeyRoleJ={0}", KeyRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Pipi={0}", KeyTemplate);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetOPOrganisations_Result>>();


                }
            }
            return result;
        }


        public async Task<string> GetTypeForaFormTemplate(string formTemplateKey)
        {
            string result = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetTypeForaFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }

            return result;

        }






        [HttpPost]
        public async Task<string> ChangePicture(string formKey = null, string answerTemplateKey = null)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringContent content = new StringContent(JsonConvert.SerializeObject(formKey + "&&&" + answerTemplateKey), Encoding.UTF8, "application/json");
                // HTTP POST
                HttpResponseMessage response = await client.PostAsync("api/Forms/ChangePicture", content);
                if (response.IsSuccessStatusCode)
                {
                    string data = await response.Content.ReadAsStringAsync();
                }

                try
                {
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                }
                catch(Exception e)
                {

                }
               
            }
            return result;
        }

        //[HttpPost]

        //public async Task<string> ChangePicture(string Image)
        //{
        //    string result = string.Empty;
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


        //        StringContent content = new StringContent(JsonConvert.SerializeObject(User.Identity.GetUserId() + "&&&" + Image), Encoding.UTF8, "application/json");
        //        // HTTP POST
        //        HttpResponseMessage response = await client.PostAsync("api/Forms/ChangePicture", content);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            string data = await response.Content.ReadAsStringAsync();
        //        }
        //        try
        //        {
        //            response.EnsureSuccessStatusCode();
        //            if (response.IsSuccessStatusCode)
        //            {
        //                result = await response.Content.ReadAsAsync<string>();
        //            }
        //        }
        //        catch(Exception e) {

        //        }
               
        //    }
        //    return result;
        //}

    }
}