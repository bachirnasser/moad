﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
//using MoAD.DataInterface.EDMX;

namespace MoAD.WebApplication.Controllers
{
    //[Globals.YourCustomAuthorize]
    public class FormsController : Controller
    {
        // GET: Forms
        public async Task<ActionResult> Index()
        {
            return View();
        }


        public async Task<string> CreateQuestion(string questionvalue,string answerstext, string FormElementKeys, string formid,string AnswerIndex)
        {
          

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateQuestion/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("questionvalue={0}", questionvalue);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("answerstext={0}", answerstext);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("FormElementKeys={0}", FormElementKeys);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formid={0}", formid);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("AnswerIndex={0}", AnswerIndex);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                 string result = await response.Content.ReadAsAsync<string>();
                }
            }
            //string questionId = await CreateNewQuestionTemplate(questionvalue, "", "Visible", formid);        
            return "";
        }


        public async Task<string> CreateNewFormTemplate(string name, string description, string formTemplateType)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateNewFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("name={0}", name);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("description={0}", description);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateType={0}", formTemplateType);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                     result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }


        public async Task<string> CreateNewQuestionTemplate(string value, string note, string displayMode, string formTemplateKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/CreateNewQuestionTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("value={0}", value);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("note={0}", note);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("displayMode={0}", displayMode);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("formTemplateKey={0}", formTemplateKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<string> EntidabSave(string Organizationkey, string delegatedOrgKey, string userKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/EntidabSave/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Organizationkey={0}", Organizationkey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("delegatedOrgKey={0}", delegatedOrgKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userKey={0}", userKey);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        public async Task<string> AddRole(string role)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AddRole/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("role={0}", role);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }

        


        public async Task<string> SaveForm(string FormId)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/SaveFormTemplate/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("FormId={0}", FormId);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return result;
        }



        //public async Task<string> saveNewForm(string formName, string questionValue, List<string> answersValue)
        //{
        //    //string strResult= "";
        //    //List<string> result= new List<string>();

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/saveNewForm/");

        //        //requestUriBuilder.Append(1);
        //        requestUriBuilder.Append("?");
        //        requestUriBuilder.AppendFormat("formName={0}", formName);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("questionValue={0}", questionValue);
        //        requestUriBuilder.Append("&");
        //        requestUriBuilder.AppendFormat("answersValue={0}", answersValue);


        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            String value = await response.Content.ReadAsStringAsync();


        //        }
        //    }


        //    //result.Add(strResult);

        //    return "success";
        //}

        //public async Task<string> fillFormElementTypes()
        //{
        //    string strResult= "";
        //    //List<string> result= new List<string>();

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        StringBuilder requestUriBuilder = new StringBuilder();
        //        requestUriBuilder.Append("api/Forms/fillFormElementTypes/");


        //        HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
        //        response.EnsureSuccessStatusCode();
        //        if (response.IsSuccessStatusCode)
        //        {
        //            String value = await response.Content.ReadAsStringAsync();


        //        }
        //    }


        //    //result.Add(strResult);

        //    return strResult;
        //}

    }
}