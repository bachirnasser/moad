﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]

    public class ExtendedDataController : Controller
    {
        private Entities db = new Entities();

        // GET: ExtendedData
        public async Task<ActionResult> Index(string HighlightedPart=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            return View(await db.ExtendedDatas.ToListAsync());
        }

        // GET: ExtendedData/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExtendedData extendedData = await db.ExtendedDatas.FindAsync(id);
            if (extendedData == null)
            {
                return HttpNotFound();
            }
            return View(extendedData);
        }

        // GET: ExtendedData/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExtendedData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FileExtensionKey,Name,Type,Size,CreatedAt,UpdatedAt,FilePathOrSource,CreatedBy,HTML,HTMLCode")] ExtendedData extendedData)
        {
            if (ModelState.IsValid)
            {
                db.ExtendedDatas.Add(extendedData);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(extendedData);
        }

        // GET: ExtendedData/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExtendedData extendedData = await db.ExtendedDatas.FindAsync(id);
            if (extendedData == null)
            {
                return HttpNotFound();
            }
            return View(extendedData);
        }

        // POST: ExtendedData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FileExtensionKey,Name,Type,Size,CreatedAt,UpdatedAt,FilePathOrSource,CreatedBy,HTML,HTMLCode")] ExtendedData extendedData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(extendedData).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(extendedData);
        }

        // GET: ExtendedData/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExtendedData extendedData = await db.ExtendedDatas.FindAsync(id);
            if (extendedData == null)
            {
                return HttpNotFound();
            }
            return View(extendedData);
        }

        // POST: ExtendedData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ExtendedData extendedData = await db.ExtendedDatas.FindAsync(id);
            db.ExtendedDatas.Remove(extendedData);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
