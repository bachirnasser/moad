﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using Microsoft.AspNet;


namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class AspNetUsersController : Controller
    {
        private Entities db = new Entities();

        // GET: AspNetUsers
        public async Task<ActionResult> Index(string HighlightedPart = null)
        {
            ViewBag.HighlightedPart = HighlightedPart;

            //ViewBag.HighlightedPart = "menu-mouwazafin";
            List<GetAllUsersPerOrganisationAndRole_Result> result = new List<GetAllUsersPerOrganisationAndRole_Result>();
            string Organisation = await GetOrganisatioKey();


            List<string> organisationList = new List<string>();
            organisationList.Add(Organisation);
            List<AspNetUser> users = new List<AspNetUser>();
            List<AspNetRole> roles = new List<AspNetRole>();

            if (User.IsInRole("ADDirector"))
            {
                ViewBag.UserRole = "ADDirector";

                users = db.AspNetUsers.Where(x => x.UserPerOrganisations.FirstOrDefault().OrganisationKey.ToString() == Organisation).ToList();

            }
            else if (User.IsInRole("ADManager"))
            {

                ViewBag.UserRole = "ADManager";
                users = db.AspNetUsers.Where(x => x.UserPerOrganisations.FirstOrDefault().OrganisationKey.ToString() == Organisation && x.AspNetUserRoles.Select(aspr => aspr.RoleId).FirstOrDefault()=="24").ToList();

            }
            else if (User.IsInRole("Administrator") || User.IsInRole("OPGeneralDirector"))
            {
                ViewBag.UserRole = "Admin";
                users = db.AspNetUsers.Where(x => x.AspNetUserRoles.Select(aspr => aspr.RoleId).FirstOrDefault() != "27" && x.AspNetUserRoles.Select(aspr => aspr.RoleId).FirstOrDefault() != "1").ToList();
            }
            return View(users);
            //return View(await db.AspNetUsers.ToListAsync());

        }
        public async Task<string> GetOrganisatioKey()
        {
            string UserKey = User.Identity.GetUserId();
            GetOrganisationAndRolePerUser_Result result = null;
            string OrganisationKey;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/testOrganisationKey/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("test2={0}", UserKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<GetOrganisationAndRolePerUser_Result>();
                }

            }
            OrganisationKey = result.OrganisationKey.ToString();
            return OrganisationKey;
        }

        // GET: AspNetUsers/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = await db.AspNetUsers.FindAsync(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        [Globals.YourCustomAuthorize]
        public async Task<List<GetAllOrganisations_Result>> GetAllOrganisations()
        {

            List<GetAllOrganisations_Result> result = new List<GetAllOrganisations_Result>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetAllOrganisations/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetAllOrganisations_Result>>();


                }
            }
            return result;
        }
        
        public async Task<string> DeleteRoles(string RoleId)
        {
            string Userkey = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/DeleteRole/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("RoleId={0}", RoleId);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }

        public async Task<string> AddRoles(string Role)
        {
            string Userkey = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AddRoles/");

                //requestUriBuilder.Append(1);
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Role={0}", Role);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    String value = await response.Content.ReadAsStringAsync();


                }
            }
            return "";
        }


        [Globals.YourCustomAuthorize]
        public async Task<List<Dictionary<string, string>>> GetRoles()
        {

            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetRoles/");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<Dictionary<string, string>>>();


                }
            }
            return result;
        }

        public async Task<ActionResult> DeleteRole(string HighlightedPart)
        {

            ViewBag.HighlightedPart = HighlightedPart;

            List<Dictionary<string, string>> roles = GetRoles().Result;

           

            ViewData["allRoles"] = roles.Select(role => new SelectListItem { Text = role.ToList()[0].Value, Value = role.ToList()[2].Value }).ToList();
            return View();
        }
        public async Task<ActionResult> AddRole(string HighlightedPart)
        {

            ViewBag.HighlightedPart = HighlightedPart;
            return View();
        }

        public async Task<ActionResult> EntidabMouwazaf()
        {
            List<GetAllUsersPerOrganisationAndRole_Result> result = new List<GetAllUsersPerOrganisationAndRole_Result>();
            string Organisation = await GetOrganisatioKey();
            List<GetAllOrganisations_Result> result1 = GetAllOrganisations().Result;
            ViewData["Organisation"] = result1.Select(org => new SelectListItem { Text = org.OrganisationName, Value = org.OrganisationKey.ToString() }).ToList();

            List<string> organisationList = new List<string>();
            organisationList.Add(Organisation);
            List<AspNetUser> users = new List<AspNetUser>();



            ViewBag.UserRole = "ADManager";
            users = db.AspNetUsers.Where(x => x.UserPerOrganisations.FirstOrDefault().OrganisationKey.ToString() == Organisation).ToList();
            //users = db.Organisations.Where(u => u.OrganisationKey.ToString() == Organisation).FirstOrDefault().AspNetUser.ToList();
            users = users.Where(x => x.AspNetUserRoles.Select(aspr=>aspr.RoleId).FirstOrDefault() == "24").ToList();
            ViewBag.CurrentOrgKey = Organisation;

            return View(users);
        }


        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,FatherName,LastName,SSN")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUser);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(aspNetUser);
        }

        // GET: AspNetUsers/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = await db.AspNetUsers.FindAsync(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,FatherName,LastName,SSN")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetUser).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = await db.AspNetUsers.FindAsync(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = await db.AspNetUsers.FindAsync(id);
            db.AspNetUsers.Remove(aspNetUser);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
