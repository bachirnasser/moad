﻿using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using MoAD.WebApplication.Models;


namespace MoAD.WebApplication.Controllers
{
    public class QuickNavController : Controller
    {
        // GET: QuickNav
        public ActionResult  Index()
        {
            Models.FormElement f = new Models.FormElement();
            f.listOfCategories= GetFormElements("1").Result;
           
            return View(f);
        }

        public async Task<List<GetFormElements_Result>> GetFormElements(string category)
        {
            List<GetFormElements_Result> listOfCategories = new List<GetFormElements_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFormElements/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("CategoryKey={0}", category);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    listOfCategories = await response.Content.ReadAsAsync<List<GetFormElements_Result>>();
                }
            }

            return listOfCategories;
        }



        public ActionResult CitizenNav()
        {          
            return View();
        }
    }
}