﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
    //[Globals.YourCustomAuthorize]
    public class PBDashboardController : Controller
    {
        // GET: PBDashbord
        public ActionResult Index()
        {
            //Report 1/////////////
            List<PBReportingTopInactiveOrganisationsWithClaims_Result> Report1 = new List<PBReportingTopInactiveOrganisationsWithClaims_Result>();
            Report1 = PBReportingTopInactiveOrganisationsWithClaims().Result;

            List<object> Report1Json = new List<object>();
            foreach (var item in Report1)
            {
                Dictionary<string, string> ItemDict = new Dictionary<string, string>();
                ItemDict.Add("year", item.OrganisationName);
                ItemDict.Add("income", item.CountOfClaims.ToString());
                ItemDict.Add("expenses", item.CountOfTreatedClaim.ToString());
                Report1Json.Add(ItemDict);
            }

            //End Report 1/////////////


            //Report 2/////////////

            List<PBReportingCountCorruption_Result> Report2 = new List<PBReportingCountCorruption_Result>();
            Report2 = PBReportingCountCorruption().Result;
            string[] color = { "#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01", "#B0DE09", "#04D215", "#0D8ECF", "#0D52D1", "#2A0CD0" };



            List<object> Report2Json = new List<object>();
            int colorIndex = 0;

            foreach (var item2 in Report2)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("country", item2.OrganisationName);
                ItemDict.Add("visits", item2.CountCorruption);
                ItemDict.Add("color", color[colorIndex]);
                colorIndex++;
                Report2Json.Add(ItemDict);
            }

            //End Report 2/////////////


            //Report 3/////////////

            List<PBReportingCountNoncompliance_Result> Report3 = new List<PBReportingCountNoncompliance_Result>();
            Report3 = PBReportingCountNoncompliance().Result;



            List<object> Report3Json = new List<object>();
            
            foreach (var item3 in Report3)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item3.OrganisationName);
                ItemDict.Add("income", item3.CountCorruption);
                
               
                Report3Json.Add(ItemDict);
            }

            //End Report 3/////////////


            //Report 4/////////////

            List<PBReportingCountClaimsPerOrganisation_Result> Report4 = new List<PBReportingCountClaimsPerOrganisation_Result>();
            Report4 = PBReportingCountClaimsPerOrganisation().Result;



            List<object> Report4Json = new List<object>();
            

            foreach (var item4 in Report4)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("country", item4.OrganisationName);
                ItemDict.Add("value", item4.CountClaims);


                Report4Json.Add(ItemDict);
            }

            //End Report 4/////////////

            //Report 5/////////////

            List<PBRanking_Result> Report5 = new List<PBRanking_Result>();
            Report5 = PBRanking().Result;



            List<object> Report5Json = new List<object>();


            foreach (var item5 in Report5)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item5.Organisation);
                ItemDict.Add("income", item5.Score);


                Report5Json.Add(ItemDict);
            }

            //End Report 5/////////////

            //Report 6/////////////

            List<GetMostActiveOrganisations_Result> Report6 = new List<GetMostActiveOrganisations_Result>();
            Report6 = GetMostActiveOrganisations().Result;



            List<object> Report6Json = new List<object>();


            foreach (var item6 in Report6)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item6.OrganisationName);
                ItemDict.Add("income", item6.CountEvaluatedClaims);


                Report6Json.Add(ItemDict);
            }

            //End Report 6/////////////

            //Report 7/////////////

            List<PBReportingSurveyAnswers_Result> Report = new List<PBReportingSurveyAnswers_Result>();
            Report = PBReportingSurveyAnswers().Result;



            List<object> Report7Json = new List<object>();


            foreach (var item in Report)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("country", item.Answer);
                ItemDict.Add("litres", item.CountVoters);


                Report7Json.Add(ItemDict);
            }

            //End Report 7/////////////


            //Report 8/////////////

            List<PBMonitoringReport_Result> Report8 = new List<PBMonitoringReport_Result>();
            Report8 = PBMonitoringReport().Result;



            List<object> Report8Json = new List<object>();


            foreach (var item8 in Report8.Where(x=>x.SystemUsing== "EachMonth"))
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item8.UserName);
                ItemDict.Add("income", item8.CountActivities);
                


                Report8Json.Add(ItemDict);
            }

            //End Report 8/////////////

            //Report 9/////////////

            List<PBMonitoringReport_Result> Report9 = new List<PBMonitoringReport_Result>();
            Report9 = PBMonitoringReport().Result;



            List<object> Report9Json = new List<object>();


            foreach (var item9 in Report9.Where(x => x.SystemUsing == "Each2Month"))
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item9.UserName);
                ItemDict.Add("income", item9.CountActivities);



                Report9Json.Add(ItemDict);
            }

            //End Report 9/////////////



            //Report 10/////////////

            List<PBMonitoringReport_Result> Report10 = new List<PBMonitoringReport_Result>();
            Report10 = PBMonitoringReport().Result;



            List<object> Report10Json = new List<object>();


            foreach (var item10 in Report10.Where(x => x.SystemUsing == "EachWeek"))
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item10.UserName);
                ItemDict.Add("income", item10.CountActivities);



                Report10Json.Add(ItemDict);
            }

            //End Report 10/////////////

            //Report 11/////////////

            List<PBReportingCountEmployeeSatisfaction_Result> Report11 = new List<PBReportingCountEmployeeSatisfaction_Result>();
            Report11 = PBReportingCountEmployeeSatisfaction().Result;



            List<object> Report11Json = new List<object>();


            foreach (var item11 in Report11)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("country", item11.OrganisationName);
                ItemDict.Add("litres", item11.Count);



                Report11Json.Add(ItemDict);
            }

            //End Report 11/////////////

            //Report 12 + 13/////////////

            List<ReportingGetSatisfactionPerSlice_Result> Report12 = new List<ReportingGetSatisfactionPerSlice_Result>();
            Report12 = ReportingGetSatisfactionPerSlice().Result;


            //End Report 12 + 13/////////////


            //Report 14/////////////

            List<ReportingGetWeakQuestionsPerSlice_Result> Report14 = new List<ReportingGetWeakQuestionsPerSlice_Result>();
            Report14 = ReportingGetWeakQuestionsPerSlice(null,null,"3", "راضي جداً ").Result;


            //End Report 14/////////////

            //Report 15/////////////

            List<ReportingGetWeakQuestionsPerSlice_Result> Report15 = new List<ReportingGetWeakQuestionsPerSlice_Result>();
            Report15 = ReportingGetWeakQuestionsPerSlice(null, null, "1", "غير راضي أبداً ").Result;


            //End Report 15/////////////
            try
            {
                ViewBag.Report1 = Report1Json;
                ViewBag.Report2 = Report2Json;
                ViewBag.Report3 = Report3Json;
                ViewBag.Report4 = Report4Json;
                ViewBag.Report5 = Report5Json;
                ViewBag.Report6 = Report6Json;
                ViewBag.Report7 = Report7Json;
                ViewBag.Report8 = Report8Json;
                ViewBag.Report9 = Report9Json;
                ViewBag.Report10 = Report10Json;
                ViewBag.Report11 = Report11Json;
                ViewBag.Report12 = Report12.FirstOrDefault().CountEmployees;
                ViewBag.Report13 = Report12.FirstOrDefault().AVRSatisfactionGeneral;
                ViewBag.Report14 = Report14;
                ViewBag.Report15 = Report15.FirstOrDefault().WeakQuestion;
            }
           catch(Exception ex)
            {

            }


            return View();
        }


        public async Task<List<ReportingGetWeakQuestionsPerSlice_Result>> ReportingGetWeakQuestionsPerSlice(string SliceLevel = null, string OrganisationKey = null, string COUNT = null, string QuestionWeakness = null)
        {
            List<ReportingGetWeakQuestionsPerSlice_Result> result = new List<ReportingGetWeakQuestionsPerSlice_Result>();
            List<string> ClaimsType = new List<string>();
            List<int?> ClaimsCount = new List<int?>();

            List<object> ReportingClaimsType = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetWeakQuestionsPerSlice/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("SliceLevel={0}", "NULL");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IdOrganKey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("IdOrgParent={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Prvce={0}", "null"); 
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("COUNT={0}", COUNT);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("QuestionWeakness={0}", QuestionWeakness);
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetWeakQuestionsPerSlice_Result>>();

                    //ClaimsType = result.Select(x => x.Value).ToList();
                    //ClaimsCount = result.Select(x => x.Evaluations).ToList();
                }
            }
            //ReportingClaimsType.Add(ClaimsType);
            //ReportingClaimsType.Add(ClaimsCount);

            return result;
        }


        public async Task<List<ReportingGetSatisfactionPerSlice_Result>> ReportingGetSatisfactionPerSlice(string SliceLevel = null, string OrganisationKey = null, string Column1 = null, string Column2 = null)
        {
            List<ReportingGetSatisfactionPerSlice_Result> result = new List<ReportingGetSatisfactionPerSlice_Result>();
            List<string> Name = new List<string>();
            List<double?> Count = new List<double?>();

            List<object> PieChartResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/ReportingGetSatisfactionPerSlice/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("sl={0}", "NULL");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("OGkey={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PTorg={0}", "null");
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Pv={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<ReportingGetSatisfactionPerSlice_Result>>();

                    //Name = result.Select(x => x.SliceName).ToList();
                    //Count = result.Select(x => x.AVRSatisfactionGeneral).ToList();
                }
            }

            List<List<string>> generalData = new List<List<string>>();
            if (result.Count == 0)
            {
                List<string> data = new List<string>();
                data.Add("لا يوجد معلومات ");
                data.Add("100");
                generalData.Add(data);
            }
            foreach (var item in result)
            {
                List<string> data = new List<string>();

                data.Add(item.SliceName);
                data.Add(item.AVRSatisfactionGeneral.ToString());

                generalData.Add(data);
            }

            //PieChartResult.Add(Name);
            //PieChartResult.Add(Count);


            if (Column1 != null)
            {
                return result;

            }
            else
            {
                return result;


            }
        }


        public async Task<ActionResult> PBMenu()
        {

            List<AccessPivilege_Result> AccessPivilegeResult = AccessPivilege(null).Result;

            ViewBag.AccessPivilege = AccessPivilegeResult;
            return PartialView();
        }


        public async Task<ActionResult> PBMenu1(string ParentPrivilege=null)
        {

            //List<AccessPivilege_Result> AccessPivilegeResult = AccessPivilege(ParentPrivilege).Result;

            List<PrivelegesperRole_Result> result = PrivilegePerRole("3").Result;
           
            Dictionary<string, object> MenuResult = new Dictionary<string, object>();
           
            List<PrivelegesperRole_Result> InitialMenuResult = result.Where(x => x.PriviligeParent == "ALL").ToList();

            object[] Menus = new object[InitialMenuResult.Count];
            int i = 0;
            foreach (var item in InitialMenuResult)
            {
                Dictionary<string, object> MenuItem = new Dictionary<string, object>();

                if (item.PositionType == "Parent")
                {
                    
                    MenuItem.Add("title", item.PrivilegeNameEnglish);
                    MenuItem.Add("icon", item.HTMLCode);
                    List<PrivelegesperRole_Result> SubMenuResult = result.Where(x => x.PriviligeParent == item.PrivilegeName.Trim()).ToList();
                    object[] SubMenus = new object[SubMenuResult.Count];
           
                    SubMenus = FillSubMenu(SubMenuResult,result).Result;
                    MenuItem.Add("menus", SubMenus);
                    Menus[i] = (MenuItem);

                }
                else
                {
                   // object text = new object();
                   // text = "function () {alert('click event callback');}";
                    MenuItem.Add("title", item.PrivilegeNameEnglish);
                    MenuItem.Add("icon", item.HTMLCode);
                    MenuItem.Add("click", item.HTML);
                   
                    Menus[i] = (MenuItem);
                }

                
                i++;
            }

            MenuResult.Add("menus", Menus);

            List<PrivelegesperRole_Result> result1 = PrivilegePerRole("3").Result;
            ViewBag.AccessPivilege = MenuResult;
            ViewBag.MaxLevel = result1.FirstOrDefault().Level;
            return PartialView();
        }

        public async Task<object[]> FillSubMenu(List<PrivelegesperRole_Result> SubMenuResult, List<PrivelegesperRole_Result> result)
        {
            object[] SubMenus = new object[SubMenuResult.Count];
            int j = 0;
            foreach (var submenuItem in SubMenuResult)
            {
                Dictionary<string, object> subMenuItem = new Dictionary<string, object>();


                if (submenuItem.PositionType == "Parent")
                {
                    subMenuItem.Add("title", submenuItem.PrivilegeNameEnglish);
                   // subMenuItem.Add("icon", submenuItem.HTMLCode);
                    List<PrivelegesperRole_Result> SubMenuResult1 = result.Where(x => x.PriviligeParent.Trim() == submenuItem.PrivilegeName.Trim()).ToList();
                    object[] SubMenus1 = new object[SubMenuResult.Count];
                    SubMenus1 = FillSubMenu(SubMenuResult1, result).Result;
                    subMenuItem.Add("menus", SubMenus1);
                    SubMenus[j] = (subMenuItem);
                }
                else
                {
                    subMenuItem.Add("title", submenuItem.PrivilegeNameEnglish);

                    subMenuItem.Add("click", submenuItem.HTML);
                    

                    SubMenus[j] = (subMenuItem);
                }
                j++;
            }


            return SubMenus;
        }
        

        [HttpGet]
        public async Task<List<PrivelegesperRole_Result>> PrivilegePerRole(string Key = null)
        {
            List<PrivelegesperRole_Result> result = new List<PrivelegesperRole_Result>();
          
            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PrivilegePerRole/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Key={0}", Key);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PositionType={0}", "Menu");


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PrivelegesperRole_Result>>();
                }
            }

            return result;
        }

        [HttpGet]
        public async Task<JsonResult> PBMonitoringReportJson(string SystemUsing=null)
        {
            List<PBMonitoringReport_Result> result = new List<PBMonitoringReport_Result>();

            result = PBMonitoringReport().Result.Where(x=>x.SystemUsing== SystemUsing).ToList();
            List<PBMonitoringReport_Result> Report9 = new List<PBMonitoringReport_Result>();
           



            List<object> Report9Json = new List<object>();


            foreach (var item9 in result)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item9.UserName);
                ItemDict.Add("income", item9.CountActivities);



                Report9Json.Add(ItemDict);
            }
            return Json(Report9Json, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public async Task<ActionResult> PBMonitoringReportPartial(string SystemUsing = "EachMonth")
        {
            List<PBMonitoringReport_Result> result = new List<PBMonitoringReport_Result>();

            result = PBMonitoringReport().Result.Where(x => x.SystemUsing == SystemUsing).ToList();
            List<PBMonitoringReport_Result> Report9 = new List<PBMonitoringReport_Result>();




            List<object> Report9Json = new List<object>();


            foreach (var item9 in result)
            {
                Dictionary<string, object> ItemDict = new Dictionary<string, object>();
                ItemDict.Add("year", item9.UserName);
                ItemDict.Add("income", item9.CountActivities);



                Report9Json.Add(ItemDict);
            }

            ViewBag.Report8 = Report9Json;
            return PartialView();
        }


        [HttpGet]
        public async Task<List<PBMonitoringReport_Result>> PBMonitoringReport()
        {
            List<PBMonitoringReport_Result> result = new List<PBMonitoringReport_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBMonitoringReport/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("x={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBMonitoringReport_Result>>();
                }
            }

            return result;
        }


        [HttpGet]
        public async Task<List<PBReportingCountEmployeeSatisfaction_Result>> PBReportingCountEmployeeSatisfaction()
        {
            List<PBReportingCountEmployeeSatisfaction_Result> result = new List<PBReportingCountEmployeeSatisfaction_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingCountEmployeeSatisfaction/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("z={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingCountEmployeeSatisfaction_Result>>();
                }
            }

            return result;
        }
        

        [HttpGet]
        public async Task<List<PBReportingTopInactiveOrganisationsWithClaims_Result>> PBReportingTopInactiveOrganisationsWithClaims()
        {
            List<PBReportingTopInactiveOrganisationsWithClaims_Result> result = new List<PBReportingTopInactiveOrganisationsWithClaims_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingTopInactiveOrganisationsWithClaims/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("f={0}", "d");
               


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingTopInactiveOrganisationsWithClaims_Result>>();
                }
            }

            return result;
        }
        

        [HttpGet]
        public async Task<List<PBReportingSurveyAnswers_Result>> PBReportingSurveyAnswers()
        {
            List<PBReportingSurveyAnswers_Result> result = new List<PBReportingSurveyAnswers_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingSurveyAnswers/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("t={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingSurveyAnswers_Result>>();
                }
            }

            return result;
        }

        [HttpGet]
        public async Task<List<GetMostActiveOrganisations_Result>> GetMostActiveOrganisations()
        {
            List<GetMostActiveOrganisations_Result> result = new List<GetMostActiveOrganisations_Result>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetMostActiveOrganisations/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("oups={0}", "105");
                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetMostActiveOrganisations_Result>>();
                    ViewBag.Top10Result = result;
                    ViewBag.Top10ResultCount = result.Count;
                }
            }

            return result;
        }
        [HttpGet]
        public async Task<List<PBRanking_Result>> PBRanking()
        {
            List<PBRanking_Result> result = new List<PBRanking_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBRanking/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("j={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBRanking_Result>>();
                }
            }

            return result;
        }
        
        [HttpGet]
        public async Task<List<PBReportingCountClaimsPerOrganisation_Result>> PBReportingCountClaimsPerOrganisation()
        {
            List<PBReportingCountClaimsPerOrganisation_Result> result = new List<PBReportingCountClaimsPerOrganisation_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingCountClaimsPerOrganisation/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("l={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingCountClaimsPerOrganisation_Result>>();
                }
            }

            return result;
        }

        
        [HttpGet]
        public async Task<List<PBReportingCountNoncompliance_Result>> PBReportingCountNoncompliance()
        {
            List<PBReportingCountNoncompliance_Result> result = new List<PBReportingCountNoncompliance_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingCountNoncompliance/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("c={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingCountNoncompliance_Result>>();
                }
            }

            return result;
        }
        
        [HttpGet]
        public async Task<List<PBReportingCountCorruption_Result>> PBReportingCountCorruption()
        {
            List<PBReportingCountCorruption_Result> result = new List<PBReportingCountCorruption_Result>();

            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PBReportingCountCorruption/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("p={0}", "d");



                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PBReportingCountCorruption_Result>>();
                }
            }

            return result;
        }
        
        [HttpGet]
        public async Task<JsonResult> GetChildrenAccessPivilege(string ParentPrivilege = null)
        {
            List<AccessPivilege_Result> result = new List<AccessPivilege_Result>();
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            string userRole = UserInfo.FirstOrDefault().RoleId;
            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessPivilege/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userRole={0}", userRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ParentId={0}", ParentPrivilege);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Position={0}", "Right");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessPivilege_Result>>();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }


        public async Task<List<AccessPivilege_Result>> AccessPivilege(string ParentPrivilege = null)
        {
            List<AccessPivilege_Result> result = new List<AccessPivilege_Result>();
            List<GetUserInformation_Result> UserInfo = GetUserInformation(User.Identity.GetUserId()).Result;
            string userRole = UserInfo.FirstOrDefault().RoleId;
            List<object> nestedResult = new List<object>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/AccessPivilege/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userRole={0}", userRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("ParentId={0}", ParentPrivilege);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("Position={0}", "Right");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<AccessPivilege_Result>>();
                }
            }

            return result;
        }

    }
}