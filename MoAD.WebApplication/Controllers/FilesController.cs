﻿using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Controllers
{
    public class FilesController : Controller
    {
        private Entities db = new Entities();
        public async System.Threading.Tasks.Task<ActionResult> GetFilePathFromExtendedData(string extensionFileKey,string resizeWidth=null, string resizeHeight = null)
        {
            if (extensionFileKey == null)
            {
                return HttpNotFound();
            }

            string url = "";
            try
            {
                var extendedData = await db.ExtendedDatas.Where(e => e.ColumnValue == extensionFileKey).FirstOrDefaultAsync();

                if (extendedData != null)
                {
                  
                    if (extendedData.Name != null)
                    {
                        url = extendedData.FilePathOrSource + extendedData.Name;
                    }
                    else
                    {
                        url = extendedData.FilePathOrSource + "Empty.png";
                    }
                   
                }
                else
                {
                    ExtendedData newExtendedData = new ExtendedData();
                    newExtendedData.Type = "Image";
                    newExtendedData.CreatedAt = DateTime.Now.ToString();
                    newExtendedData.FilePathOrSource = Globals.Configuration.UPLOADS_CMS_FOLDER;
                    newExtendedData.ColumnValue = extensionFileKey;

                    db.ExtendedDatas.Add(newExtendedData);
                    await db.SaveChangesAsync();

                    url = newExtendedData.FilePathOrSource + "Empty.png";
                }

                if (resizeWidth != null)
                {
                    int resizeWidthInt = int.Parse(resizeWidth);
                    string fileNameResized = Globals.Configuration.UPLOADS_CMS_FOLDER + resizeWidthInt + "-" + extendedData.Name;

                    if (System.IO.File.Exists(fileNameResized))
                    {
                        return File(fileNameResized, "image/png");
                    }
                }
                //Image Resizing
                try
                {
                    Image img = Image.FromFile(url);
                    int resizeWidthInt = int.Parse(resizeWidth);
                    if (resizeWidth !=null && img.Width >= resizeWidthInt)
                    {
                        int? resizeHeightInt = null;
                        if (resizeHeight != null)
                        {
                            resizeHeightInt = int.Parse(resizeHeight);
                        }
                        Bitmap imgbitmap = new Bitmap(img);
                        Image resizedImage = Managers.Utilities.resizeImage(imgbitmap, resizeWidthInt, resizeHeightInt);
                        string fileNameResized = Globals.Configuration.UPLOADS_CMS_FOLDER + resizeWidthInt + "-" + extendedData.Name;
                        
                        resizedImage.Save(fileNameResized, resizedImage.RawFormat);

                        return File(fileNameResized, "image/png");
                    }
                  
                }
                catch (Exception e)
                {

                }
            }
            catch (Exception e)
            {

            }

          

            return File(url, "image/png");
        }

        public async System.Threading.Tasks.Task<ActionResult> GetPdfPathFromExtendedData(string extensionFileKey)
        {
            if (extensionFileKey == null)
            {
                return HttpNotFound();
            }

            string url = "";
            try
            {
                var extendedData = db.ExtendedDatas.Where(e => e.ColumnValue == extensionFileKey).FirstOrDefault();

                if (extendedData != null)
                {
                    if (extendedData.Name != null)
                    {
                        url = extendedData.FilePathOrSource + extendedData.Name;
                    }
                    else
                    {
                        url = extendedData.FilePathOrSource + "Empty.png";
                    }
                }
                else
                {
                    ExtendedData newExtendedData = new ExtendedData();
                    newExtendedData.Type = "Pdf";
                    newExtendedData.CreatedAt = DateTime.Now.ToString();
                    newExtendedData.FilePathOrSource = Globals.Configuration.UPLOADS_CMS_PDF_FOLDER;
                    newExtendedData.ColumnValue = extensionFileKey;

                    db.ExtendedDatas.Add(newExtendedData);
                    await db.SaveChangesAsync();

                    url = newExtendedData.FilePathOrSource + "Empty.png";
                }
            }
            catch (Exception e)
            {

            }

            // return url;
            return File(url, "application/pdf");
        }

        public async System.Threading.Tasks.Task<ActionResult> GetPdfPathFromGuid(string GuidKey,string pageName=null)
        {
            if (GuidKey == null)
            {
                return HttpNotFound();
            }

            string url = "";
            try
            {
                PageComponentsHierarchy pageComponentsHierarchy = Managers.Utilities.GetFilePathFromGuid(GuidKey);

                RedirectionsType redirectionsType = new RedirectionsType();
                redirectionsType = db.RedirectionsTypes.Where(r => r.RedirectionTypeKey == pageComponentsHierarchy.RedirectionTypeKey).FirstOrDefault();

                if (redirectionsType.RedirectionType == "File")
                {
                    var extendedData = db.ExtendedDatas.Where(e => e.FileExtensionKey == pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity).FirstOrDefault();
                    url = extendedData.FilePathOrSource + extendedData.Name;
                    return File(url, "application/pdf");
                }
                else
                {
                    //string domain = string.Format("{0}://{1}{2}/", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    //var lessonUrl = domain + "Template/Index?id=" + pageComponentsHierarchy.RedirectionEntity;
                    PageComponentsHierarchy pageComponentsHierarchy1 = await db.PageComponentsHierarchies.Where(p => p.PageComponentHierarchyKey == pageComponentsHierarchy.PageComponentHierarchyKey).FirstOrDefaultAsync();
                    return RedirectToAction("Index","Template",new { id= pageComponentsHierarchy1.PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity,pageName=pageName });
                }
            }
            catch (Exception e)
            {

            }

            // return url;
            return RedirectToAction("Index","Home");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> CheckRedirection(string g_key)
        {
            if (g_key == null)
            {
                return HttpNotFound();
            }

            string url = "";
            try
            {
                PageComponentsHierarchy pageComponentsHierarchy = Managers.Utilities.GetFilePathFromGuid(g_key);

                RedirectionsType redirectionsType = new RedirectionsType();
                redirectionsType = db.RedirectionsTypes.Where(r => r.RedirectionTypeKey == pageComponentsHierarchy.RedirectionTypeKey).FirstOrDefault();

                if (redirectionsType.RedirectionType == "File")
                {
                    var extendedData = db.ExtendedDatas.Where(e => e.FileExtensionKey == pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity).FirstOrDefault();
                    url = extendedData.FilePathOrSource + extendedData.Name;
                    return Json("True");
                }
                else
                {
                    //string domain = string.Format("{0}://{1}{2}/", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    //var lessonUrl = domain + "Template/Index?id=" + pageComponentsHierarchy.RedirectionEntity;

                    return Json("True");
                }
            }
            catch (Exception e)
            {

            }

            // return url;
            return Json("False");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}