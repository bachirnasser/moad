﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Controllers
{
    [Globals.YourCustomAuthorize]
    public class FormElementTypesController : Controller
    {
        private Entities db = new Entities();

        // GET: FormElementTypes
        public async Task<ActionResult> Index(string HighlightedPart=null)
        {
            ViewBag.HighlightedPart = HighlightedPart;
            return View(await db.FormElementTypes.ToListAsync());
        }

        // GET: FormElementTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormElementType formElementType = await db.FormElementTypes.FindAsync(id);
            if (formElementType == null)
            {
                return HttpNotFound();
            }
            return View(formElementType);
        }

        // GET: FormElementTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FormElementTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,FileExtensionKey,FormElementTypeKey")] FormElementType formElementType)
        {
            if (ModelState.IsValid)
            {
                db.FormElementTypes.Add(formElementType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(formElementType);
        }

        // GET: FormElementTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormElementType formElementType = await db.FormElementTypes.FindAsync(id);
            if (formElementType == null)
            {
                return HttpNotFound();
            }
            return View(formElementType);
        }

        // POST: FormElementTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Name,FileExtensionKey,FormElementTypeKey")] FormElementType formElementType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formElementType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(formElementType);
        }

        // GET: FormElementTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormElementType formElementType = await db.FormElementTypes.FindAsync(id);
            if (formElementType == null)
            {
                return HttpNotFound();
            }
            return View(formElementType);
        }

        // POST: FormElementTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FormElementType formElementType = await db.FormElementTypes.FindAsync(id);
            db.FormElementTypes.Remove(formElementType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
