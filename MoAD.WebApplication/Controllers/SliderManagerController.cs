﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Objects;
using MoAD.WebApplication.Models;
using System.IO;
using MoAD.WebApplication.Managers;
using System.Web.Script.Serialization;

namespace MoAD.WebApplication.Controllers
{
    public class SliderManagerController : BaseController
    {
        private Entities db = new Entities();
        // GET: SliderManager
        public ActionResult Index()
        {
            return View();
        }
        public async Task<int?> CreateSliderComponentForPage(int? pageKey)
        {
            PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
            try
            {
                Component component = new Component();
                component.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName("Slider");
                db.Components.Add(component);
                await db.SaveChangesAsync();

                pageComponentsHierarchy.PageKey = pageKey;
                pageComponentsHierarchy.ComponentKey = component.ComponentKey;
                pageComponentsHierarchy.ComponentOrder = 1;
                pageComponentsHierarchy.ComponentIndex = 1;
                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return 400;
            }

            return pageComponentsHierarchy.ComponentKey;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AdjustSliderComponents(string components,string sliderType)
        {
            try
            {
                string pageName = "";
                JavaScriptSerializer js = new JavaScriptSerializer();
                List<CreateItemSliderViewModel> componentsLst = (List<CreateItemSliderViewModel>)js.Deserialize(components, typeof(List<CreateItemSliderViewModel>));
               
                foreach (var component in componentsLst)
                {
                pageName = await CreateSliderItem(component,sliderType);
                }
                //get pageName 
                var pageKey = componentsLst.FirstOrDefault().PageKey;
                 pageName = (await db.Pages.FindAsync(pageKey)).PageName;
                if(sliderType == "slider2") return await ReturnSliderPartial(pageName,"slider2");
                return await ReturnSliderPartial(pageName);
                
            }
            catch (Exception e)
            {
                return Json(400);
            }


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> CreateSliderItem(CreateItemSliderViewModel createItemSliderModel,string sliderType="")
        {
            string pageName = (await db.Pages.FindAsync(createItemSliderModel.PageKey)).PageName;
            List<NavBarViewModel> navbarItemsGroupByOrder = new List<NavBarViewModel>();
            List<PageComponentsHierarchy> navbarItems = new List<PageComponentsHierarchy>();
            if (ModelState.IsValid)
            {
                var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
                PageComponentsHierarchy sliderComponent;
                if(sliderType == "slider1")
                {
                sliderComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
                }
                else
                {
                    sliderComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault();
                }
                int? navBarItemComponentKey = null;
                if (sliderComponent != null)
                {
                    navBarItemComponentKey = sliderComponent.ComponentKey;
                }
                else
                {
                    navBarItemComponentKey = await CreateSliderComponentForPage(createItemSliderModel.PageKey);
                }
                navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();
                Component newComponent = new Component();
                string componentSourceEntityKey = "";
                if (createItemSliderModel.ComponentType == "Text")
                {
                    Guid newGuid = await GetNewGuidForDataTranslation(createItemSliderModel.ItemName, pageName);
                    componentSourceEntityKey = newGuid.ToString();
                }
                else
                {
                    componentSourceEntityKey = createItemSliderModel.ItemTypeKey.ToString();
                }

                newComponent.ComponentTypeKey = await GetComponentTypeKeyByComponentTypeName(createItemSliderModel.ComponentType);
                newComponent.ComponentSourceEntityKey = componentSourceEntityKey;
                db.Components.Add(newComponent);
                await db.SaveChangesAsync();

                var slideExists = navbarItems.Where(s => s.ComponentOrder == createItemSliderModel.ComponentOrder && s.ComponentIndex == createItemSliderModel.ComponentIndex).FirstOrDefault();
                if (slideExists != null)
                {
                    int componentOrder = (int)slideExists.ComponentOrder;
                    int parentComponentKey = (int)slideExists.ParentComponentKey;
                    int deleteExistingSlideResponse = await DeleteExistingSlide(componentOrder, parentComponentKey);
                    if (deleteExistingSlideResponse == 400)
                    {
                        return HttpNotFound().ToString();
                    }
                }

                //int? componentOrder = navbarItems.Max(n => n.ComponentOrder) + 1;
                PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                pageComponentsHierarchy.PageKey = createItemSliderModel.PageKey;
                pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                pageComponentsHierarchy.ParentComponentKey = createItemSliderModel.NavBarComponentKey != null ? createItemSliderModel.NavBarComponentKey : navBarItemComponentKey;
                pageComponentsHierarchy.ComponentLevel = createItemSliderModel.ComponentLevel;//Cause it is an item
                pageComponentsHierarchy.ComponentOrder = createItemSliderModel.ComponentOrder==null ?1: createItemSliderModel.ComponentOrder;// componentOrder == null ? 1 : componentOrder;
                pageComponentsHierarchy.ComponentIndex = createItemSliderModel.ComponentIndex;
                int? redirectionTypeKey = null;
                if (createItemSliderModel.ItemType == "Pdf")
                {
                    redirectionTypeKey = (await db.RedirectionsTypes.Where(r => r.RedirectionType == "File").FirstOrDefaultAsync()).RedirectionTypeKey;
                }
                else if (createItemSliderModel.ItemType == "Page")
                {
                    redirectionTypeKey = (await db.RedirectionsTypes.Where(r => r.RedirectionType == "Page").FirstOrDefaultAsync()).RedirectionTypeKey;
                }
                pageComponentsHierarchy.RedirectionTypeKey = redirectionTypeKey;

                bool createRediretion = false;
                if (createItemSliderModel.ItemType != null)
                {
                    createRediretion = await Utilities.CreateRedirectionForComponent((int)pageComponentsHierarchy.ComponentKey, (int)createItemSliderModel.ItemTypeKey, CurrentLanguageIdentifier);
                }
                //pageComponentsHierarchy.RedirectionEntity = createItemSliderModel.ItemTypeKey;
                db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                await db.SaveChangesAsync();
            }

            navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentIndex).GroupBy(
                   p => p.ComponentOrder,
                   p => p,
            (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
            {
                ComponentOrder = p.ComponentOrder,
                Items = p.Items
            }).ToList();

            return pageName;
            // return PartialView("_NavbarMenu", new ViewDataDictionary { { "NavbarData", navbarItemsGroupByOrder }, { "LanguageIdentifier", CurrentLanguageIdentifier } });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CheckIfSlideExists(int pageKey,int componentOrder,int navbarComponentKey)
        {
            List<PageComponentsHierarchy> navbarItems = new List<PageComponentsHierarchy>();
            string pageName = (await db.Pages.FindAsync(pageKey)).PageName;
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();
           

                var navBarItemComponentKey = navbarComponentKey;
                navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();
                var slideExists = navbarItems.Where(s => s.ComponentOrder == componentOrder).FirstOrDefault();
                if (slideExists == null) return Json(400);

            return Json(200);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteSlide(int componentOrder, int parentItemKey,string type="")
        {
            try
            {
                List<PageComponentsHierarchy> pageComponentsHierarchies = new List<PageComponentsHierarchy>();
                pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == parentItemKey && p.ComponentOrder == componentOrder).ToListAsync();
                int?  pageKey = pageComponentsHierarchies.FirstOrDefault().PageKey;
               string  pageName = (await db.Pages.FindAsync(pageKey)).PageName;
                
                foreach (PageComponentsHierarchy pageComponentsHierarchy in pageComponentsHierarchies)
                {
                    int? componentKey = pageComponentsHierarchy.ComponentKey;
                    string redirectionKey = null;
                    //if (pageComponentsHierarchy.Component.ComponentType.ComponentType1 == "Image")
                    //{
                        redirectionKey =pageComponentsHierarchy.Component.ComponentSourceEntityKey;
                //}
                //    else
                //{
                //    redirectionKey = pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity;

                //}
                string redirectionType = "";
                    if (pageComponentsHierarchy.RedirectionsType != null)
                    {
                        redirectionType = pageComponentsHierarchy.RedirectionsType.RedirectionType;
                    }
                    

                    db.PageComponentsHierarchies.Remove(pageComponentsHierarchy);
                    await db.SaveChangesAsync();

                    if (redirectionType == "File")
                    {
                        await DeleteExtendedData(int.Parse(redirectionKey));
                    }

                    if (componentKey != null)
                    {
                        await DeleteComponent(componentKey);
                    }
                }
                if(type == "slider2") return await ReturnSliderPartial(pageName,"slider2");
                return await ReturnSliderPartial(pageName);
            }
            catch (Exception e)
            {
                return Json(400, JsonRequestBehavior.AllowGet);
            }

           
        }
        public async Task<int> DeleteExistingSlide(int componentOrder, int parentItemKey)
        {
            try
            {
                List<PageComponentsHierarchy> pageComponentsHierarchies = new List<PageComponentsHierarchy>();
                pageComponentsHierarchies = await db.PageComponentsHierarchies.Where(p => p.ParentComponentKey == parentItemKey && p.ComponentOrder == componentOrder).ToListAsync();
                int? pageKey = pageComponentsHierarchies.FirstOrDefault().PageKey;
                string pageName = (await db.Pages.FindAsync(pageKey)).PageName;

                foreach (PageComponentsHierarchy pageComponentsHierarchy in pageComponentsHierarchies)
                {
                    int? componentKey = pageComponentsHierarchy.ComponentKey;
                    int? redirectionKey = pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity;
                    string redirectionType = "";
                    if (pageComponentsHierarchy.RedirectionsType != null)
                    {
                        redirectionType = pageComponentsHierarchy.RedirectionsType.RedirectionType;
                    }


                    db.PageComponentsHierarchies.Remove(pageComponentsHierarchy);
                    await db.SaveChangesAsync();

                    if (redirectionType == "File")
                    {
                        await DeleteExtendedData(redirectionKey);
                    }

                    if (componentKey != null)
                    {
                        await DeleteComponent(componentKey);
                    }
                }
                return 200;
            }
            catch (Exception e)
            {
                return 400;
            }


        }
        public async Task<int> DeleteDataTranslation(string dataGuid)
        {
            try
            {
                Guid guid = Guid.Parse(dataGuid);
                DataTranslation dataTranslation = await db.DataTranslations.Where(d => d.DataGUID == guid).FirstOrDefaultAsync();
                db.DataTranslations.Remove(dataTranslation);
                await db.SaveChangesAsync();

                DataGUID dataGUID = await db.DataGUIDs.FindAsync(guid);
                db.DataGUIDs.Remove(dataGUID);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return 400;
            }

            return 200;
        }
        public async Task<int> DeleteExtendedData(int? extendedDataKey)
        {
            try
            {
                ExtendedData extendedData = await db.ExtendedDatas.FindAsync(extendedDataKey);
                string uploadsFolderPath = extendedData.FilePathOrSource + extendedData.Name;
                try
                {
                    System.IO.File.Delete(uploadsFolderPath);
                }catch(Exception e)
                {

                }
                db.ExtendedDatas.Remove(extendedData);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return 400;
            }

            return 200;
        }
        public async Task<int> DeleteComponent(int? c_id)
        {
            try
            {
                Component component = await db.Components.FindAsync(c_id);
                string componentSourceEntityKey = component.ComponentSourceEntityKey;
                string componentType = component.ComponentType.ComponentType1;

                if (componentType == "Text")
                {
                    await DeleteDataTranslation(componentSourceEntityKey);
                }

                if (componentType == "Image" || componentType == "Pdf")
                {
                    await DeleteExtendedData(int.Parse(componentSourceEntityKey));
                }

                db.Components.Remove(component);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return 400;
            }

            return 200;
        }
        public async Task<ActionResult> ReturnSliderPartial(string pageName,string type="")
        {
           
            var allComponentsInPage = await db.PageComponentsHierarchies.Where(p => p.Page.PageName == pageName).ToListAsync();

            PageComponentsHierarchy navBarItemComponent;
            if(type == "slider2")
            {

                navBarItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").LastOrDefault();
            }
            else
            {
                navBarItemComponent = allComponentsInPage.Where(a => a.Component.ComponentType.ComponentType1 == "Slider").FirstOrDefault();
            }
            int? navBarItemComponentKey = null;
            if (navBarItemComponent != null)
            {
                navBarItemComponentKey = navBarItemComponent.ComponentKey;
            }

            var navbarItems = allComponentsInPage.Where(i => i.ParentComponentKey == navBarItemComponentKey).ToList();
            
            List<NavBarViewModel> navbarItemsGroupByOrder = navbarItems.OrderBy(n => n.ComponentOrder).GroupBy(
                  p => p.ComponentOrder,
                  p => p,
           (key, g) => new { ComponentOrder = key, Items = g.ToList() }).Select(p => new NavBarViewModel
           {
               ComponentOrder = p.ComponentOrder,
               Items = p.Items
           }).ToList();

            ViewData["NavbarData"] = navbarItemsGroupByOrder;
            ViewData["LanguageIdentifier"] = CurrentLanguageIdentifier;

            if (type == "slider2")
            {
                return PartialView("~/Views/CMS/_SpecialCarousel.cshtml", new ViewDataDictionary { { "NavbarData", ViewData["NavbarData"] }, { "LanguageIdentifier", ViewData["LanguageIdentifier"] } });
            }

            return PartialView("~/Views/CMS/_SlidersPartial.cshtml", new ViewDataDictionary { { "NavbarData", ViewData["NavbarData"] }, { "LanguageIdentifier", ViewData["LanguageIdentifier"] } });
        }
        public async Task<Guid> GetNewGuidForDataTranslation(string value, string source)
        {
            Guid newGuid = Guid.NewGuid();
            DataGUID dataGUID = new DataGUID();
            dataGUID.DataGUID1 = newGuid;
            dataGUID.SourceTable = source;
            db.DataGUIDs.Add(dataGUID);
            await db.SaveChangesAsync();

            DataTranslation dataTranslation = new DataTranslation();
            dataTranslation.DataGUID = newGuid;
            dataTranslation.Value = value;
            dataTranslation.LanguageKey = CurrentLanguageIdentifier;
            db.DataTranslations.Add(dataTranslation);
            await db.SaveChangesAsync();
             dataTranslation = new DataTranslation();
            dataTranslation.DataGUID = newGuid;
            dataTranslation.Value = value;
            dataTranslation.LanguageKey = 4;
            db.DataTranslations.Add(dataTranslation);
            await db.SaveChangesAsync();

            return newGuid;
        }
        public async Task<int> GetComponentTypeKeyByComponentTypeName(string type)
        {
            int componentTypeKey = (await db.ComponentTypes.Where(c => c.ComponentType1 == type).FirstOrDefaultAsync()).ComponentTypeKey;
            return componentTypeKey;
        }
    }
}