﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MoAD.WebApplication.Startup))]
namespace MoAD.WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}


