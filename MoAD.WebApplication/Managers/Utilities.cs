﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MoAD.DataObjects.EDMX;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace MoAD.WebApplication.Managers
{
    public static class Utilities
    {
        public static bool TryGetFromBase64String(string input)
        {
            
            try
            {
                 Convert.FromBase64String(input);
                return true;
            }
            catch (FormatException ex)
            {
                return false;
            }
        }
        public async static Task<Dictionary<string, string>> GetLocalisationPerControllerAndAction (string controller,string action,int currentLanguageIdentifier)
        {
            Dictionary<string, string> Locale = new Dictionary<string, string>();
            using (MoAD.DataObjects.EDMX.Entities db = new MoAD.DataObjects.EDMX.Entities())
            {

                var test = await db.LanguagesContents.Where(lc => (lc.LanguageKey == currentLanguageIdentifier) && ((lc.Controller == controller && (lc.Action == action || action == null)) || (lc.Controller == null && lc.Action == null))).ToListAsync();

                if (test.Count() != 0)
                {
                    Locale = test.ToDictionary(lc => lc.Field.Field1, lc => lc.FieldValue);
                }
            }
            return Locale;

        }


        public static string GetDataTransaltionByGuid(string dataGuid, int language = 3)
        {
            string translationText = "";
            Guid dataGUIDConverted = Guid.Parse(dataGuid);
            using (Entities db = new Entities())
            {
                try
                {
                    DataGUID dataGUID =  db.DataGUIDs.Where(d=>d.DataGUID1== dataGUIDConverted).FirstOrDefault();
                    if (dataGUID != null)
                    {
                        var dataTranslation = db.DataTranslations.Where(d => d.DataGUID == dataGUIDConverted && d.LanguageKey == language).FirstOrDefault();
                        if (dataTranslation != null)
                        {
                            translationText = dataTranslation.Value;
                        }
                        else
                        {
                            if(language == 3)
                            {
                                translationText = db.DataTranslations.Where(d => d.DataGUID == dataGUIDConverted && d.LanguageKey == 4).FirstOrDefault().Value;
                            }
                            else
                            {
                                translationText = db.DataTranslations.Where(d => d.DataGUID == dataGUIDConverted && d.LanguageKey == 3).FirstOrDefault().Value;
                            }
                           
                        }
                    }
                    else
                    {
                        DataGUID newDataGUIDRow = new DataGUID();
                        newDataGUIDRow.DataGUID1 = dataGUIDConverted;
                        newDataGUIDRow.SourceTable = "AutomaticGuid";
                        db.DataGUIDs.Add(newDataGUIDRow);
                        db.SaveChanges();

                        DataTranslation newDataTranslation = new DataTranslation();
                        newDataTranslation.DataGUID = dataGUIDConverted;
                        newDataTranslation.Value = "FillTransaltion";
                        newDataTranslation.LanguageKey = 3;
                        db.DataTranslations.Add(newDataTranslation);
                        db.SaveChanges();

                        DataTranslation newDataTranslationEn = new DataTranslation();
                        newDataTranslationEn.DataGUID = dataGUIDConverted;
                        newDataTranslationEn.Value = "FillTransaltion";
                        newDataTranslationEn.LanguageKey = 4;
                        db.DataTranslations.Add(newDataTranslationEn);
                        db.SaveChanges();
                        translationText = "FillTransaltion";
                    }

                    //if (translationText == "FillTransaltion")
                    //{
                    //    if (language == 4)
                    //    {
                    //        var arabicTranslation = db.DataTranslations.Where(d => d.DataGUID == dataGUIDConverted && d.LanguageKey == 3).FirstOrDefault().Value;
                    //        translationText = TranslateText(arabicTranslation,"ar","en");
                    //    }
                    //}
                }
                catch(Exception e)
                {
                    return "";
                }
            }

            return translationText;
        }

        public static string TranslateText(string text, string sourceLanguage, string destinationLanguage)
        {
            string url = String.Format
            ("http://translate.googleapis.com/translate_a/single?client=gtx&sl={0}&tl={1}&dt=t&q={2}",
             sourceLanguage, destinationLanguage, Uri.EscapeUriString(text));
            HttpClient httpClient = new HttpClient();
            string result = httpClient.GetStringAsync(url).Result;

            var jsonData = new JavaScriptSerializer().Deserialize<List<dynamic>>(result);

            var translationItems = jsonData[0];

            string translation = "";

            foreach (object item in translationItems)
            {
                IEnumerable translationLineObject = item as IEnumerable;

                IEnumerator translationLineString = translationLineObject.GetEnumerator();

                translationLineString.MoveNext();

                translation += string.Format(" {0}", Convert.ToString(translationLineString.Current));
            }

            if (translation.Length > 1) { translation = translation.Substring(1); };

            return translation;
        }


        public static async Task<bool> CreateRedirectionForComponent(int PageComponentHierarchyKey, int redirectionKey,int language = 3)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    PageComponentsHierarchyRedirectionEntity pageComponentsHierarchyRedirectionEntity = new PageComponentsHierarchyRedirectionEntity();
                    pageComponentsHierarchyRedirectionEntity.LanguageKey = language;
                    pageComponentsHierarchyRedirectionEntity.PageComponentHierarchyKey = PageComponentHierarchyKey;
                    pageComponentsHierarchyRedirectionEntity.RedirectionEntity = redirectionKey;

                    db.PageComponentsHierarchyRedirectionEntities.Add(pageComponentsHierarchyRedirectionEntity);
                    await db.SaveChangesAsync();
                }catch(Exception e)
                {
                    return false;
                }
            }
               
            return true;
        }

        public static async Task<bool> UpdateRedirectionForComponent(int PageComponentHierarchyKey, int redirectionKey, int language = 3)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    PageComponentsHierarchyRedirectionEntity pageComponentsHierarchyRedirectionEntity = new PageComponentsHierarchyRedirectionEntity();
                    pageComponentsHierarchyRedirectionEntity = await db.PageComponentsHierarchyRedirectionEntities.Where(p => p.PageComponentHierarchyKey == PageComponentHierarchyKey && p.LanguageKey == language).FirstOrDefaultAsync();

                    if (pageComponentsHierarchyRedirectionEntity != null)
                    {
                        pageComponentsHierarchyRedirectionEntity.RedirectionEntity = redirectionKey;
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        await CreateRedirectionForComponent(PageComponentHierarchyKey, redirectionKey, language);
                    }
                    
                }
                catch (Exception e)
                {
                    return false;
                }
            }

            return true;
        }

        public static int? GetRedirectionEntity(int? componentKey)
        {
            int? redirectionEntity = null;
            using (Entities db = new Entities())
            {
                try
                {
                        redirectionEntity = db.PageComponentsHierarchyRedirectionEntities.Where(p => p.PageComponentHierarchyKey == componentKey).FirstOrDefault().RedirectionEntity;
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return redirectionEntity;
        }

        public static string GetSliderImageEntity(int? componentKey)
        {
            string redirectionEntity = null;
            using (Entities db = new Entities())
            {
                try
                {
                    redirectionEntity = db.Components.Where(p => p.ComponentKey == componentKey).FirstOrDefault().ComponentSourceEntityKey;
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return redirectionEntity;
        }
        public static async Task<bool> SaveRedirectionPerComponent(string dataGuid,int pageKey,int redirectionType, int redirectionKey=0,int languageKEY=3)
        {
            using (Entities db = new Entities())
            {
                Component component = await db.Components.Where(c => c.ComponentSourceEntityKey == dataGuid).FirstOrDefaultAsync();

                if (component == null)
                {
                    Component newComponent = new Component();
                    int componentTypeKey = (await db.ComponentTypes.Where(c => c.ComponentType1 == "Text").FirstOrDefaultAsync()).ComponentTypeKey;

                    newComponent.ComponentTypeKey = componentTypeKey;
                    newComponent.ComponentSourceEntityKey = dataGuid;

                    db.Components.Add(newComponent);
                    await db.SaveChangesAsync();

                    PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                    pageComponentsHierarchy.PageKey = pageKey;
                    pageComponentsHierarchy.ComponentKey = newComponent.ComponentKey;
                    pageComponentsHierarchy.RedirectionTypeKey = redirectionType;

                    bool  createRediretion = await Utilities.CreateRedirectionForComponent((int)pageComponentsHierarchy.PageComponentHierarchyKey, (int)redirectionKey, languageKEY);
                  
                   // pageComponentsHierarchy.RedirectionEntity = redirectionKey;

                    db.PageComponentsHierarchies.Add(pageComponentsHierarchy);
                    await db.SaveChangesAsync();
                }
                else
                {
                    if (redirectionKey != 0)
                    {
                        PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
                        pageComponentsHierarchy = await db.PageComponentsHierarchies.Where(p => p.ComponentKey == component.ComponentKey).FirstOrDefaultAsync();

                        RedirectionsType redirectionsType = new RedirectionsType();
                        redirectionsType = await db.RedirectionsTypes.Where(r => r.RedirectionTypeKey == pageComponentsHierarchy.RedirectionTypeKey).FirstOrDefaultAsync();
                        if(redirectionsType.RedirectionType == "File")
                        {
                           await RemoveFile(pageComponentsHierarchy.Component.PageComponentsHierarchies.FirstOrDefault().Component.PageComponentsHierarchies.FirstOrDefault().PageComponentsHierarchyRedirectionEntities.FirstOrDefault().RedirectionEntity.ToString());
                        }
                        
                        pageComponentsHierarchy.RedirectionTypeKey = redirectionType;

                        bool createRediretion = await Utilities.UpdateRedirectionForComponent((int)pageComponentsHierarchy.PageComponentHierarchyKey, (int)redirectionKey, languageKEY);
                       // pageComponentsHierarchy.RedirectionEntity = redirectionKey;

                        await db.SaveChangesAsync();
                    }
                }
            }
                
            return true;
        }

        public static async Task<bool> RemoveFile(string extendedDatakey)
        {
            using (Entities db = new Entities())
            {
                PageComponentsHierarchy pageComponent = await db.PageComponentsHierarchies.Where(p => p.Component.ComponentSourceEntityKey == extendedDatakey.ToString()).FirstOrDefaultAsync();

                ExtendedData data = await db.ExtendedDatas.Where(e => e.ColumnValue == extendedDatakey).FirstOrDefaultAsync();

                Component component = await db.Components.Where(p => p.ComponentSourceEntityKey == extendedDatakey).FirstOrDefaultAsync();

                string uploadsFolderPath = data.FilePathOrSource + data.Name;

                System.IO.File.Delete(uploadsFolderPath);

                db.PageComponentsHierarchies.Remove(pageComponent);
                db.Components.Remove(component);
                db.ExtendedDatas.Remove(data);
                await db.SaveChangesAsync();
            }
                
            return true;
        }
        public static string GetFilePathFromExtendedData(int? extensionFileKey)
        {
            string url = "";
            try
            {
                using (Entities db = new Entities())
                {
                    var extendedData = db.ExtendedDatas.Where(e => e.FileExtensionKey == extensionFileKey).FirstOrDefault();
                    url = extendedData.FilePathOrSource + extendedData.Name;
                }
            }catch(Exception e)
            {

            }

            return url;
        }

        public static Image resizeImage(Image image, int width, int? height=null)
        {
  
            decimal imageRatio = (decimal)image.Size.Width / (decimal)image.Size.Height;
            decimal newHeightFloat = (decimal)width / imageRatio;
            int newHeightint = (int)Math.Abs(newHeightFloat);
            double fractionalPercentage = (50 / 100.0);
            
            if (height != null)
            {
                newHeightint =(int)height;
            }

            int outputWidth = (int)(width * fractionalPercentage);

            int outputHeight = (int)(newHeightint * fractionalPercentage);

            return ScaleImage(image, outputWidth, outputHeight);
        }
        //Scale an image to a given width and height.

        public static Image ScaleImage(Image img, int outputWidth, int outputHeight)

        {

            Bitmap outputImage = new Bitmap(outputWidth, outputHeight, img.PixelFormat);

            outputImage.SetResolution(img.HorizontalResolution, img.VerticalResolution);

            Graphics graphics = Graphics.FromImage(outputImage);

            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            graphics.DrawImage(img, new Rectangle(0, 0, outputWidth, outputHeight),

            new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

            graphics.Dispose();

            return outputImage;

        }

        public static PageComponentsHierarchy GetFilePathFromGuid(string guid)
        {
            PageComponentsHierarchy pageComponentsHierarchy = new PageComponentsHierarchy();
            string url = "";
            try
            {
                using (Entities db = new Entities())
                {
                    Component component = db.Components.Where(c => c.ComponentSourceEntityKey == guid).FirstOrDefault();

                    if (component!= null)
                    {
                        pageComponentsHierarchy =  db.PageComponentsHierarchies.Where(p => p.ComponentKey == component.ComponentKey).FirstOrDefault();
                    }
                    
                }
            }
            catch (Exception e)
            {

            }

            return pageComponentsHierarchy;
        }
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }


    }
}