﻿using MoAD.DataObjects.EDMX;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MoAD.ERM.WebApplication.Objects
{
    public class UserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            if (!request.User.Identity.IsAuthenticated)
            {
                return string.Empty;
            }

            AspNetUser user;

            using (Entities db = new Entities())
            {
                Task<AspNetUser> getUserIdTask = db.AspNetUsers.FirstOrDefaultAsync(u => u.UserName == request.User.Identity.Name);

                getUserIdTask.Wait();

                user = getUserIdTask.Result;
            }

            return user.Id;
        }
    }
}