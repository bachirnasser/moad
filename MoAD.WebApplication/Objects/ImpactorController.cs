﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MoAD.WebApplication.Objects
{
    internal class Http403Result : ActionResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            // Set the response code to 403.
            context.HttpContext.Response.StatusCode = 403;
        }
    }
    public class ImpactorController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.Result == null)
                return;

            if (!Turing())
            {
                filterContext.Result = new Http403Result();//new RedirectResult("~/Error/UnAuthorized");

            }

        }

        public static bool Turing()
        {

            //RegistryKey key = Registry.LocalMachine.OpenSubKey(@"Software\Luna", true);
            //string regValue = Registry.GetValue(keyName, "Appollo", "Default").ToString();
            string key = "";
            using (RegistryKey registryKey = Registry.Users.OpenSubKey(@".DEFAULT\SOFTWARE"))
            {
                RegistryKey registryKey1 = registryKey.OpenSubKey("Luna");
                key = (string)registryKey1.GetValue("Appollo");
            }

            //key.CreateSubKey("AppVersion");
            //key = key.OpenSubKey("Luna", true);

            List<DateTime> newDateList = new List<DateTime>();

            if (key == "2D01766027685262ACE0107C2887F629")
            {
                newDateList.Add(DateTime.Today);

                var stringDateList = JsonConvert.SerializeObject(newDateList);

                Registry.SetValue(Registry.Users.OpenSubKey(@".DEFAULT\SOFTWARE\Luna").ToString(), "Appollo", EncryptString(stringDateList));

                return true;
            }

            if (key != null)
            {
                string decryptKey = DecryptString(key);

                IList<DateTime> dateList = JsonConvert.DeserializeObject<List<DateTime>>(decryptKey);


                //, 
                //    new JsonSerializerSettings
                //{
                //    DateFormatString = "d MMMM, yyyy"
                //}

                if (dateList.Count() > 53)
                {
                    return false;
                }

                int index = 1;
                foreach (DateTime dateTime in dateList)
                {
                    //Console.WriteLine(dateTime.ToLongDateString());

                    if (dateTime > DateTime.Today)
                    {
                        return false;
                    }

                    if (dateTime <= DateTime.Today && dateList.Count() <= 30 && index < dateList.Count())
                    {
                        index++;
                        continue;
                    }

                    if (dateTime < DateTime.Today && dateList.Count() <= 30 && index == dateList.Count())
                    {
                        dateList.Add(DateTime.Today);

                        var stringDateList = JsonConvert.SerializeObject(dateList);

                        Registry.SetValue(Registry.Users.OpenSubKey(@".DEFAULT\SOFTWARE\Luna").ToString(), "Appollo", EncryptString(stringDateList));

                        index++;

                        return true;
                    }

                    if (dateTime == DateTime.Today && dateList.Count() <= 30 && index == dateList.Count())
                    {
                        return true;
                    }

                }

            }
            else
            {
                return false;
            }

            return false;
        }

        private static string EncryptString(string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            string key = "FlyBy";

            using (Aes aes = Aes.Create())
            {

                byte[] keyBytes = ASCIIEncoding.ASCII.GetBytes(key.PadLeft(32));
                byte[] ivBytes = ASCIIEncoding.ASCII.GetBytes(key.PadLeft(16));
                aes.Key = keyBytes;//Encoding.UTF8.GetBytes(key);
                aes.IV = ivBytes;//iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        private static string DecryptString(string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);

            string key = "FlyBy";

            using (Aes aes = Aes.Create())
            {

                byte[] keyBytes = ASCIIEncoding.ASCII.GetBytes(key.PadLeft(32));
                byte[] ivBytes = ASCIIEncoding.ASCII.GetBytes(key.PadLeft(16));
                aes.Key = keyBytes;//Encoding.UTF8.GetBytes(key);
                aes.IV = ivBytes;//iv;
                //aes.Key = Encoding.UTF8.GetBytes(key);
                //aes.IV = iv;

                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

    }
}