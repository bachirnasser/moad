﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoAD.DataObjects.EDMX;
using MoAD.WebApplication.Controllers;

namespace MoAD.WebApplication.Objects
{
    [RequireHttps]
#if DISABLE_PUBLIC_ACCESS
    [System.Web.Mvc.Authorize]
#endif
    public class BaseController : ImpactorController
    {
        #region User Management

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        #endregion

       
        public AspNetUser CurrentUser { get; set; }

        public Dictionary<string, string> Locale { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }
        public int CurrentLanguageIdentifier { get; set; }

        public BaseController()
        {
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

#if DISABLE_PUBLIC_ACCESS
            ViewBag.DISABLE_PUBLIC_ACCESS = true;
#else
            ViewBag.DISABLE_PUBLIC_ACCESS = false;
#endif

#if DEBUG
            ViewBag.DEBUG = true;
#else
            ViewBag.DEBUG = false;
#endif

            var getCurrentUser = Task.Run(async () =>
            {
                using (Entities db = new Entities())
                {
                    CurrentUser = await db.AspNetUsers.FindAsync(User.Identity.GetUserId());

                    var userId = User.Identity.GetUserId();
                }
            });

            getCurrentUser.Wait();

            //CurrentUser = UserManager.FindById(User.Identity.GetUserId());

            ViewBag.CurrentUser = CurrentUser;

            //here goes the LinQ query to find the fields for the given action/controller.

            int currentLanguageIdentifier = 3;

            HttpCookie cookie = HttpContext.Request.Cookies.Get("Language");
            if(cookie == null)
            {
                HttpCookie myCookie = new HttpCookie("Language");
                myCookie.Value="3";
                Response.Cookies.Add(myCookie);
            }
            if (cookie != null)
            {
                try
                {
                    int languageCookieValue = Convert.ToInt32(cookie.Value);

                    bool cookieLanguageIsValid = false;

                    var checkLanguageTask = Task.Run(async () =>
                    {
                        using (Entities db = new Entities())
                        {
                            cookieLanguageIsValid = await db.Languages.AnyAsync(l => l.LanguageKey == languageCookieValue);
                        }
                    });
                    checkLanguageTask.Wait();

                    currentLanguageIdentifier = cookieLanguageIsValid ? languageCookieValue : currentLanguageIdentifier;
                }
                catch (Exception e)
                {
                    var logErrorTask = Task.Run(async () =>
                    {
                        await AccountController.ErrorSetLog(CurrentUser.Id, MethodBase.GetCurrentMethod().Name, MethodBase.GetCurrentMethod().Name, e.Message);
                    });
                    logErrorTask.Wait();
                }
            }

            try
            {
                currentLanguageIdentifier = Convert.ToInt32(Request.QueryString["lang"] == null ? currentLanguageIdentifier.ToString() : Request.QueryString["lang"]);
            }
            catch
            {

            }

            CurrentLanguageIdentifier = currentLanguageIdentifier;

            Action = filterContext.RouteData.Values["action"].ToString();
            Controller = filterContext.RouteData.Values["controller"].ToString();

            Action = getAction(Action);

            var getLanguageTask = Task.Run(async () =>
            {
                using (Entities db = new Entities())
                {

                    var test = await db.LanguagesContents.Where(lc => (lc.LanguageKey == currentLanguageIdentifier) && ((lc.Controller == Controller && (lc.Action == Action || Action == null)) || (lc.Controller == null && lc.Action == null))).ToListAsync();

                    if (test.Count() != 0)
                    {
                        Locale = test.ToDictionary(lc => lc.Field.Field1, lc => lc.FieldValue);
                    }

                        ViewBag.Locale = Locale;

                }
            });

                getLanguageTask.Wait();

            var getLanguageOrientationTask = Task.Run(async () =>
            {
                using (Entities db = new Entities())
                {
                    ViewBag.CurrentLanguage = await db.Languages.FirstOrDefaultAsync(l => l.LanguageKey == currentLanguageIdentifier);
                }
            });

            getLanguageOrientationTask.Wait();

        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

//#if !DEBUG
//            var recordUserTrailTask = Task.Run(async () =>
//            {
//                await LogManager.recordUserTrail(User.Identity.GetUserId(), filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName, filterContext.HttpContext.Request.Url.AbsoluteUri);
//            });

//            recordUserTrailTask.Wait();
//#endif
        }

        protected override void OnException(System.Web.Mvc.ExceptionContext filterContext)
        {
            base.OnException(filterContext);

//#if !DEBUG
//            filterContext.ExceptionHandled = true;

//            var logTask = Task.Run(async () =>
//            {
//                await LogManager.log("Base Exception", String.Format($"Exception occured {filterContext.Exception.Message}.\nUser: {User.Identity.GetUserId()}.\nURL: {filterContext.HttpContext.Request.Url.AbsoluteUri}."));
//            });

//            logTask.Wait();

//            filterContext.Result = RedirectToAction("Index", "Error");
//#endif
        }

        private string getAction(string action)
        {
            string result;

            switch (action)
            {
                default:
                    result = action;
                    break;
            }

            return result;
        }

        /// <summary>  
        /// Get Field Label of the localization function.  
        /// </summary>
        public static string GetFieldLabel(Dictionary<string, string> languageContent, string fieldKey)
        {
            if (!languageContent.ContainsKey(fieldKey) || (languageContent[fieldKey] == " " || languageContent[fieldKey] == "" || languageContent[fieldKey] == null))
            {
                if (!fieldKey.Contains("_"))
                    return fieldKey.Split(new string[] { "Page" }, StringSplitOptions.None)[1];
                else
                    return fieldKey;
            }

            return languageContent[fieldKey];
        }

    }
}