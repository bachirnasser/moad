﻿//CMS Functionalities
var clickedElement = "";



$(document).on('click', '#saveBtn', function () {

    var textChanged = $("#inputModal").val();
    var guidToSend = $("#inputModal").data("translation");
    changeText(guidToSend, textChanged, "none");
    $('.modal').modal('hide');

})

$(document).on('click', '#saveBtnCard', function () {

    if ($(".title-container").is(":visible")) {
        var titleModalVal = $("#titleModal").val();
        var titleModalTranslation = $("#titleModal").data('translation');
        changeText(titleModalTranslation, titleModalVal, "title");
    }
    if ($(".title-text-container").is(":visible")) {
        var titleTextModalval = $("#titleTextModal").val();
        var titleTextModalTranslation = $("#titleTextModal").data('translation');
        changeText(titleTextModalTranslation, titleTextModalval, "titleText");
    }

    if ($(".paragraph-container").is(":visible")) {
        var paragraphModalVal = $("#textParagraphModal").val();
        var paragraphTranslation = $("#textParagraphModal").data('translation');
        changeText(paragraphTranslation, paragraphModalVal, "paragraph");
    }


    $('.modal').modal('hide');


})


//$(document).on('click', '.blog-slider__button', function () {
//    var parent = $(this).parent();
//    var children = $(parent).children();
//    var title = $(children[0]).text();
//    var text = $(children[2]).text();
//    $(".modal-head-title").text(title);
//    $(".modal-head-text").text(text);


//})

$(document).on('click', '.card', function () {
    clickedElement = $(this);

    var title = "";
    var titleText = "";
    var paragraph = "";
    var titleTranslation = "";
    var titleTextTranslation = "";
    var paragraphTranslation = "";
    var imageKey = 0;

    title = $(this).find(".title").text().trim();

    var titleTranslation = $(this).find(".title").data('translation');



    titleText = $(this).find(".titleText").text().trim();
    var titleTextTranslation = $(this).find(".titleText").data('translation');

    paragraph = $(this).find(".paragraph").text().trim();
    var paragraphTranslation = $(this).find(".paragraph").data('translation');

    imageKey = $(this).find(".imageKeyinput").val();
    console.log(imageKey);
    var language = @languageIdentifier;
    var cardModal = {};
    cardModal["TitleTranslation"] = titleTranslation;
    cardModal["SubTitleTranslation"] = titleTextTranslation;
    cardModal["ParagraphTranslation"] = paragraphTranslation;
    cardModal["languageNeeded"] = language;
    cardModal["imageKey"] = imageKey;
    var cardModalJsonString = JSON.stringify(cardModal);

    $.ajax({

        url: '@Url.Action("ChangeLanguageInModal")',



        type: 'POST',



        data: {

            __RequestVerificationToken: $('@Html.AntiForgeryToken()').val(),

            componentName: "card",

            componentStringObject: cardModalJsonString

        },



        success: function (data) {

            $(".modal-body-card").empty();
            $(".modal-body-card").append(data);





        },



        error: function (error) {

            // when an error occure this error function will be called

        }
    });



})
$(document).on('click', '.img-to-change', function () {
    $(".modal").hide();
})
function changeText(guidToSend, textChanged, inputChangedName) {
    var lang = "";

    if ($(clickedElement).hasClass("card")) {
        lang = $("#languageDropDownCard option:selected").val();
        console.log("here in sa7");
    }
    else {
        lang = $("#languageDropDownSingle option:selected").val();
        console.log("here in ghalat");
    }

    $.ajax({

        url: '@Url.Action("UpdateDataTranslation", "CMS")',



        type: 'POST',



        data: {

            __RequestVerificationToken: $('@Html.AntiForgeryToken()').val(),

            guid: guidToSend,

            newTransaltion: textChanged,

            language: parseInt(lang)

        },



        success: function (data) {

            //if ($(clickedElement).hasClass("card")) {

            //    if (inputChangedName == "title") {

            //        $(clickedElement).find(".title").text(data);
            //    }
            //    if (inputChangedName == "titleText") {
            //        $(clickedElement).find(".titleText").text(data);
            //    }
            //    if (inputChangedName == "paragraph") {
            //        $(clickedElement).find(".paragraph").text(data);
            //    }

            //}
            //else {
            //    $(clickedElement).text(data);
            //}





        },



        error: function (error) {

            // when an error occure this error function will be called

        }
    });
}

$(document).on('change', "#languageDropDownCard", function () {

    var language = $("#languageDropDownCard option:selected").val();

    // 1) get the data translations needed from inputs
    var titleModalTranslation = "";
    var titleTextModalTranslation = "";
    var paragraphModalTranslation = "";
    var imageKey = 0;
    if ($(".title-container").is(":visible")) {
        titleModalTranslation = $("#titleModal").data('translation');
    }
    if ($(".title-text-container").is(":visible")) {
        titleTextModalTranslation = $("#titleTextModal").data('translation');
    }

    if ($(".paragraph-container").is(":visible")) {
        paragraphModalTranslation = $("#textParagraphModal").data('translation');
    }
    imageKey = $("#imgModal").data("translation");
    // create object of these translations
    var cardModal = {};
    cardModal["TitleTranslation"] = titleModalTranslation;
    cardModal["SubTitleTranslation"] = titleTextModalTranslation;
    cardModal["ParagraphTranslation"] = paragraphModalTranslation;
    cardModal["languageNeeded"] = language;
    cardModal["imageKey"] = imageKey;
    var cardModalJsonString = JSON.stringify(cardModal);

    $.ajax({

        url: '@Url.Action("ChangeLanguageInModal")',



        type: 'POST',



        data: {

            __RequestVerificationToken: $('@Html.AntiForgeryToken()').val(),

            componentName: "card",

            componentStringObject: cardModalJsonString

        },



        success: function (data) {

            $(".modal-body-card").empty();
            $(".modal-body-card").append(data);





        },



        error: function (error) {

            // when an error occure this error function will be called

        }
    });


})
$(document).on('change', "#languageDropDownSingle", function () {
    var language = $("#languageDropDownSingle option:selected").val();

    // 1) get the data translation
    var singleElementModal = {};
    singleElementModal["ElementTranslation"] = $("#inputModal").data("translation");
    singleElementModal["languageNeeded"] = language;

    var singleElementModalJsonString = JSON.stringify(singleElementModal);
    $.ajax({

        url: '@Url.Action("ChangeLanguageInModal")',



        type: 'POST',



        data: {

            __RequestVerificationToken: $('@Html.AntiForgeryToken()').val(),

            componentName: "singleElement",

            componentStringObject: singleElementModalJsonString

        },



        success: function (data) {

            $(".modal-body-single-element").empty();
            $(".modal-body-single-element").append(data);
            //$(".modal-body-card").empty();
            //$(".modal-body-card").append(data);





        },



        error: function (error) {

            // when an error occure this error function will be called

        }
    });




})