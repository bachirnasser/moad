﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Models
{
    public class NavBarViewModel
    {
        public int? ComponentOrder { get; set; }

        public List<PageComponentsHierarchy> Items { get; set; }
    }
}