﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Models
{
    public class FormTemplate
    {
        public List<GetAllFormTemplatesForaType_Result> FormsTemplates { get; set; }

    }
}