﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Models
{
    public class ComponentViewModel
    {
        public int componentId { get; set; }
        public int componentOrder { get; set; }

        public int componentIndex { get; set; }

        public string componentType { get; set; }

        public int language { get; set; }

        public string componentSourceEntityKey { get; set; }
    }
}