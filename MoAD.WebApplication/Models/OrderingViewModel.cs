﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Models
{
    public class OrderingViewModel
    {
        public int Key { get; set; }
        public int Order { get; set; }
    }
}