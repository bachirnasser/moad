﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Models
{
    public class CreateChildNavbarViewModel
    {
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public int? ItemTypeKey { get; set; }
        public int? ComponentOrder { get; set; }
        public int? ComponentIndex { get; set; }
        public int? NavBarComponentKey { get; set; }
        public int? PageKey { get; set; }
    }
}