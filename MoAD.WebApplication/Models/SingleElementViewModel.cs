﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Models
{
    public class SingleElementViewModel
    {
        public int languageNeeded { get; set; }
        public string ElementTranslation { get; set; }

        public string ElementType { get; set; }
        public int imageKey { get; set; }
        public string componentId { get; set; }
        public string redirectComponent { get; set; }
        public bool isSummernote { get; set; }
        public bool isDropZone { get; set; }
        public bool isTextArea { get; set; }
    }
}