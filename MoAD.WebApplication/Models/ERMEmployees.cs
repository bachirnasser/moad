﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Models
{
    public class ERMEmployees
    {
        public int EmployeesRecordKey { get; set; }
        public string PersonalNumber { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string BirthPlace { get; set; }
        public Nullable<double> BirthDay { get; set; }
        public Nullable<double> BirthMonth { get; set; }
        public Nullable<double> BirthYear { get; set; }
        public string OrganisationParent { get; set; }
        public string Organisation { get; set; }
        public int? StatusKey { get; set; }
        public object Status { get; set; }
        public List<object> Folders { get; set; }
        public List<object> EmployeePersonalInformations { get; set; }
        public List<object> EmployeeSpokenLanguages { get; set; }
        public List<object> CareerPaths { get; set; }
        public List<object> EmployeeCourses { get; set; }
        public List<object> EmployeeEducations { get; set; }
        public List<object> FinancialEvents { get; set; }
    }
}