﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.Models
{
    public class CardModalViewModel
    {
        public int languageNeeded { get; set; }
        public string  TitleTranslation{ get; set; }
        public string  SubTitleTranslation{ get; set; }
        public string  ParagraphTranslation{ get; set; }
        public int imageKey { get; set; }
        public string imageSize { get; set; }

        public int? FormTemplateKey { get; set; }

        public string redirectComponent { get; set; }
    }
}