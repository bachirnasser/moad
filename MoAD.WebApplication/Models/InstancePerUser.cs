﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoAD.DataObjects.EDMX;

namespace MoAD.WebApplication.Models
{
    public class InstancePerUser
    {
        public List<GetWorkFlowItemInstancePerUser_Result> listOfForms { get; set; }

    }
}