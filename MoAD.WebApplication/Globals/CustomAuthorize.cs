﻿using Microsoft.AspNet.Identity;
using MoAD.DataObjects.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace MoAD.WebApplication.Globals
{

    public class YourCustomAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            // If they are authorized, handle accordingly
            if (this.AuthorizeCore(filterContext.HttpContext))
            {
                // string RoleKey = Globals.Configuration.Rolekey;

               // string koko = Globals.Configuration.UserKey;
                string koko = filterContext.HttpContext.User.Identity.GetUserId();
                List <GetUserInformation_Result> UserInfo = GetUserInformation(koko).Result;
                
                string RoleKey = UserInfo.FirstOrDefault().RoleId;
                string path = filterContext.HttpContext.Request.AppRelativeCurrentExecutionFilePath;
                List<PrivelegesperRole_Result> AccessPrivilege = PrivilegePerRole(RoleKey).Result;
                AccessPrivilege = AccessPrivilege.Where(x => x.PositionType == "Leaf").ToList();
                var index = 0;
                List<GetFunctionsPerPrivilige_Result> FinalPrivilegeKeyResult = new List<GetFunctionsPerPrivilige_Result>();

                foreach (var item in AccessPrivilege)
                {
                    List<GetFunctionsPerPrivilige_Result> privilegeKeyResult = GetFunctionsPerPrivilige(item.PrivilegeKey.ToString()).Result;
                    FinalPrivilegeKeyResult = privilegeKeyResult.Where(x => x.PrivilegeKey == item.PrivilegeKey).ToList();
                    foreach (var item1 in FinalPrivilegeKeyResult)
                    {
                        if (path.Contains(item1.Functionname))
                            
                            {
                            index = 1;
                        }
                    }
                }

                if (path.Contains("~/UserDashboard") || path.Contains("~/Home/CreateNewWorkFlowInstance") || path.Contains("~/Home/FormInstance") || path.Contains("~/Account/ChangePassword"))
                {
                    base.OnAuthorization(filterContext);

                }
                else if (path == "~/PBDashboard" && RoleKey=="3")
                {
                    base.OnAuthorization(filterContext);

                }
                else if (index == 1)
                {
                    base.OnAuthorization(filterContext);

                }else
                {
                    filterContext.Result = new RedirectResult("~/Home/RedirectionPage");
                }
                //else if (index == 0 && path != "~/UserDashboard" && path != "~/Privileges/AccessPivilege" && path != "~/Privileges/DeleteAccessPrivilege" && path != "~/Privileges/SetAccessPrivilege" && path != "~/Home/CreateNewWorkFlowInstance" && path != "~/Home/FormInstance" && path != "~/Home/SignupView" && path != "~/Forms/EntidabSave" && path != "~/UserDashboard/" && path != "~/OrganisationCharts" && path != "~/OrganisationCharts/TableForm" && path != "~/OrganisationCharts/TableFormChariha3" && path != "~/OrganisationCharts/Charihakhamisa" && path != "~/OrganisationCharts/AltadribWaltashilAlmoustamer" && path != "~/OrganisationCharts/TableFormHaikalWazifiNew" && path != "~/OrganisationCharts/CharihaOulaRepeatedQuestions")
                //{

                //    filterContext.Result = new RedirectResult("~/Home/RedirectionPage");
                //}


            }
            else
            {
              
                // Otherwise redirect to your specific authorized area
                filterContext.Result = new RedirectResult("~/Home/Index");
            }
        }


        [HttpGet]
        public async Task<List<PrivelegesperRole_Result>> PrivilegePerRole(string userRole)
        {
            List<PrivelegesperRole_Result> result = new List<PrivelegesperRole_Result>();

            List<object> nestedResult = new List<object>();

            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/PrivilegePerRole/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("Key={0}", userRole);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("PositionType={0}", "null");

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<PrivelegesperRole_Result>>();
                }
            }

            return result;
        }

        [HttpGet]
        public async Task<List<GetFunctionsPerPrivilige_Result>> GetFunctionsPerPrivilige(string PrivilegeKey)
        {
            List<GetFunctionsPerPrivilige_Result> result = new List<GetFunctionsPerPrivilige_Result>();
            

           
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetFunctionsPerPrivilige/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("PrivilegeKey={0}", PrivilegeKey);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetFunctionsPerPrivilige_Result>>();
                }
            }

            return result;
        }


        public async Task<List<GetUserInformation_Result>> GetUserInformation(string userID)
        {
            List<GetUserInformation_Result> result = new List<GetUserInformation_Result>();

            //var userID = User.Identity.GetUserId();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Globals.Configuration.API_SECURITY_KEY);


                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("api/Forms/GetUserInformation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userID={0}", userID);


                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<List<GetUserInformation_Result>>();
                }
            }
            return result;
        }


    }
}