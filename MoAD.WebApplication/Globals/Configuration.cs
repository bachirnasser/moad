﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
namespace MoAD.WebApplication.Globals
{
    public static class Configuration
    {
        public static string APPLICATION_NAME = "MoAd";
        public static string COMPANY_NAME = "";
        public static string COMPANY_SITE = "";
        public static string URL_ENCRYPTION_KEY = "kteh543498#";
        public static string API_SECURITY_KEY = "eE1ad01MQ0h2VjhxYmRucTpYcGdFckhZc0M5dFhjVUQy";
        public static string UserKey = "";
        public static string Rolekey = "";
        public const string UPLOADS_CMS_FOLDER = @"C:\MoAD_CMS\";
        public const string UPLOADS_CMS_PDF_FOLDER = @"C:\MoAD_CMS\pdf\";

#if PRODUCTION
        public static string ERMAPI = "http://192.168.11.11:1671/";
#else
        public static string ERMAPI = "http://localhost/MoAD.ERM.DataInterface/";
#endif

#if PRODUCTION
        public static string DATA_INTERFACE_URL = "http://192.168.11.11:1670/";
      // public static string DATA_INTERFACE_URL = "http://127.0.0.1:1670/";
        public static string WEB_APPLICATION_URL = "online";
        
#else

        //public static string DATA_INTERFACE_URL = "http://localhost:41007/";
        public static string DATA_INTERFACE_URL = "http://localhost/MoAD.DataInterface/";
        public static string WEB_APPLICATION_URL = "https://localhost/MoAD.WebApplication";

#endif

    }
}