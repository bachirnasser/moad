﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.DataObjects
{
    public class Answer
    {
       

        public int? AnswerTemplateKey { get; set; }
        public string AnswerValue { get; set; }
        public int? AnswerTemplateKeyIndex { get; set; }
        public int? AnswerFileExtensionKey { get; set; }
        public string AnswerFileExtensionName { get; set; }
        public string AnswerFileExtensionType { get; set; }
        public string AnswerFileExtensionSize { get; set; }
        public string AnswerFileExtensionPath { get; set; }
        public string AnswerFileExtensionHTML { get; set; }
        public string AnswerFileExtensionHTMLCode { get; set; }
        public string AnswerTemplateDependency { get; set; }
        public string FormElement { get; set; }
        public int? FormElementFileExtensionKey { get; set; }
        public string FormElementTypeKeyDependancy { get; set; }
        public string FormElementFileExtensionName { get; set; }
        public string FormElementFileExtensionType { get; set; }
        public string FormElementFileExtensionSize { get; set; }
        public string FormElementFileExtensionPathOrSource { get; set; }
        public string FormElementFileExtensionHTML { get; set; }
        public string FormElementFileExtensionHTMLCode { get; set; }
        public string FormElementTypeName { get; set; }
        public int? FormElementTypeFileExtensionKey { get; set; }
        public int? FormElementTypeKey { get; set; }
        public string FormElementTypeFileExtensionName { get; set; }
        public string FormElementTypeFileExtensionType { get; set; }
        public string FormElementTypeFileExtensionSize { get; set; }
        public string FormElementTypeFileExtensionFilePathOrSource { get; set; }
        public string FormElementTypeFileExtensionHTML { get; set; }
        public string FormElementTypeFileExtensionHTMLCode { get; set; }
        public string SqlQueries { get; set; }
        public string SqlParameters { get; set; }
        public string ColumnValue { get; set; }
        public string FormTemplateName { get; set; }
        public string FormElementKey { get; set; }
        public string AnswerInstanceFileExtensionName { get; set; }
        public Nullable<int> AnswerKey { get; set; }
        public string AnswerInstanceFileExtensionFilePathOrSource { get; set; }

        
    }
}