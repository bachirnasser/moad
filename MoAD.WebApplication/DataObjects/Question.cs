﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoAD.WebApplication.DataObjects
{
    public class Question
    {

       public int? QuestionKey { get; set; }
        public string QuestionNote { get; set; }

        public string QuestionDisplayMode { get; set; }

        public string QuestionValue { get; set; }


        
        public int? QuestionKeyIndex { get; set; }
        public List <Answer> Answers { get; set; }


    }
}